﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void LRMTest::Start()
extern void LRMTest_Start_m5D52D3731ED040D688F0D2BDBFCC45E200EFA839 (void);
// 0x00000002 System.Void LRMTest::OnDisable()
extern void LRMTest_OnDisable_mE5F627CB74B7397074C0B31839A62A6CB7D6B0D8 (void);
// 0x00000003 System.Void LRMTest::RefreshServerList()
extern void LRMTest_RefreshServerList_mC1B44DA857219C1517F085E309E71CEF2D1EA201 (void);
// 0x00000004 System.Void LRMTest::ServerListUpdate()
extern void LRMTest_ServerListUpdate_mDC64CAFD8A50F56CD2A60AA00B63DBE1741E07FB (void);
// 0x00000005 System.Void LRMTest::ConnectToserver(System.Int32)
extern void LRMTest_ConnectToserver_mB22E043690A8E96C201FA21AD8ABC9A89C13B6BD (void);
// 0x00000006 System.Void LRMTest::.ctor()
extern void LRMTest__ctor_mD51418BE2C7A5593BE073D5A801E8367C0E3C09F (void);
// 0x00000007 System.Void LRMTest/<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mD6CDE7A81B6226E22BA914E58274C96CEF587CDE (void);
// 0x00000008 System.Void LRMTest/<>c__DisplayClass6_0::<ServerListUpdate>b__0()
extern void U3CU3Ec__DisplayClass6_0_U3CServerListUpdateU3Eb__0_m33082D8EBF7D1DE545AB16913428438C64843B50 (void);
static Il2CppMethodPointer s_methodPointers[8] = 
{
	LRMTest_Start_m5D52D3731ED040D688F0D2BDBFCC45E200EFA839,
	LRMTest_OnDisable_mE5F627CB74B7397074C0B31839A62A6CB7D6B0D8,
	LRMTest_RefreshServerList_mC1B44DA857219C1517F085E309E71CEF2D1EA201,
	LRMTest_ServerListUpdate_mDC64CAFD8A50F56CD2A60AA00B63DBE1741E07FB,
	LRMTest_ConnectToserver_mB22E043690A8E96C201FA21AD8ABC9A89C13B6BD,
	LRMTest__ctor_mD51418BE2C7A5593BE073D5A801E8367C0E3C09F,
	U3CU3Ec__DisplayClass6_0__ctor_mD6CDE7A81B6226E22BA914E58274C96CEF587CDE,
	U3CU3Ec__DisplayClass6_0_U3CServerListUpdateU3Eb__0_m33082D8EBF7D1DE545AB16913428438C64843B50,
};
static const int32_t s_InvokerIndices[8] = 
{
	2754,
	2754,
	2754,
	2754,
	2261,
	2754,
	2754,
	2754,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	8,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
