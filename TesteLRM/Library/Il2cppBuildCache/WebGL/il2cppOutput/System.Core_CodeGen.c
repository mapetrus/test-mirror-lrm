﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412 (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937 (void);
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018 (void);
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E (void);
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31 (void);
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1 (void);
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6 (void);
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93 (void);
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6 (void);
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460 (void);
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45 (void);
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B (void);
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268 (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153 (void);
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477 (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91 (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E (void);
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265 (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4 (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502 (void);
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465 (void);
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34 (void);
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB (void);
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252 (void);
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F (void);
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058 (void);
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218 (void);
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D (void);
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63 (void);
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E (void);
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF (void);
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB (void);
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E (void);
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598 (void);
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1 (void);
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679 (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129 (void);
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD (void);
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2 (void);
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mDB56C9FA4C344A86553D6AFB66D10B290302C53A (void);
// 0x00000031 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000032 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000033 System.Exception System.Linq.Error::NotSupported()
extern void Error_NotSupported_m51A0560ABF374B66CF6D1208DAF27C4CBAD9AABA (void);
// 0x00000034 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000036 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000037 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x0000003A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<System.Linq.IGrouping`2<TKey,TSource>> System.Linq.Enumerable::GroupBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000040 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000041 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000042 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000043 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000044 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000046 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000047 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000048 TSource System.Linq.Enumerable::LastOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000049 TSource System.Linq.Enumerable::Single(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004A TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004B TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000004D System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004E System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004F System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000050 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000051 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000052 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000053 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000054 System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_m6CFC8CEAC70AE3C469A5D1993FAF8EEEC6A06FB5 (void);
// 0x00000055 System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Int32>)
// 0x00000056 System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000057 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000058 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000059 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x0000005A System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x0000005B System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x0000005C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000005D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000005E System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000005F System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000060 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000061 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000062 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x00000063 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x00000064 System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x00000065 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000066 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000067 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000068 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000069 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x0000006A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000006B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006C System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000006D System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x0000006E System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x0000006F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000070 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000071 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000072 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x00000073 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x00000074 System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000075 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000076 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000077 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000078 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x00000079 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x0000007A System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000007B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007C System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000007D System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x0000007E System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x0000007F System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000080 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000081 System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x00000082 System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000083 System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x00000084 TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000085 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x00000086 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x00000087 System.Boolean System.Linq.Enumerable/<SelectManyIterator>d__17`2::MoveNext()
// 0x00000088 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000089 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x0000008A TResult System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000008B System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x0000008C System.Object System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x0000008D System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x0000008E System.Collections.IEnumerator System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x0000008F System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x00000090 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x00000091 System.Boolean System.Linq.Enumerable/<TakeIterator>d__25`1::MoveNext()
// 0x00000092 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::<>m__Finally1()
// 0x00000093 TSource System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000094 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x00000095 System.Object System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x00000096 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000097 System.Collections.IEnumerator System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000098 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x00000099 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x0000009A System.Boolean System.Linq.Enumerable/<UnionIterator>d__71`1::MoveNext()
// 0x0000009B System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally1()
// 0x0000009C System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally2()
// 0x0000009D TSource System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x0000009E System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x0000009F System.Object System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x000000A0 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000A1 System.Collections.IEnumerator System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A2 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::.ctor(System.Int32)
// 0x000000A3 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x000000A4 System.Boolean System.Linq.Enumerable/<CastIterator>d__99`1::MoveNext()
// 0x000000A5 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::<>m__Finally1()
// 0x000000A6 TResult System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000A7 System.Void System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x000000A8 System.Object System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x000000A9 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000AA System.Collections.IEnumerator System.Linq.Enumerable/<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AB System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x000000AC System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x000000AD System.Void System.Linq.IdentityFunction`1/<>c::.cctor()
// 0x000000AE System.Void System.Linq.IdentityFunction`1/<>c::.ctor()
// 0x000000AF TElement System.Linq.IdentityFunction`1/<>c::<get_Instance>b__1_0(TElement)
// 0x000000B0 System.Linq.Lookup`2<TKey,TElement> System.Linq.Lookup`2::Create(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000B1 System.Void System.Linq.Lookup`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000B2 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.Lookup`2::GetEnumerator()
// 0x000000B3 System.Collections.IEnumerator System.Linq.Lookup`2::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B4 System.Int32 System.Linq.Lookup`2::InternalGetHashCode(TKey)
// 0x000000B5 System.Linq.Lookup`2/Grouping<TKey,TElement> System.Linq.Lookup`2::GetGrouping(TKey,System.Boolean)
// 0x000000B6 System.Void System.Linq.Lookup`2::Resize()
// 0x000000B7 System.Void System.Linq.Lookup`2/Grouping::Add(TElement)
// 0x000000B8 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.Lookup`2/Grouping::GetEnumerator()
// 0x000000B9 System.Collections.IEnumerator System.Linq.Lookup`2/Grouping::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BA System.Int32 System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.get_Count()
// 0x000000BB System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.get_IsReadOnly()
// 0x000000BC System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Add(TElement)
// 0x000000BD System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Clear()
// 0x000000BE System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Contains(TElement)
// 0x000000BF System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.CopyTo(TElement[],System.Int32)
// 0x000000C0 System.Boolean System.Linq.Lookup`2/Grouping::System.Collections.Generic.ICollection<TElement>.Remove(TElement)
// 0x000000C1 System.Int32 System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.IndexOf(TElement)
// 0x000000C2 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.Insert(System.Int32,TElement)
// 0x000000C3 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.RemoveAt(System.Int32)
// 0x000000C4 TElement System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.get_Item(System.Int32)
// 0x000000C5 System.Void System.Linq.Lookup`2/Grouping::System.Collections.Generic.IList<TElement>.set_Item(System.Int32,TElement)
// 0x000000C6 System.Void System.Linq.Lookup`2/Grouping::.ctor()
// 0x000000C7 System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::.ctor(System.Int32)
// 0x000000C8 System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.IDisposable.Dispose()
// 0x000000C9 System.Boolean System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::MoveNext()
// 0x000000CA TElement System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000CB System.Void System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.IEnumerator.Reset()
// 0x000000CC System.Object System.Linq.Lookup`2/Grouping/<GetEnumerator>d__7::System.Collections.IEnumerator.get_Current()
// 0x000000CD System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::.ctor(System.Int32)
// 0x000000CE System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::System.IDisposable.Dispose()
// 0x000000CF System.Boolean System.Linq.Lookup`2/<GetEnumerator>d__12::MoveNext()
// 0x000000D0 System.Linq.IGrouping`2<TKey,TElement> System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.Generic.IEnumerator<System.Linq.IGrouping<TKey,TElement>>.get_Current()
// 0x000000D1 System.Void System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.IEnumerator.Reset()
// 0x000000D2 System.Object System.Linq.Lookup`2/<GetEnumerator>d__12::System.Collections.IEnumerator.get_Current()
// 0x000000D3 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000D4 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000D5 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000D6 System.Void System.Linq.Set`1::Resize()
// 0x000000D7 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000D8 System.Void System.Linq.GroupedEnumerable`3::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x000000D9 System.Collections.Generic.IEnumerator`1<System.Linq.IGrouping`2<TKey,TElement>> System.Linq.GroupedEnumerable`3::GetEnumerator()
// 0x000000DA System.Collections.IEnumerator System.Linq.GroupedEnumerable`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000DB System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000DC System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000DD System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000DE System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000DF System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000E0 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000E1 System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x000000E2 TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000E3 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000E4 System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000E5 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000E6 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000E7 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000E8 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000E9 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000EA System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000EB System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000EC System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000ED System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000EE System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000EF System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000F0 TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000F1 System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32*,System.Int32)
extern void BitHelper__ctor_m2770BB414919277B2CF522840B54819F5082CD80 (void);
// 0x000000F2 System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32[],System.Int32)
extern void BitHelper__ctor_m7A8E43BE1D2A4ED086E708B6FFE693322FC9D2EB (void);
// 0x000000F3 System.Void System.Collections.Generic.BitHelper::MarkBit(System.Int32)
extern void BitHelper_MarkBit_m1C6D787021BEA9D02DCA0762C09E5D443E04A86B (void);
// 0x000000F4 System.Boolean System.Collections.Generic.BitHelper::IsMarked(System.Int32)
extern void BitHelper_IsMarked_m6036A81F50D820045D3F62E52D57098A332AB608 (void);
// 0x000000F5 System.Int32 System.Collections.Generic.BitHelper::ToIntArrayLength(System.Int32)
extern void BitHelper_ToIntArrayLength_m32A0B1B014CB81891165AC325514784171C8E7B3 (void);
// 0x000000F6 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000F7 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000F8 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000F9 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000FA System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000FB System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x000000FC System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000FD System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000FE System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000FF System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000100 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000101 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000102 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000103 System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000104 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000105 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000106 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000107 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000108 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000109 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000010A System.Boolean System.Collections.Generic.HashSet`1::IsSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000010B System.Boolean System.Collections.Generic.HashSet`1::IsProperSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000010C System.Boolean System.Collections.Generic.HashSet`1::IsSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000010D System.Boolean System.Collections.Generic.HashSet`1::IsProperSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000010E System.Boolean System.Collections.Generic.HashSet`1::Overlaps(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000010F System.Boolean System.Collections.Generic.HashSet`1::SetEquals(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000110 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000111 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000112 System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x00000113 System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x00000114 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000115 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000116 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000117 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000118 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x00000119 System.Boolean System.Collections.Generic.HashSet`1::ContainsAllElements(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000011A System.Boolean System.Collections.Generic.HashSet`1::IsSubsetOfHashSetWithSameEC(System.Collections.Generic.HashSet`1<T>)
// 0x0000011B System.Int32 System.Collections.Generic.HashSet`1::InternalIndexOf(T)
// 0x0000011C System.Collections.Generic.HashSet`1/ElementCount<T> System.Collections.Generic.HashSet`1::CheckUniqueAndUnfoundElements(System.Collections.Generic.IEnumerable`1<T>,System.Boolean)
// 0x0000011D System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x0000011E System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000011F System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x00000120 System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x00000121 System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x00000122 T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x00000123 System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000124 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
// 0x00000125 System.Void System.Collections.Generic.ICollectionDebugView`1::.ctor(System.Collections.Generic.ICollection`1<T>)
// 0x00000126 T[] System.Collections.Generic.ICollectionDebugView`1::get_Items()
static Il2CppMethodPointer s_methodPointers[294] = 
{
	SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412,
	AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57,
	AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937,
	AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018,
	AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E,
	AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31,
	AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1,
	AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6,
	AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93,
	AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6,
	AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460,
	AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45,
	AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B,
	AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C,
	AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268,
	AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF,
	AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153,
	AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477,
	AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91,
	AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E,
	AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265,
	AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4,
	AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502,
	AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1,
	AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465,
	AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34,
	AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB,
	AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252,
	AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F,
	AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058,
	AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218,
	AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D,
	AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D,
	AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63,
	AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD,
	AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E,
	AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF,
	AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB,
	AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E,
	AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598,
	AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1,
	AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679,
	AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129,
	AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A,
	AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD,
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2,
	Error_MoreThanOneElement_mDB56C9FA4C344A86553D6AFB66D10B290302C53A,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	Error_NotSupported_m51A0560ABF374B66CF6D1208DAF27C4CBAD9AABA,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_m6CFC8CEAC70AE3C469A5D1993FAF8EEEC6A06FB5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BitHelper__ctor_m2770BB414919277B2CF522840B54819F5082CD80,
	BitHelper__ctor_m7A8E43BE1D2A4ED086E708B6FFE693322FC9D2EB,
	BitHelper_MarkBit_m1C6D787021BEA9D02DCA0762C09E5D443E04A86B,
	BitHelper_IsMarked_m6036A81F50D820045D3F62E52D57098A332AB608,
	BitHelper_ToIntArrayLength_m32A0B1B014CB81891165AC325514784171C8E7B3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[294] = 
{
	4033,
	2754,
	2679,
	2699,
	2279,
	2699,
	2279,
	2679,
	2261,
	2679,
	2261,
	2679,
	2261,
	2699,
	996,
	2699,
	996,
	2307,
	2754,
	2754,
	2754,
	2754,
	2754,
	996,
	996,
	2699,
	2279,
	2699,
	2279,
	2679,
	2261,
	2679,
	2679,
	2261,
	2679,
	2261,
	2699,
	2699,
	2307,
	577,
	1348,
	1682,
	860,
	860,
	4198,
	4033,
	4033,
	4181,
	4181,
	4181,
	4181,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3964,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1131,
	1339,
	2261,
	1994,
	3960,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[70] = 
{
	{ 0x02000008, { 105, 4 } },
	{ 0x02000009, { 109, 9 } },
	{ 0x0200000A, { 120, 7 } },
	{ 0x0200000B, { 129, 10 } },
	{ 0x0200000C, { 141, 11 } },
	{ 0x0200000D, { 155, 9 } },
	{ 0x0200000E, { 167, 12 } },
	{ 0x0200000F, { 182, 1 } },
	{ 0x02000010, { 183, 2 } },
	{ 0x02000011, { 185, 12 } },
	{ 0x02000012, { 197, 8 } },
	{ 0x02000013, { 205, 12 } },
	{ 0x02000014, { 217, 6 } },
	{ 0x02000015, { 223, 2 } },
	{ 0x02000016, { 225, 4 } },
	{ 0x02000017, { 229, 3 } },
	{ 0x0200001A, { 232, 17 } },
	{ 0x0200001B, { 253, 5 } },
	{ 0x0200001C, { 258, 1 } },
	{ 0x0200001E, { 259, 8 } },
	{ 0x02000020, { 267, 4 } },
	{ 0x02000021, { 271, 3 } },
	{ 0x02000022, { 274, 5 } },
	{ 0x02000023, { 279, 7 } },
	{ 0x02000024, { 286, 3 } },
	{ 0x02000025, { 289, 7 } },
	{ 0x02000026, { 296, 4 } },
	{ 0x02000028, { 300, 43 } },
	{ 0x0200002B, { 343, 2 } },
	{ 0x0200002C, { 345, 2 } },
	{ 0x06000034, { 0, 10 } },
	{ 0x06000035, { 10, 10 } },
	{ 0x06000036, { 20, 5 } },
	{ 0x06000037, { 25, 5 } },
	{ 0x06000038, { 30, 1 } },
	{ 0x06000039, { 31, 2 } },
	{ 0x0600003A, { 33, 1 } },
	{ 0x0600003B, { 34, 2 } },
	{ 0x0600003C, { 36, 2 } },
	{ 0x0600003D, { 38, 4 } },
	{ 0x0600003E, { 42, 1 } },
	{ 0x0600003F, { 43, 2 } },
	{ 0x06000040, { 45, 3 } },
	{ 0x06000041, { 48, 2 } },
	{ 0x06000042, { 50, 1 } },
	{ 0x06000043, { 51, 7 } },
	{ 0x06000044, { 58, 2 } },
	{ 0x06000045, { 60, 2 } },
	{ 0x06000046, { 62, 4 } },
	{ 0x06000047, { 66, 4 } },
	{ 0x06000048, { 70, 3 } },
	{ 0x06000049, { 73, 4 } },
	{ 0x0600004A, { 77, 4 } },
	{ 0x0600004B, { 81, 3 } },
	{ 0x0600004C, { 84, 1 } },
	{ 0x0600004D, { 85, 1 } },
	{ 0x0600004E, { 86, 3 } },
	{ 0x0600004F, { 89, 3 } },
	{ 0x06000050, { 92, 2 } },
	{ 0x06000051, { 94, 3 } },
	{ 0x06000052, { 97, 2 } },
	{ 0x06000053, { 99, 5 } },
	{ 0x06000055, { 104, 1 } },
	{ 0x06000065, { 118, 2 } },
	{ 0x0600006A, { 127, 2 } },
	{ 0x0600006F, { 139, 2 } },
	{ 0x06000075, { 152, 3 } },
	{ 0x0600007A, { 164, 3 } },
	{ 0x0600007F, { 179, 3 } },
	{ 0x060000B0, { 249, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[347] = 
{
	{ (Il2CppRGCTXDataType)2, 2602 },
	{ (Il2CppRGCTXDataType)3, 9562 },
	{ (Il2CppRGCTXDataType)2, 4013 },
	{ (Il2CppRGCTXDataType)2, 3532 },
	{ (Il2CppRGCTXDataType)3, 16939 },
	{ (Il2CppRGCTXDataType)2, 2713 },
	{ (Il2CppRGCTXDataType)2, 3539 },
	{ (Il2CppRGCTXDataType)3, 16976 },
	{ (Il2CppRGCTXDataType)2, 3534 },
	{ (Il2CppRGCTXDataType)3, 16948 },
	{ (Il2CppRGCTXDataType)2, 2603 },
	{ (Il2CppRGCTXDataType)3, 9563 },
	{ (Il2CppRGCTXDataType)2, 4033 },
	{ (Il2CppRGCTXDataType)2, 3541 },
	{ (Il2CppRGCTXDataType)3, 16985 },
	{ (Il2CppRGCTXDataType)2, 2730 },
	{ (Il2CppRGCTXDataType)2, 3549 },
	{ (Il2CppRGCTXDataType)3, 17035 },
	{ (Il2CppRGCTXDataType)2, 3545 },
	{ (Il2CppRGCTXDataType)3, 17008 },
	{ (Il2CppRGCTXDataType)2, 807 },
	{ (Il2CppRGCTXDataType)3, 237 },
	{ (Il2CppRGCTXDataType)3, 238 },
	{ (Il2CppRGCTXDataType)2, 1616 },
	{ (Il2CppRGCTXDataType)3, 7442 },
	{ (Il2CppRGCTXDataType)2, 810 },
	{ (Il2CppRGCTXDataType)3, 255 },
	{ (Il2CppRGCTXDataType)3, 256 },
	{ (Il2CppRGCTXDataType)2, 1640 },
	{ (Il2CppRGCTXDataType)3, 7453 },
	{ (Il2CppRGCTXDataType)3, 19371 },
	{ (Il2CppRGCTXDataType)2, 840 },
	{ (Il2CppRGCTXDataType)3, 373 },
	{ (Il2CppRGCTXDataType)3, 19386 },
	{ (Il2CppRGCTXDataType)2, 847 },
	{ (Il2CppRGCTXDataType)3, 406 },
	{ (Il2CppRGCTXDataType)2, 3112 },
	{ (Il2CppRGCTXDataType)3, 14388 },
	{ (Il2CppRGCTXDataType)3, 8446 },
	{ (Il2CppRGCTXDataType)2, 2590 },
	{ (Il2CppRGCTXDataType)2, 1878 },
	{ (Il2CppRGCTXDataType)3, 8227 },
	{ (Il2CppRGCTXDataType)3, 19421 },
	{ (Il2CppRGCTXDataType)2, 849 },
	{ (Il2CppRGCTXDataType)3, 429 },
	{ (Il2CppRGCTXDataType)2, 1093 },
	{ (Il2CppRGCTXDataType)3, 1889 },
	{ (Il2CppRGCTXDataType)3, 1890 },
	{ (Il2CppRGCTXDataType)2, 2714 },
	{ (Il2CppRGCTXDataType)3, 10389 },
	{ (Il2CppRGCTXDataType)3, 19407 },
	{ (Il2CppRGCTXDataType)2, 1251 },
	{ (Il2CppRGCTXDataType)3, 2952 },
	{ (Il2CppRGCTXDataType)2, 2108 },
	{ (Il2CppRGCTXDataType)2, 2246 },
	{ (Il2CppRGCTXDataType)3, 7451 },
	{ (Il2CppRGCTXDataType)3, 7452 },
	{ (Il2CppRGCTXDataType)3, 2953 },
	{ (Il2CppRGCTXDataType)2, 2072 },
	{ (Il2CppRGCTXDataType)3, 19313 },
	{ (Il2CppRGCTXDataType)2, 816 },
	{ (Il2CppRGCTXDataType)3, 286 },
	{ (Il2CppRGCTXDataType)2, 2475 },
	{ (Il2CppRGCTXDataType)2, 1908 },
	{ (Il2CppRGCTXDataType)2, 2084 },
	{ (Il2CppRGCTXDataType)2, 2239 },
	{ (Il2CppRGCTXDataType)2, 2476 },
	{ (Il2CppRGCTXDataType)2, 1909 },
	{ (Il2CppRGCTXDataType)2, 2085 },
	{ (Il2CppRGCTXDataType)2, 2240 },
	{ (Il2CppRGCTXDataType)2, 2086 },
	{ (Il2CppRGCTXDataType)2, 2241 },
	{ (Il2CppRGCTXDataType)3, 7444 },
	{ (Il2CppRGCTXDataType)2, 2477 },
	{ (Il2CppRGCTXDataType)2, 1910 },
	{ (Il2CppRGCTXDataType)2, 2087 },
	{ (Il2CppRGCTXDataType)2, 2242 },
	{ (Il2CppRGCTXDataType)2, 2478 },
	{ (Il2CppRGCTXDataType)2, 1911 },
	{ (Il2CppRGCTXDataType)2, 2088 },
	{ (Il2CppRGCTXDataType)2, 2243 },
	{ (Il2CppRGCTXDataType)2, 2089 },
	{ (Il2CppRGCTXDataType)2, 2244 },
	{ (Il2CppRGCTXDataType)3, 7445 },
	{ (Il2CppRGCTXDataType)2, 1407 },
	{ (Il2CppRGCTXDataType)2, 2068 },
	{ (Il2CppRGCTXDataType)2, 2069 },
	{ (Il2CppRGCTXDataType)2, 2236 },
	{ (Il2CppRGCTXDataType)3, 7441 },
	{ (Il2CppRGCTXDataType)2, 2067 },
	{ (Il2CppRGCTXDataType)2, 2235 },
	{ (Il2CppRGCTXDataType)3, 7440 },
	{ (Il2CppRGCTXDataType)2, 1907 },
	{ (Il2CppRGCTXDataType)2, 2081 },
	{ (Il2CppRGCTXDataType)2, 2082 },
	{ (Il2CppRGCTXDataType)2, 2238 },
	{ (Il2CppRGCTXDataType)3, 7443 },
	{ (Il2CppRGCTXDataType)2, 1906 },
	{ (Il2CppRGCTXDataType)3, 19330 },
	{ (Il2CppRGCTXDataType)3, 6737 },
	{ (Il2CppRGCTXDataType)2, 1479 },
	{ (Il2CppRGCTXDataType)2, 2071 },
	{ (Il2CppRGCTXDataType)2, 2237 },
	{ (Il2CppRGCTXDataType)2, 2353 },
	{ (Il2CppRGCTXDataType)3, 19358 },
	{ (Il2CppRGCTXDataType)3, 9564 },
	{ (Il2CppRGCTXDataType)3, 9566 },
	{ (Il2CppRGCTXDataType)2, 483 },
	{ (Il2CppRGCTXDataType)3, 9565 },
	{ (Il2CppRGCTXDataType)3, 9574 },
	{ (Il2CppRGCTXDataType)2, 2606 },
	{ (Il2CppRGCTXDataType)2, 3535 },
	{ (Il2CppRGCTXDataType)3, 16949 },
	{ (Il2CppRGCTXDataType)3, 9575 },
	{ (Il2CppRGCTXDataType)2, 2160 },
	{ (Il2CppRGCTXDataType)2, 2281 },
	{ (Il2CppRGCTXDataType)3, 7462 },
	{ (Il2CppRGCTXDataType)3, 19316 },
	{ (Il2CppRGCTXDataType)2, 3546 },
	{ (Il2CppRGCTXDataType)3, 17009 },
	{ (Il2CppRGCTXDataType)3, 9567 },
	{ (Il2CppRGCTXDataType)2, 2605 },
	{ (Il2CppRGCTXDataType)2, 3533 },
	{ (Il2CppRGCTXDataType)3, 16940 },
	{ (Il2CppRGCTXDataType)3, 7461 },
	{ (Il2CppRGCTXDataType)3, 9568 },
	{ (Il2CppRGCTXDataType)3, 19315 },
	{ (Il2CppRGCTXDataType)2, 3542 },
	{ (Il2CppRGCTXDataType)3, 16986 },
	{ (Il2CppRGCTXDataType)3, 9581 },
	{ (Il2CppRGCTXDataType)2, 2607 },
	{ (Il2CppRGCTXDataType)2, 3540 },
	{ (Il2CppRGCTXDataType)3, 16977 },
	{ (Il2CppRGCTXDataType)3, 10439 },
	{ (Il2CppRGCTXDataType)3, 5565 },
	{ (Il2CppRGCTXDataType)3, 7463 },
	{ (Il2CppRGCTXDataType)3, 5564 },
	{ (Il2CppRGCTXDataType)3, 9582 },
	{ (Il2CppRGCTXDataType)3, 19317 },
	{ (Il2CppRGCTXDataType)2, 3550 },
	{ (Il2CppRGCTXDataType)3, 17036 },
	{ (Il2CppRGCTXDataType)3, 9595 },
	{ (Il2CppRGCTXDataType)2, 2609 },
	{ (Il2CppRGCTXDataType)2, 3548 },
	{ (Il2CppRGCTXDataType)3, 17011 },
	{ (Il2CppRGCTXDataType)3, 9596 },
	{ (Il2CppRGCTXDataType)2, 2163 },
	{ (Il2CppRGCTXDataType)2, 2284 },
	{ (Il2CppRGCTXDataType)3, 7467 },
	{ (Il2CppRGCTXDataType)3, 7466 },
	{ (Il2CppRGCTXDataType)2, 3537 },
	{ (Il2CppRGCTXDataType)3, 16951 },
	{ (Il2CppRGCTXDataType)3, 19323 },
	{ (Il2CppRGCTXDataType)2, 3547 },
	{ (Il2CppRGCTXDataType)3, 17010 },
	{ (Il2CppRGCTXDataType)3, 9588 },
	{ (Il2CppRGCTXDataType)2, 2608 },
	{ (Il2CppRGCTXDataType)2, 3544 },
	{ (Il2CppRGCTXDataType)3, 16988 },
	{ (Il2CppRGCTXDataType)3, 7465 },
	{ (Il2CppRGCTXDataType)3, 7464 },
	{ (Il2CppRGCTXDataType)3, 9589 },
	{ (Il2CppRGCTXDataType)2, 3536 },
	{ (Il2CppRGCTXDataType)3, 16950 },
	{ (Il2CppRGCTXDataType)3, 19322 },
	{ (Il2CppRGCTXDataType)2, 3543 },
	{ (Il2CppRGCTXDataType)3, 16987 },
	{ (Il2CppRGCTXDataType)3, 9602 },
	{ (Il2CppRGCTXDataType)2, 2610 },
	{ (Il2CppRGCTXDataType)2, 3552 },
	{ (Il2CppRGCTXDataType)3, 17038 },
	{ (Il2CppRGCTXDataType)3, 10440 },
	{ (Il2CppRGCTXDataType)3, 5567 },
	{ (Il2CppRGCTXDataType)3, 7469 },
	{ (Il2CppRGCTXDataType)3, 7468 },
	{ (Il2CppRGCTXDataType)3, 5566 },
	{ (Il2CppRGCTXDataType)3, 9603 },
	{ (Il2CppRGCTXDataType)2, 3538 },
	{ (Il2CppRGCTXDataType)3, 16952 },
	{ (Il2CppRGCTXDataType)3, 19324 },
	{ (Il2CppRGCTXDataType)2, 3551 },
	{ (Il2CppRGCTXDataType)3, 17037 },
	{ (Il2CppRGCTXDataType)3, 7458 },
	{ (Il2CppRGCTXDataType)3, 7459 },
	{ (Il2CppRGCTXDataType)3, 7471 },
	{ (Il2CppRGCTXDataType)3, 376 },
	{ (Il2CppRGCTXDataType)3, 375 },
	{ (Il2CppRGCTXDataType)2, 2149 },
	{ (Il2CppRGCTXDataType)2, 2273 },
	{ (Il2CppRGCTXDataType)3, 7460 },
	{ (Il2CppRGCTXDataType)2, 2177 },
	{ (Il2CppRGCTXDataType)2, 2299 },
	{ (Il2CppRGCTXDataType)3, 378 },
	{ (Il2CppRGCTXDataType)2, 683 },
	{ (Il2CppRGCTXDataType)2, 841 },
	{ (Il2CppRGCTXDataType)3, 374 },
	{ (Il2CppRGCTXDataType)3, 377 },
	{ (Il2CppRGCTXDataType)3, 408 },
	{ (Il2CppRGCTXDataType)2, 2152 },
	{ (Il2CppRGCTXDataType)2, 2275 },
	{ (Il2CppRGCTXDataType)3, 410 },
	{ (Il2CppRGCTXDataType)2, 479 },
	{ (Il2CppRGCTXDataType)2, 848 },
	{ (Il2CppRGCTXDataType)3, 407 },
	{ (Il2CppRGCTXDataType)3, 409 },
	{ (Il2CppRGCTXDataType)3, 431 },
	{ (Il2CppRGCTXDataType)3, 432 },
	{ (Il2CppRGCTXDataType)2, 3256 },
	{ (Il2CppRGCTXDataType)3, 14887 },
	{ (Il2CppRGCTXDataType)2, 2155 },
	{ (Il2CppRGCTXDataType)2, 2277 },
	{ (Il2CppRGCTXDataType)3, 14888 },
	{ (Il2CppRGCTXDataType)3, 434 },
	{ (Il2CppRGCTXDataType)2, 481 },
	{ (Il2CppRGCTXDataType)2, 850 },
	{ (Il2CppRGCTXDataType)3, 430 },
	{ (Il2CppRGCTXDataType)3, 433 },
	{ (Il2CppRGCTXDataType)3, 288 },
	{ (Il2CppRGCTXDataType)2, 476 },
	{ (Il2CppRGCTXDataType)3, 290 },
	{ (Il2CppRGCTXDataType)2, 817 },
	{ (Il2CppRGCTXDataType)3, 287 },
	{ (Il2CppRGCTXDataType)3, 289 },
	{ (Il2CppRGCTXDataType)2, 4045 },
	{ (Il2CppRGCTXDataType)2, 1408 },
	{ (Il2CppRGCTXDataType)2, 760 },
	{ (Il2CppRGCTXDataType)3, 0 },
	{ (Il2CppRGCTXDataType)2, 1648 },
	{ (Il2CppRGCTXDataType)3, 7456 },
	{ (Il2CppRGCTXDataType)2, 763 },
	{ (Il2CppRGCTXDataType)3, 3 },
	{ (Il2CppRGCTXDataType)2, 763 },
	{ (Il2CppRGCTXDataType)2, 2947 },
	{ (Il2CppRGCTXDataType)3, 13162 },
	{ (Il2CppRGCTXDataType)3, 13164 },
	{ (Il2CppRGCTXDataType)3, 8233 },
	{ (Il2CppRGCTXDataType)3, 6767 },
	{ (Il2CppRGCTXDataType)2, 1494 },
	{ (Il2CppRGCTXDataType)2, 4108 },
	{ (Il2CppRGCTXDataType)2, 824 },
	{ (Il2CppRGCTXDataType)3, 324 },
	{ (Il2CppRGCTXDataType)3, 13163 },
	{ (Il2CppRGCTXDataType)2, 378 },
	{ (Il2CppRGCTXDataType)2, 2368 },
	{ (Il2CppRGCTXDataType)3, 13165 },
	{ (Il2CppRGCTXDataType)3, 13166 },
	{ (Il2CppRGCTXDataType)2, 1879 },
	{ (Il2CppRGCTXDataType)3, 8232 },
	{ (Il2CppRGCTXDataType)2, 4099 },
	{ (Il2CppRGCTXDataType)2, 2091 },
	{ (Il2CppRGCTXDataType)2, 2245 },
	{ (Il2CppRGCTXDataType)3, 7448 },
	{ (Il2CppRGCTXDataType)3, 7449 },
	{ (Il2CppRGCTXDataType)3, 18902 },
	{ (Il2CppRGCTXDataType)2, 827 },
	{ (Il2CppRGCTXDataType)3, 354 },
	{ (Il2CppRGCTXDataType)3, 8234 },
	{ (Il2CppRGCTXDataType)3, 17139 },
	{ (Il2CppRGCTXDataType)2, 709 },
	{ (Il2CppRGCTXDataType)3, 6775 },
	{ (Il2CppRGCTXDataType)2, 1497 },
	{ (Il2CppRGCTXDataType)2, 4125 },
	{ (Il2CppRGCTXDataType)3, 14884 },
	{ (Il2CppRGCTXDataType)3, 14885 },
	{ (Il2CppRGCTXDataType)2, 2372 },
	{ (Il2CppRGCTXDataType)3, 14886 },
	{ (Il2CppRGCTXDataType)2, 403 },
	{ (Il2CppRGCTXDataType)3, 13167 },
	{ (Il2CppRGCTXDataType)2, 2949 },
	{ (Il2CppRGCTXDataType)3, 13168 },
	{ (Il2CppRGCTXDataType)3, 8228 },
	{ (Il2CppRGCTXDataType)2, 823 },
	{ (Il2CppRGCTXDataType)3, 317 },
	{ (Il2CppRGCTXDataType)3, 14378 },
	{ (Il2CppRGCTXDataType)2, 1094 },
	{ (Il2CppRGCTXDataType)3, 1891 },
	{ (Il2CppRGCTXDataType)3, 14383 },
	{ (Il2CppRGCTXDataType)3, 5530 },
	{ (Il2CppRGCTXDataType)2, 528 },
	{ (Il2CppRGCTXDataType)3, 14379 },
	{ (Il2CppRGCTXDataType)2, 3109 },
	{ (Il2CppRGCTXDataType)3, 2108 },
	{ (Il2CppRGCTXDataType)2, 1133 },
	{ (Il2CppRGCTXDataType)2, 1446 },
	{ (Il2CppRGCTXDataType)3, 5536 },
	{ (Il2CppRGCTXDataType)3, 14380 },
	{ (Il2CppRGCTXDataType)3, 5525 },
	{ (Il2CppRGCTXDataType)3, 5526 },
	{ (Il2CppRGCTXDataType)3, 5524 },
	{ (Il2CppRGCTXDataType)3, 5527 },
	{ (Il2CppRGCTXDataType)2, 1442 },
	{ (Il2CppRGCTXDataType)2, 4097 },
	{ (Il2CppRGCTXDataType)3, 7455 },
	{ (Il2CppRGCTXDataType)3, 5529 },
	{ (Il2CppRGCTXDataType)2, 2030 },
	{ (Il2CppRGCTXDataType)3, 5528 },
	{ (Il2CppRGCTXDataType)2, 1914 },
	{ (Il2CppRGCTXDataType)2, 4038 },
	{ (Il2CppRGCTXDataType)2, 2110 },
	{ (Il2CppRGCTXDataType)2, 2248 },
	{ (Il2CppRGCTXDataType)3, 6756 },
	{ (Il2CppRGCTXDataType)2, 1489 },
	{ (Il2CppRGCTXDataType)3, 8255 },
	{ (Il2CppRGCTXDataType)3, 8256 },
	{ (Il2CppRGCTXDataType)2, 1885 },
	{ (Il2CppRGCTXDataType)3, 8259 },
	{ (Il2CppRGCTXDataType)2, 1885 },
	{ (Il2CppRGCTXDataType)3, 8263 },
	{ (Il2CppRGCTXDataType)2, 1920 },
	{ (Il2CppRGCTXDataType)3, 8268 },
	{ (Il2CppRGCTXDataType)3, 8274 },
	{ (Il2CppRGCTXDataType)3, 8273 },
	{ (Il2CppRGCTXDataType)2, 4123 },
	{ (Il2CppRGCTXDataType)3, 8258 },
	{ (Il2CppRGCTXDataType)3, 8257 },
	{ (Il2CppRGCTXDataType)3, 8269 },
	{ (Il2CppRGCTXDataType)2, 2363 },
	{ (Il2CppRGCTXDataType)3, 8265 },
	{ (Il2CppRGCTXDataType)3, 20005 },
	{ (Il2CppRGCTXDataType)2, 1448 },
	{ (Il2CppRGCTXDataType)3, 5548 },
	{ (Il2CppRGCTXDataType)1, 2023 },
	{ (Il2CppRGCTXDataType)2, 4051 },
	{ (Il2CppRGCTXDataType)3, 8264 },
	{ (Il2CppRGCTXDataType)1, 4051 },
	{ (Il2CppRGCTXDataType)1, 2363 },
	{ (Il2CppRGCTXDataType)2, 4123 },
	{ (Il2CppRGCTXDataType)2, 4051 },
	{ (Il2CppRGCTXDataType)2, 2118 },
	{ (Il2CppRGCTXDataType)2, 2252 },
	{ (Il2CppRGCTXDataType)3, 8276 },
	{ (Il2CppRGCTXDataType)3, 8271 },
	{ (Il2CppRGCTXDataType)3, 8260 },
	{ (Il2CppRGCTXDataType)3, 8262 },
	{ (Il2CppRGCTXDataType)3, 8261 },
	{ (Il2CppRGCTXDataType)3, 8272 },
	{ (Il2CppRGCTXDataType)3, 8267 },
	{ (Il2CppRGCTXDataType)3, 8266 },
	{ (Il2CppRGCTXDataType)3, 5550 },
	{ (Il2CppRGCTXDataType)3, 5549 },
	{ (Il2CppRGCTXDataType)3, 8270 },
	{ (Il2CppRGCTXDataType)3, 8275 },
	{ (Il2CppRGCTXDataType)2, 337 },
	{ (Il2CppRGCTXDataType)3, 5568 },
	{ (Il2CppRGCTXDataType)2, 497 },
	{ (Il2CppRGCTXDataType)2, 1923 },
	{ (Il2CppRGCTXDataType)2, 4054 },
};
extern const CustomAttributesCacheGenerator g_System_Core_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Core_CodeGenModule;
const Il2CppCodeGenModule g_System_Core_CodeGenModule = 
{
	"System.Core.dll",
	294,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	70,
	s_rgctxIndices,
	347,
	s_rgctxValues,
	NULL,
	g_System_Core_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
