﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Action`1<System.Exception>
struct Action_1_t34F00247DCE829C59C4C5AAECAE03F05F060DD90;
// System.Action`1<System.Int32>
struct Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B;
// System.Action`2<System.ArraySegment`1<System.Byte>,System.Int32>
struct Action_2_tD4220498136D5B8049F5DA471AAB03E317F604AC;
// System.Action`2<System.Int32,System.Exception>
struct Action_2_t65966DF8C2BC50E2630B66B1C17C9846754A4E67;
// System.Action`3<System.Int32,System.ArraySegment`1<System.Byte>,System.Int32>
struct Action_3_t47376D6133EE9BC1FE8FA49DBD664F9E4D3D4AA0;
// LightReflectiveMirror.BiDictionary`2<System.Net.IPEndPoint,LightReflectiveMirror.SocketProxy>
struct BiDictionary_2_t02863639173B3E88410B13A1B77825A7FBB2EAED;
// LightReflectiveMirror.BiDictionary`2<System.Int32,System.Int32>
struct BiDictionary_2_t435A1CAADB2DEFD15AD040E750DB4B57F2107025;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5;
// System.Collections.Generic.List`1<LightReflectiveMirror.RelayServerInfo>
struct List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// LightReflectiveMirror.RelayServerInfo[]
struct RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// UnityEngine.AsyncOperation
struct AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.UI.FontData
struct FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// System.Net.IPEndPoint
struct IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// LRMDirectConnectModule
struct LRMDirectConnectModule_t6175D673D3641571039F6867CEF547EB2F403C7C;
// LRMTest
struct LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11;
// LightReflectiveMirror.LightReflectiveMirrorTransport
struct LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// Mirror.NetworkAuthenticator
struct NetworkAuthenticator_t6A4A785E3C44826F033B6E4DC140A784D32341DF;
// Mirror.NetworkConnection
struct NetworkConnection_t5DD5A384AFA5946D766F7D3A5F8E5418FAB10311;
// Mirror.NetworkManager
struct NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// LightReflectiveMirror.SocketProxy
struct SocketProxy_tF629C82B2E383575BCE511E0C812AFD4D9BDAE54;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.TextGenerator
struct TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// Mirror.Transport
struct Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D;
// System.Net.Sockets.UdpClient
struct UdpClient_tB1B7578C96A20B6A0B58AC3FD3E1CB469375B920;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.Events.UnityEvent
struct UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F;
// LRMTest/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;

IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LRMTest_ServerListUpdate_mDC64CAFD8A50F56CD2A60AA00B63DBE1741E07FB_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Count_mD2AD27EADD5B52963866260417280CEAD554A5B2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mCF8188FFB57EE709BF3711CCAAC72E45D437B280_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass6_0_U3CServerListUpdateU3Eb__0_m33082D8EBF7D1DE545AB16913428438C64843B50_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<LightReflectiveMirror.RelayServerInfo>
struct  List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497, ____items_1)); }
	inline RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492* get__items_1() const { return ____items_1; }
	inline RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497_StaticFields, ____emptyArray_5)); }
	inline RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492* get__emptyArray_5() const { return ____emptyArray_5; }
	inline RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// LRMTest/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73  : public RuntimeObject
{
public:
	// System.Int32 LRMTest/<>c__DisplayClass6_0::serverId
	int32_t ___serverId_0;
	// LRMTest LRMTest/<>c__DisplayClass6_0::<>4__this
	LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_serverId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73, ___serverId_0)); }
	inline int32_t get_serverId_0() const { return ___serverId_0; }
	inline int32_t* get_address_of_serverId_0() { return &___serverId_0; }
	inline void set_serverId_0(int32_t value)
	{
		___serverId_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73, ___U3CU3E4__this_1)); }
	inline LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_1), (void*)value);
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// LightReflectiveMirror.RelayServerInfo
struct  RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD 
{
public:
	// System.String LightReflectiveMirror.RelayServerInfo::serverName
	String_t* ___serverName_0;
	// System.Int32 LightReflectiveMirror.RelayServerInfo::currentPlayers
	int32_t ___currentPlayers_1;
	// System.Int32 LightReflectiveMirror.RelayServerInfo::maxPlayers
	int32_t ___maxPlayers_2;
	// System.Int32 LightReflectiveMirror.RelayServerInfo::serverId
	int32_t ___serverId_3;
	// System.String LightReflectiveMirror.RelayServerInfo::serverData
	String_t* ___serverData_4;

public:
	inline static int32_t get_offset_of_serverName_0() { return static_cast<int32_t>(offsetof(RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD, ___serverName_0)); }
	inline String_t* get_serverName_0() const { return ___serverName_0; }
	inline String_t** get_address_of_serverName_0() { return &___serverName_0; }
	inline void set_serverName_0(String_t* value)
	{
		___serverName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serverName_0), (void*)value);
	}

	inline static int32_t get_offset_of_currentPlayers_1() { return static_cast<int32_t>(offsetof(RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD, ___currentPlayers_1)); }
	inline int32_t get_currentPlayers_1() const { return ___currentPlayers_1; }
	inline int32_t* get_address_of_currentPlayers_1() { return &___currentPlayers_1; }
	inline void set_currentPlayers_1(int32_t value)
	{
		___currentPlayers_1 = value;
	}

	inline static int32_t get_offset_of_maxPlayers_2() { return static_cast<int32_t>(offsetof(RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD, ___maxPlayers_2)); }
	inline int32_t get_maxPlayers_2() const { return ___maxPlayers_2; }
	inline int32_t* get_address_of_maxPlayers_2() { return &___maxPlayers_2; }
	inline void set_maxPlayers_2(int32_t value)
	{
		___maxPlayers_2 = value;
	}

	inline static int32_t get_offset_of_serverId_3() { return static_cast<int32_t>(offsetof(RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD, ___serverId_3)); }
	inline int32_t get_serverId_3() const { return ___serverId_3; }
	inline int32_t* get_address_of_serverId_3() { return &___serverId_3; }
	inline void set_serverId_3(int32_t value)
	{
		___serverId_3 = value;
	}

	inline static int32_t get_offset_of_serverData_4() { return static_cast<int32_t>(offsetof(RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD, ___serverData_4)); }
	inline String_t* get_serverData_4() const { return ___serverData_4; }
	inline String_t** get_address_of_serverData_4() { return &___serverData_4; }
	inline void set_serverData_4(String_t* value)
	{
		___serverData_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serverData_4), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of LightReflectiveMirror.RelayServerInfo
struct RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD_marshaled_pinvoke
{
	char* ___serverName_0;
	int32_t ___currentPlayers_1;
	int32_t ___maxPlayers_2;
	int32_t ___serverId_3;
	char* ___serverData_4;
};
// Native definition for COM marshalling of LightReflectiveMirror.RelayServerInfo
struct RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD_marshaled_com
{
	Il2CppChar* ___serverName_0;
	int32_t ___currentPlayers_1;
	int32_t ___maxPlayers_2;
	int32_t ___serverId_3;
	Il2CppChar* ___serverData_4;
};

// UnityEngine.UI.SpriteState
struct  SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Events.UnityEvent
struct  UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.UI.ColorBlock
struct  ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// Mirror.NetworkManagerMode
struct  NetworkManagerMode_t53B2212CEC3ED0A68CA0BE2365C233C7E2C83DFD 
{
public:
	// System.Int32 Mirror.NetworkManagerMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NetworkManagerMode_t53B2212CEC3ED0A68CA0BE2365C233C7E2C83DFD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// Mirror.PlayerSpawnMethod
struct  PlayerSpawnMethod_t621298130B9992D978B1AC813054DBE1D43D7D96 
{
public:
	// System.Int32 Mirror.PlayerSpawnMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PlayerSpawnMethod_t621298130B9992D978B1AC813054DBE1D43D7D96, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Mirror.SceneOperation
struct  SceneOperation_t43180D185C1D8F6FF33FC8A61F3AC3528627667E 
{
public:
	// System.Byte Mirror.SceneOperation::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SceneOperation_t43180D185C1D8F6FF33FC8A61F3AC3528627667E, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Button/ButtonClickedEvent
struct  ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F  : public UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4
{
public:

public:
};


// UnityEngine.UI.Navigation/Mode
struct  Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct  Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.UI.Navigation
struct  Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Events.UnityAction
struct  UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// LRMTest
struct  LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform LRMTest::scrollParent
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___scrollParent_4;
	// UnityEngine.GameObject LRMTest::serverEntry
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___serverEntry_5;
	// LightReflectiveMirror.LightReflectiveMirrorTransport LRMTest::_LMR
	LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * ____LMR_6;

public:
	inline static int32_t get_offset_of_scrollParent_4() { return static_cast<int32_t>(offsetof(LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11, ___scrollParent_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_scrollParent_4() const { return ___scrollParent_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_scrollParent_4() { return &___scrollParent_4; }
	inline void set_scrollParent_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___scrollParent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___scrollParent_4), (void*)value);
	}

	inline static int32_t get_offset_of_serverEntry_5() { return static_cast<int32_t>(offsetof(LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11, ___serverEntry_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_serverEntry_5() const { return ___serverEntry_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_serverEntry_5() { return &___serverEntry_5; }
	inline void set_serverEntry_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___serverEntry_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serverEntry_5), (void*)value);
	}

	inline static int32_t get_offset_of__LMR_6() { return static_cast<int32_t>(offsetof(LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11, ____LMR_6)); }
	inline LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * get__LMR_6() const { return ____LMR_6; }
	inline LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B ** get_address_of__LMR_6() { return &____LMR_6; }
	inline void set__LMR_6(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * value)
	{
		____LMR_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____LMR_6), (void*)value);
	}
};


// Mirror.NetworkManager
struct  NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean Mirror.NetworkManager::dontDestroyOnLoad
	bool ___dontDestroyOnLoad_4;
	// System.Boolean Mirror.NetworkManager::PersistNetworkManagerToOfflineScene
	bool ___PersistNetworkManagerToOfflineScene_5;
	// System.Boolean Mirror.NetworkManager::runInBackground
	bool ___runInBackground_6;
	// System.Boolean Mirror.NetworkManager::autoStartServerBuild
	bool ___autoStartServerBuild_7;
	// System.Int32 Mirror.NetworkManager::serverTickRate
	int32_t ___serverTickRate_8;
	// System.Boolean Mirror.NetworkManager::serverBatching
	bool ___serverBatching_9;
	// System.Single Mirror.NetworkManager::serverBatchInterval
	float ___serverBatchInterval_10;
	// System.String Mirror.NetworkManager::offlineScene
	String_t* ___offlineScene_11;
	// System.String Mirror.NetworkManager::onlineScene
	String_t* ___onlineScene_12;
	// Mirror.Transport Mirror.NetworkManager::transport
	Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D * ___transport_13;
	// System.String Mirror.NetworkManager::networkAddress
	String_t* ___networkAddress_14;
	// System.Int32 Mirror.NetworkManager::maxConnections
	int32_t ___maxConnections_15;
	// System.Boolean Mirror.NetworkManager::disconnectInactiveConnections
	bool ___disconnectInactiveConnections_16;
	// System.Single Mirror.NetworkManager::disconnectInactiveTimeout
	float ___disconnectInactiveTimeout_17;
	// Mirror.NetworkAuthenticator Mirror.NetworkManager::authenticator
	NetworkAuthenticator_t6A4A785E3C44826F033B6E4DC140A784D32341DF * ___authenticator_18;
	// UnityEngine.GameObject Mirror.NetworkManager::playerPrefab
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___playerPrefab_19;
	// System.Boolean Mirror.NetworkManager::autoCreatePlayer
	bool ___autoCreatePlayer_20;
	// Mirror.PlayerSpawnMethod Mirror.NetworkManager::playerSpawnMethod
	int32_t ___playerSpawnMethod_21;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> Mirror.NetworkManager::spawnPrefabs
	List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * ___spawnPrefabs_22;
	// System.Boolean Mirror.NetworkManager::isNetworkActive
	bool ___isNetworkActive_26;
	// System.Boolean Mirror.NetworkManager::clientLoadedScene
	bool ___clientLoadedScene_28;
	// Mirror.NetworkManagerMode Mirror.NetworkManager::<mode>k__BackingField
	int32_t ___U3CmodeU3Ek__BackingField_29;
	// System.Boolean Mirror.NetworkManager::finishStartHostPending
	bool ___finishStartHostPending_30;
	// Mirror.SceneOperation Mirror.NetworkManager::clientSceneOperation
	uint8_t ___clientSceneOperation_33;

public:
	inline static int32_t get_offset_of_dontDestroyOnLoad_4() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___dontDestroyOnLoad_4)); }
	inline bool get_dontDestroyOnLoad_4() const { return ___dontDestroyOnLoad_4; }
	inline bool* get_address_of_dontDestroyOnLoad_4() { return &___dontDestroyOnLoad_4; }
	inline void set_dontDestroyOnLoad_4(bool value)
	{
		___dontDestroyOnLoad_4 = value;
	}

	inline static int32_t get_offset_of_PersistNetworkManagerToOfflineScene_5() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___PersistNetworkManagerToOfflineScene_5)); }
	inline bool get_PersistNetworkManagerToOfflineScene_5() const { return ___PersistNetworkManagerToOfflineScene_5; }
	inline bool* get_address_of_PersistNetworkManagerToOfflineScene_5() { return &___PersistNetworkManagerToOfflineScene_5; }
	inline void set_PersistNetworkManagerToOfflineScene_5(bool value)
	{
		___PersistNetworkManagerToOfflineScene_5 = value;
	}

	inline static int32_t get_offset_of_runInBackground_6() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___runInBackground_6)); }
	inline bool get_runInBackground_6() const { return ___runInBackground_6; }
	inline bool* get_address_of_runInBackground_6() { return &___runInBackground_6; }
	inline void set_runInBackground_6(bool value)
	{
		___runInBackground_6 = value;
	}

	inline static int32_t get_offset_of_autoStartServerBuild_7() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___autoStartServerBuild_7)); }
	inline bool get_autoStartServerBuild_7() const { return ___autoStartServerBuild_7; }
	inline bool* get_address_of_autoStartServerBuild_7() { return &___autoStartServerBuild_7; }
	inline void set_autoStartServerBuild_7(bool value)
	{
		___autoStartServerBuild_7 = value;
	}

	inline static int32_t get_offset_of_serverTickRate_8() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___serverTickRate_8)); }
	inline int32_t get_serverTickRate_8() const { return ___serverTickRate_8; }
	inline int32_t* get_address_of_serverTickRate_8() { return &___serverTickRate_8; }
	inline void set_serverTickRate_8(int32_t value)
	{
		___serverTickRate_8 = value;
	}

	inline static int32_t get_offset_of_serverBatching_9() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___serverBatching_9)); }
	inline bool get_serverBatching_9() const { return ___serverBatching_9; }
	inline bool* get_address_of_serverBatching_9() { return &___serverBatching_9; }
	inline void set_serverBatching_9(bool value)
	{
		___serverBatching_9 = value;
	}

	inline static int32_t get_offset_of_serverBatchInterval_10() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___serverBatchInterval_10)); }
	inline float get_serverBatchInterval_10() const { return ___serverBatchInterval_10; }
	inline float* get_address_of_serverBatchInterval_10() { return &___serverBatchInterval_10; }
	inline void set_serverBatchInterval_10(float value)
	{
		___serverBatchInterval_10 = value;
	}

	inline static int32_t get_offset_of_offlineScene_11() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___offlineScene_11)); }
	inline String_t* get_offlineScene_11() const { return ___offlineScene_11; }
	inline String_t** get_address_of_offlineScene_11() { return &___offlineScene_11; }
	inline void set_offlineScene_11(String_t* value)
	{
		___offlineScene_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___offlineScene_11), (void*)value);
	}

	inline static int32_t get_offset_of_onlineScene_12() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___onlineScene_12)); }
	inline String_t* get_onlineScene_12() const { return ___onlineScene_12; }
	inline String_t** get_address_of_onlineScene_12() { return &___onlineScene_12; }
	inline void set_onlineScene_12(String_t* value)
	{
		___onlineScene_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onlineScene_12), (void*)value);
	}

	inline static int32_t get_offset_of_transport_13() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___transport_13)); }
	inline Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D * get_transport_13() const { return ___transport_13; }
	inline Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D ** get_address_of_transport_13() { return &___transport_13; }
	inline void set_transport_13(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D * value)
	{
		___transport_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___transport_13), (void*)value);
	}

	inline static int32_t get_offset_of_networkAddress_14() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___networkAddress_14)); }
	inline String_t* get_networkAddress_14() const { return ___networkAddress_14; }
	inline String_t** get_address_of_networkAddress_14() { return &___networkAddress_14; }
	inline void set_networkAddress_14(String_t* value)
	{
		___networkAddress_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___networkAddress_14), (void*)value);
	}

	inline static int32_t get_offset_of_maxConnections_15() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___maxConnections_15)); }
	inline int32_t get_maxConnections_15() const { return ___maxConnections_15; }
	inline int32_t* get_address_of_maxConnections_15() { return &___maxConnections_15; }
	inline void set_maxConnections_15(int32_t value)
	{
		___maxConnections_15 = value;
	}

	inline static int32_t get_offset_of_disconnectInactiveConnections_16() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___disconnectInactiveConnections_16)); }
	inline bool get_disconnectInactiveConnections_16() const { return ___disconnectInactiveConnections_16; }
	inline bool* get_address_of_disconnectInactiveConnections_16() { return &___disconnectInactiveConnections_16; }
	inline void set_disconnectInactiveConnections_16(bool value)
	{
		___disconnectInactiveConnections_16 = value;
	}

	inline static int32_t get_offset_of_disconnectInactiveTimeout_17() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___disconnectInactiveTimeout_17)); }
	inline float get_disconnectInactiveTimeout_17() const { return ___disconnectInactiveTimeout_17; }
	inline float* get_address_of_disconnectInactiveTimeout_17() { return &___disconnectInactiveTimeout_17; }
	inline void set_disconnectInactiveTimeout_17(float value)
	{
		___disconnectInactiveTimeout_17 = value;
	}

	inline static int32_t get_offset_of_authenticator_18() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___authenticator_18)); }
	inline NetworkAuthenticator_t6A4A785E3C44826F033B6E4DC140A784D32341DF * get_authenticator_18() const { return ___authenticator_18; }
	inline NetworkAuthenticator_t6A4A785E3C44826F033B6E4DC140A784D32341DF ** get_address_of_authenticator_18() { return &___authenticator_18; }
	inline void set_authenticator_18(NetworkAuthenticator_t6A4A785E3C44826F033B6E4DC140A784D32341DF * value)
	{
		___authenticator_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___authenticator_18), (void*)value);
	}

	inline static int32_t get_offset_of_playerPrefab_19() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___playerPrefab_19)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_playerPrefab_19() const { return ___playerPrefab_19; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_playerPrefab_19() { return &___playerPrefab_19; }
	inline void set_playerPrefab_19(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___playerPrefab_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerPrefab_19), (void*)value);
	}

	inline static int32_t get_offset_of_autoCreatePlayer_20() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___autoCreatePlayer_20)); }
	inline bool get_autoCreatePlayer_20() const { return ___autoCreatePlayer_20; }
	inline bool* get_address_of_autoCreatePlayer_20() { return &___autoCreatePlayer_20; }
	inline void set_autoCreatePlayer_20(bool value)
	{
		___autoCreatePlayer_20 = value;
	}

	inline static int32_t get_offset_of_playerSpawnMethod_21() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___playerSpawnMethod_21)); }
	inline int32_t get_playerSpawnMethod_21() const { return ___playerSpawnMethod_21; }
	inline int32_t* get_address_of_playerSpawnMethod_21() { return &___playerSpawnMethod_21; }
	inline void set_playerSpawnMethod_21(int32_t value)
	{
		___playerSpawnMethod_21 = value;
	}

	inline static int32_t get_offset_of_spawnPrefabs_22() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___spawnPrefabs_22)); }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * get_spawnPrefabs_22() const { return ___spawnPrefabs_22; }
	inline List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 ** get_address_of_spawnPrefabs_22() { return &___spawnPrefabs_22; }
	inline void set_spawnPrefabs_22(List_1_t6D0A10F47F3440798295D2FFFC6D016477AF38E5 * value)
	{
		___spawnPrefabs_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spawnPrefabs_22), (void*)value);
	}

	inline static int32_t get_offset_of_isNetworkActive_26() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___isNetworkActive_26)); }
	inline bool get_isNetworkActive_26() const { return ___isNetworkActive_26; }
	inline bool* get_address_of_isNetworkActive_26() { return &___isNetworkActive_26; }
	inline void set_isNetworkActive_26(bool value)
	{
		___isNetworkActive_26 = value;
	}

	inline static int32_t get_offset_of_clientLoadedScene_28() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___clientLoadedScene_28)); }
	inline bool get_clientLoadedScene_28() const { return ___clientLoadedScene_28; }
	inline bool* get_address_of_clientLoadedScene_28() { return &___clientLoadedScene_28; }
	inline void set_clientLoadedScene_28(bool value)
	{
		___clientLoadedScene_28 = value;
	}

	inline static int32_t get_offset_of_U3CmodeU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___U3CmodeU3Ek__BackingField_29)); }
	inline int32_t get_U3CmodeU3Ek__BackingField_29() const { return ___U3CmodeU3Ek__BackingField_29; }
	inline int32_t* get_address_of_U3CmodeU3Ek__BackingField_29() { return &___U3CmodeU3Ek__BackingField_29; }
	inline void set_U3CmodeU3Ek__BackingField_29(int32_t value)
	{
		___U3CmodeU3Ek__BackingField_29 = value;
	}

	inline static int32_t get_offset_of_finishStartHostPending_30() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___finishStartHostPending_30)); }
	inline bool get_finishStartHostPending_30() const { return ___finishStartHostPending_30; }
	inline bool* get_address_of_finishStartHostPending_30() { return &___finishStartHostPending_30; }
	inline void set_finishStartHostPending_30(bool value)
	{
		___finishStartHostPending_30 = value;
	}

	inline static int32_t get_offset_of_clientSceneOperation_33() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15, ___clientSceneOperation_33)); }
	inline uint8_t get_clientSceneOperation_33() const { return ___clientSceneOperation_33; }
	inline uint8_t* get_address_of_clientSceneOperation_33() { return &___clientSceneOperation_33; }
	inline void set_clientSceneOperation_33(uint8_t value)
	{
		___clientSceneOperation_33 = value;
	}
};

struct NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Transform> Mirror.NetworkManager::startPositions
	List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * ___startPositions_23;
	// System.Int32 Mirror.NetworkManager::startPositionIndex
	int32_t ___startPositionIndex_24;
	// Mirror.NetworkManager Mirror.NetworkManager::<singleton>k__BackingField
	NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15 * ___U3CsingletonU3Ek__BackingField_25;
	// Mirror.NetworkConnection Mirror.NetworkManager::clientReadyConnection
	NetworkConnection_t5DD5A384AFA5946D766F7D3A5F8E5418FAB10311 * ___clientReadyConnection_27;
	// System.String Mirror.NetworkManager::<networkSceneName>k__BackingField
	String_t* ___U3CnetworkSceneNameU3Ek__BackingField_31;
	// UnityEngine.AsyncOperation Mirror.NetworkManager::loadingSceneAsync
	AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * ___loadingSceneAsync_32;

public:
	inline static int32_t get_offset_of_startPositions_23() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_StaticFields, ___startPositions_23)); }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * get_startPositions_23() const { return ___startPositions_23; }
	inline List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 ** get_address_of_startPositions_23() { return &___startPositions_23; }
	inline void set_startPositions_23(List_1_t27D7842CA3FD659C9BE64845F118C2590EE2D2C0 * value)
	{
		___startPositions_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___startPositions_23), (void*)value);
	}

	inline static int32_t get_offset_of_startPositionIndex_24() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_StaticFields, ___startPositionIndex_24)); }
	inline int32_t get_startPositionIndex_24() const { return ___startPositionIndex_24; }
	inline int32_t* get_address_of_startPositionIndex_24() { return &___startPositionIndex_24; }
	inline void set_startPositionIndex_24(int32_t value)
	{
		___startPositionIndex_24 = value;
	}

	inline static int32_t get_offset_of_U3CsingletonU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_StaticFields, ___U3CsingletonU3Ek__BackingField_25)); }
	inline NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15 * get_U3CsingletonU3Ek__BackingField_25() const { return ___U3CsingletonU3Ek__BackingField_25; }
	inline NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15 ** get_address_of_U3CsingletonU3Ek__BackingField_25() { return &___U3CsingletonU3Ek__BackingField_25; }
	inline void set_U3CsingletonU3Ek__BackingField_25(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15 * value)
	{
		___U3CsingletonU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CsingletonU3Ek__BackingField_25), (void*)value);
	}

	inline static int32_t get_offset_of_clientReadyConnection_27() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_StaticFields, ___clientReadyConnection_27)); }
	inline NetworkConnection_t5DD5A384AFA5946D766F7D3A5F8E5418FAB10311 * get_clientReadyConnection_27() const { return ___clientReadyConnection_27; }
	inline NetworkConnection_t5DD5A384AFA5946D766F7D3A5F8E5418FAB10311 ** get_address_of_clientReadyConnection_27() { return &___clientReadyConnection_27; }
	inline void set_clientReadyConnection_27(NetworkConnection_t5DD5A384AFA5946D766F7D3A5F8E5418FAB10311 * value)
	{
		___clientReadyConnection_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clientReadyConnection_27), (void*)value);
	}

	inline static int32_t get_offset_of_U3CnetworkSceneNameU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_StaticFields, ___U3CnetworkSceneNameU3Ek__BackingField_31)); }
	inline String_t* get_U3CnetworkSceneNameU3Ek__BackingField_31() const { return ___U3CnetworkSceneNameU3Ek__BackingField_31; }
	inline String_t** get_address_of_U3CnetworkSceneNameU3Ek__BackingField_31() { return &___U3CnetworkSceneNameU3Ek__BackingField_31; }
	inline void set_U3CnetworkSceneNameU3Ek__BackingField_31(String_t* value)
	{
		___U3CnetworkSceneNameU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnetworkSceneNameU3Ek__BackingField_31), (void*)value);
	}

	inline static int32_t get_offset_of_loadingSceneAsync_32() { return static_cast<int32_t>(offsetof(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_StaticFields, ___loadingSceneAsync_32)); }
	inline AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * get_loadingSceneAsync_32() const { return ___loadingSceneAsync_32; }
	inline AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 ** get_address_of_loadingSceneAsync_32() { return &___loadingSceneAsync_32; }
	inline void set_loadingSceneAsync_32(AsyncOperation_tB6913CEC83169F22E96067CE8C7117A221E51A86 * value)
	{
		___loadingSceneAsync_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___loadingSceneAsync_32), (void*)value);
	}
};


// Mirror.Transport
struct  Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Action Mirror.Transport::OnClientConnected
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___OnClientConnected_5;
	// System.Action`2<System.ArraySegment`1<System.Byte>,System.Int32> Mirror.Transport::OnClientDataReceived
	Action_2_tD4220498136D5B8049F5DA471AAB03E317F604AC * ___OnClientDataReceived_6;
	// System.Action`1<System.Exception> Mirror.Transport::OnClientError
	Action_1_t34F00247DCE829C59C4C5AAECAE03F05F060DD90 * ___OnClientError_7;
	// System.Action Mirror.Transport::OnClientDisconnected
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___OnClientDisconnected_8;
	// System.Action`1<System.Int32> Mirror.Transport::OnServerConnected
	Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * ___OnServerConnected_9;
	// System.Action`3<System.Int32,System.ArraySegment`1<System.Byte>,System.Int32> Mirror.Transport::OnServerDataReceived
	Action_3_t47376D6133EE9BC1FE8FA49DBD664F9E4D3D4AA0 * ___OnServerDataReceived_10;
	// System.Action`2<System.Int32,System.Exception> Mirror.Transport::OnServerError
	Action_2_t65966DF8C2BC50E2630B66B1C17C9846754A4E67 * ___OnServerError_11;
	// System.Action`1<System.Int32> Mirror.Transport::OnServerDisconnected
	Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * ___OnServerDisconnected_12;

public:
	inline static int32_t get_offset_of_OnClientConnected_5() { return static_cast<int32_t>(offsetof(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D, ___OnClientConnected_5)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_OnClientConnected_5() const { return ___OnClientConnected_5; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_OnClientConnected_5() { return &___OnClientConnected_5; }
	inline void set_OnClientConnected_5(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___OnClientConnected_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnClientConnected_5), (void*)value);
	}

	inline static int32_t get_offset_of_OnClientDataReceived_6() { return static_cast<int32_t>(offsetof(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D, ___OnClientDataReceived_6)); }
	inline Action_2_tD4220498136D5B8049F5DA471AAB03E317F604AC * get_OnClientDataReceived_6() const { return ___OnClientDataReceived_6; }
	inline Action_2_tD4220498136D5B8049F5DA471AAB03E317F604AC ** get_address_of_OnClientDataReceived_6() { return &___OnClientDataReceived_6; }
	inline void set_OnClientDataReceived_6(Action_2_tD4220498136D5B8049F5DA471AAB03E317F604AC * value)
	{
		___OnClientDataReceived_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnClientDataReceived_6), (void*)value);
	}

	inline static int32_t get_offset_of_OnClientError_7() { return static_cast<int32_t>(offsetof(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D, ___OnClientError_7)); }
	inline Action_1_t34F00247DCE829C59C4C5AAECAE03F05F060DD90 * get_OnClientError_7() const { return ___OnClientError_7; }
	inline Action_1_t34F00247DCE829C59C4C5AAECAE03F05F060DD90 ** get_address_of_OnClientError_7() { return &___OnClientError_7; }
	inline void set_OnClientError_7(Action_1_t34F00247DCE829C59C4C5AAECAE03F05F060DD90 * value)
	{
		___OnClientError_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnClientError_7), (void*)value);
	}

	inline static int32_t get_offset_of_OnClientDisconnected_8() { return static_cast<int32_t>(offsetof(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D, ___OnClientDisconnected_8)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_OnClientDisconnected_8() const { return ___OnClientDisconnected_8; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_OnClientDisconnected_8() { return &___OnClientDisconnected_8; }
	inline void set_OnClientDisconnected_8(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___OnClientDisconnected_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnClientDisconnected_8), (void*)value);
	}

	inline static int32_t get_offset_of_OnServerConnected_9() { return static_cast<int32_t>(offsetof(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D, ___OnServerConnected_9)); }
	inline Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * get_OnServerConnected_9() const { return ___OnServerConnected_9; }
	inline Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B ** get_address_of_OnServerConnected_9() { return &___OnServerConnected_9; }
	inline void set_OnServerConnected_9(Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * value)
	{
		___OnServerConnected_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnServerConnected_9), (void*)value);
	}

	inline static int32_t get_offset_of_OnServerDataReceived_10() { return static_cast<int32_t>(offsetof(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D, ___OnServerDataReceived_10)); }
	inline Action_3_t47376D6133EE9BC1FE8FA49DBD664F9E4D3D4AA0 * get_OnServerDataReceived_10() const { return ___OnServerDataReceived_10; }
	inline Action_3_t47376D6133EE9BC1FE8FA49DBD664F9E4D3D4AA0 ** get_address_of_OnServerDataReceived_10() { return &___OnServerDataReceived_10; }
	inline void set_OnServerDataReceived_10(Action_3_t47376D6133EE9BC1FE8FA49DBD664F9E4D3D4AA0 * value)
	{
		___OnServerDataReceived_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnServerDataReceived_10), (void*)value);
	}

	inline static int32_t get_offset_of_OnServerError_11() { return static_cast<int32_t>(offsetof(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D, ___OnServerError_11)); }
	inline Action_2_t65966DF8C2BC50E2630B66B1C17C9846754A4E67 * get_OnServerError_11() const { return ___OnServerError_11; }
	inline Action_2_t65966DF8C2BC50E2630B66B1C17C9846754A4E67 ** get_address_of_OnServerError_11() { return &___OnServerError_11; }
	inline void set_OnServerError_11(Action_2_t65966DF8C2BC50E2630B66B1C17C9846754A4E67 * value)
	{
		___OnServerError_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnServerError_11), (void*)value);
	}

	inline static int32_t get_offset_of_OnServerDisconnected_12() { return static_cast<int32_t>(offsetof(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D, ___OnServerDisconnected_12)); }
	inline Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * get_OnServerDisconnected_12() const { return ___OnServerDisconnected_12; }
	inline Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B ** get_address_of_OnServerDisconnected_12() { return &___OnServerDisconnected_12; }
	inline void set_OnServerDisconnected_12(Action_1_t080BA8EFA9616A494EBB4DD352BFEF943792E05B * value)
	{
		___OnServerDisconnected_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___OnServerDisconnected_12), (void*)value);
	}
};

struct Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D_StaticFields
{
public:
	// Mirror.Transport Mirror.Transport::activeTransport
	Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D * ___activeTransport_4;

public:
	inline static int32_t get_offset_of_activeTransport_4() { return static_cast<int32_t>(offsetof(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D_StaticFields, ___activeTransport_4)); }
	inline Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D * get_activeTransport_4() const { return ___activeTransport_4; }
	inline Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D ** get_address_of_activeTransport_4() { return &___activeTransport_4; }
	inline void set_activeTransport_4(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D * value)
	{
		___activeTransport_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___activeTransport_4), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// LightReflectiveMirror.LightReflectiveMirrorTransport
struct  LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B  : public Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D
{
public:
	// UnityEngine.UI.Text LightReflectiveMirror.LightReflectiveMirrorTransport::_IDServerText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ____IDServerText_13;
	// Mirror.Transport LightReflectiveMirror.LightReflectiveMirrorTransport::clientToServerTransport
	Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D * ___clientToServerTransport_14;
	// System.String LightReflectiveMirror.LightReflectiveMirrorTransport::serverIP
	String_t* ___serverIP_15;
	// System.UInt16 LightReflectiveMirror.LightReflectiveMirrorTransport::endpointServerPort
	uint16_t ___endpointServerPort_16;
	// System.Single LightReflectiveMirror.LightReflectiveMirrorTransport::heartBeatInterval
	float ___heartBeatInterval_17;
	// System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::connectOnAwake
	bool ___connectOnAwake_18;
	// System.String LightReflectiveMirror.LightReflectiveMirrorTransport::authenticationKey
	String_t* ___authenticationKey_19;
	// UnityEngine.Events.UnityEvent LightReflectiveMirror.LightReflectiveMirrorTransport::diconnectedFromRelay
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___diconnectedFromRelay_20;
	// System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::useNATPunch
	bool ___useNATPunch_21;
	// System.UInt16 LightReflectiveMirror.LightReflectiveMirrorTransport::NATPunchtroughPort
	uint16_t ___NATPunchtroughPort_22;
	// System.String LightReflectiveMirror.LightReflectiveMirrorTransport::serverName
	String_t* ___serverName_23;
	// System.String LightReflectiveMirror.LightReflectiveMirrorTransport::extraServerData
	String_t* ___extraServerData_24;
	// System.Int32 LightReflectiveMirror.LightReflectiveMirrorTransport::maxServerPlayers
	int32_t ___maxServerPlayers_25;
	// System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::isPublicServer
	bool ___isPublicServer_26;
	// UnityEngine.Events.UnityEvent LightReflectiveMirror.LightReflectiveMirrorTransport::serverListUpdated
	UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * ___serverListUpdated_27;
	// System.Collections.Generic.List`1<LightReflectiveMirror.RelayServerInfo> LightReflectiveMirror.LightReflectiveMirrorTransport::<relayServerList>k__BackingField
	List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * ___U3CrelayServerListU3Ek__BackingField_28;
	// System.Int32 LightReflectiveMirror.LightReflectiveMirrorTransport::serverId
	int32_t ___serverId_29;
	// LRMDirectConnectModule LightReflectiveMirror.LightReflectiveMirrorTransport::_directConnectModule
	LRMDirectConnectModule_t6175D673D3641571039F6867CEF547EB2F403C7C * ____directConnectModule_30;
	// System.Byte[] LightReflectiveMirror.LightReflectiveMirrorTransport::_clientSendBuffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____clientSendBuffer_31;
	// System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::_connectedToRelay
	bool ____connectedToRelay_32;
	// System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::_isClient
	bool ____isClient_33;
	// System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::_isServer
	bool ____isServer_34;
	// System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::_directConnected
	bool ____directConnected_35;
	// System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::_isAuthenticated
	bool ____isAuthenticated_36;
	// System.Int32 LightReflectiveMirror.LightReflectiveMirrorTransport::_currentMemberId
	int32_t ____currentMemberId_37;
	// System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::_callbacksInitialized
	bool ____callbacksInitialized_38;
	// System.Int32 LightReflectiveMirror.LightReflectiveMirrorTransport::_cachedHostID
	int32_t ____cachedHostID_39;
	// System.Net.Sockets.UdpClient LightReflectiveMirror.LightReflectiveMirrorTransport::_NATPuncher
	UdpClient_tB1B7578C96A20B6A0B58AC3FD3E1CB469375B920 * ____NATPuncher_40;
	// System.Net.IPEndPoint LightReflectiveMirror.LightReflectiveMirrorTransport::_NATIP
	IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * ____NATIP_41;
	// System.Net.IPEndPoint LightReflectiveMirror.LightReflectiveMirrorTransport::_relayPuncherIP
	IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * ____relayPuncherIP_42;
	// System.Byte[] LightReflectiveMirror.LightReflectiveMirrorTransport::_punchData
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ____punchData_43;
	// System.Net.IPEndPoint LightReflectiveMirror.LightReflectiveMirrorTransport::_directConnectEndpoint
	IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * ____directConnectEndpoint_44;
	// LightReflectiveMirror.SocketProxy LightReflectiveMirror.LightReflectiveMirrorTransport::_clientProxy
	SocketProxy_tF629C82B2E383575BCE511E0C812AFD4D9BDAE54 * ____clientProxy_45;
	// LightReflectiveMirror.BiDictionary`2<System.Net.IPEndPoint,LightReflectiveMirror.SocketProxy> LightReflectiveMirror.LightReflectiveMirrorTransport::_serverProxies
	BiDictionary_2_t02863639173B3E88410B13A1B77825A7FBB2EAED * ____serverProxies_46;
	// LightReflectiveMirror.BiDictionary`2<System.Int32,System.Int32> LightReflectiveMirror.LightReflectiveMirrorTransport::_connectedRelayClients
	BiDictionary_2_t435A1CAADB2DEFD15AD040E750DB4B57F2107025 * ____connectedRelayClients_47;
	// LightReflectiveMirror.BiDictionary`2<System.Int32,System.Int32> LightReflectiveMirror.LightReflectiveMirrorTransport::_connectedDirectClients
	BiDictionary_2_t435A1CAADB2DEFD15AD040E750DB4B57F2107025 * ____connectedDirectClients_48;

public:
	inline static int32_t get_offset_of__IDServerText_13() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____IDServerText_13)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get__IDServerText_13() const { return ____IDServerText_13; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of__IDServerText_13() { return &____IDServerText_13; }
	inline void set__IDServerText_13(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		____IDServerText_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____IDServerText_13), (void*)value);
	}

	inline static int32_t get_offset_of_clientToServerTransport_14() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___clientToServerTransport_14)); }
	inline Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D * get_clientToServerTransport_14() const { return ___clientToServerTransport_14; }
	inline Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D ** get_address_of_clientToServerTransport_14() { return &___clientToServerTransport_14; }
	inline void set_clientToServerTransport_14(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D * value)
	{
		___clientToServerTransport_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___clientToServerTransport_14), (void*)value);
	}

	inline static int32_t get_offset_of_serverIP_15() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___serverIP_15)); }
	inline String_t* get_serverIP_15() const { return ___serverIP_15; }
	inline String_t** get_address_of_serverIP_15() { return &___serverIP_15; }
	inline void set_serverIP_15(String_t* value)
	{
		___serverIP_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serverIP_15), (void*)value);
	}

	inline static int32_t get_offset_of_endpointServerPort_16() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___endpointServerPort_16)); }
	inline uint16_t get_endpointServerPort_16() const { return ___endpointServerPort_16; }
	inline uint16_t* get_address_of_endpointServerPort_16() { return &___endpointServerPort_16; }
	inline void set_endpointServerPort_16(uint16_t value)
	{
		___endpointServerPort_16 = value;
	}

	inline static int32_t get_offset_of_heartBeatInterval_17() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___heartBeatInterval_17)); }
	inline float get_heartBeatInterval_17() const { return ___heartBeatInterval_17; }
	inline float* get_address_of_heartBeatInterval_17() { return &___heartBeatInterval_17; }
	inline void set_heartBeatInterval_17(float value)
	{
		___heartBeatInterval_17 = value;
	}

	inline static int32_t get_offset_of_connectOnAwake_18() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___connectOnAwake_18)); }
	inline bool get_connectOnAwake_18() const { return ___connectOnAwake_18; }
	inline bool* get_address_of_connectOnAwake_18() { return &___connectOnAwake_18; }
	inline void set_connectOnAwake_18(bool value)
	{
		___connectOnAwake_18 = value;
	}

	inline static int32_t get_offset_of_authenticationKey_19() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___authenticationKey_19)); }
	inline String_t* get_authenticationKey_19() const { return ___authenticationKey_19; }
	inline String_t** get_address_of_authenticationKey_19() { return &___authenticationKey_19; }
	inline void set_authenticationKey_19(String_t* value)
	{
		___authenticationKey_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___authenticationKey_19), (void*)value);
	}

	inline static int32_t get_offset_of_diconnectedFromRelay_20() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___diconnectedFromRelay_20)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_diconnectedFromRelay_20() const { return ___diconnectedFromRelay_20; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_diconnectedFromRelay_20() { return &___diconnectedFromRelay_20; }
	inline void set_diconnectedFromRelay_20(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___diconnectedFromRelay_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___diconnectedFromRelay_20), (void*)value);
	}

	inline static int32_t get_offset_of_useNATPunch_21() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___useNATPunch_21)); }
	inline bool get_useNATPunch_21() const { return ___useNATPunch_21; }
	inline bool* get_address_of_useNATPunch_21() { return &___useNATPunch_21; }
	inline void set_useNATPunch_21(bool value)
	{
		___useNATPunch_21 = value;
	}

	inline static int32_t get_offset_of_NATPunchtroughPort_22() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___NATPunchtroughPort_22)); }
	inline uint16_t get_NATPunchtroughPort_22() const { return ___NATPunchtroughPort_22; }
	inline uint16_t* get_address_of_NATPunchtroughPort_22() { return &___NATPunchtroughPort_22; }
	inline void set_NATPunchtroughPort_22(uint16_t value)
	{
		___NATPunchtroughPort_22 = value;
	}

	inline static int32_t get_offset_of_serverName_23() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___serverName_23)); }
	inline String_t* get_serverName_23() const { return ___serverName_23; }
	inline String_t** get_address_of_serverName_23() { return &___serverName_23; }
	inline void set_serverName_23(String_t* value)
	{
		___serverName_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serverName_23), (void*)value);
	}

	inline static int32_t get_offset_of_extraServerData_24() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___extraServerData_24)); }
	inline String_t* get_extraServerData_24() const { return ___extraServerData_24; }
	inline String_t** get_address_of_extraServerData_24() { return &___extraServerData_24; }
	inline void set_extraServerData_24(String_t* value)
	{
		___extraServerData_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extraServerData_24), (void*)value);
	}

	inline static int32_t get_offset_of_maxServerPlayers_25() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___maxServerPlayers_25)); }
	inline int32_t get_maxServerPlayers_25() const { return ___maxServerPlayers_25; }
	inline int32_t* get_address_of_maxServerPlayers_25() { return &___maxServerPlayers_25; }
	inline void set_maxServerPlayers_25(int32_t value)
	{
		___maxServerPlayers_25 = value;
	}

	inline static int32_t get_offset_of_isPublicServer_26() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___isPublicServer_26)); }
	inline bool get_isPublicServer_26() const { return ___isPublicServer_26; }
	inline bool* get_address_of_isPublicServer_26() { return &___isPublicServer_26; }
	inline void set_isPublicServer_26(bool value)
	{
		___isPublicServer_26 = value;
	}

	inline static int32_t get_offset_of_serverListUpdated_27() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___serverListUpdated_27)); }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * get_serverListUpdated_27() const { return ___serverListUpdated_27; }
	inline UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 ** get_address_of_serverListUpdated_27() { return &___serverListUpdated_27; }
	inline void set_serverListUpdated_27(UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * value)
	{
		___serverListUpdated_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___serverListUpdated_27), (void*)value);
	}

	inline static int32_t get_offset_of_U3CrelayServerListU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___U3CrelayServerListU3Ek__BackingField_28)); }
	inline List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * get_U3CrelayServerListU3Ek__BackingField_28() const { return ___U3CrelayServerListU3Ek__BackingField_28; }
	inline List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 ** get_address_of_U3CrelayServerListU3Ek__BackingField_28() { return &___U3CrelayServerListU3Ek__BackingField_28; }
	inline void set_U3CrelayServerListU3Ek__BackingField_28(List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * value)
	{
		___U3CrelayServerListU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CrelayServerListU3Ek__BackingField_28), (void*)value);
	}

	inline static int32_t get_offset_of_serverId_29() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ___serverId_29)); }
	inline int32_t get_serverId_29() const { return ___serverId_29; }
	inline int32_t* get_address_of_serverId_29() { return &___serverId_29; }
	inline void set_serverId_29(int32_t value)
	{
		___serverId_29 = value;
	}

	inline static int32_t get_offset_of__directConnectModule_30() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____directConnectModule_30)); }
	inline LRMDirectConnectModule_t6175D673D3641571039F6867CEF547EB2F403C7C * get__directConnectModule_30() const { return ____directConnectModule_30; }
	inline LRMDirectConnectModule_t6175D673D3641571039F6867CEF547EB2F403C7C ** get_address_of__directConnectModule_30() { return &____directConnectModule_30; }
	inline void set__directConnectModule_30(LRMDirectConnectModule_t6175D673D3641571039F6867CEF547EB2F403C7C * value)
	{
		____directConnectModule_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____directConnectModule_30), (void*)value);
	}

	inline static int32_t get_offset_of__clientSendBuffer_31() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____clientSendBuffer_31)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__clientSendBuffer_31() const { return ____clientSendBuffer_31; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__clientSendBuffer_31() { return &____clientSendBuffer_31; }
	inline void set__clientSendBuffer_31(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____clientSendBuffer_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____clientSendBuffer_31), (void*)value);
	}

	inline static int32_t get_offset_of__connectedToRelay_32() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____connectedToRelay_32)); }
	inline bool get__connectedToRelay_32() const { return ____connectedToRelay_32; }
	inline bool* get_address_of__connectedToRelay_32() { return &____connectedToRelay_32; }
	inline void set__connectedToRelay_32(bool value)
	{
		____connectedToRelay_32 = value;
	}

	inline static int32_t get_offset_of__isClient_33() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____isClient_33)); }
	inline bool get__isClient_33() const { return ____isClient_33; }
	inline bool* get_address_of__isClient_33() { return &____isClient_33; }
	inline void set__isClient_33(bool value)
	{
		____isClient_33 = value;
	}

	inline static int32_t get_offset_of__isServer_34() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____isServer_34)); }
	inline bool get__isServer_34() const { return ____isServer_34; }
	inline bool* get_address_of__isServer_34() { return &____isServer_34; }
	inline void set__isServer_34(bool value)
	{
		____isServer_34 = value;
	}

	inline static int32_t get_offset_of__directConnected_35() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____directConnected_35)); }
	inline bool get__directConnected_35() const { return ____directConnected_35; }
	inline bool* get_address_of__directConnected_35() { return &____directConnected_35; }
	inline void set__directConnected_35(bool value)
	{
		____directConnected_35 = value;
	}

	inline static int32_t get_offset_of__isAuthenticated_36() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____isAuthenticated_36)); }
	inline bool get__isAuthenticated_36() const { return ____isAuthenticated_36; }
	inline bool* get_address_of__isAuthenticated_36() { return &____isAuthenticated_36; }
	inline void set__isAuthenticated_36(bool value)
	{
		____isAuthenticated_36 = value;
	}

	inline static int32_t get_offset_of__currentMemberId_37() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____currentMemberId_37)); }
	inline int32_t get__currentMemberId_37() const { return ____currentMemberId_37; }
	inline int32_t* get_address_of__currentMemberId_37() { return &____currentMemberId_37; }
	inline void set__currentMemberId_37(int32_t value)
	{
		____currentMemberId_37 = value;
	}

	inline static int32_t get_offset_of__callbacksInitialized_38() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____callbacksInitialized_38)); }
	inline bool get__callbacksInitialized_38() const { return ____callbacksInitialized_38; }
	inline bool* get_address_of__callbacksInitialized_38() { return &____callbacksInitialized_38; }
	inline void set__callbacksInitialized_38(bool value)
	{
		____callbacksInitialized_38 = value;
	}

	inline static int32_t get_offset_of__cachedHostID_39() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____cachedHostID_39)); }
	inline int32_t get__cachedHostID_39() const { return ____cachedHostID_39; }
	inline int32_t* get_address_of__cachedHostID_39() { return &____cachedHostID_39; }
	inline void set__cachedHostID_39(int32_t value)
	{
		____cachedHostID_39 = value;
	}

	inline static int32_t get_offset_of__NATPuncher_40() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____NATPuncher_40)); }
	inline UdpClient_tB1B7578C96A20B6A0B58AC3FD3E1CB469375B920 * get__NATPuncher_40() const { return ____NATPuncher_40; }
	inline UdpClient_tB1B7578C96A20B6A0B58AC3FD3E1CB469375B920 ** get_address_of__NATPuncher_40() { return &____NATPuncher_40; }
	inline void set__NATPuncher_40(UdpClient_tB1B7578C96A20B6A0B58AC3FD3E1CB469375B920 * value)
	{
		____NATPuncher_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____NATPuncher_40), (void*)value);
	}

	inline static int32_t get_offset_of__NATIP_41() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____NATIP_41)); }
	inline IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * get__NATIP_41() const { return ____NATIP_41; }
	inline IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E ** get_address_of__NATIP_41() { return &____NATIP_41; }
	inline void set__NATIP_41(IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * value)
	{
		____NATIP_41 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____NATIP_41), (void*)value);
	}

	inline static int32_t get_offset_of__relayPuncherIP_42() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____relayPuncherIP_42)); }
	inline IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * get__relayPuncherIP_42() const { return ____relayPuncherIP_42; }
	inline IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E ** get_address_of__relayPuncherIP_42() { return &____relayPuncherIP_42; }
	inline void set__relayPuncherIP_42(IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * value)
	{
		____relayPuncherIP_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____relayPuncherIP_42), (void*)value);
	}

	inline static int32_t get_offset_of__punchData_43() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____punchData_43)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get__punchData_43() const { return ____punchData_43; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of__punchData_43() { return &____punchData_43; }
	inline void set__punchData_43(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		____punchData_43 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____punchData_43), (void*)value);
	}

	inline static int32_t get_offset_of__directConnectEndpoint_44() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____directConnectEndpoint_44)); }
	inline IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * get__directConnectEndpoint_44() const { return ____directConnectEndpoint_44; }
	inline IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E ** get_address_of__directConnectEndpoint_44() { return &____directConnectEndpoint_44; }
	inline void set__directConnectEndpoint_44(IPEndPoint_t41C675C79A8B4EA6D5211D9B907137A2C015EA3E * value)
	{
		____directConnectEndpoint_44 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____directConnectEndpoint_44), (void*)value);
	}

	inline static int32_t get_offset_of__clientProxy_45() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____clientProxy_45)); }
	inline SocketProxy_tF629C82B2E383575BCE511E0C812AFD4D9BDAE54 * get__clientProxy_45() const { return ____clientProxy_45; }
	inline SocketProxy_tF629C82B2E383575BCE511E0C812AFD4D9BDAE54 ** get_address_of__clientProxy_45() { return &____clientProxy_45; }
	inline void set__clientProxy_45(SocketProxy_tF629C82B2E383575BCE511E0C812AFD4D9BDAE54 * value)
	{
		____clientProxy_45 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____clientProxy_45), (void*)value);
	}

	inline static int32_t get_offset_of__serverProxies_46() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____serverProxies_46)); }
	inline BiDictionary_2_t02863639173B3E88410B13A1B77825A7FBB2EAED * get__serverProxies_46() const { return ____serverProxies_46; }
	inline BiDictionary_2_t02863639173B3E88410B13A1B77825A7FBB2EAED ** get_address_of__serverProxies_46() { return &____serverProxies_46; }
	inline void set__serverProxies_46(BiDictionary_2_t02863639173B3E88410B13A1B77825A7FBB2EAED * value)
	{
		____serverProxies_46 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____serverProxies_46), (void*)value);
	}

	inline static int32_t get_offset_of__connectedRelayClients_47() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____connectedRelayClients_47)); }
	inline BiDictionary_2_t435A1CAADB2DEFD15AD040E750DB4B57F2107025 * get__connectedRelayClients_47() const { return ____connectedRelayClients_47; }
	inline BiDictionary_2_t435A1CAADB2DEFD15AD040E750DB4B57F2107025 ** get_address_of__connectedRelayClients_47() { return &____connectedRelayClients_47; }
	inline void set__connectedRelayClients_47(BiDictionary_2_t435A1CAADB2DEFD15AD040E750DB4B57F2107025 * value)
	{
		____connectedRelayClients_47 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____connectedRelayClients_47), (void*)value);
	}

	inline static int32_t get_offset_of__connectedDirectClients_48() { return static_cast<int32_t>(offsetof(LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B, ____connectedDirectClients_48)); }
	inline BiDictionary_2_t435A1CAADB2DEFD15AD040E750DB4B57F2107025 * get__connectedDirectClients_48() const { return ____connectedDirectClients_48; }
	inline BiDictionary_2_t435A1CAADB2DEFD15AD040E750DB4B57F2107025 ** get_address_of__connectedDirectClients_48() { return &____connectedDirectClients_48; }
	inline void set__connectedDirectClients_48(BiDictionary_2_t435A1CAADB2DEFD15AD040E750DB4B57F2107025 * value)
	{
		____connectedDirectClients_48 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____connectedDirectClients_48), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Button
struct  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D, ___m_OnClick_20)); }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Text
struct  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.UI.FontData UnityEngine.UI.Text::m_FontData
	FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * ___m_FontData_36;
	// System.String UnityEngine.UI.Text::m_Text
	String_t* ___m_Text_37;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCache
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCache_38;
	// UnityEngine.TextGenerator UnityEngine.UI.Text::m_TextCacheForLayout
	TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * ___m_TextCacheForLayout_39;
	// System.Boolean UnityEngine.UI.Text::m_DisableFontTextureRebuiltCallback
	bool ___m_DisableFontTextureRebuiltCallback_41;
	// UnityEngine.UIVertex[] UnityEngine.UI.Text::m_TempVerts
	UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* ___m_TempVerts_42;

public:
	inline static int32_t get_offset_of_m_FontData_36() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_FontData_36)); }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * get_m_FontData_36() const { return ___m_FontData_36; }
	inline FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 ** get_address_of_m_FontData_36() { return &___m_FontData_36; }
	inline void set_m_FontData_36(FontData_t0F1E9B3ED8136CD40782AC9A6AFB69CAD127C738 * value)
	{
		___m_FontData_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FontData_36), (void*)value);
	}

	inline static int32_t get_offset_of_m_Text_37() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_Text_37)); }
	inline String_t* get_m_Text_37() const { return ___m_Text_37; }
	inline String_t** get_address_of_m_Text_37() { return &___m_Text_37; }
	inline void set_m_Text_37(String_t* value)
	{
		___m_Text_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCache_38() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCache_38)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCache_38() const { return ___m_TextCache_38; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCache_38() { return &___m_TextCache_38; }
	inline void set_m_TextCache_38(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCache_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCache_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_TextCacheForLayout_39() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TextCacheForLayout_39)); }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * get_m_TextCacheForLayout_39() const { return ___m_TextCacheForLayout_39; }
	inline TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 ** get_address_of_m_TextCacheForLayout_39() { return &___m_TextCacheForLayout_39; }
	inline void set_m_TextCacheForLayout_39(TextGenerator_t893F256D3587633108E00E5731CDC5A77AFF1B70 * value)
	{
		___m_TextCacheForLayout_39 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextCacheForLayout_39), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisableFontTextureRebuiltCallback_41() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_DisableFontTextureRebuiltCallback_41)); }
	inline bool get_m_DisableFontTextureRebuiltCallback_41() const { return ___m_DisableFontTextureRebuiltCallback_41; }
	inline bool* get_address_of_m_DisableFontTextureRebuiltCallback_41() { return &___m_DisableFontTextureRebuiltCallback_41; }
	inline void set_m_DisableFontTextureRebuiltCallback_41(bool value)
	{
		___m_DisableFontTextureRebuiltCallback_41 = value;
	}

	inline static int32_t get_offset_of_m_TempVerts_42() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1, ___m_TempVerts_42)); }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* get_m_TempVerts_42() const { return ___m_TempVerts_42; }
	inline UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A** get_address_of_m_TempVerts_42() { return &___m_TempVerts_42; }
	inline void set_m_TempVerts_42(UIVertexU5BU5D_tE3D523C48DFEBC775876720DE2539A79FB7E5E5A* value)
	{
		___m_TempVerts_42 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TempVerts_42), (void*)value);
	}
};

struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Text::s_DefaultText
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultText_40;

public:
	inline static int32_t get_offset_of_s_DefaultText_40() { return static_cast<int32_t>(offsetof(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_StaticFields, ___s_DefaultText_40)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultText_40() const { return ___s_DefaultText_40; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultText_40() { return &___s_DefaultText_40; }
	inline void set_s_DefaultText_40(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultText_40 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultText_40), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// LightReflectiveMirror.RelayServerInfo[]
struct RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  m_Items[1];

public:
	inline RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___serverName_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___serverData_4), (void*)NULL);
		#endif
	}
	inline RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___serverName_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___serverData_4), (void*)NULL);
		#endif
	}
};


// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mD211EB15E9E128684605B4CC7277F10840F8E8CF_gshared (RuntimeObject * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<LightReflectiveMirror.RelayServerInfo>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  List_1_get_Item_mCF8188FFB57EE709BF3711CCAAC72E45D437B280_gshared_inline (List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<LightReflectiveMirror.RelayServerInfo>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mD2AD27EADD5B52963866260417280CEAD554A5B2_gshared_inline (List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * __this, const RuntimeMethod* method);

// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___call0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent::RemoveListener(UnityEngine.Events.UnityAction)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_RemoveListener_m2EB96C90EFA456EB833B618513CECB86493AF956 (UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * __this, UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___call0, const RuntimeMethod* method);
// System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::RequestServerList()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LightReflectiveMirrorTransport_RequestServerList_m97128167F0BA44C83429C4DCC4197703F1D58A46 (LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * __this, const RuntimeMethod* method);
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, const RuntimeMethod* method);
// System.Void LRMTest/<>c__DisplayClass6_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass6_0__ctor_mD6CDE7A81B6226E22BA914E58274C96CEF587CDE (U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Transform)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___parent1, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *, const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mD211EB15E9E128684605B4CC7277F10840F8E8CF_gshared)(___original0, ___parent1, method);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, int32_t ___index0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Text>()
inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Collections.Generic.List`1<LightReflectiveMirror.RelayServerInfo> LightReflectiveMirror.LightReflectiveMirrorTransport::get_relayServerList()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * LightReflectiveMirrorTransport_get_relayServerList_mC55A665EF2A23B7AEA6104117D9E002510ED1C12_inline (LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<LightReflectiveMirror.RelayServerInfo>::get_Item(System.Int32)
inline RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  List_1_get_Item_mCF8188FFB57EE709BF3711CCAAC72E45D437B280_inline (List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  (*) (List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 *, int32_t, const RuntimeMethod*))List_1_get_Item_mCF8188FFB57EE709BF3711CCAAC72E45D437B280_gshared_inline)(__this, ___index0, method);
}
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::get_onClick()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4 (Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<LightReflectiveMirror.RelayServerInfo>::get_Count()
inline int32_t List_1_get_Count_mD2AD27EADD5B52963866260417280CEAD554A5B2_inline (List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 *, const RuntimeMethod*))List_1_get_Count_mD2AD27EADD5B52963866260417280CEAD554A5B2_gshared_inline)(__this, method);
}
// Mirror.NetworkManager Mirror.NetworkManager::get_singleton()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15 * NetworkManager_get_singleton_m3687E70BBF51C41E6F20B606EF0E0E40D13E7641_inline (const RuntimeMethod* method);
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.Void Mirror.NetworkManager::StartClient()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NetworkManager_StartClient_m76D46DACCCD2C70BB78014AD376FD99D91EC36C3 (NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void LRMTest::ConnectToserver(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LRMTest_ConnectToserver_mB22E043690A8E96C201FA21AD8ABC9A89C13B6BD (LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * __this, int32_t ___serverId0, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LRMTest::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LRMTest_Start_m5D52D3731ED040D688F0D2BDBFCC45E200EFA839 (LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LRMTest_ServerListUpdate_mDC64CAFD8A50F56CD2A60AA00B63DBE1741E07FB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (_LMR== null)
		LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * L_0 = __this->get__LMR_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0040;
		}
	}
	{
		// _LMR = (LightReflectiveMirrorTransport) Transport.activeTransport;
		Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D * L_3 = ((Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D_StaticFields*)il2cpp_codegen_static_fields_for(Transport_t426FCBB81EC1C49127D5442573876AF4F83E0A1D_il2cpp_TypeInfo_var))->get_activeTransport_4();
		__this->set__LMR_6(((LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B *)CastclassClass((RuntimeObject*)L_3, LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B_il2cpp_TypeInfo_var)));
		// _LMR.serverListUpdated.AddListener(ServerListUpdate);
		LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * L_4 = __this->get__LMR_6();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_5 = L_4->get_serverListUpdated_27();
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_6 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_6, __this, (intptr_t)((intptr_t)LRMTest_ServerListUpdate_mDC64CAFD8A50F56CD2A60AA00B63DBE1741E07FB_RuntimeMethod_var), /*hidden argument*/NULL);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0040:
	{
		// }
		return;
	}
}
// System.Void LRMTest::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LRMTest_OnDisable_mE5F627CB74B7397074C0B31839A62A6CB7D6B0D8 (LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&LRMTest_ServerListUpdate_mDC64CAFD8A50F56CD2A60AA00B63DBE1741E07FB_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		// if (_LMR!=null)
		LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * L_0 = __this->get__LMR_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		V_0 = L_1;
		bool L_2 = V_0;
		if (!L_2)
		{
			goto IL_0030;
		}
	}
	{
		// _LMR.serverListUpdated.RemoveListener(ServerListUpdate);
		LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * L_3 = __this->get__LMR_6();
		UnityEvent_tA0EA9BC49FD7D5185E7A238EF2E0E6F5D0EE27F4 * L_4 = L_3->get_serverListUpdated_27();
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_5 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_5, __this, (intptr_t)((intptr_t)LRMTest_ServerListUpdate_mDC64CAFD8A50F56CD2A60AA00B63DBE1741E07FB_RuntimeMethod_var), /*hidden argument*/NULL);
		UnityEvent_RemoveListener_m2EB96C90EFA456EB833B618513CECB86493AF956(L_4, L_5, /*hidden argument*/NULL);
	}

IL_0030:
	{
		// }
		return;
	}
}
// System.Void LRMTest::RefreshServerList()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LRMTest_RefreshServerList_mC1B44DA857219C1517F085E309E71CEF2D1EA201 (LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * __this, const RuntimeMethod* method)
{
	{
		// _LMR.RequestServerList();
		LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * L_0 = __this->get__LMR_6();
		LightReflectiveMirrorTransport_RequestServerList_m97128167F0BA44C83429C4DCC4197703F1D58A46(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LRMTest::ServerListUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LRMTest_ServerListUpdate_mDC64CAFD8A50F56CD2A60AA00B63DBE1741E07FB (LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Count_mD2AD27EADD5B52963866260417280CEAD554A5B2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mCF8188FFB57EE709BF3711CCAAC72E45D437B280_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass6_0_U3CServerListUpdateU3Eb__0_m33082D8EBF7D1DE545AB16913428438C64843B50_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject* V_0 = NULL;
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_1 = NULL;
	RuntimeObject* V_2 = NULL;
	int32_t V_3 = 0;
	U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73 * V_4 = NULL;
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * V_5 = NULL;
	bool V_6 = false;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// foreach (Transform t in scrollParent)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_scrollParent_4();
		RuntimeObject* L_1;
		L_1 = Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
	}

IL_000e:
	try
	{ // begin try (depth: 1)
		{
			goto IL_002a;
		}

IL_0010:
		{
			// foreach (Transform t in scrollParent)
			RuntimeObject* L_2 = V_0;
			RuntimeObject * L_3;
			L_3 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(1 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_2);
			V_1 = ((Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)CastclassClass((RuntimeObject*)L_3, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1_il2cpp_TypeInfo_var));
			// Destroy(t.gameObject);
			Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = V_1;
			GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5;
			L_5 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(L_4, /*hidden argument*/NULL);
			IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
			Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30(L_5, /*hidden argument*/NULL);
		}

IL_002a:
		{
			// foreach (Transform t in scrollParent)
			RuntimeObject* L_6 = V_0;
			bool L_7;
			L_7 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_6);
			if (L_7)
			{
				goto IL_0010;
			}
		}

IL_0032:
		{
			IL2CPP_LEAVE(0x46, FINALLY_0034);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0034;
	}

FINALLY_0034:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_8 = V_0;
			V_2 = ((RuntimeObject*)IsInst((RuntimeObject*)L_8, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var));
			RuntimeObject* L_9 = V_2;
			if (!L_9)
			{
				goto IL_0045;
			}
		}

IL_003e:
		{
			RuntimeObject* L_10 = V_2;
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_10);
		}

IL_0045:
		{
			IL2CPP_END_FINALLY(52)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(52)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x46, IL_0046)
	}

IL_0046:
	{
		// for (int i = 0; i < _LMR.relayServerList.Count; i++)
		V_3 = 0;
		goto IL_00df;
	}

IL_004d:
	{
		U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73 * L_11 = (U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73 *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass6_0__ctor_mD6CDE7A81B6226E22BA914E58274C96CEF587CDE(L_11, /*hidden argument*/NULL);
		V_4 = L_11;
		U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73 * L_12 = V_4;
		L_12->set_U3CU3E4__this_1(__this);
		// var newEntry = Instantiate(serverEntry, scrollParent);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13 = __this->get_serverEntry_5();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14 = __this->get_scrollParent_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15;
		L_15 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8(L_13, L_14, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_mF131D53AB04E75E849487A7ACF79A8B27527F4B8_RuntimeMethod_var);
		V_5 = L_15;
		// newEntry.transform.GetChild(0).GetComponent<Text>().text = _LMR.relayServerList[i].serverName;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_16 = V_5;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_16, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_17, 0, /*hidden argument*/NULL);
		Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * L_19;
		L_19 = Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137(L_18, /*hidden argument*/Component_GetComponent_TisText_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1_m2D99AC2081683F963C56EC738451EC0B59B5D137_RuntimeMethod_var);
		LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * L_20 = __this->get__LMR_6();
		List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * L_21;
		L_21 = LightReflectiveMirrorTransport_get_relayServerList_mC55A665EF2A23B7AEA6104117D9E002510ED1C12_inline(L_20, /*hidden argument*/NULL);
		int32_t L_22 = V_3;
		RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  L_23;
		L_23 = List_1_get_Item_mCF8188FFB57EE709BF3711CCAAC72E45D437B280_inline(L_21, L_22, /*hidden argument*/List_1_get_Item_mCF8188FFB57EE709BF3711CCAAC72E45D437B280_RuntimeMethod_var);
		String_t* L_24 = L_23.get_serverName_0();
		VirtActionInvoker1< String_t* >::Invoke(75 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_19, L_24);
		// int serverId = _LMR.relayServerList[i].serverId;
		U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73 * L_25 = V_4;
		LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * L_26 = __this->get__LMR_6();
		List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * L_27;
		L_27 = LightReflectiveMirrorTransport_get_relayServerList_mC55A665EF2A23B7AEA6104117D9E002510ED1C12_inline(L_26, /*hidden argument*/NULL);
		int32_t L_28 = V_3;
		RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  L_29;
		L_29 = List_1_get_Item_mCF8188FFB57EE709BF3711CCAAC72E45D437B280_inline(L_27, L_28, /*hidden argument*/List_1_get_Item_mCF8188FFB57EE709BF3711CCAAC72E45D437B280_RuntimeMethod_var);
		int32_t L_30 = L_29.get_serverId_3();
		L_25->set_serverId_0(L_30);
		// newEntry.GetComponent<Button>().onClick.AddListener(()=>{ConnectToserver(serverId);});
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_31 = V_5;
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_32;
		L_32 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_31, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * L_33;
		L_33 = Button_get_onClick_m28BD8C670676D4E2B292B5A7F59387D4BF61F8F4(L_32, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73 * L_34 = V_4;
		UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * L_35 = (UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 *)il2cpp_codegen_object_new(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099_il2cpp_TypeInfo_var);
		UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA(L_35, L_34, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass6_0_U3CServerListUpdateU3Eb__0_m33082D8EBF7D1DE545AB16913428438C64843B50_RuntimeMethod_var), /*hidden argument*/NULL);
		UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D(L_33, L_35, /*hidden argument*/NULL);
		// for (int i = 0; i < _LMR.relayServerList.Count; i++)
		int32_t L_36 = V_3;
		V_3 = ((int32_t)il2cpp_codegen_add((int32_t)L_36, (int32_t)1));
	}

IL_00df:
	{
		// for (int i = 0; i < _LMR.relayServerList.Count; i++)
		int32_t L_37 = V_3;
		LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * L_38 = __this->get__LMR_6();
		List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * L_39;
		L_39 = LightReflectiveMirrorTransport_get_relayServerList_mC55A665EF2A23B7AEA6104117D9E002510ED1C12_inline(L_38, /*hidden argument*/NULL);
		int32_t L_40;
		L_40 = List_1_get_Count_mD2AD27EADD5B52963866260417280CEAD554A5B2_inline(L_39, /*hidden argument*/List_1_get_Count_mD2AD27EADD5B52963866260417280CEAD554A5B2_RuntimeMethod_var);
		V_6 = (bool)((((int32_t)L_37) < ((int32_t)L_40))? 1 : 0);
		bool L_41 = V_6;
		if (L_41)
		{
			goto IL_004d;
		}
	}
	{
		// }
		return;
	}
}
// System.Void LRMTest::ConnectToserver(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LRMTest_ConnectToserver_mB22E043690A8E96C201FA21AD8ABC9A89C13B6BD (LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * __this, int32_t ___serverId0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// NetworkManager.singleton.networkAddress = serverId.ToString();
		IL2CPP_RUNTIME_CLASS_INIT(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_il2cpp_TypeInfo_var);
		NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15 * L_0;
		L_0 = NetworkManager_get_singleton_m3687E70BBF51C41E6F20B606EF0E0E40D13E7641_inline(/*hidden argument*/NULL);
		String_t* L_1;
		L_1 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)(&___serverId0), /*hidden argument*/NULL);
		L_0->set_networkAddress_14(L_1);
		// NetworkManager.singleton.StartClient();
		NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15 * L_2;
		L_2 = NetworkManager_get_singleton_m3687E70BBF51C41E6F20B606EF0E0E40D13E7641_inline(/*hidden argument*/NULL);
		NetworkManager_StartClient_m76D46DACCCD2C70BB78014AD376FD99D91EC36C3(L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void LRMTest::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LRMTest__ctor_mD51418BE2C7A5593BE073D5A801E8367C0E3C09F (LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LRMTest/<>c__DisplayClass6_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass6_0__ctor_mD6CDE7A81B6226E22BA914E58274C96CEF587CDE (U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LRMTest/<>c__DisplayClass6_0::<ServerListUpdate>b__0()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass6_0_U3CServerListUpdateU3Eb__0_m33082D8EBF7D1DE545AB16913428438C64843B50 (U3CU3Ec__DisplayClass6_0_t342245E18CC4F138DF843DFC5ACE27BAECDCEC73 * __this, const RuntimeMethod* method)
{
	{
		// newEntry.GetComponent<Button>().onClick.AddListener(()=>{ConnectToserver(serverId);});
		LRMTest_tD13571EB4CF6F1E085890B190C2B53065C2DDC11 * L_0 = __this->get_U3CU3E4__this_1();
		int32_t L_1 = __this->get_serverId_0();
		LRMTest_ConnectToserver_mB22E043690A8E96C201FA21AD8ABC9A89C13B6BD(L_0, L_1, /*hidden argument*/NULL);
		// newEntry.GetComponent<Button>().onClick.AddListener(()=>{ConnectToserver(serverId);});
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * LightReflectiveMirrorTransport_get_relayServerList_mC55A665EF2A23B7AEA6104117D9E002510ED1C12_inline (LightReflectiveMirrorTransport_tCC4D6E5F944FFEF693D1E3E51B6A17FCB871AE3B * __this, const RuntimeMethod* method)
{
	{
		// public List<RelayServerInfo> relayServerList { private set; get; } = new List<RelayServerInfo>();
		List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * L_0 = __this->get_U3CrelayServerListU3Ek__BackingField_28();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15 * NetworkManager_get_singleton_m3687E70BBF51C41E6F20B606EF0E0E40D13E7641_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// public static NetworkManager singleton { get; private set; }
		IL2CPP_RUNTIME_CLASS_INIT(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_il2cpp_TypeInfo_var);
		NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15 * L_0 = ((NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_StaticFields*)il2cpp_codegen_static_fields_for(NetworkManager_t4FCA51C2717E9324BB66A81749DFB3D7EBFBFA15_il2cpp_TypeInfo_var))->get_U3CsingletonU3Ek__BackingField_25();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  List_1_get_Item_mCF8188FFB57EE709BF3711CCAAC72E45D437B280_gshared_inline (List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492* L_2 = (RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD  L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((RelayServerInfoU5BU5D_t6B810067E4C7A718D4C47F7B4A30130640B5E492*)L_2, (int32_t)L_3);
		return (RelayServerInfo_tEB9F964E045263C9A9BDA74E07FE8B1D1F9289AD )L_4;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t List_1_get_Count_mD2AD27EADD5B52963866260417280CEAD554A5B2_gshared_inline (List_1_tC1CBA5BB76243DA5EA2C3E79A31A043EA693A497 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_2();
		return (int32_t)L_0;
	}
}
