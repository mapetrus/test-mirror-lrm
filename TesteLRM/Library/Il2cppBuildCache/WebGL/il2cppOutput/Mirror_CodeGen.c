﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.CodeAnalysis.EmbeddedAttribute::.ctor()
extern void EmbeddedAttribute__ctor_mBAD8A67721F7598BE1151A9142ACE8229337284E (void);
// 0x00000002 System.Void System.Runtime.CompilerServices.IsReadOnlyAttribute::.ctor()
extern void IsReadOnlyAttribute__ctor_m2EA1DDCC3375C83D2922C167CD0BA462DA85B421 (void);
// 0x00000003 System.Void HelpAttribute::.ctor(System.String,MessageType)
extern void HelpAttribute__ctor_m63346A32916D628231B0C7F7A414557F38012EE0 (void);
// 0x00000004 System.Void LRMDirectConnectModule::Awake()
extern void LRMDirectConnectModule_Awake_mEC4458D15832F875FAFE029FC064855739FB9535 (void);
// 0x00000005 System.Void LRMDirectConnectModule::StartServer(System.Int32)
extern void LRMDirectConnectModule_StartServer_m546F44C56D6F7EF5224AE478A0D2FDDE004B7D0D (void);
// 0x00000006 System.Void LRMDirectConnectModule::StopServer()
extern void LRMDirectConnectModule_StopServer_mDAFAB2CC6E40F30CD73284F4F1F7028E63A01E22 (void);
// 0x00000007 System.Void LRMDirectConnectModule::JoinServer(System.String,System.Int32)
extern void LRMDirectConnectModule_JoinServer_m01BB689C9D3F72AB79B90BF262E1F233FC773E61 (void);
// 0x00000008 System.Void LRMDirectConnectModule::SetTransportPort(System.Int32)
extern void LRMDirectConnectModule_SetTransportPort_mE74ECF7015E531FB4177DFCC9642ACDEE861CEC7 (void);
// 0x00000009 System.Int32 LRMDirectConnectModule::GetTransportPort()
extern void LRMDirectConnectModule_GetTransportPort_m8F30CFB6FC3D84D45267C9220E0D96EEB1835AF8 (void);
// 0x0000000A System.Boolean LRMDirectConnectModule::SupportsNATPunch()
extern void LRMDirectConnectModule_SupportsNATPunch_m8789E7511768E43970EB97781351147A822A5A15 (void);
// 0x0000000B System.Boolean LRMDirectConnectModule::KickClient(System.Int32)
extern void LRMDirectConnectModule_KickClient_mCA064B64D2D4A5D11398F3A9EB470785C9FAE206 (void);
// 0x0000000C System.Void LRMDirectConnectModule::ClientDisconnect()
extern void LRMDirectConnectModule_ClientDisconnect_mCCC633387389458A30BFB3E7241FC434210D3F40 (void);
// 0x0000000D System.Void LRMDirectConnectModule::ServerSend(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void LRMDirectConnectModule_ServerSend_mEBD4B72ED1D613F094FB86DA28D3D48D4C5AA40B (void);
// 0x0000000E System.Void LRMDirectConnectModule::ClientSend(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LRMDirectConnectModule_ClientSend_mBADEF5ACE416C5F3AAE7B377DF89C32AE894CB7C (void);
// 0x0000000F System.Void LRMDirectConnectModule::OnServerConnected(System.Int32)
extern void LRMDirectConnectModule_OnServerConnected_m4CEE888E150C215DF56CA7426FD82229781A1FA3 (void);
// 0x00000010 System.Void LRMDirectConnectModule::OnServerDataReceived(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void LRMDirectConnectModule_OnServerDataReceived_m55FF68D55DFA42E8BD75937FAB854953ABF7B56D (void);
// 0x00000011 System.Void LRMDirectConnectModule::OnServerDisconnected(System.Int32)
extern void LRMDirectConnectModule_OnServerDisconnected_mDE779FE03224232B24EBAD2D3611BE25A2399EDD (void);
// 0x00000012 System.Void LRMDirectConnectModule::OnServerError(System.Int32,System.Exception)
extern void LRMDirectConnectModule_OnServerError_mC6C29AED15ED6A386C58AF9851AE76391BDAE26F (void);
// 0x00000013 System.Void LRMDirectConnectModule::OnClientConnected()
extern void LRMDirectConnectModule_OnClientConnected_mA3201A9FAB69D57F1346DAE49A82CF794A78F5FB (void);
// 0x00000014 System.Void LRMDirectConnectModule::OnClientDisconnected()
extern void LRMDirectConnectModule_OnClientDisconnected_mA4560F887041E5F2A4EE87DD8378F1EAA302F0AB (void);
// 0x00000015 System.Void LRMDirectConnectModule::OnClientDataReceived(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LRMDirectConnectModule_OnClientDataReceived_m5C24E9CADE03E9941A3805A0942B1205A5F520E8 (void);
// 0x00000016 System.Void LRMDirectConnectModule::OnClientError(System.Exception)
extern void LRMDirectConnectModule_OnClientError_mC32910382ADC51E2A3C6330460F2BA4C11869582 (void);
// 0x00000017 System.Void LRMDirectConnectModule::.ctor()
extern void LRMDirectConnectModule__ctor_mF00514B8F505588DB473BAC7124AEDEE67850518 (void);
// 0x00000018 System.Void LightReflectiveMirror.BiDictionary`2::Add(TFirst,TSecond)
// 0x00000019 System.Boolean LightReflectiveMirror.BiDictionary`2::TryGetByFirst(TFirst,TSecond&)
// 0x0000001A System.Void LightReflectiveMirror.BiDictionary`2::Remove(TFirst)
// 0x0000001B System.Collections.Generic.ICollection`1<TFirst> LightReflectiveMirror.BiDictionary`2::GetAllKeys()
// 0x0000001C System.Boolean LightReflectiveMirror.BiDictionary`2::TryGetBySecond(TSecond,TFirst&)
// 0x0000001D TSecond LightReflectiveMirror.BiDictionary`2::GetByFirst(TFirst)
// 0x0000001E TFirst LightReflectiveMirror.BiDictionary`2::GetBySecond(TSecond)
// 0x0000001F System.Void LightReflectiveMirror.BiDictionary`2::.ctor()
// 0x00000020 System.Void LightReflectiveMirror.LRMTools::WriteByte(System.Byte[],System.Int32&,System.Byte)
extern void LRMTools_WriteByte_m46932D1564BBD00029DA6A3F0DC7B6DC07061307 (void);
// 0x00000021 System.Byte LightReflectiveMirror.LRMTools::ReadByte(System.Byte[],System.Int32&)
extern void LRMTools_ReadByte_m4CF4175B9617DA677BAE555C76807B992E9B5B7E (void);
// 0x00000022 System.Void LightReflectiveMirror.LRMTools::WriteBool(System.Byte[],System.Int32&,System.Boolean)
extern void LRMTools_WriteBool_m9681206EA15FF3F7D26B4ED2E1461E786F0C4C7F (void);
// 0x00000023 System.Boolean LightReflectiveMirror.LRMTools::ReadBool(System.Byte[],System.Int32&)
extern void LRMTools_ReadBool_m23C9E333CC44B6120BE7FDB7CA3898313F913021 (void);
// 0x00000024 System.Void LightReflectiveMirror.LRMTools::WriteString(System.Byte[],System.Int32&,System.String)
extern void LRMTools_WriteString_m670EAF5DE20387F8917BD9B73302BDAE51D356A4 (void);
// 0x00000025 System.String LightReflectiveMirror.LRMTools::ReadString(System.Byte[],System.Int32&)
extern void LRMTools_ReadString_mF27A9D875373AA7FB836D41568ACE9B4F778272A (void);
// 0x00000026 System.Void LightReflectiveMirror.LRMTools::WriteBytes(System.Byte[],System.Int32&,System.Byte[])
extern void LRMTools_WriteBytes_mD65C540FF51B3D7D27BBA01B66927BF0F77F7AE3 (void);
// 0x00000027 System.Byte[] LightReflectiveMirror.LRMTools::ReadBytes(System.Byte[],System.Int32&)
extern void LRMTools_ReadBytes_m980980D75AA20094E855A3F2489C00A5C6D635DF (void);
// 0x00000028 System.Void LightReflectiveMirror.LRMTools::WriteChar(System.Byte[],System.Int32&,System.Char)
extern void LRMTools_WriteChar_mD44F266656465556B9D8A545527B85A452E1D99D (void);
// 0x00000029 System.Char LightReflectiveMirror.LRMTools::ReadChar(System.Byte[],System.Int32&)
extern void LRMTools_ReadChar_m519D7FA943126A3E42E17F62AC045D60C3F909FB (void);
// 0x0000002A System.Void LightReflectiveMirror.LRMTools::WriteInt(System.Byte[],System.Int32&,System.Int32)
extern void LRMTools_WriteInt_m61658435FBD3024FD1919E948AFEA66DF95FB553 (void);
// 0x0000002B System.Int32 LightReflectiveMirror.LRMTools::ReadInt(System.Byte[],System.Int32&)
extern void LRMTools_ReadInt_m640B1BD8E7E1A324D6E4FD99363BFE8689FFB458 (void);
// 0x0000002C System.String LightReflectiveMirror.CompressorExtensions::Decompress(System.String)
extern void CompressorExtensions_Decompress_mABE9D3BDB437B0B11F92BD054FA57F58BD26FFEB (void);
// 0x0000002D System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::set_relayServerList(System.Collections.Generic.List`1<LightReflectiveMirror.RelayServerInfo>)
extern void LightReflectiveMirrorTransport_set_relayServerList_m864AAF361A722A5FAA238BE1C6DA01F38224AD00 (void);
// 0x0000002E System.Collections.Generic.List`1<LightReflectiveMirror.RelayServerInfo> LightReflectiveMirror.LightReflectiveMirrorTransport::get_relayServerList()
extern void LightReflectiveMirrorTransport_get_relayServerList_mC55A665EF2A23B7AEA6104117D9E002510ED1C12 (void);
// 0x0000002F System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::ClientConnected()
extern void LightReflectiveMirrorTransport_ClientConnected_m417C77CBD1763FF00BB0C53118AF78B5F997F3CB (void);
// 0x00000030 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::OnConnectedToRelay()
extern void LightReflectiveMirrorTransport_OnConnectedToRelay_mC85C728706F33FB359A8C1A0BC77F287C569FA78 (void);
// 0x00000031 System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::IsAuthenticated()
extern void LightReflectiveMirrorTransport_IsAuthenticated_mCE1748BE7D14614B57960E53E3D7E2FDCD5D9DC9 (void);
// 0x00000032 System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::ServerActive()
extern void LightReflectiveMirrorTransport_ServerActive_mA8602CB7D1E57D1A6E83CF6E52A0A4AAF0AFAEAE (void);
// 0x00000033 System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::Available()
extern void LightReflectiveMirrorTransport_Available_m195D962667B9760CC36E2676140969810A358C06 (void);
// 0x00000034 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ClientConnect(System.Uri)
extern void LightReflectiveMirrorTransport_ClientConnect_m32A35C87C480F1828648AA4EC981A2E914B7670E (void);
// 0x00000035 System.Int32 LightReflectiveMirror.LightReflectiveMirrorTransport::GetMaxPacketSize(System.Int32)
extern void LightReflectiveMirrorTransport_GetMaxPacketSize_mDC3F1A4273C1C4C69649F3E54E3F15B7DAD09EEF (void);
// 0x00000036 System.String LightReflectiveMirror.LightReflectiveMirrorTransport::ServerGetClientAddress(System.Int32)
extern void LightReflectiveMirrorTransport_ServerGetClientAddress_m2EF195FD89F310B3ADE080499874E5D8880CFAE2 (void);
// 0x00000037 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ClientEarlyUpdate()
extern void LightReflectiveMirrorTransport_ClientEarlyUpdate_m24057440EEC5DE42DF48E1F1CAF5AB67746C75C1 (void);
// 0x00000038 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ClientLateUpdate()
extern void LightReflectiveMirrorTransport_ClientLateUpdate_mBFFD9F2AF42685FBAB7632D5D6084DB807A20866 (void);
// 0x00000039 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ServerEarlyUpdate()
extern void LightReflectiveMirrorTransport_ServerEarlyUpdate_m28AC3BC848D37E82FD795994E3211C77F164E049 (void);
// 0x0000003A System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::RecvData(System.IAsyncResult)
extern void LightReflectiveMirrorTransport_RecvData_m60237396B44A7433664C4786C8644D800BC987C9 (void);
// 0x0000003B System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ServerProcessProxyData(System.Net.IPEndPoint,System.Byte[])
extern void LightReflectiveMirrorTransport_ServerProcessProxyData_m7815E3A19FF1C7078B3D2299F449BB37BD5C0490 (void);
// 0x0000003C System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ClientProcessProxyData(System.Net.IPEndPoint,System.Byte[])
extern void LightReflectiveMirrorTransport_ClientProcessProxyData_mC11BD7F61EC9994D18A46AA6C51BC557102EA929 (void);
// 0x0000003D System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ServerLateUpdate()
extern void LightReflectiveMirrorTransport_ServerLateUpdate_m13E5B4F9E1E8DFBAB7D43A8B7F581E431AA4366B (void);
// 0x0000003E System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::Awake()
extern void LightReflectiveMirrorTransport_Awake_m135B27F6A5B6C429D8AF5EF1E50289FB2606D941 (void);
// 0x0000003F System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::SetupCallbacks()
extern void LightReflectiveMirrorTransport_SetupCallbacks_m3B5035292970239A80E9FF178AD937D5AC36D07F (void);
// 0x00000040 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::Disconnected()
extern void LightReflectiveMirrorTransport_Disconnected_m5557C59387B692554FD445860800284B32C01C7F (void);
// 0x00000041 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ConnectToRelay()
extern void LightReflectiveMirrorTransport_ConnectToRelay_m218751FC55C78E57A8320BAA718F7B1666F81A9A (void);
// 0x00000042 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::SendHeartbeat()
extern void LightReflectiveMirrorTransport_SendHeartbeat_m787EB035A673E35BE7B1A87DECDB901B10588EE6 (void);
// 0x00000043 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::RequestServerList()
extern void LightReflectiveMirrorTransport_RequestServerList_m97128167F0BA44C83429C4DCC4197703F1D58A46 (void);
// 0x00000044 System.Collections.IEnumerator LightReflectiveMirror.LightReflectiveMirrorTransport::NATPunch(System.Net.IPEndPoint)
extern void LightReflectiveMirrorTransport_NATPunch_m262DFE42E3FA2E16A28CC594EEED355092BBEB25 (void);
// 0x00000045 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::DataReceived(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LightReflectiveMirrorTransport_DataReceived_m76C79362AE4F92138A231FFE3E3CFAE9B8AF7914 (void);
// 0x00000046 System.Collections.IEnumerator LightReflectiveMirror.LightReflectiveMirrorTransport::GetServerList()
extern void LightReflectiveMirrorTransport_GetServerList_m23FA6123771FB2F1573D3E7191DC7488AF648DF7 (void);
// 0x00000047 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::UpdateRoomInfo(System.String,System.String,System.Nullable`1<System.Boolean>,System.Nullable`1<System.Int32>)
extern void LightReflectiveMirrorTransport_UpdateRoomInfo_mD3DE38A013C8171A229930BF117559687FBEBC6A (void);
// 0x00000048 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::SendAuthKey()
extern void LightReflectiveMirrorTransport_SendAuthKey_mBE4CDC5040585E06614E028FC8DE5107280F3DC6 (void);
// 0x00000049 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ClientConnect(System.String)
extern void LightReflectiveMirrorTransport_ClientConnect_m0B76174186B97A4370724519FDD1975B3359779A (void);
// 0x0000004A System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ClientDisconnect()
extern void LightReflectiveMirrorTransport_ClientDisconnect_mE564E5030FBA427C4A3CF5639476E525A23C29AD (void);
// 0x0000004B System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ClientSend(System.Int32,System.ArraySegment`1<System.Byte>)
extern void LightReflectiveMirrorTransport_ClientSend_mFF858CCD0DBECAB115086367C625B11475EB673B (void);
// 0x0000004C System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport::ServerDisconnect(System.Int32)
extern void LightReflectiveMirrorTransport_ServerDisconnect_m4A6C1C0A484C6E6020870B738655E30BFD5314AE (void);
// 0x0000004D System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ServerSend(System.Int32,System.Int32,System.ArraySegment`1<System.Byte>)
extern void LightReflectiveMirrorTransport_ServerSend_m95119F0BECC59C1A7A642E6B78C86CDFD66DE954 (void);
// 0x0000004E System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ServerStart()
extern void LightReflectiveMirrorTransport_ServerStart_m22919D8DA904D7598FEA55516CBD6579950EEC3A (void);
// 0x0000004F System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::ServerStop()
extern void LightReflectiveMirrorTransport_ServerStop_mBE2F5610041D3BD91E23B11E53250036ECEFC4DF (void);
// 0x00000050 System.Uri LightReflectiveMirror.LightReflectiveMirrorTransport::ServerUri()
extern void LightReflectiveMirrorTransport_ServerUri_m4B53BE39251E16F005D57D087F3E179C8B78A349 (void);
// 0x00000051 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::Shutdown()
extern void LightReflectiveMirrorTransport_Shutdown_m62B44DA6A3419EBD6A2B436DEB6A9160FEB750F3 (void);
// 0x00000052 System.String LightReflectiveMirror.LightReflectiveMirrorTransport::GetLocalIp()
extern void LightReflectiveMirrorTransport_GetLocalIp_mBD8393477D0C387F14DF44483A8A1C85A78BFCE7 (void);
// 0x00000053 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::DirectAddClient(System.Int32)
extern void LightReflectiveMirrorTransport_DirectAddClient_mA8765DD930FB2E2AF31ADC909E564AA203972478 (void);
// 0x00000054 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::DirectRemoveClient(System.Int32)
extern void LightReflectiveMirrorTransport_DirectRemoveClient_m02BD751B1D2AD6B2BE2ABBE41CF27B45E49EB9F9 (void);
// 0x00000055 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::DirectReceiveData(System.ArraySegment`1<System.Byte>,System.Int32,System.Int32)
extern void LightReflectiveMirrorTransport_DirectReceiveData_m895AFBD7DEE630965609016AF7F2E17E2D1BF44F (void);
// 0x00000056 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::DirectClientConnected()
extern void LightReflectiveMirrorTransport_DirectClientConnected_mBB3350A5BD5566BC34EA33263202D965DDF6109C (void);
// 0x00000057 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::DirectDisconnected()
extern void LightReflectiveMirrorTransport_DirectDisconnected_m18F01A13BF74EC522C56B9642E3041529F5C1563 (void);
// 0x00000058 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport::.ctor()
extern void LightReflectiveMirrorTransport__ctor_m69FB2137F359F74F3AC3694FAAAF46C188E09892 (void);
// 0x00000059 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport/<NATPunch>d__60::.ctor(System.Int32)
extern void U3CNATPunchU3Ed__60__ctor_m8AFFC096C0D5CB45DB1ADD9963088414BED799A3 (void);
// 0x0000005A System.Void LightReflectiveMirror.LightReflectiveMirrorTransport/<NATPunch>d__60::System.IDisposable.Dispose()
extern void U3CNATPunchU3Ed__60_System_IDisposable_Dispose_m24B6085FE737785C5AA79962EE4C7430ADCFAE59 (void);
// 0x0000005B System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport/<NATPunch>d__60::MoveNext()
extern void U3CNATPunchU3Ed__60_MoveNext_m938D530EE227ACC4CF6752F2C6B3529D05784548 (void);
// 0x0000005C System.Object LightReflectiveMirror.LightReflectiveMirrorTransport/<NATPunch>d__60::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNATPunchU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF552C1CA316A3847A928D558B9FA7C9132F77719 (void);
// 0x0000005D System.Void LightReflectiveMirror.LightReflectiveMirrorTransport/<NATPunch>d__60::System.Collections.IEnumerator.Reset()
extern void U3CNATPunchU3Ed__60_System_Collections_IEnumerator_Reset_mF9BB6188618B8206B2210F35BE183A7B269DC967 (void);
// 0x0000005E System.Object LightReflectiveMirror.LightReflectiveMirrorTransport/<NATPunch>d__60::System.Collections.IEnumerator.get_Current()
extern void U3CNATPunchU3Ed__60_System_Collections_IEnumerator_get_Current_m48C8FA01805E9BDE54DD0BE71475F963E37BBD66 (void);
// 0x0000005F System.Void LightReflectiveMirror.LightReflectiveMirrorTransport/<GetServerList>d__62::.ctor(System.Int32)
extern void U3CGetServerListU3Ed__62__ctor_mF2829A884C695D524F5DEFD794A050B9D5287B51 (void);
// 0x00000060 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport/<GetServerList>d__62::System.IDisposable.Dispose()
extern void U3CGetServerListU3Ed__62_System_IDisposable_Dispose_m1E30448DCC8FF3CDC1A67E590C43DA3C875F54C9 (void);
// 0x00000061 System.Boolean LightReflectiveMirror.LightReflectiveMirrorTransport/<GetServerList>d__62::MoveNext()
extern void U3CGetServerListU3Ed__62_MoveNext_mEC4597B38482B5A0BE251FE286B903BA09516FB9 (void);
// 0x00000062 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport/<GetServerList>d__62::<>m__Finally1()
extern void U3CGetServerListU3Ed__62_U3CU3Em__Finally1_m5AC54002B1028E121C7CCB1716CECA48FD620E3C (void);
// 0x00000063 System.Object LightReflectiveMirror.LightReflectiveMirrorTransport/<GetServerList>d__62::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetServerListU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07BAE2737A26A9B0EF500D9B43C6B2599166386C (void);
// 0x00000064 System.Void LightReflectiveMirror.LightReflectiveMirrorTransport/<GetServerList>d__62::System.Collections.IEnumerator.Reset()
extern void U3CGetServerListU3Ed__62_System_Collections_IEnumerator_Reset_m41CD9566134AA4481D77F62703EBD7D48CBFAF2F (void);
// 0x00000065 System.Object LightReflectiveMirror.LightReflectiveMirrorTransport/<GetServerList>d__62::System.Collections.IEnumerator.get_Current()
extern void U3CGetServerListU3Ed__62_System_Collections_IEnumerator_get_Current_mDC593EA4675E9C4334BCC34C0BBD7812DA961CBD (void);
// 0x00000066 System.Void LightReflectiveMirror.SocketProxy::.ctor(System.Int32,System.Net.IPEndPoint)
extern void SocketProxy__ctor_m5AFC2469A9389CB8736A4490A8C54D42BCA39563 (void);
// 0x00000067 System.Void LightReflectiveMirror.SocketProxy::.ctor(System.Int32)
extern void SocketProxy__ctor_m9A2BE733F6825F86A883D572ECE79CBE6BC49144 (void);
// 0x00000068 System.Void LightReflectiveMirror.SocketProxy::RelayData(System.Byte[],System.Int32)
extern void SocketProxy_RelayData_mFD76CE0CF373054899441994B01C15BFD4342761 (void);
// 0x00000069 System.Void LightReflectiveMirror.SocketProxy::ClientRelayData(System.Byte[],System.Int32)
extern void SocketProxy_ClientRelayData_m2ACCB759BE9FF926CAAF282017BBFA62E32FDDCF (void);
// 0x0000006A System.Void LightReflectiveMirror.SocketProxy::Dispose()
extern void SocketProxy_Dispose_mF8A1212804F0FF230BC2D9B1FF02295189C759A5 (void);
// 0x0000006B System.Void LightReflectiveMirror.SocketProxy::RecvData(System.IAsyncResult)
extern void SocketProxy_RecvData_mA02E281CDE6D06A99BF178F0D474832C7946EA22 (void);
// 0x0000006C System.Void kcp2k.KcpTransport::Awake()
extern void KcpTransport_Awake_m62B40AF0A488B3ECE15089856A16BE4623A51BEE (void);
// 0x0000006D System.Boolean kcp2k.KcpTransport::Available()
extern void KcpTransport_Available_mC28A19601D78A80E81F13231A059929D2E118C54 (void);
// 0x0000006E System.Boolean kcp2k.KcpTransport::ClientConnected()
extern void KcpTransport_ClientConnected_m4370351A8E8B89BABF8AC5BF2423F05BEC959358 (void);
// 0x0000006F System.Void kcp2k.KcpTransport::ClientConnect(System.String)
extern void KcpTransport_ClientConnect_mDB9736490D95A3F288721B88A33DC79FCEFB98DD (void);
// 0x00000070 System.Void kcp2k.KcpTransport::ClientSend(System.Int32,System.ArraySegment`1<System.Byte>)
extern void KcpTransport_ClientSend_mB01F20BA3B12A84B379AD1C9E2630E8C902E1F03 (void);
// 0x00000071 System.Void kcp2k.KcpTransport::ClientDisconnect()
extern void KcpTransport_ClientDisconnect_mADDC6439D972EC6CA801E57F55FE211B755093B1 (void);
// 0x00000072 System.Void kcp2k.KcpTransport::ClientEarlyUpdate()
extern void KcpTransport_ClientEarlyUpdate_m4AE48F297CFB0B6ED62E6C89FF7AD5B5BE04B1CF (void);
// 0x00000073 System.Void kcp2k.KcpTransport::ClientLateUpdate()
extern void KcpTransport_ClientLateUpdate_mFCA6124C57C7836AE664951F986CCB11F3206D75 (void);
// 0x00000074 System.Void kcp2k.KcpTransport::OnEnable()
extern void KcpTransport_OnEnable_m41EBE345D83D2C022290C3ED0027822F97CA7AC2 (void);
// 0x00000075 System.Void kcp2k.KcpTransport::OnDisable()
extern void KcpTransport_OnDisable_m8E7252C83ABAC0CBDC39BB4D95C7A9891AE7A05E (void);
// 0x00000076 System.Uri kcp2k.KcpTransport::ServerUri()
extern void KcpTransport_ServerUri_mB32E80AE3973B31A562FA9BE8954A9EC833B3E2D (void);
// 0x00000077 System.Boolean kcp2k.KcpTransport::ServerActive()
extern void KcpTransport_ServerActive_mFFF858F3F362B80BFBB8DA38870901126AD33D0F (void);
// 0x00000078 System.Void kcp2k.KcpTransport::ServerStart()
extern void KcpTransport_ServerStart_mB19F14CED645C0AF8BEE683B2911105972CB4B17 (void);
// 0x00000079 System.Void kcp2k.KcpTransport::ServerSend(System.Int32,System.Int32,System.ArraySegment`1<System.Byte>)
extern void KcpTransport_ServerSend_mD2BF36CDE8B85CF99D1C4E149FCFC53E18288968 (void);
// 0x0000007A System.Boolean kcp2k.KcpTransport::ServerDisconnect(System.Int32)
extern void KcpTransport_ServerDisconnect_m3A57930E65019F03CD4E60CA12D37F32F9465A74 (void);
// 0x0000007B System.String kcp2k.KcpTransport::ServerGetClientAddress(System.Int32)
extern void KcpTransport_ServerGetClientAddress_m0DC394B917B355C3FD4798EE0C0022B438188BCA (void);
// 0x0000007C System.Void kcp2k.KcpTransport::ServerStop()
extern void KcpTransport_ServerStop_m3EE5CD2DE9953E2EFE10720382DC9D5DC4F13E3C (void);
// 0x0000007D System.Void kcp2k.KcpTransport::ServerEarlyUpdate()
extern void KcpTransport_ServerEarlyUpdate_m79B9CC4F151CD8FCE544D2A8F549B54A049BCEEF (void);
// 0x0000007E System.Void kcp2k.KcpTransport::ServerLateUpdate()
extern void KcpTransport_ServerLateUpdate_m06B80D50F2CC1A33AC95FAE891E4E95F5B2DF97B (void);
// 0x0000007F System.Void kcp2k.KcpTransport::Shutdown()
extern void KcpTransport_Shutdown_m6CF63A2AC8B565B3411F462352420A5427A0A3BC (void);
// 0x00000080 System.Int32 kcp2k.KcpTransport::GetMaxPacketSize(System.Int32)
extern void KcpTransport_GetMaxPacketSize_mEB9B1E5DE977F2A6225576122ECE6C505B8DC257 (void);
// 0x00000081 System.Int32 kcp2k.KcpTransport::GetMaxBatchSize(System.Int32)
extern void KcpTransport_GetMaxBatchSize_m22EDA55F53E193054768C72C67DBF68FB91F1E0A (void);
// 0x00000082 System.Int32 kcp2k.KcpTransport::GetAverageMaxSendRate()
extern void KcpTransport_GetAverageMaxSendRate_mA9F73B527E675130B8EB848A0F5A760F401108E1 (void);
// 0x00000083 System.Int32 kcp2k.KcpTransport::GetAverageMaxReceiveRate()
extern void KcpTransport_GetAverageMaxReceiveRate_mFFB2312D0735C076527DAB81D7A3D033FD1BA2C6 (void);
// 0x00000084 System.Int32 kcp2k.KcpTransport::GetTotalSendQueue()
extern void KcpTransport_GetTotalSendQueue_m53E4091F9E3B93F287C7672C3210C33A1826B57A (void);
// 0x00000085 System.Int32 kcp2k.KcpTransport::GetTotalReceiveQueue()
extern void KcpTransport_GetTotalReceiveQueue_m0ECBEBD26783AEB47B7CD98CDDDD0AEC4F3F5E75 (void);
// 0x00000086 System.Int32 kcp2k.KcpTransport::GetTotalSendBuffer()
extern void KcpTransport_GetTotalSendBuffer_m62E270E288AEE80D94074FAE809462D4E4EA3EDF (void);
// 0x00000087 System.Int32 kcp2k.KcpTransport::GetTotalReceiveBuffer()
extern void KcpTransport_GetTotalReceiveBuffer_m21FE207058439DCAE1BDE0AF7BEC22C098C2025C (void);
// 0x00000088 System.String kcp2k.KcpTransport::PrettyBytes(System.Int64)
extern void KcpTransport_PrettyBytes_m60CBBB4165C1D52DD392C9D6E4F7359F28345CA4 (void);
// 0x00000089 System.Void kcp2k.KcpTransport::OnGUI()
extern void KcpTransport_OnGUI_m70129E74516C2A543EA6EE0C11023677CC8BD2EA (void);
// 0x0000008A System.Void kcp2k.KcpTransport::OnLogStatistics()
extern void KcpTransport_OnLogStatistics_m6251A4F09FBD2348C24444E46A89E385F32D6693 (void);
// 0x0000008B System.String kcp2k.KcpTransport::ToString()
extern void KcpTransport_ToString_mAE9B5329A0035FC14BD3ECB6BFAA3E3B9C0911E5 (void);
// 0x0000008C System.Void kcp2k.KcpTransport::.ctor()
extern void KcpTransport__ctor_m0CF4CC09A52CDE5F1CB9A576C0650B0FA7A21075 (void);
// 0x0000008D System.Void kcp2k.KcpTransport::<Awake>b__13_1()
extern void KcpTransport_U3CAwakeU3Eb__13_1_mBA80BFEA53D389F1C6CD1A9DA97012FD408E2A8E (void);
// 0x0000008E System.Void kcp2k.KcpTransport::<Awake>b__13_2(System.ArraySegment`1<System.Byte>)
extern void KcpTransport_U3CAwakeU3Eb__13_2_m0F9E548314A3B634B62487A2562C810B229D4C40 (void);
// 0x0000008F System.Void kcp2k.KcpTransport::<Awake>b__13_3()
extern void KcpTransport_U3CAwakeU3Eb__13_3_mB5E4C4292B31BED5A89C5A9C2854D9EBEF3454B0 (void);
// 0x00000090 System.Void kcp2k.KcpTransport::<Awake>b__13_4(System.Int32)
extern void KcpTransport_U3CAwakeU3Eb__13_4_m59DD0FFE7DA7A5830C6EF59F4394C18BC0BD1754 (void);
// 0x00000091 System.Void kcp2k.KcpTransport::<Awake>b__13_5(System.Int32,System.ArraySegment`1<System.Byte>)
extern void KcpTransport_U3CAwakeU3Eb__13_5_mE5452CB5EC509ACA8CE7AAC4CBD5BC849CEC8B03 (void);
// 0x00000092 System.Void kcp2k.KcpTransport::<Awake>b__13_6(System.Int32)
extern void KcpTransport_U3CAwakeU3Eb__13_6_mA75EC14D01A4EAD0B4FA2FAD0DD5DF09D76E7359 (void);
// 0x00000093 System.Void kcp2k.KcpTransport/<>c::.cctor()
extern void U3CU3Ec__cctor_m74DDBDACB2E47803B0ECB09111C9BCD8E1FBC2FF (void);
// 0x00000094 System.Void kcp2k.KcpTransport/<>c::.ctor()
extern void U3CU3Ec__ctor_m198B95BF0E13C9FF9E4CF6A86E7F21801E5234E0 (void);
// 0x00000095 System.Void kcp2k.KcpTransport/<>c::<Awake>b__13_0(System.String)
extern void U3CU3Ec_U3CAwakeU3Eb__13_0_m734C59CAF6A3A5F86C7C74B6D0B502FE1660C9E9 (void);
// 0x00000096 System.Int32 kcp2k.KcpTransport/<>c::<GetAverageMaxSendRate>b__35_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetAverageMaxSendRateU3Eb__35_0_m4FA0E31E7D8E09D87780AE77B6ADBF3B4C67F39C (void);
// 0x00000097 System.Int32 kcp2k.KcpTransport/<>c::<GetAverageMaxReceiveRate>b__36_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetAverageMaxReceiveRateU3Eb__36_0_m35CC40E5FEEBD969C13D40C1D310FC9ECAB1DBA3 (void);
// 0x00000098 System.Int32 kcp2k.KcpTransport/<>c::<GetTotalSendQueue>b__37_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalSendQueueU3Eb__37_0_mA750649976DD14B26E587CF216B33FF05773C12C (void);
// 0x00000099 System.Int32 kcp2k.KcpTransport/<>c::<GetTotalReceiveQueue>b__38_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalReceiveQueueU3Eb__38_0_m2CC37246A2B07544CE74082F7DFD5B4B68663AB9 (void);
// 0x0000009A System.Int32 kcp2k.KcpTransport/<>c::<GetTotalSendBuffer>b__39_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalSendBufferU3Eb__39_0_m86D09D49E1E98A625F5CB78DF23CED311719A71A (void);
// 0x0000009B System.Int32 kcp2k.KcpTransport/<>c::<GetTotalReceiveBuffer>b__40_0(kcp2k.KcpServerConnection)
extern void U3CU3Ec_U3CGetTotalReceiveBufferU3Eb__40_0_m7028B49A8E209110599965F22C9E24A4148922DB (void);
// 0x0000009C System.Void Mirror.SyncVarAttribute::.ctor()
extern void SyncVarAttribute__ctor_m4E99C2B45DDA311F1D9B684B967978541348C875 (void);
// 0x0000009D System.Void Mirror.CommandAttribute::.ctor()
extern void CommandAttribute__ctor_m583940E69A6305050FEF54E57CBAF339A8E77FFC (void);
// 0x0000009E System.Void Mirror.ClientRpcAttribute::.ctor()
extern void ClientRpcAttribute__ctor_mCE8A483D94F06D1FA29EA2342BE860018B29BCA7 (void);
// 0x0000009F System.Void Mirror.TargetRpcAttribute::.ctor()
extern void TargetRpcAttribute__ctor_m5954E4349796FC0A7AA8BE4857791C38E867CA8C (void);
// 0x000000A0 System.Void Mirror.ServerAttribute::.ctor()
extern void ServerAttribute__ctor_m413F135143ED76C9B8C3F41B2DB6597E4382E0A6 (void);
// 0x000000A1 System.Void Mirror.ServerCallbackAttribute::.ctor()
extern void ServerCallbackAttribute__ctor_m7258C253CBA5BF5C92BAAB6345240F9418C0B61D (void);
// 0x000000A2 System.Void Mirror.ClientAttribute::.ctor()
extern void ClientAttribute__ctor_mD3E93370918D30F063F1CBCA1CF1BC46B90D9065 (void);
// 0x000000A3 System.Void Mirror.ClientCallbackAttribute::.ctor()
extern void ClientCallbackAttribute__ctor_m1CCA6B90CA8E84DD7A6C6AC9C862434AA9560703 (void);
// 0x000000A4 System.Void Mirror.SceneAttribute::.ctor()
extern void SceneAttribute__ctor_m7F8095E94F9D74BDB458EE8B5CF10781C8EEEB62 (void);
// 0x000000A5 System.Void Mirror.ShowInInspectorAttribute::.ctor()
extern void ShowInInspectorAttribute__ctor_mFA3EF79FD7E9F573D31F156DE62A6CBDBE254688 (void);
// 0x000000A6 Mirror.NetworkIdentity Mirror.ClientScene::get_localPlayer()
extern void ClientScene_get_localPlayer_m7354A403C57BAD08DDD216140CAD2DADF66E14CB (void);
// 0x000000A7 System.Void Mirror.ClientScene::set_localPlayer(Mirror.NetworkIdentity)
extern void ClientScene_set_localPlayer_m9B10A22E524AB15A6AFA5718F1091D376A4F6601 (void);
// 0x000000A8 System.Boolean Mirror.ClientScene::get_ready()
extern void ClientScene_get_ready_mB54FAEC0572DA5D58EBB567A0EE6859645447D88 (void);
// 0x000000A9 System.Void Mirror.ClientScene::set_ready(System.Boolean)
extern void ClientScene_set_ready_mABEC71FE5F73FC877590968BE73E334AAE3273E5 (void);
// 0x000000AA Mirror.NetworkConnection Mirror.ClientScene::get_readyConnection()
extern void ClientScene_get_readyConnection_m859DA753D266F4E23C94506EA7DC4B1912F9D7B2 (void);
// 0x000000AB System.Void Mirror.ClientScene::set_readyConnection(Mirror.NetworkConnection)
extern void ClientScene_set_readyConnection_m3A7D2DB324742C36362604CE01227FF74F6F1A52 (void);
// 0x000000AC System.Collections.Generic.Dictionary`2<System.Guid,UnityEngine.GameObject> Mirror.ClientScene::get_prefabs()
extern void ClientScene_get_prefabs_mD45B7363513B74457FB9C4F7655871D6B4925581 (void);
// 0x000000AD System.Boolean Mirror.ClientScene::AddPlayer(Mirror.NetworkConnection)
extern void ClientScene_AddPlayer_m717002A8CC2C09CBEC0A1D77CCF3D4FC212D330B (void);
// 0x000000AE System.Boolean Mirror.ClientScene::Ready(Mirror.NetworkConnection)
extern void ClientScene_Ready_m6F20EC8711B95E25AC2CD86A7AAFFACD94B0B0C8 (void);
// 0x000000AF System.Void Mirror.ClientScene::PrepareToSpawnSceneObjects()
extern void ClientScene_PrepareToSpawnSceneObjects_m76376BBBD0D412406A7DE3F5F53D35DC3321E6C2 (void);
// 0x000000B0 System.Boolean Mirror.ClientScene::GetPrefab(System.Guid,UnityEngine.GameObject&)
extern void ClientScene_GetPrefab_m5B13A0AEBB8C211F607E2A30EE6A16EF12A2C2A8 (void);
// 0x000000B1 System.Void Mirror.ClientScene::RegisterPrefab(UnityEngine.GameObject,System.Guid)
extern void ClientScene_RegisterPrefab_m599C53F7DF8D5A96902E837FD34DD50F4ECBCFA0 (void);
// 0x000000B2 System.Void Mirror.ClientScene::RegisterPrefab(UnityEngine.GameObject)
extern void ClientScene_RegisterPrefab_m375B868F146218C25F23599A22850C3720F27A44 (void);
// 0x000000B3 System.Void Mirror.ClientScene::RegisterPrefab(UnityEngine.GameObject,System.Guid,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void ClientScene_RegisterPrefab_m32E3953CA679E0B9A810F9A6705C1C7D05C57088 (void);
// 0x000000B4 System.Void Mirror.ClientScene::RegisterPrefab(UnityEngine.GameObject,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void ClientScene_RegisterPrefab_mD255CBA550EA3CC7C8526D306ED510A27EF05E48 (void);
// 0x000000B5 System.Void Mirror.ClientScene::RegisterPrefab(UnityEngine.GameObject,System.Guid,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void ClientScene_RegisterPrefab_m9EA45290A99BFDA6EA6266E29D82C97ED8B063CC (void);
// 0x000000B6 System.Void Mirror.ClientScene::RegisterPrefab(UnityEngine.GameObject,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void ClientScene_RegisterPrefab_m7697016B21EF3BA13BD038714805EA7603387B7A (void);
// 0x000000B7 System.Void Mirror.ClientScene::UnregisterPrefab(UnityEngine.GameObject)
extern void ClientScene_UnregisterPrefab_mBC420F46C60944C88672D583503567804D99E495 (void);
// 0x000000B8 System.Void Mirror.ClientScene::RegisterSpawnHandler(System.Guid,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void ClientScene_RegisterSpawnHandler_m2A4533EA9EFA2F9DE52E7CB876EB7AF0179E4D01 (void);
// 0x000000B9 System.Void Mirror.ClientScene::RegisterSpawnHandler(System.Guid,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void ClientScene_RegisterSpawnHandler_m8100B832CE32D2E6B95081A47B385F07364DEEBE (void);
// 0x000000BA System.Void Mirror.ClientScene::UnregisterSpawnHandler(System.Guid)
extern void ClientScene_UnregisterSpawnHandler_mE01566E3C9DCA81951D4ABBF4F3721AF206164EE (void);
// 0x000000BB System.Void Mirror.ClientScene::ClearSpawners()
extern void ClientScene_ClearSpawners_mC043809CA24CA4132F2B7D1AC836EAD9F0DD0CCA (void);
// 0x000000BC System.Void Mirror.ClientScene::DestroyAllClientObjects()
extern void ClientScene_DestroyAllClientObjects_m0E9698A38AEA490C1C862BF296F9646502E2A6C0 (void);
// 0x000000BD System.Int32 Mirror.Compression::LargestAbsoluteComponentIndex(UnityEngine.Vector4,System.Single&,UnityEngine.Vector3&)
extern void Compression_LargestAbsoluteComponentIndex_mB176E3E544821911CE8E10C6E766E73063A18F8F (void);
// 0x000000BE System.UInt16 Mirror.Compression::ScaleFloatToUShort(System.Single,System.Single,System.Single,System.UInt16,System.UInt16)
extern void Compression_ScaleFloatToUShort_mAFB60A1D68E5F3B953184D66C1DE4A57E9D21A3F (void);
// 0x000000BF System.Single Mirror.Compression::ScaleUShortToFloat(System.UInt16,System.UInt16,System.UInt16,System.Single,System.Single)
extern void Compression_ScaleUShortToFloat_mAB1609B5E7B4C38EAA298BBF3249C06A272B5865 (void);
// 0x000000C0 System.Single Mirror.Compression::QuaternionElement(UnityEngine.Quaternion,System.Int32)
extern void Compression_QuaternionElement_m60537C4CF1CA1B44907E0F6D49F7A88E6CF4E604 (void);
// 0x000000C1 System.UInt32 Mirror.Compression::CompressQuaternion(UnityEngine.Quaternion)
extern void Compression_CompressQuaternion_mC1DD43E3FA152477293A562CA2713DAC89B89CFE (void);
// 0x000000C2 UnityEngine.Quaternion Mirror.Compression::QuaternionNormalizeSafe(UnityEngine.Quaternion)
extern void Compression_QuaternionNormalizeSafe_mD1517F3E0392C60CCEF02EAC2857A64312F83F09 (void);
// 0x000000C3 UnityEngine.Quaternion Mirror.Compression::DecompressQuaternion(System.UInt32)
extern void Compression_DecompressQuaternion_m235BB50DA4C0504C27545528ED733C1E0CD246EC (void);
// 0x000000C4 System.Double Mirror.ExponentialMovingAverage::get_Value()
extern void ExponentialMovingAverage_get_Value_mB278B5333872C5EAEAF519E1F95BF4B437F1A00E (void);
// 0x000000C5 System.Void Mirror.ExponentialMovingAverage::set_Value(System.Double)
extern void ExponentialMovingAverage_set_Value_m7839F5E48C1B64C159F98A9D14213DD5C390EEA7 (void);
// 0x000000C6 System.Double Mirror.ExponentialMovingAverage::get_Var()
extern void ExponentialMovingAverage_get_Var_m9ED5A56A0D2B778547F20E4D762562F0F927D8BD (void);
// 0x000000C7 System.Void Mirror.ExponentialMovingAverage::set_Var(System.Double)
extern void ExponentialMovingAverage_set_Var_m58941EF0646BC5D3EF2CD0EDCAC7C6D539AF0D54 (void);
// 0x000000C8 System.Void Mirror.ExponentialMovingAverage::.ctor(System.Int32)
extern void ExponentialMovingAverage__ctor_m3EB10AAA23643AF85E68D21E0EEDE15219287268 (void);
// 0x000000C9 System.Void Mirror.ExponentialMovingAverage::Add(System.Double)
extern void ExponentialMovingAverage_Add_mC19B600AC4A4ABB827290B157B0A830103D54F11 (void);
// 0x000000CA System.Int32 Mirror.Extensions::GetStableHashCode(System.String)
extern void Extensions_GetStableHashCode_m0592E21267B4D12BAF6A3124018FFDB85AA1EDB9 (void);
// 0x000000CB System.String Mirror.Extensions::GetMethodName(System.Delegate)
extern void Extensions_GetMethodName_mBD201B7767E472A8E4F2116F18AD20D437783E0F (void);
// 0x000000CC System.Void Mirror.InterestManagement::Awake()
extern void InterestManagement_Awake_m0020EDD8ABA4470A17089BC2A660147FA087F3D9 (void);
// 0x000000CD System.Boolean Mirror.InterestManagement::OnCheckObserver(Mirror.NetworkIdentity,Mirror.NetworkConnection)
// 0x000000CE System.Void Mirror.InterestManagement::OnRebuildObservers(Mirror.NetworkIdentity,System.Collections.Generic.HashSet`1<Mirror.NetworkConnection>,System.Boolean)
// 0x000000CF System.Void Mirror.InterestManagement::RebuildAll()
extern void InterestManagement_RebuildAll_m143CB3DE5E99AF95FA1285E5E2D91E5FE2DEA1AD (void);
// 0x000000D0 System.Void Mirror.InterestManagement::.ctor()
extern void InterestManagement__ctor_mC2CB6AFC383E02ACBABDD728A970658A1C3CCFF4 (void);
// 0x000000D1 System.Void Mirror.LocalConnectionToClient::.ctor()
extern void LocalConnectionToClient__ctor_m428D1E3E51E34E982DF40E0DCE961195AA003337 (void);
// 0x000000D2 System.String Mirror.LocalConnectionToClient::get_address()
extern void LocalConnectionToClient_get_address_m2D7FB00302E2915434C1E5DB9797D4A5A4E33059 (void);
// 0x000000D3 System.Void Mirror.LocalConnectionToClient::Send(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LocalConnectionToClient_Send_m2D2251FEA3914828444ED06315BFBE2643AF019E (void);
// 0x000000D4 System.Boolean Mirror.LocalConnectionToClient::IsAlive(System.Single)
extern void LocalConnectionToClient_IsAlive_m7A9507EB1C35012CFF5DB54FC6B30C58232A73D6 (void);
// 0x000000D5 System.Void Mirror.LocalConnectionToClient::DisconnectInternal()
extern void LocalConnectionToClient_DisconnectInternal_m703C5C7C8D1B2C4C30BF993266497288D47016C2 (void);
// 0x000000D6 System.Void Mirror.LocalConnectionToClient::Disconnect()
extern void LocalConnectionToClient_Disconnect_m62485E44133767987CFE31DC010CE7281AE84A92 (void);
// 0x000000D7 System.String Mirror.LocalConnectionToServer::get_address()
extern void LocalConnectionToServer_get_address_m0C6B7946CCE095F5AD805DBD421620B031AA6953 (void);
// 0x000000D8 System.Void Mirror.LocalConnectionToServer::QueueConnectedEvent()
extern void LocalConnectionToServer_QueueConnectedEvent_m5248D0F3CB749C8DAB1C8D2F8067D0C4291A03BB (void);
// 0x000000D9 System.Void Mirror.LocalConnectionToServer::QueueDisconnectedEvent()
extern void LocalConnectionToServer_QueueDisconnectedEvent_m5199F8C95A1A01F7367E0C1B32B2B8E08D673168 (void);
// 0x000000DA System.Void Mirror.LocalConnectionToServer::Send(System.ArraySegment`1<System.Byte>,System.Int32)
extern void LocalConnectionToServer_Send_m7101806705A74ECC702860F1DA86A8E61A3FCC7E (void);
// 0x000000DB System.Void Mirror.LocalConnectionToServer::Update()
extern void LocalConnectionToServer_Update_mD992865B7AB76B7164B41CE40EB7ABBD978805D8 (void);
// 0x000000DC System.Void Mirror.LocalConnectionToServer::DisconnectInternal()
extern void LocalConnectionToServer_DisconnectInternal_m9F0657A0BEBDCCA743A4F3BBCF6AA4E624A9B89B (void);
// 0x000000DD System.Void Mirror.LocalConnectionToServer::Disconnect()
extern void LocalConnectionToServer_Disconnect_m702169BB2063D9E348421A2EB47E83FF04D31E4D (void);
// 0x000000DE System.Boolean Mirror.LocalConnectionToServer::IsAlive(System.Single)
extern void LocalConnectionToServer_IsAlive_m249A5454FEE2D49C444B5D2F181EBB9E773E906D (void);
// 0x000000DF System.Void Mirror.LocalConnectionToServer::.ctor()
extern void LocalConnectionToServer__ctor_m958A380BF9B110F4AD9C701DE3E61583FE6D58AA (void);
// 0x000000E0 System.Int32 Mirror.MessagePacking::GetId()
// 0x000000E1 System.Void Mirror.MessagePacking::Pack(T,Mirror.NetworkWriter)
// 0x000000E2 System.Boolean Mirror.MessagePacking::Unpack(Mirror.NetworkReader,System.Int32&)
extern void MessagePacking_Unpack_mAC1CB53798EC435BEF84F601D015968F78DA809E (void);
// 0x000000E3 System.Boolean Mirror.MessagePacking::UnpackMessage(Mirror.NetworkReader,System.Int32&)
extern void MessagePacking_UnpackMessage_m6EC1ECC6F23DBC743A41869942E255B6BB3BCB1B (void);
// 0x000000E4 Mirror.NetworkMessageDelegate Mirror.MessagePacking::WrapHandler(System.Action`2<C,T>,System.Boolean)
// 0x000000E5 System.Void Mirror.MessagePacking/<>c__DisplayClass5_0`2::.ctor()
// 0x000000E6 System.Void Mirror.MessagePacking/<>c__DisplayClass5_0`2::<WrapHandler>b__0(Mirror.NetworkConnection,Mirror.NetworkReader,System.Int32)
// 0x000000E7 System.Void Mirror.MessageBase::.ctor()
extern void MessageBase__ctor_mDC9103481D5807FBF080F7D92B051ED5F642D916 (void);
// 0x000000E8 System.Void Mirror.NetworkPingMessage::.ctor(System.Double)
extern void NetworkPingMessage__ctor_mF340573CD5E74C9BECF60F7789C8170559A95C5D (void);
// 0x000000E9 System.Void Mirror.UnityEventNetworkConnection::.ctor()
extern void UnityEventNetworkConnection__ctor_m2E143132E593982075D6CFB3ACE9FC22A2E66F55 (void);
// 0x000000EA System.Void Mirror.NetworkAuthenticator::OnStartServer()
extern void NetworkAuthenticator_OnStartServer_m846E62361157BE3A9A6280B0314DA5D22BDF11B8 (void);
// 0x000000EB System.Void Mirror.NetworkAuthenticator::OnStopServer()
extern void NetworkAuthenticator_OnStopServer_m41CE4A338AE0CA96A3BAB3034A5A56A35A4BF72A (void);
// 0x000000EC System.Void Mirror.NetworkAuthenticator::OnServerAuthenticate(Mirror.NetworkConnection)
// 0x000000ED System.Void Mirror.NetworkAuthenticator::ServerAccept(Mirror.NetworkConnection)
extern void NetworkAuthenticator_ServerAccept_m07EB37E92BACEE4DD3CCB27ACA443F79276BF322 (void);
// 0x000000EE System.Void Mirror.NetworkAuthenticator::ServerReject(Mirror.NetworkConnection)
extern void NetworkAuthenticator_ServerReject_m4A3B6983A2212970095DBED053E743E849071034 (void);
// 0x000000EF System.Void Mirror.NetworkAuthenticator::OnStartClient()
extern void NetworkAuthenticator_OnStartClient_m5C32E06D0A5DD87660C6B25EDD8FBDB30D9BC644 (void);
// 0x000000F0 System.Void Mirror.NetworkAuthenticator::OnStopClient()
extern void NetworkAuthenticator_OnStopClient_m1679E3E9ECFA23353E52BABF80DB494ADE6B5B62 (void);
// 0x000000F1 System.Void Mirror.NetworkAuthenticator::OnClientAuthenticate(Mirror.NetworkConnection)
// 0x000000F2 System.Void Mirror.NetworkAuthenticator::ClientAccept(Mirror.NetworkConnection)
extern void NetworkAuthenticator_ClientAccept_mE0A35177C44F4DFD7C20B1A48180E75EEA90467C (void);
// 0x000000F3 System.Void Mirror.NetworkAuthenticator::ClientReject(Mirror.NetworkConnection)
extern void NetworkAuthenticator_ClientReject_m8FA28F908B17DB620AF67913BD591610BF79B580 (void);
// 0x000000F4 System.Void Mirror.NetworkAuthenticator::OnValidate()
extern void NetworkAuthenticator_OnValidate_mFD01EC300AB0E1FFEA3E127B9591D5D0FFBCDAA4 (void);
// 0x000000F5 System.Void Mirror.NetworkAuthenticator::.ctor()
extern void NetworkAuthenticator__ctor_m1EFE9D58997FA1E95FE79D6196AEBDE5CEC414E5 (void);
// 0x000000F6 System.Boolean Mirror.NetworkBehaviour::get_isServer()
extern void NetworkBehaviour_get_isServer_m6CF3499812C1F2679BB924165AA79C59E6D2EBCF (void);
// 0x000000F7 System.Boolean Mirror.NetworkBehaviour::get_isClient()
extern void NetworkBehaviour_get_isClient_m87FF41CC03AD448B05627F3711F8E27C63D5C615 (void);
// 0x000000F8 System.Boolean Mirror.NetworkBehaviour::get_isLocalPlayer()
extern void NetworkBehaviour_get_isLocalPlayer_mFA35EE97B42DEEE92E4FD5562C8C6A1717607DE0 (void);
// 0x000000F9 System.Boolean Mirror.NetworkBehaviour::get_isServerOnly()
extern void NetworkBehaviour_get_isServerOnly_m6247E79FF74BBEC947D4D605095A8A24E5E16FD8 (void);
// 0x000000FA System.Boolean Mirror.NetworkBehaviour::get_isClientOnly()
extern void NetworkBehaviour_get_isClientOnly_m1082A236ADF41204D352DAEE63F652244950C3B1 (void);
// 0x000000FB System.Boolean Mirror.NetworkBehaviour::get_hasAuthority()
extern void NetworkBehaviour_get_hasAuthority_m8C249EBFD9F083DE67F38A74E3245727C6035167 (void);
// 0x000000FC System.UInt32 Mirror.NetworkBehaviour::get_netId()
extern void NetworkBehaviour_get_netId_mFD41F9D183B23443AA528BC0244E0835CCC94826 (void);
// 0x000000FD Mirror.NetworkConnection Mirror.NetworkBehaviour::get_connectionToServer()
extern void NetworkBehaviour_get_connectionToServer_m269C003EE53BFA1EE6A2DBA16A975723C8829421 (void);
// 0x000000FE Mirror.NetworkConnection Mirror.NetworkBehaviour::get_connectionToClient()
extern void NetworkBehaviour_get_connectionToClient_m59547ED6321FBCB2B5BFF0B58F22202E981C2BD2 (void);
// 0x000000FF System.UInt64 Mirror.NetworkBehaviour::get_syncVarDirtyBits()
extern void NetworkBehaviour_get_syncVarDirtyBits_m1690C7716CF78A4CC6360088FB682385EE76C9E3 (void);
// 0x00000100 System.Void Mirror.NetworkBehaviour::set_syncVarDirtyBits(System.UInt64)
extern void NetworkBehaviour_set_syncVarDirtyBits_m1FA90B23C70060A48A39E1284A9516B24F996824 (void);
// 0x00000101 System.Boolean Mirror.NetworkBehaviour::getSyncVarHookGuard(System.UInt64)
extern void NetworkBehaviour_getSyncVarHookGuard_m04C7E518D055E786F2EDD648D995543282812D09 (void);
// 0x00000102 System.Void Mirror.NetworkBehaviour::setSyncVarHookGuard(System.UInt64,System.Boolean)
extern void NetworkBehaviour_setSyncVarHookGuard_m44C276207929797CD147789B1E2D5EFF0E8D0B98 (void);
// 0x00000103 Mirror.NetworkIdentity Mirror.NetworkBehaviour::get_netIdentity()
extern void NetworkBehaviour_get_netIdentity_m67CAF485E29AFA1CB0540A7ADBE68FCE2326151D (void);
// 0x00000104 System.Int32 Mirror.NetworkBehaviour::get_ComponentIndex()
extern void NetworkBehaviour_get_ComponentIndex_mACCD123A66C72A3D062535CF936618C2158E9D76 (void);
// 0x00000105 System.Void Mirror.NetworkBehaviour::InitSyncObject(Mirror.SyncObject)
extern void NetworkBehaviour_InitSyncObject_m627E07D5152DFE01AC855565E09043BA70692CDF (void);
// 0x00000106 System.Void Mirror.NetworkBehaviour::SendCommandInternal(System.Type,System.String,Mirror.NetworkWriter,System.Int32,System.Boolean)
extern void NetworkBehaviour_SendCommandInternal_m81D09DA3B313767B581AFB3F5BC4535807CC5700 (void);
// 0x00000107 System.Void Mirror.NetworkBehaviour::SendRPCInternal(System.Type,System.String,Mirror.NetworkWriter,System.Int32,System.Boolean)
extern void NetworkBehaviour_SendRPCInternal_m3E76275A3E52E54BF11483D924A21D58285586DF (void);
// 0x00000108 System.Void Mirror.NetworkBehaviour::SendTargetRPCInternal(Mirror.NetworkConnection,System.Type,System.String,Mirror.NetworkWriter,System.Int32)
extern void NetworkBehaviour_SendTargetRPCInternal_m0FEC7EA740CF00DABE333A27172BAB1217430756 (void);
// 0x00000109 System.Boolean Mirror.NetworkBehaviour::SyncVarGameObjectEqual(UnityEngine.GameObject,System.UInt32)
extern void NetworkBehaviour_SyncVarGameObjectEqual_m538EA781DEC2D9D0DF307035D039850460DC5690 (void);
// 0x0000010A System.Void Mirror.NetworkBehaviour::SetSyncVarGameObject(UnityEngine.GameObject,UnityEngine.GameObject&,System.UInt64,System.UInt32&)
extern void NetworkBehaviour_SetSyncVarGameObject_m58A39D6DE471F63603EDDA893781EACE528246AF (void);
// 0x0000010B UnityEngine.GameObject Mirror.NetworkBehaviour::GetSyncVarGameObject(System.UInt32,UnityEngine.GameObject&)
extern void NetworkBehaviour_GetSyncVarGameObject_mBD2A0B361900E77AF101885A491B0921FDA4AE3F (void);
// 0x0000010C System.Boolean Mirror.NetworkBehaviour::SyncVarNetworkIdentityEqual(Mirror.NetworkIdentity,System.UInt32)
extern void NetworkBehaviour_SyncVarNetworkIdentityEqual_mBF505C08B55B2AA9120AE69F365A0C1D7EC092E2 (void);
// 0x0000010D System.Void Mirror.NetworkBehaviour::SetSyncVarNetworkIdentity(Mirror.NetworkIdentity,Mirror.NetworkIdentity&,System.UInt64,System.UInt32&)
extern void NetworkBehaviour_SetSyncVarNetworkIdentity_m662595FF753AE6B0E87DBC7E72517250A4244339 (void);
// 0x0000010E Mirror.NetworkIdentity Mirror.NetworkBehaviour::GetSyncVarNetworkIdentity(System.UInt32,Mirror.NetworkIdentity&)
extern void NetworkBehaviour_GetSyncVarNetworkIdentity_m7C2CF1934178FA42C8D93CD008F4E174FD944927 (void);
// 0x0000010F System.Boolean Mirror.NetworkBehaviour::SyncVarNetworkBehaviourEqual(T,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar)
// 0x00000110 System.Void Mirror.NetworkBehaviour::SetSyncVarNetworkBehaviour(T,T&,System.UInt64,Mirror.NetworkBehaviour/NetworkBehaviourSyncVar&)
// 0x00000111 T Mirror.NetworkBehaviour::GetSyncVarNetworkBehaviour(Mirror.NetworkBehaviour/NetworkBehaviourSyncVar,T&)
// 0x00000112 System.Boolean Mirror.NetworkBehaviour::SyncVarEqual(T,T&)
// 0x00000113 System.Void Mirror.NetworkBehaviour::SetSyncVar(T,T&,System.UInt64)
// 0x00000114 System.Void Mirror.NetworkBehaviour::SetDirtyBit(System.UInt64)
extern void NetworkBehaviour_SetDirtyBit_mDE4C6DE9ECC6E882CBDC4BF66BB65CC3B92A49E5 (void);
// 0x00000115 System.Void Mirror.NetworkBehaviour::ClearAllDirtyBits()
extern void NetworkBehaviour_ClearAllDirtyBits_mD37CF3E0D4864DA51521989A6D3F4D012BC5E3BE (void);
// 0x00000116 System.Boolean Mirror.NetworkBehaviour::AnySyncObjectDirty()
extern void NetworkBehaviour_AnySyncObjectDirty_m2A8BAF5C173B492706D9847E02CA23FD799C3F00 (void);
// 0x00000117 System.Boolean Mirror.NetworkBehaviour::IsDirty()
extern void NetworkBehaviour_IsDirty_m50CC8C762BF12BBF3D796B0266BBF1E0D90BA1EA (void);
// 0x00000118 System.Boolean Mirror.NetworkBehaviour::OnSerialize(Mirror.NetworkWriter,System.Boolean)
extern void NetworkBehaviour_OnSerialize_mBE2870081075933577F25C4760A3758FC5A73391 (void);
// 0x00000119 System.Void Mirror.NetworkBehaviour::OnDeserialize(Mirror.NetworkReader,System.Boolean)
extern void NetworkBehaviour_OnDeserialize_mDA83841054FF1970D8C87C246CB56F426195768A (void);
// 0x0000011A System.Boolean Mirror.NetworkBehaviour::SerializeSyncVars(Mirror.NetworkWriter,System.Boolean)
extern void NetworkBehaviour_SerializeSyncVars_m4AF8BE097726BA018E3762C14E33BDBF3A5C4819 (void);
// 0x0000011B System.Void Mirror.NetworkBehaviour::DeserializeSyncVars(Mirror.NetworkReader,System.Boolean)
extern void NetworkBehaviour_DeserializeSyncVars_mC87ECFD6F9C2D5C1306ABDF2CF6498864C84677A (void);
// 0x0000011C System.UInt64 Mirror.NetworkBehaviour::DirtyObjectBits()
extern void NetworkBehaviour_DirtyObjectBits_m072FC9684EFE21FB3C5C8743E22D098BD091D8BC (void);
// 0x0000011D System.Boolean Mirror.NetworkBehaviour::SerializeObjectsAll(Mirror.NetworkWriter)
extern void NetworkBehaviour_SerializeObjectsAll_mE77BC9DA6B1BA7417C5FF4AA5CB0F31F1882ABF9 (void);
// 0x0000011E System.Boolean Mirror.NetworkBehaviour::SerializeObjectsDelta(Mirror.NetworkWriter)
extern void NetworkBehaviour_SerializeObjectsDelta_mF7FA7AF00832E10CB3A3A9E9E18A9DFEF315C8FF (void);
// 0x0000011F System.Void Mirror.NetworkBehaviour::DeSerializeObjectsAll(Mirror.NetworkReader)
extern void NetworkBehaviour_DeSerializeObjectsAll_m6597B6F683F3DCD0FF34B30AE24A95B5CD004C4F (void);
// 0x00000120 System.Void Mirror.NetworkBehaviour::DeSerializeObjectsDelta(Mirror.NetworkReader)
extern void NetworkBehaviour_DeSerializeObjectsDelta_m33229FDA09E4E1EB32F3432C26DFF174563D57CA (void);
// 0x00000121 System.Void Mirror.NetworkBehaviour::ResetSyncObjects()
extern void NetworkBehaviour_ResetSyncObjects_mCC577FB1959F7BE96220152489E55AD993C98D14 (void);
// 0x00000122 System.Void Mirror.NetworkBehaviour::OnStartServer()
extern void NetworkBehaviour_OnStartServer_m7E498D9B74008DCD512C4A01C847795893A5ED2D (void);
// 0x00000123 System.Void Mirror.NetworkBehaviour::OnStopServer()
extern void NetworkBehaviour_OnStopServer_m3553611E0C2705734BD005AF6396A6A7C4994EF1 (void);
// 0x00000124 System.Void Mirror.NetworkBehaviour::OnStartClient()
extern void NetworkBehaviour_OnStartClient_mBE0F27A3CDF5C4F76C777885B2D21AB60A9647E8 (void);
// 0x00000125 System.Void Mirror.NetworkBehaviour::OnStopClient()
extern void NetworkBehaviour_OnStopClient_m6D76DD7B8DC4A4E29D1F58DD69CA6C84E0CC9B81 (void);
// 0x00000126 System.Void Mirror.NetworkBehaviour::OnStartLocalPlayer()
extern void NetworkBehaviour_OnStartLocalPlayer_m884B22DB458ECACC0E00809576CC2DD2F41DE2B4 (void);
// 0x00000127 System.Void Mirror.NetworkBehaviour::OnStartAuthority()
extern void NetworkBehaviour_OnStartAuthority_mC224FDE4601A24F19B2B4A085010764783BB2CED (void);
// 0x00000128 System.Void Mirror.NetworkBehaviour::OnStopAuthority()
extern void NetworkBehaviour_OnStopAuthority_m3040A6FF682924E89E26C06E7B629D024D87D376 (void);
// 0x00000129 System.Void Mirror.NetworkBehaviour::.ctor()
extern void NetworkBehaviour__ctor_mB98FF8F52DCEBEB3BC7679DE03FA50785207EE78 (void);
// 0x0000012A System.Void Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::.ctor(System.UInt32,System.Int32)
extern void NetworkBehaviourSyncVar__ctor_mFB219E84A8139BFFCC899543F4057E03D963B0FF (void);
// 0x0000012B System.Boolean Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::Equals(Mirror.NetworkBehaviour/NetworkBehaviourSyncVar)
extern void NetworkBehaviourSyncVar_Equals_m2EE0BD631368A1EA28D3E4422A9F14C43B9B0A27 (void);
// 0x0000012C System.Boolean Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::Equals(System.UInt32,System.Int32)
extern void NetworkBehaviourSyncVar_Equals_m2EE82223A7405A371A233BCED2C78292E69AA85F (void);
// 0x0000012D System.String Mirror.NetworkBehaviour/NetworkBehaviourSyncVar::ToString()
extern void NetworkBehaviourSyncVar_ToString_m4DE9A94D17F9F28FCDB963AC8665144C37076363 (void);
// 0x0000012E Mirror.NetworkConnection Mirror.NetworkClient::get_connection()
extern void NetworkClient_get_connection_m5439CD4BADA80C781B783F013464517F581F9557 (void);
// 0x0000012F System.Void Mirror.NetworkClient::set_connection(Mirror.NetworkConnection)
extern void NetworkClient_set_connection_mED66882874FA7C5891570DFDC873B4EC5B678AFC (void);
// 0x00000130 Mirror.NetworkConnection Mirror.NetworkClient::get_readyConnection()
extern void NetworkClient_get_readyConnection_mE2865B9681090E2743E3B49F44683D0825FC0B05 (void);
// 0x00000131 Mirror.NetworkIdentity Mirror.NetworkClient::get_localPlayer()
extern void NetworkClient_get_localPlayer_mFA6B51032C92C1B18EAA3B7FCD0369A089A0020C (void);
// 0x00000132 System.Void Mirror.NetworkClient::set_localPlayer(Mirror.NetworkIdentity)
extern void NetworkClient_set_localPlayer_mCBB4FBB1B611801ABEB2B2BBD6A0FD03365D534F (void);
// 0x00000133 System.String Mirror.NetworkClient::get_serverIp()
extern void NetworkClient_get_serverIp_m3952EAE51FB2B2067755FDFFE7481BB95A4E146B (void);
// 0x00000134 System.Boolean Mirror.NetworkClient::get_active()
extern void NetworkClient_get_active_m80C7ACA728DE7F6F6B5DC6C0E80B5AD0D287EA37 (void);
// 0x00000135 System.Boolean Mirror.NetworkClient::get_isConnecting()
extern void NetworkClient_get_isConnecting_mEFCFA8E2F8F117CED0643652F62F0683B2350C72 (void);
// 0x00000136 System.Boolean Mirror.NetworkClient::get_isConnected()
extern void NetworkClient_get_isConnected_m58D71078B898D6DA731CDC3D4EDA737E7B04A4E6 (void);
// 0x00000137 System.Boolean Mirror.NetworkClient::get_isLocalClient()
extern void NetworkClient_get_isLocalClient_mE3722DD1EFD1B66370682ACD271ADF980AC15288 (void);
// 0x00000138 System.Void Mirror.NetworkClient::AddTransportHandlers()
extern void NetworkClient_AddTransportHandlers_mF0A64666B0628413F9F37D72B169C8A7C9DBC920 (void);
// 0x00000139 System.Void Mirror.NetworkClient::RegisterSystemHandlers(System.Boolean)
extern void NetworkClient_RegisterSystemHandlers_m5BF1C0E926656B42C70C67A146B65FFF8378AB55 (void);
// 0x0000013A System.Void Mirror.NetworkClient::Connect(System.String)
extern void NetworkClient_Connect_m1012B6CB8305CF83DE993CB961DCBAC2E5366489 (void);
// 0x0000013B System.Void Mirror.NetworkClient::Connect(System.Uri)
extern void NetworkClient_Connect_mD685E75AAE9CF8F6765C91640C84437777546925 (void);
// 0x0000013C System.Void Mirror.NetworkClient::ConnectHost()
extern void NetworkClient_ConnectHost_m2359D0E1763DEE430B6920B2C7B4D67A270927C9 (void);
// 0x0000013D System.Void Mirror.NetworkClient::ConnectLocalServer()
extern void NetworkClient_ConnectLocalServer_mCCDFBA08C7B898756DF7812127468B9F68966C7E (void);
// 0x0000013E System.Void Mirror.NetworkClient::Disconnect()
extern void NetworkClient_Disconnect_mD13276168C810E45CA0ED75ABDC3DBF7CBC2F439 (void);
// 0x0000013F System.Void Mirror.NetworkClient::DisconnectLocalServer()
extern void NetworkClient_DisconnectLocalServer_m104FA4038FF5069FB3317822722597560503E9ED (void);
// 0x00000140 System.Void Mirror.NetworkClient::OnConnected()
extern void NetworkClient_OnConnected_mD2B7E0A0007140112D7B406DFAF3E10FAAF5C3ED (void);
// 0x00000141 System.Void Mirror.NetworkClient::OnDataReceived(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkClient_OnDataReceived_m6DC574679B247729F8E6E0740D488AA68FF6E1BB (void);
// 0x00000142 System.Void Mirror.NetworkClient::OnDisconnected()
extern void NetworkClient_OnDisconnected_m68881B16A8A8A85C1618EAB923DE40E2FA67FEA1 (void);
// 0x00000143 System.Void Mirror.NetworkClient::OnError(System.Exception)
extern void NetworkClient_OnError_m3CBE84BB09E2B3B7D4741ECA4F9F2DDAAF5184A1 (void);
// 0x00000144 System.Void Mirror.NetworkClient::Send(T,System.Int32)
// 0x00000145 System.Void Mirror.NetworkClient::RegisterHandler(System.Action`2<Mirror.NetworkConnection,T>,System.Boolean)
// 0x00000146 System.Void Mirror.NetworkClient::RegisterHandler(System.Action`1<T>,System.Boolean)
// 0x00000147 System.Void Mirror.NetworkClient::ReplaceHandler(System.Action`2<Mirror.NetworkConnection,T>,System.Boolean)
// 0x00000148 System.Void Mirror.NetworkClient::ReplaceHandler(System.Action`1<T>,System.Boolean)
// 0x00000149 System.Boolean Mirror.NetworkClient::UnregisterHandler()
// 0x0000014A System.Boolean Mirror.NetworkClient::GetPrefab(System.Guid,UnityEngine.GameObject&)
extern void NetworkClient_GetPrefab_mB24435E12D19697A887BFADE8F8B3D586742999E (void);
// 0x0000014B System.Void Mirror.NetworkClient::RegisterPrefabIdentity(Mirror.NetworkIdentity)
extern void NetworkClient_RegisterPrefabIdentity_m70F93AE7E4C64AA218627822BB68FFA632E40238 (void);
// 0x0000014C System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,System.Guid)
extern void NetworkClient_RegisterPrefab_mDDF91101ACDA56F5CF4F69D165E33040C587BA80 (void);
// 0x0000014D System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject)
extern void NetworkClient_RegisterPrefab_mD3E596F19CF1A608C524A4A95CEF56BE6EBC5FF4 (void);
// 0x0000014E System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,System.Guid,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_mADEC9110EDEDFC7D2769EDFBC5AABE4BA782D24A (void);
// 0x0000014F System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_m83A8C1F431CC6E48040A642CB71B3B8E2ED91E2B (void);
// 0x00000150 System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,System.Guid,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_mFBD356840D4DD5EF716B650E3C09D5A6CA0A23BB (void);
// 0x00000151 System.Void Mirror.NetworkClient::RegisterPrefab(UnityEngine.GameObject,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterPrefab_mA11E07E698EDFC41138E2F9FD3A9CCD94ADAEF05 (void);
// 0x00000152 System.Void Mirror.NetworkClient::UnregisterPrefab(UnityEngine.GameObject)
extern void NetworkClient_UnregisterPrefab_m624DDE9ADA9681E06079404F2B9A80F9D16023E6 (void);
// 0x00000153 System.Void Mirror.NetworkClient::RegisterSpawnHandler(System.Guid,Mirror.SpawnDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterSpawnHandler_m5246FEC89A9B3C563F54F2775BA6CFFC3BA00D9D (void);
// 0x00000154 System.Void Mirror.NetworkClient::RegisterSpawnHandler(System.Guid,Mirror.SpawnHandlerDelegate,Mirror.UnSpawnDelegate)
extern void NetworkClient_RegisterSpawnHandler_m1391B1F148D5E508A90FCC69B39B4A3913A98AB9 (void);
// 0x00000155 System.Void Mirror.NetworkClient::UnregisterSpawnHandler(System.Guid)
extern void NetworkClient_UnregisterSpawnHandler_mFCBC83634E281D816898E45D25C41337B3B69AAE (void);
// 0x00000156 System.Void Mirror.NetworkClient::ClearSpawners()
extern void NetworkClient_ClearSpawners_m92F7ADAD3F0327103A44F3D132D9F89E60F917A7 (void);
// 0x00000157 System.Boolean Mirror.NetworkClient::InvokeUnSpawnHandler(System.Guid,UnityEngine.GameObject)
extern void NetworkClient_InvokeUnSpawnHandler_m303A55418A1FBA4403512BF5B0D7F037850999DB (void);
// 0x00000158 System.Boolean Mirror.NetworkClient::Ready()
extern void NetworkClient_Ready_m0290F8ED5D23A2C8DC4DCEAE68295AAC9644B926 (void);
// 0x00000159 System.Boolean Mirror.NetworkClient::Ready(Mirror.NetworkConnection)
extern void NetworkClient_Ready_m70AF81088C3D82C47973BD23EF814AD46FE0D58A (void);
// 0x0000015A System.Void Mirror.NetworkClient::InternalAddPlayer(Mirror.NetworkIdentity)
extern void NetworkClient_InternalAddPlayer_m53CB34CF7951291A275A589AAB770D9668363B70 (void);
// 0x0000015B System.Boolean Mirror.NetworkClient::AddPlayer()
extern void NetworkClient_AddPlayer_m0C6015BEBE42CDDEB5AE5C5F787108737505B107 (void);
// 0x0000015C System.Boolean Mirror.NetworkClient::AddPlayer(Mirror.NetworkConnection)
extern void NetworkClient_AddPlayer_mCC3962505C3611B0851E8C102CD374638249D948 (void);
// 0x0000015D System.Void Mirror.NetworkClient::ApplySpawnPayload(Mirror.NetworkIdentity,Mirror.SpawnMessage)
extern void NetworkClient_ApplySpawnPayload_mEB9CA6042E62F380DA7B3F49BAB4538448FF84FF (void);
// 0x0000015E System.Boolean Mirror.NetworkClient::FindOrSpawnObject(Mirror.SpawnMessage,Mirror.NetworkIdentity&)
extern void NetworkClient_FindOrSpawnObject_m73DBFF69AAEC9489D2B8AD54381DB5B39E35A189 (void);
// 0x0000015F Mirror.NetworkIdentity Mirror.NetworkClient::GetExistingObject(System.UInt32)
extern void NetworkClient_GetExistingObject_m8B4FB7CA960913040F4AD6B2082D6106A2E875E2 (void);
// 0x00000160 Mirror.NetworkIdentity Mirror.NetworkClient::SpawnPrefab(Mirror.SpawnMessage)
extern void NetworkClient_SpawnPrefab_m31B1515BF10E16933F18CA5930B06BDD0EEF6595 (void);
// 0x00000161 Mirror.NetworkIdentity Mirror.NetworkClient::SpawnSceneObject(Mirror.SpawnMessage)
extern void NetworkClient_SpawnSceneObject_mECFD944994566F90482C2D8ECE23931DE98D7571 (void);
// 0x00000162 Mirror.NetworkIdentity Mirror.NetworkClient::GetAndRemoveSceneObject(System.UInt64)
extern void NetworkClient_GetAndRemoveSceneObject_mF4E2C3C8F607D9848D5D449AAD77D90A6551CD42 (void);
// 0x00000163 System.Boolean Mirror.NetworkClient::ConsiderForSpawning(Mirror.NetworkIdentity)
extern void NetworkClient_ConsiderForSpawning_m775C21D2195F17002BF0F7F60F0C052AAA6934B2 (void);
// 0x00000164 System.Void Mirror.NetworkClient::PrepareToSpawnSceneObjects()
extern void NetworkClient_PrepareToSpawnSceneObjects_m1F51AE31B33D48FD9DDBF1CEC08AA546AEB50D30 (void);
// 0x00000165 System.Void Mirror.NetworkClient::OnObjectSpawnStarted(Mirror.ObjectSpawnStartedMessage)
extern void NetworkClient_OnObjectSpawnStarted_m2F1A04282BE3688D0750D225100B003AE5FE7658 (void);
// 0x00000166 System.Void Mirror.NetworkClient::OnObjectSpawnFinished(Mirror.ObjectSpawnFinishedMessage)
extern void NetworkClient_OnObjectSpawnFinished_m08BC661EAE49DB5C7955421AB8D43CBA1CD78B90 (void);
// 0x00000167 System.Void Mirror.NetworkClient::ClearNullFromSpawned()
extern void NetworkClient_ClearNullFromSpawned_m294A3CF498EC00F34F3E5D27E07E42DB6CF120A6 (void);
// 0x00000168 System.Void Mirror.NetworkClient::OnHostClientObjectDestroy(Mirror.ObjectDestroyMessage)
extern void NetworkClient_OnHostClientObjectDestroy_m821445496B743A10A13697A519B1B7BE76D7154B (void);
// 0x00000169 System.Void Mirror.NetworkClient::OnHostClientObjectHide(Mirror.ObjectHideMessage)
extern void NetworkClient_OnHostClientObjectHide_m9C1D529B105CAD107D472163C75E52130C5CB8BB (void);
// 0x0000016A System.Void Mirror.NetworkClient::OnHostClientSpawn(Mirror.SpawnMessage)
extern void NetworkClient_OnHostClientSpawn_m0B461537BFDC210A94C92142EF85F18E772FCAA5 (void);
// 0x0000016B System.Void Mirror.NetworkClient::OnUpdateVarsMessage(Mirror.UpdateVarsMessage)
extern void NetworkClient_OnUpdateVarsMessage_m86917F97D01CE0695F635CB993EFB4393F9EF904 (void);
// 0x0000016C System.Void Mirror.NetworkClient::OnRPCMessage(Mirror.RpcMessage)
extern void NetworkClient_OnRPCMessage_mDB1DC6D93DA81BCFC6604686FBF20951BD087242 (void);
// 0x0000016D System.Void Mirror.NetworkClient::OnObjectHide(Mirror.ObjectHideMessage)
extern void NetworkClient_OnObjectHide_mECE8726EE3F62C50CB3541A558AB0A478995D0E7 (void);
// 0x0000016E System.Void Mirror.NetworkClient::OnObjectDestroy(Mirror.ObjectDestroyMessage)
extern void NetworkClient_OnObjectDestroy_mCD8DDAF1FD872969E841C2C7E9946E8FA5299766 (void);
// 0x0000016F System.Void Mirror.NetworkClient::OnSpawn(Mirror.SpawnMessage)
extern void NetworkClient_OnSpawn_m757BA45D182CA43B9FFBF7E03120358BC0836C98 (void);
// 0x00000170 System.Void Mirror.NetworkClient::CheckForLocalPlayer(Mirror.NetworkIdentity)
extern void NetworkClient_CheckForLocalPlayer_m9AA764A2840876F38D3CC123AE637365A9A025C8 (void);
// 0x00000171 System.Void Mirror.NetworkClient::DestroyObject(System.UInt32)
extern void NetworkClient_DestroyObject_m58E70E859CF6E27F0018B060C2990757BFC5DEFC (void);
// 0x00000172 System.Void Mirror.NetworkClient::NetworkEarlyUpdate()
extern void NetworkClient_NetworkEarlyUpdate_m4BB991D8FD06E909BDE076B87895113E9A510F94 (void);
// 0x00000173 System.Void Mirror.NetworkClient::NetworkLateUpdate()
extern void NetworkClient_NetworkLateUpdate_mD996BE78A2574AA44050056BAC9E3F7AC4FFDBDB (void);
// 0x00000174 System.Void Mirror.NetworkClient::Update()
extern void NetworkClient_Update_mE5F6424944504CF457A06BF18B8D1877B259D4F1 (void);
// 0x00000175 System.Void Mirror.NetworkClient::DestroyAllClientObjects()
extern void NetworkClient_DestroyAllClientObjects_m895C02874F863A82A1FDB4015F223CF4DDBAD324 (void);
// 0x00000176 System.Void Mirror.NetworkClient::Shutdown()
extern void NetworkClient_Shutdown_m362C444833B9302098E6CC73EFA59309FC7B3D4D (void);
// 0x00000177 System.Void Mirror.NetworkClient::.cctor()
extern void NetworkClient__cctor_m051C6F073FEAF3D99E7E41C2F5A72AACBA005313 (void);
// 0x00000178 System.Void Mirror.NetworkClient/<>c::.cctor()
extern void U3CU3Ec__cctor_mBED38AB857BA061D80B1B06261A24C9C56744C10 (void);
// 0x00000179 System.Void Mirror.NetworkClient/<>c::.ctor()
extern void U3CU3Ec__ctor_m66C0ED4876EBA658BD0CF465BFA73ECA9EBDA408 (void);
// 0x0000017A System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__31_0(Mirror.NetworkPongMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__31_0_mBB73054C087C5BE037928DE02DE4A95F2C362FE9 (void);
// 0x0000017B System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__31_1(Mirror.ObjectSpawnStartedMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__31_1_m6B0938DA6FB2F882894A1CF7DEC1A7F7A8647566 (void);
// 0x0000017C System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__31_2(Mirror.ObjectSpawnFinishedMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__31_2_m1CF74B2A83C5D72B56F58D2526DE536296734F2A (void);
// 0x0000017D System.Void Mirror.NetworkClient/<>c::<RegisterSystemHandlers>b__31_3(Mirror.UpdateVarsMessage)
extern void U3CU3Ec_U3CRegisterSystemHandlersU3Eb__31_3_m144B2391E0D61068B1CC719EBABF266A2058A559 (void);
// 0x0000017E System.UInt32 Mirror.NetworkClient/<>c::<OnObjectSpawnFinished>b__76_0(Mirror.NetworkIdentity)
extern void U3CU3Ec_U3COnObjectSpawnFinishedU3Eb__76_0_m9650D466911A0B71616F89A878B302721616A9FC (void);
// 0x0000017F System.Void Mirror.NetworkClient/<>c__DisplayClass44_0`1::.ctor()
// 0x00000180 System.Void Mirror.NetworkClient/<>c__DisplayClass44_0`1::<RegisterHandler>b__0(Mirror.NetworkConnection,T)
// 0x00000181 System.Void Mirror.NetworkClient/<>c__DisplayClass46_0`1::.ctor()
// 0x00000182 System.Void Mirror.NetworkClient/<>c__DisplayClass46_0`1::<ReplaceHandler>b__0(Mirror.NetworkConnection,T)
// 0x00000183 System.Void Mirror.NetworkClient/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_m083F79DCE5353AD7B8FB2DD76622252E571D1A8C (void);
// 0x00000184 UnityEngine.GameObject Mirror.NetworkClient/<>c__DisplayClass52_0::<RegisterPrefab>b__0(Mirror.SpawnMessage)
extern void U3CU3Ec__DisplayClass52_0_U3CRegisterPrefabU3Eb__0_mDEEE7295F00571B18554B708EF45479606DBA09B (void);
// 0x00000185 System.Void Mirror.NetworkClient/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_mA4170F1DD5BDC1B10B5C797F4B7C29C8E7F3869A (void);
// 0x00000186 UnityEngine.GameObject Mirror.NetworkClient/<>c__DisplayClass53_0::<RegisterPrefab>b__0(Mirror.SpawnMessage)
extern void U3CU3Ec__DisplayClass53_0_U3CRegisterPrefabU3Eb__0_m0D570AC51B427E497769324C83236D801BE0EB78 (void);
// 0x00000187 System.Void Mirror.NetworkClient/<>c__DisplayClass57_0::.ctor()
extern void U3CU3Ec__DisplayClass57_0__ctor_mB83A6AF7A70B4DC66FEC2C7C3E814543357902F1 (void);
// 0x00000188 UnityEngine.GameObject Mirror.NetworkClient/<>c__DisplayClass57_0::<RegisterSpawnHandler>b__0(Mirror.SpawnMessage)
extern void U3CU3Ec__DisplayClass57_0_U3CRegisterSpawnHandlerU3Eb__0_m1626DA596A45631087BF6945FEAAF5BEFC34D6BB (void);
// 0x00000189 System.String Mirror.NetworkConnection::get_address()
// 0x0000018A Mirror.NetworkIdentity Mirror.NetworkConnection::get_identity()
extern void NetworkConnection_get_identity_mF8F7D1AA28117C2F53450E6697D2966DB1B16F45 (void);
// 0x0000018B System.Void Mirror.NetworkConnection::set_identity(Mirror.NetworkIdentity)
extern void NetworkConnection_set_identity_m6BD8F3D5B3542256F7BB47F17C18AC51B6EB5CC9 (void);
// 0x0000018C System.Void Mirror.NetworkConnection::.ctor()
extern void NetworkConnection__ctor_mA4169F8A1B0EF786615F138B3395E621AC733E3D (void);
// 0x0000018D System.Void Mirror.NetworkConnection::.ctor(System.Int32)
extern void NetworkConnection__ctor_m617332B65E39A90B5D3AC66B867A5338BDFEB85E (void);
// 0x0000018E System.Void Mirror.NetworkConnection::Disconnect()
// 0x0000018F System.Void Mirror.NetworkConnection::SetHandlers(System.Collections.Generic.Dictionary`2<System.Int32,Mirror.NetworkMessageDelegate>)
extern void NetworkConnection_SetHandlers_mEE69D50F1A14AF77AEF54F961F151BC96D857852 (void);
// 0x00000190 System.Void Mirror.NetworkConnection::Send(T,System.Int32)
// 0x00000191 System.Boolean Mirror.NetworkConnection::ValidatePacketSize(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnection_ValidatePacketSize_m623E054D7EACF8628D11D6CC96CBF18D5575F42E (void);
// 0x00000192 System.Void Mirror.NetworkConnection::Send(System.ArraySegment`1<System.Byte>,System.Int32)
// 0x00000193 System.String Mirror.NetworkConnection::ToString()
extern void NetworkConnection_ToString_m15D8D27A4FE3967B3BE72F9B5BFC08D4EF2D0CDB (void);
// 0x00000194 System.Void Mirror.NetworkConnection::AddToObserving(Mirror.NetworkIdentity)
extern void NetworkConnection_AddToObserving_mD2B65BA312D85B6E7608E057DCCAD78A8AAFBE04 (void);
// 0x00000195 System.Void Mirror.NetworkConnection::RemoveFromObserving(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkConnection_RemoveFromObserving_mA35C23978CB2F27BD59BFA0E4DEA171B7CED03C0 (void);
// 0x00000196 System.Void Mirror.NetworkConnection::RemoveObservers()
extern void NetworkConnection_RemoveObservers_m2D462ECA3222CCEEBBA825CEB64E55628960F8A8 (void);
// 0x00000197 System.Boolean Mirror.NetworkConnection::UnpackAndInvoke(Mirror.NetworkReader,System.Int32)
extern void NetworkConnection_UnpackAndInvoke_m0B4A668A43B74300757AECD2BB2C9906BB46BE3D (void);
// 0x00000198 System.Void Mirror.NetworkConnection::TransportReceive(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnection_TransportReceive_m473F63A4BCD1C0C9E7AE6C68CD070AB2BB25F049 (void);
// 0x00000199 System.Boolean Mirror.NetworkConnection::IsAlive(System.Single)
extern void NetworkConnection_IsAlive_mB23FF1F3CB3DCF3ECE7533A1179FC1977B9524FC (void);
// 0x0000019A System.Void Mirror.NetworkConnection::AddOwnedObject(Mirror.NetworkIdentity)
extern void NetworkConnection_AddOwnedObject_m93256C83BF06EFAC884A0C715C96AECF7779B241 (void);
// 0x0000019B System.Void Mirror.NetworkConnection::RemoveOwnedObject(Mirror.NetworkIdentity)
extern void NetworkConnection_RemoveOwnedObject_m6D5767CAD88E52A83159CC6D54935C5D599A6A09 (void);
// 0x0000019C System.Void Mirror.NetworkConnection::DestroyOwnedObjects()
extern void NetworkConnection_DestroyOwnedObjects_mF3297F8824C855C68E66E7A252278B65D01EA939 (void);
// 0x0000019D System.String Mirror.NetworkConnectionToClient::get_address()
extern void NetworkConnectionToClient_get_address_mB7234483EEF04B6B7E5F3837918B606ACA8F77BD (void);
// 0x0000019E System.Void Mirror.NetworkConnectionToClient::.ctor(System.Int32,System.Boolean,System.Single)
extern void NetworkConnectionToClient__ctor_m1EBB90F7A66264C36B5033A3EEF522D1EE415835 (void);
// 0x0000019F Mirror.NetworkConnectionToClient/Batch Mirror.NetworkConnectionToClient::GetBatchForChannelId(System.Int32)
extern void NetworkConnectionToClient_GetBatchForChannelId_m2AA219CC7E0BF7D29177C276956506294BE5CBBD (void);
// 0x000001A0 System.Void Mirror.NetworkConnectionToClient::SendBatch(System.Int32,Mirror.NetworkConnectionToClient/Batch)
extern void NetworkConnectionToClient_SendBatch_m1332DE6A017E771C7D59363D97CDAC82CD986F02 (void);
// 0x000001A1 System.Void Mirror.NetworkConnectionToClient::Send(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnectionToClient_Send_m7CB08498C245714AE1683C91049AD4968BF365C9 (void);
// 0x000001A2 System.Void Mirror.NetworkConnectionToClient::Update()
extern void NetworkConnectionToClient_Update_m7044EA736760C6ED0115CA1F391DD368AFAD1A87 (void);
// 0x000001A3 System.Void Mirror.NetworkConnectionToClient::Disconnect()
extern void NetworkConnectionToClient_Disconnect_mF451E9202AE6F4664F777BBE2A6CCA497EB9E7AC (void);
// 0x000001A4 System.Void Mirror.NetworkConnectionToClient/Batch::.ctor()
extern void Batch__ctor_mC38954B624162C3C15FE977087CEAB6D395B3FAF (void);
// 0x000001A5 System.String Mirror.NetworkConnectionToServer::get_address()
extern void NetworkConnectionToServer_get_address_m93A6B095B543ECB8D3E8D7C62FD04ED19B9DF3F2 (void);
// 0x000001A6 System.Void Mirror.NetworkConnectionToServer::Send(System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkConnectionToServer_Send_mA01F41D34EA1FB8EE4F5F2498ED7428B2143AD15 (void);
// 0x000001A7 System.Void Mirror.NetworkConnectionToServer::Disconnect()
extern void NetworkConnectionToServer_Disconnect_mAF11CAB599FAAA6907A6A53DF0FF35E34903EDC1 (void);
// 0x000001A8 System.Void Mirror.NetworkConnectionToServer::.ctor()
extern void NetworkConnectionToServer__ctor_m59A1F6CDD13D875CDC80886DF418F29331FD12F4 (void);
// 0x000001A9 System.Void Mirror.NetworkDiagnostics::add_OutMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_add_OutMessageEvent_m509EFD06CA54100CA6D78D553E686D705AA7E14B (void);
// 0x000001AA System.Void Mirror.NetworkDiagnostics::remove_OutMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_remove_OutMessageEvent_m1FE3832B053A95A4B9DE0775797F009BEA175AB2 (void);
// 0x000001AB System.Void Mirror.NetworkDiagnostics::OnSend(T,System.Int32,System.Int32,System.Int32)
// 0x000001AC System.Void Mirror.NetworkDiagnostics::add_InMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_add_InMessageEvent_mC6E41C981DFCF20D9FAA9C2CB603D15F543E2729 (void);
// 0x000001AD System.Void Mirror.NetworkDiagnostics::remove_InMessageEvent(System.Action`1<Mirror.NetworkDiagnostics/MessageInfo>)
extern void NetworkDiagnostics_remove_InMessageEvent_m548727ACDD2579D73D281D4422E1DFC561FE96FB (void);
// 0x000001AE System.Void Mirror.NetworkDiagnostics::OnReceive(T,System.Int32,System.Int32)
// 0x000001AF System.Void Mirror.NetworkDiagnostics/MessageInfo::.ctor(Mirror.NetworkMessage,System.Int32,System.Int32,System.Int32)
extern void MessageInfo__ctor_mAC67988BF4FBFD84F5201A8DBFEA962167787671 (void);
// 0x000001B0 System.Boolean Mirror.NetworkIdentity::get_isClient()
extern void NetworkIdentity_get_isClient_mA664D866C9B791BA0D554F5F1373FCB0FA1A571C (void);
// 0x000001B1 System.Void Mirror.NetworkIdentity::set_isClient(System.Boolean)
extern void NetworkIdentity_set_isClient_m6B0DD90AF225C53612890CD949C344DC3AF17CB6 (void);
// 0x000001B2 System.Boolean Mirror.NetworkIdentity::get_isServer()
extern void NetworkIdentity_get_isServer_m4BE8C60D704E40109BC7A11007F77178A557B6F2 (void);
// 0x000001B3 System.Void Mirror.NetworkIdentity::set_isServer(System.Boolean)
extern void NetworkIdentity_set_isServer_mE11EFCB6F277961CF8BF2FF363F3D4ACA936C2B3 (void);
// 0x000001B4 System.Boolean Mirror.NetworkIdentity::get_isLocalPlayer()
extern void NetworkIdentity_get_isLocalPlayer_m9E8156906B197598DD5E56CCB463BFD80EF0BC76 (void);
// 0x000001B5 System.Void Mirror.NetworkIdentity::set_isLocalPlayer(System.Boolean)
extern void NetworkIdentity_set_isLocalPlayer_mC5BD33CD0B88ED65D5B55FA49FF86F7635291193 (void);
// 0x000001B6 System.Boolean Mirror.NetworkIdentity::get_isServerOnly()
extern void NetworkIdentity_get_isServerOnly_m7FE05E43DDC8562E9C848BE93EA80827F8B2431C (void);
// 0x000001B7 System.Boolean Mirror.NetworkIdentity::get_isClientOnly()
extern void NetworkIdentity_get_isClientOnly_m315291F7D383386522570D0E3455CBA9A8DD2871 (void);
// 0x000001B8 System.Boolean Mirror.NetworkIdentity::get_hasAuthority()
extern void NetworkIdentity_get_hasAuthority_mC0FA6F347408FB65CC629E407CA2DBE9EB4B520B (void);
// 0x000001B9 System.Void Mirror.NetworkIdentity::set_hasAuthority(System.Boolean)
extern void NetworkIdentity_set_hasAuthority_mB13BDE976C8B19F28C6F8219BBC28D426CB92EE8 (void);
// 0x000001BA System.UInt32 Mirror.NetworkIdentity::get_netId()
extern void NetworkIdentity_get_netId_m3FF02B719B8AE0B6A3483063A373AFFB2489C0FA (void);
// 0x000001BB System.Void Mirror.NetworkIdentity::set_netId(System.UInt32)
extern void NetworkIdentity_set_netId_m7DCA5820ABD4B361D3B6B08A93429BB932A610A9 (void);
// 0x000001BC Mirror.NetworkConnection Mirror.NetworkIdentity::get_connectionToServer()
extern void NetworkIdentity_get_connectionToServer_m5B5462ECC017128A99205EDEF095B3FFDBF4EF39 (void);
// 0x000001BD System.Void Mirror.NetworkIdentity::set_connectionToServer(Mirror.NetworkConnection)
extern void NetworkIdentity_set_connectionToServer_m4E36D6FCC72BB5F99830549C9DE19DD350535ADF (void);
// 0x000001BE Mirror.NetworkConnectionToClient Mirror.NetworkIdentity::get_connectionToClient()
extern void NetworkIdentity_get_connectionToClient_mF85737F2CC90FC7E77FE8385F35F2FF2E692D82A (void);
// 0x000001BF System.Void Mirror.NetworkIdentity::set_connectionToClient(Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_set_connectionToClient_mD0DBB925AA6F5BEFD9D79C0BB1EE4E0362971129 (void);
// 0x000001C0 Mirror.NetworkBehaviour[] Mirror.NetworkIdentity::get_NetworkBehaviours()
extern void NetworkIdentity_get_NetworkBehaviours_m7D27CEBD20ABC0925B9C7187E04E43CAC99AE8B0 (void);
// 0x000001C1 Mirror.NetworkVisibility Mirror.NetworkIdentity::get_visibility()
extern void NetworkIdentity_get_visibility_mABBA00BB1370A3CB3C5FC79277EB507381E0DDC0 (void);
// 0x000001C2 System.Guid Mirror.NetworkIdentity::get_assetId()
extern void NetworkIdentity_get_assetId_m3285D38FE7DE4D1F3640E5E50B2D2C88D44BA2F6 (void);
// 0x000001C3 System.Void Mirror.NetworkIdentity::set_assetId(System.Guid)
extern void NetworkIdentity_set_assetId_m695FA033D0740CBF83F1EEDE8B29FDF5AA8D83AF (void);
// 0x000001C4 Mirror.NetworkIdentity Mirror.NetworkIdentity::GetSceneIdentity(System.UInt64)
extern void NetworkIdentity_GetSceneIdentity_mD960C920AAEA6C0BF5BD16A3DEDED4A80F7DFF05 (void);
// 0x000001C5 System.Void Mirror.NetworkIdentity::SetClientOwner(Mirror.NetworkConnection)
extern void NetworkIdentity_SetClientOwner_m840CB3D53A219C47CF00777D59BB12FA82292AB2 (void);
// 0x000001C6 System.UInt32 Mirror.NetworkIdentity::GetNextNetworkId()
extern void NetworkIdentity_GetNextNetworkId_m4B5B89CE5A7B120E0F1AC8092103D2A7C959D3B3 (void);
// 0x000001C7 System.Void Mirror.NetworkIdentity::ResetNextNetworkId()
extern void NetworkIdentity_ResetNextNetworkId_m53391AEF97823B5133DB4F523A957998AF349014 (void);
// 0x000001C8 System.Void Mirror.NetworkIdentity::add_clientAuthorityCallback(Mirror.NetworkIdentity/ClientAuthorityCallback)
extern void NetworkIdentity_add_clientAuthorityCallback_m633CD9516DED0EB53EB4E1E87BB9EAA15AD40308 (void);
// 0x000001C9 System.Void Mirror.NetworkIdentity::remove_clientAuthorityCallback(Mirror.NetworkIdentity/ClientAuthorityCallback)
extern void NetworkIdentity_remove_clientAuthorityCallback_mB13DCC6DA39776FA5286B9A836FB6EE302740AE0 (void);
// 0x000001CA System.Void Mirror.NetworkIdentity::RemoveObserverInternal(Mirror.NetworkConnection)
extern void NetworkIdentity_RemoveObserverInternal_m92C4D050393BE63F326B85815D52FD75060D6944 (void);
// 0x000001CB System.Boolean Mirror.NetworkIdentity::get_SpawnedFromInstantiate()
extern void NetworkIdentity_get_SpawnedFromInstantiate_mF9CCC6D13CD07F8D25FD2D4762D3A2E82F12E2CC (void);
// 0x000001CC System.Void Mirror.NetworkIdentity::set_SpawnedFromInstantiate(System.Boolean)
extern void NetworkIdentity_set_SpawnedFromInstantiate_mF6F78EE5B73A8AD10495BE8EF356812CE273D469 (void);
// 0x000001CD System.Void Mirror.NetworkIdentity::Awake()
extern void NetworkIdentity_Awake_m5278295D326C59B7A020FD899C7BC97922D05FE6 (void);
// 0x000001CE System.Void Mirror.NetworkIdentity::OnValidate()
extern void NetworkIdentity_OnValidate_m226702DAC2E56D685492FD4AC1C6F06A6C1B22E4 (void);
// 0x000001CF System.Void Mirror.NetworkIdentity::OnDestroy()
extern void NetworkIdentity_OnDestroy_mBE43AC5BCEC90EA29BEF95B400892C62439E4BF0 (void);
// 0x000001D0 System.Void Mirror.NetworkIdentity::OnStartServer()
extern void NetworkIdentity_OnStartServer_m0F2FCCC99FA0C984742DED2406C184A9B883226A (void);
// 0x000001D1 System.Void Mirror.NetworkIdentity::OnStopServer()
extern void NetworkIdentity_OnStopServer_m256B4D399B052376C17A63ED5866B9B1AD22F2EF (void);
// 0x000001D2 System.Void Mirror.NetworkIdentity::OnStartClient()
extern void NetworkIdentity_OnStartClient_m32E83A6A595E3F99CCC214FC71AB952D4B409541 (void);
// 0x000001D3 System.Void Mirror.NetworkIdentity::OnStopClient()
extern void NetworkIdentity_OnStopClient_m0BAA5DBA51A92275C387D90D17A54D8033733D82 (void);
// 0x000001D4 System.Void Mirror.NetworkIdentity::OnStartLocalPlayer()
extern void NetworkIdentity_OnStartLocalPlayer_m8BB0D60CFE045D1D76E78C71BB12287275F1195E (void);
// 0x000001D5 System.Void Mirror.NetworkIdentity::NotifyAuthority()
extern void NetworkIdentity_NotifyAuthority_mABE2E3765FF6730E79FFA54064DA772FF2F185F3 (void);
// 0x000001D6 System.Void Mirror.NetworkIdentity::OnStartAuthority()
extern void NetworkIdentity_OnStartAuthority_mF7593974D2A81557BE023303A33B724590D8E194 (void);
// 0x000001D7 System.Void Mirror.NetworkIdentity::OnStopAuthority()
extern void NetworkIdentity_OnStopAuthority_m6124985322A401EFFAA46CB41C4B5141E6DBA592 (void);
// 0x000001D8 System.Void Mirror.NetworkIdentity::RebuildObservers(System.Boolean)
extern void NetworkIdentity_RebuildObservers_m4D470C5582F2C09787280AE96B410328E387085D (void);
// 0x000001D9 System.Void Mirror.NetworkIdentity::OnSetHostVisibility(System.Boolean)
extern void NetworkIdentity_OnSetHostVisibility_mF32DAB5040C377758170B27CAD39BB7E2C0501EC (void);
// 0x000001DA System.Boolean Mirror.NetworkIdentity::OnSerializeSafely(Mirror.NetworkBehaviour,Mirror.NetworkWriter,System.Boolean)
extern void NetworkIdentity_OnSerializeSafely_m5612BD0E29030C4166F0BF245E7F4253C0EC1A46 (void);
// 0x000001DB System.Void Mirror.NetworkIdentity::OnSerializeAllSafely(System.Boolean,Mirror.NetworkWriter,System.Int32&,Mirror.NetworkWriter,System.Int32&)
extern void NetworkIdentity_OnSerializeAllSafely_m241671203CD1B3A5123AE9124A8756905D1D6357 (void);
// 0x000001DC System.Void Mirror.NetworkIdentity::OnDeserializeSafely(Mirror.NetworkBehaviour,Mirror.NetworkReader,System.Boolean)
extern void NetworkIdentity_OnDeserializeSafely_m930E63C2BA46D9E0678F3A7F9AA98E347EDD6BBA (void);
// 0x000001DD System.Void Mirror.NetworkIdentity::OnDeserializeAllSafely(Mirror.NetworkReader,System.Boolean)
extern void NetworkIdentity_OnDeserializeAllSafely_m09CD02B14342498B755DCEA0B498E4085CE45699 (void);
// 0x000001DE System.Void Mirror.NetworkIdentity::HandleRemoteCall(System.Int32,System.Int32,Mirror.MirrorInvokeType,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void NetworkIdentity_HandleRemoteCall_m7A6C4A9C31DCAA6261EF0F6193BC8CF6EB3AF792 (void);
// 0x000001DF Mirror.RemoteCalls.CommandInfo Mirror.NetworkIdentity::GetCommandInfo(System.Int32,System.Int32)
extern void NetworkIdentity_GetCommandInfo_mC6246232E15D3E658D7240777C91406D12D0B7D0 (void);
// 0x000001E0 System.Void Mirror.NetworkIdentity::ClearObservers()
extern void NetworkIdentity_ClearObservers_m989F20077372E5E0E1248D5DB8865BF1BED11216 (void);
// 0x000001E1 System.Void Mirror.NetworkIdentity::AddObserver(Mirror.NetworkConnection)
extern void NetworkIdentity_AddObserver_m5CFA5EF3EE411EA32D356BE89299078072CA5B65 (void);
// 0x000001E2 System.Boolean Mirror.NetworkIdentity::AssignClientAuthority(Mirror.NetworkConnection)
extern void NetworkIdentity_AssignClientAuthority_m4949EA554E5308A38FECB4D27901E812EBDB985E (void);
// 0x000001E3 System.Void Mirror.NetworkIdentity::RemoveClientAuthority()
extern void NetworkIdentity_RemoveClientAuthority_mB0A56953931D826CA1ACF66DF53F98B961674E8C (void);
// 0x000001E4 System.Void Mirror.NetworkIdentity::Reset()
extern void NetworkIdentity_Reset_m883C3F9B8DE395014DB12FE2787BA4975BA3E50E (void);
// 0x000001E5 System.Void Mirror.NetworkIdentity::ClearAllComponentsDirtyBits()
extern void NetworkIdentity_ClearAllComponentsDirtyBits_m98279299BDD30A3A319544445922E128065FCFEB (void);
// 0x000001E6 System.Void Mirror.NetworkIdentity::ClearDirtyComponentsDirtyBits()
extern void NetworkIdentity_ClearDirtyComponentsDirtyBits_m02EBEB1FF48E0892B9E33863D1ABE6200B916FD9 (void);
// 0x000001E7 System.Void Mirror.NetworkIdentity::ResetSyncObjects()
extern void NetworkIdentity_ResetSyncObjects_m01A06B2CED1EFF81D34B22058741D54AADDCB2BC (void);
// 0x000001E8 System.Void Mirror.NetworkIdentity::.ctor()
extern void NetworkIdentity__ctor_m4105C071BA18EBD036577EB3F22D2D346A512A28 (void);
// 0x000001E9 System.Void Mirror.NetworkIdentity::.cctor()
extern void NetworkIdentity__cctor_m0715F33040D20006B1E16209699F5A4BA7F5FE5E (void);
// 0x000001EA System.Void Mirror.NetworkIdentity/ClientAuthorityCallback::.ctor(System.Object,System.IntPtr)
extern void ClientAuthorityCallback__ctor_m5CB257AFC43DEA6B8CF7BDC3B8F05DB42B6BD7C7 (void);
// 0x000001EB System.Void Mirror.NetworkIdentity/ClientAuthorityCallback::Invoke(Mirror.NetworkConnection,Mirror.NetworkIdentity,System.Boolean)
extern void ClientAuthorityCallback_Invoke_m7D934B03E2A5EECD750AEA84F0581DFCAB5FCAB5 (void);
// 0x000001EC System.IAsyncResult Mirror.NetworkIdentity/ClientAuthorityCallback::BeginInvoke(Mirror.NetworkConnection,Mirror.NetworkIdentity,System.Boolean,System.AsyncCallback,System.Object)
extern void ClientAuthorityCallback_BeginInvoke_m7C864923D390D7907FAFFA752AA01B181E3DAD4E (void);
// 0x000001ED System.Void Mirror.NetworkIdentity/ClientAuthorityCallback::EndInvoke(System.IAsyncResult)
extern void ClientAuthorityCallback_EndInvoke_m9D51E32557679438EA41FEE710455EBF916C4C97 (void);
// 0x000001EE System.Int32 Mirror.NetworkLoop::FindPlayerLoopEntryIndex(UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction,UnityEngine.LowLevel.PlayerLoopSystem,System.Type)
extern void NetworkLoop_FindPlayerLoopEntryIndex_m8401214D75A51D4212D332E8E5CB5D9337B27FEE (void);
// 0x000001EF System.Boolean Mirror.NetworkLoop::AddToPlayerLoop(UnityEngine.LowLevel.PlayerLoopSystem/UpdateFunction,System.Type,UnityEngine.LowLevel.PlayerLoopSystem&,System.Type,Mirror.NetworkLoop/AddMode)
extern void NetworkLoop_AddToPlayerLoop_mA203DBCE1BA2E03091F1A59F981936532FFC3101 (void);
// 0x000001F0 System.Void Mirror.NetworkLoop::RuntimeInitializeOnLoad()
extern void NetworkLoop_RuntimeInitializeOnLoad_m9DCD6DC6C58A6A3E1D750A2BE7B910D9C4C013D8 (void);
// 0x000001F1 System.Void Mirror.NetworkLoop::NetworkEarlyUpdate()
extern void NetworkLoop_NetworkEarlyUpdate_m45238F04DFD59A6D6937BCE61DEA4EB5C512141F (void);
// 0x000001F2 System.Void Mirror.NetworkLoop::NetworkLateUpdate()
extern void NetworkLoop_NetworkLateUpdate_m5A9039F8703B7D81FC5CD97674E42F9EFE90007E (void);
// 0x000001F3 System.Void Mirror.NetworkLoop/<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m15643106A40EEFAFAC816B59F96EBCB72FA70518 (void);
// 0x000001F4 System.Boolean Mirror.NetworkLoop/<>c__DisplayClass1_0::<FindPlayerLoopEntryIndex>b__0(UnityEngine.LowLevel.PlayerLoopSystem)
extern void U3CU3Ec__DisplayClass1_0_U3CFindPlayerLoopEntryIndexU3Eb__0_m3154F15A533CFE0182B0AE7D57B513F9407D351D (void);
// 0x000001F5 Mirror.NetworkManager Mirror.NetworkManager::get_singleton()
extern void NetworkManager_get_singleton_m3687E70BBF51C41E6F20B606EF0E0E40D13E7641 (void);
// 0x000001F6 System.Void Mirror.NetworkManager::set_singleton(Mirror.NetworkManager)
extern void NetworkManager_set_singleton_m3E3F67766BFA7EFD8A3360377DEABEAAFCC271F9 (void);
// 0x000001F7 System.Int32 Mirror.NetworkManager::get_numPlayers()
extern void NetworkManager_get_numPlayers_mA4CC3D7D3A2C4967F21E4EC6554CADB2EB239816 (void);
// 0x000001F8 Mirror.NetworkManagerMode Mirror.NetworkManager::get_mode()
extern void NetworkManager_get_mode_m7DDDC4AFB7EC130F64E6BA4E916235B46C338337 (void);
// 0x000001F9 System.Void Mirror.NetworkManager::set_mode(Mirror.NetworkManagerMode)
extern void NetworkManager_set_mode_mFD99BDFC3978E7B505C6062BBB31BFDE7A3751FF (void);
// 0x000001FA System.Void Mirror.NetworkManager::OnValidate()
extern void NetworkManager_OnValidate_m1E5FDC67423166450D021672C314C8346B0CECA0 (void);
// 0x000001FB System.Void Mirror.NetworkManager::Awake()
extern void NetworkManager_Awake_mED76ED7E0D4767AD1F7ADB7E845C2679E84E6B8A (void);
// 0x000001FC System.Void Mirror.NetworkManager::Start()
extern void NetworkManager_Start_mD2069EBE9124584CAF85D88EEC28A841F386A4F0 (void);
// 0x000001FD System.Void Mirror.NetworkManager::LateUpdate()
extern void NetworkManager_LateUpdate_m5A729F6E28B3C3D49254D02A1A6BACFC9255A8F2 (void);
// 0x000001FE System.Boolean Mirror.NetworkManager::IsServerOnlineSceneChangeNeeded()
extern void NetworkManager_IsServerOnlineSceneChangeNeeded_m635D78E9B5D3883BC0DB0526DC8BE88F4C9FB9C4 (void);
// 0x000001FF System.Boolean Mirror.NetworkManager::IsSceneActive(System.String)
extern void NetworkManager_IsSceneActive_mA50E21AD716E48D445A23383FFD249D21149F3E2 (void);
// 0x00000200 System.Void Mirror.NetworkManager::SetupServer()
extern void NetworkManager_SetupServer_m9D6BCC8D75D3F91E34218B4DCFA6EE3E813CD2F0 (void);
// 0x00000201 System.Void Mirror.NetworkManager::StartServer()
extern void NetworkManager_StartServer_m7F3D716905D518790D749225F100D0841051B017 (void);
// 0x00000202 System.Void Mirror.NetworkManager::StartClient()
extern void NetworkManager_StartClient_m76D46DACCCD2C70BB78014AD376FD99D91EC36C3 (void);
// 0x00000203 System.Void Mirror.NetworkManager::StartClient(System.Uri)
extern void NetworkManager_StartClient_mC4B472F3B09F109813AD8B7AC133D0475BBCECAE (void);
// 0x00000204 System.Void Mirror.NetworkManager::StartHost()
extern void NetworkManager_StartHost_m075A9DF45CC23B3CE251028E7258583AB32B5710 (void);
// 0x00000205 System.Void Mirror.NetworkManager::FinishStartHost()
extern void NetworkManager_FinishStartHost_mA7C04417B8D8E5FCF593CC8BDAD1B7A1316B0B6E (void);
// 0x00000206 System.Void Mirror.NetworkManager::StartHostClient()
extern void NetworkManager_StartHostClient_m63E4184E021964794FB3F6BD67C3C15036203F4D (void);
// 0x00000207 System.Void Mirror.NetworkManager::StopHost()
extern void NetworkManager_StopHost_m9BE1DF0275204D698472DCB8B1DBF62FC83994CF (void);
// 0x00000208 System.Void Mirror.NetworkManager::StopServer()
extern void NetworkManager_StopServer_m796F0F4CA3CA42289EBD5F1F6CA6975C9993D988 (void);
// 0x00000209 System.Void Mirror.NetworkManager::StopClient()
extern void NetworkManager_StopClient_m749293B1F5FA88A3CBD079B8A5A45B6400F3FE47 (void);
// 0x0000020A System.Void Mirror.NetworkManager::OnApplicationQuit()
extern void NetworkManager_OnApplicationQuit_m720A6D0FE0EC77E009A55942C2FB006CE9253DA0 (void);
// 0x0000020B System.Void Mirror.NetworkManager::ConfigureServerFrameRate()
extern void NetworkManager_ConfigureServerFrameRate_m0940490CF1C0798B5B0CE00DAE010C0C0939A28B (void);
// 0x0000020C System.Boolean Mirror.NetworkManager::InitializeSingleton()
extern void NetworkManager_InitializeSingleton_m2C2E16FAF3FC4D0F4246B344C83E2F864DB21F71 (void);
// 0x0000020D System.Void Mirror.NetworkManager::RegisterServerMessages()
extern void NetworkManager_RegisterServerMessages_m62B2385AE088F6F8C8708221989D9E8601FB025C (void);
// 0x0000020E System.Void Mirror.NetworkManager::RegisterClientMessages()
extern void NetworkManager_RegisterClientMessages_m59E1AB1EC3667065785D052268B8DC5115B18091 (void);
// 0x0000020F System.Void Mirror.NetworkManager::Shutdown()
extern void NetworkManager_Shutdown_m8E5824821C3188358A8683640CB208728597B9F7 (void);
// 0x00000210 System.Void Mirror.NetworkManager::OnDestroy()
extern void NetworkManager_OnDestroy_m4BA90829754CAA8DBC58D177E18D6AE8E4F0842D (void);
// 0x00000211 System.String Mirror.NetworkManager::get_networkSceneName()
extern void NetworkManager_get_networkSceneName_m80DB5F4473BF7D3CCD9F5A5F3E0F2DDCEA7D76BD (void);
// 0x00000212 System.Void Mirror.NetworkManager::set_networkSceneName(System.String)
extern void NetworkManager_set_networkSceneName_mBEC371D12F3669C066AC523EB7FD6A9885BB3685 (void);
// 0x00000213 System.Void Mirror.NetworkManager::ServerChangeScene(System.String)
extern void NetworkManager_ServerChangeScene_mE7B47AD1D7BA092859F28E4500046C8B6BF52D8B (void);
// 0x00000214 System.Void Mirror.NetworkManager::ClientChangeScene(System.String,Mirror.SceneOperation,System.Boolean)
extern void NetworkManager_ClientChangeScene_m6F6D640C2E6D989B53DA115B766555057D279B23 (void);
// 0x00000215 System.Void Mirror.NetworkManager::OnSceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void NetworkManager_OnSceneLoaded_mB69266EA76D21E1DA2CC2364C9EA4D4D1D2971F2 (void);
// 0x00000216 System.Void Mirror.NetworkManager::UpdateScene()
extern void NetworkManager_UpdateScene_m0DC87E5D4A8881A2363E79F42C71C14C0B0E31E0 (void);
// 0x00000217 System.Void Mirror.NetworkManager::FinishLoadScene()
extern void NetworkManager_FinishLoadScene_m2EF8C54A639E7BE29E9FC587B3CE651C96602AF4 (void);
// 0x00000218 System.Void Mirror.NetworkManager::FinishLoadSceneHost()
extern void NetworkManager_FinishLoadSceneHost_m055894F1A6AA4D805395C8302D7861744A9FC931 (void);
// 0x00000219 System.Void Mirror.NetworkManager::FinishLoadSceneServerOnly()
extern void NetworkManager_FinishLoadSceneServerOnly_mBF8ABAF755EE1A2887DBF1B9E0D8946BEF6B6EB9 (void);
// 0x0000021A System.Void Mirror.NetworkManager::FinishLoadSceneClientOnly()
extern void NetworkManager_FinishLoadSceneClientOnly_mDC883960B695D08D84046B1584F4D1766D7FEEF2 (void);
// 0x0000021B System.Void Mirror.NetworkManager::RegisterStartPosition(UnityEngine.Transform)
extern void NetworkManager_RegisterStartPosition_mDE79A6E850F2627C193B28E33307C73C120B7C8E (void);
// 0x0000021C System.Void Mirror.NetworkManager::UnRegisterStartPosition(UnityEngine.Transform)
extern void NetworkManager_UnRegisterStartPosition_mC51E52EFA2E8B98D0077FA9D654C7548F12CED28 (void);
// 0x0000021D UnityEngine.Transform Mirror.NetworkManager::GetStartPosition()
extern void NetworkManager_GetStartPosition_m25078697FA64905AF7618A26E75ED10750B1A754 (void);
// 0x0000021E System.Void Mirror.NetworkManager::OnServerConnectInternal(Mirror.NetworkConnection)
extern void NetworkManager_OnServerConnectInternal_m22C84AA41EF747A326DB68A65839E2C7DD75EE2E (void);
// 0x0000021F System.Void Mirror.NetworkManager::OnServerAuthenticated(Mirror.NetworkConnection)
extern void NetworkManager_OnServerAuthenticated_m18A8F6EB0B0104AFEC3F6856BD7FC3A0DD8364B0 (void);
// 0x00000220 System.Void Mirror.NetworkManager::OnServerDisconnectInternal(Mirror.NetworkConnection)
extern void NetworkManager_OnServerDisconnectInternal_m79DA7FE2A9522BA50C0BF4C3CCC083DAC0B9C56A (void);
// 0x00000221 System.Void Mirror.NetworkManager::OnServerReadyMessageInternal(Mirror.NetworkConnection,Mirror.ReadyMessage)
extern void NetworkManager_OnServerReadyMessageInternal_m839A57A605435CEB8C94D114B5DE4A116A0B19F2 (void);
// 0x00000222 System.Void Mirror.NetworkManager::OnServerAddPlayerInternal(Mirror.NetworkConnection,Mirror.AddPlayerMessage)
extern void NetworkManager_OnServerAddPlayerInternal_m9928BD170680E3DBD9ACBC2D33C4025468A7BBE4 (void);
// 0x00000223 System.Void Mirror.NetworkManager::OnClientConnectInternal()
extern void NetworkManager_OnClientConnectInternal_m3598A44D5ED77226B51464DE553F99537ED7085A (void);
// 0x00000224 System.Void Mirror.NetworkManager::OnClientAuthenticated(Mirror.NetworkConnection)
extern void NetworkManager_OnClientAuthenticated_m605807F8A208467EDB8DA114F6400BA496EEE53E (void);
// 0x00000225 System.Void Mirror.NetworkManager::OnClientDisconnectInternal()
extern void NetworkManager_OnClientDisconnectInternal_mBABA2400A07727928C4DAFD31AD0F6C518CC516D (void);
// 0x00000226 System.Void Mirror.NetworkManager::OnClientNotReadyMessageInternal(Mirror.NotReadyMessage)
extern void NetworkManager_OnClientNotReadyMessageInternal_m59761D864BB5B6E5AB9E8773F5B3084B7BF3C097 (void);
// 0x00000227 System.Void Mirror.NetworkManager::OnClientSceneInternal(Mirror.SceneMessage)
extern void NetworkManager_OnClientSceneInternal_m577FA355F23D1BBA446A69CD950CF45FCAD34019 (void);
// 0x00000228 System.Void Mirror.NetworkManager::OnServerConnect(Mirror.NetworkConnection)
extern void NetworkManager_OnServerConnect_m35D78B9144A937ADDD1FEA17E15A8DE6DF606AB6 (void);
// 0x00000229 System.Void Mirror.NetworkManager::OnServerDisconnect(Mirror.NetworkConnection)
extern void NetworkManager_OnServerDisconnect_m2A4B300BE2D36D5FFFA0C7AF59296E527CB10EE2 (void);
// 0x0000022A System.Void Mirror.NetworkManager::OnServerReady(Mirror.NetworkConnection)
extern void NetworkManager_OnServerReady_m23B3C0B320257CA426A134ACC6086945E856EF8B (void);
// 0x0000022B System.Void Mirror.NetworkManager::OnServerAddPlayer(Mirror.NetworkConnection)
extern void NetworkManager_OnServerAddPlayer_m2E0E936F41E55AFB82D0AB9908A3918A60F237DC (void);
// 0x0000022C System.Void Mirror.NetworkManager::OnServerError(Mirror.NetworkConnection,System.Int32)
extern void NetworkManager_OnServerError_mB7621EB1EBDE940902E7658C9382F56B23158493 (void);
// 0x0000022D System.Void Mirror.NetworkManager::OnServerChangeScene(System.String)
extern void NetworkManager_OnServerChangeScene_m78A746FB47504CEFACC6AF32A27B7C22C21ECC0A (void);
// 0x0000022E System.Void Mirror.NetworkManager::OnServerSceneChanged(System.String)
extern void NetworkManager_OnServerSceneChanged_m3A6AD198811FBCA588A5B4429A003180AEE8A08B (void);
// 0x0000022F System.Void Mirror.NetworkManager::OnClientConnect(Mirror.NetworkConnection)
extern void NetworkManager_OnClientConnect_m23A364AC2D7C3AEE64B43C25453A7BC2378CCC1A (void);
// 0x00000230 System.Void Mirror.NetworkManager::OnClientDisconnect(Mirror.NetworkConnection)
extern void NetworkManager_OnClientDisconnect_m02FA88A6123126271FF5EB97903A4149A0D5D69B (void);
// 0x00000231 System.Void Mirror.NetworkManager::OnClientError(Mirror.NetworkConnection,System.Int32)
extern void NetworkManager_OnClientError_mCBD5732D0CB7A41DD608F8169F0B3A0F6BCE0EDC (void);
// 0x00000232 System.Void Mirror.NetworkManager::OnClientNotReady(Mirror.NetworkConnection)
extern void NetworkManager_OnClientNotReady_m1378C1AD4DCB54A286F0A35EFC73287C8A2A2C7F (void);
// 0x00000233 System.Void Mirror.NetworkManager::OnClientChangeScene(System.String,Mirror.SceneOperation,System.Boolean)
extern void NetworkManager_OnClientChangeScene_mA38EC559CA0D4CFA58ACD65B6E0568683748F411 (void);
// 0x00000234 System.Void Mirror.NetworkManager::OnClientSceneChanged(Mirror.NetworkConnection)
extern void NetworkManager_OnClientSceneChanged_mE627B70A20ABD429D2D8F8C3D0E32F70323442AE (void);
// 0x00000235 System.Void Mirror.NetworkManager::OnStartHost()
extern void NetworkManager_OnStartHost_m377F4A7C84B04AB4F90A786995899EA7EE38B3CE (void);
// 0x00000236 System.Void Mirror.NetworkManager::OnStartServer()
extern void NetworkManager_OnStartServer_m05E1F97124AFA01D00BC671476F834089724C97A (void);
// 0x00000237 System.Void Mirror.NetworkManager::OnStartClient()
extern void NetworkManager_OnStartClient_m6C90B4D7405831A55142A14BB05BD89154116C24 (void);
// 0x00000238 System.Void Mirror.NetworkManager::OnStopServer()
extern void NetworkManager_OnStopServer_mBE7B67DF4F17E2302A423B6BAAF6C3A137DCA77D (void);
// 0x00000239 System.Void Mirror.NetworkManager::OnStopClient()
extern void NetworkManager_OnStopClient_m84A76651AF99B829787A74F417B32D44688681BD (void);
// 0x0000023A System.Void Mirror.NetworkManager::OnStopHost()
extern void NetworkManager_OnStopHost_mF7DF15C4F927F247E625E3F9B97962190AF08F0F (void);
// 0x0000023B System.Void Mirror.NetworkManager::.ctor()
extern void NetworkManager__ctor_m831DF4BB5F616C10CC2B272AD9DA2C660B3A1924 (void);
// 0x0000023C System.Void Mirror.NetworkManager::.cctor()
extern void NetworkManager__cctor_mB48BA1AD4B577F594D044976E4F190A877F0198F (void);
// 0x0000023D System.Void Mirror.NetworkManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m37D13BDF4CBED4CBCBB1249A2281187B67BA788E (void);
// 0x0000023E System.Void Mirror.NetworkManager/<>c::.ctor()
extern void U3CU3Ec__ctor_m450CE0D75EBA856B0B9F8B7F888C07156E680F20 (void);
// 0x0000023F System.Boolean Mirror.NetworkManager/<>c::<get_numPlayers>b__26_0(System.Collections.Generic.KeyValuePair`2<System.Int32,Mirror.NetworkConnectionToClient>)
extern void U3CU3Ec_U3Cget_numPlayersU3Eb__26_0_m2B608786386286EAFF249E77B10E54B6CE68914D (void);
// 0x00000240 System.Boolean Mirror.NetworkManager/<>c::<RegisterClientMessages>b__55_0(UnityEngine.GameObject)
extern void U3CU3Ec_U3CRegisterClientMessagesU3Eb__55_0_m58EE1182E5BABAAE81670C7F35A2DDD6B47F2319 (void);
// 0x00000241 System.Int32 Mirror.NetworkManager/<>c::<RegisterStartPosition>b__72_0(UnityEngine.Transform)
extern void U3CU3Ec_U3CRegisterStartPositionU3Eb__72_0_mF7915E6A5ADA857C925F8240D62090FB1C0F3924 (void);
// 0x00000242 System.Boolean Mirror.NetworkManager/<>c::<GetStartPosition>b__74_0(UnityEngine.Transform)
extern void U3CU3Ec_U3CGetStartPositionU3Eb__74_0_m05AFD0C503A61EC61B25F88CE277A173C0AD071A (void);
// 0x00000243 System.Void Mirror.NetworkManagerHUD::Awake()
extern void NetworkManagerHUD_Awake_m4EA9A71E137E6FB2CF2F69CD1355B8FFAEB2C91A (void);
// 0x00000244 System.Void Mirror.NetworkManagerHUD::OnGUI()
extern void NetworkManagerHUD_OnGUI_mF1BD1A2A022586475B91CC9491377150601855E4 (void);
// 0x00000245 System.Void Mirror.NetworkManagerHUD::StartButtons()
extern void NetworkManagerHUD_StartButtons_m77E114195F788922578CAEAD16F04C8529070859 (void);
// 0x00000246 System.Void Mirror.NetworkManagerHUD::StatusLabels()
extern void NetworkManagerHUD_StatusLabels_m5BC7A80BFA34A4CDEB4F8DFF207766A36CFD6B56 (void);
// 0x00000247 System.Void Mirror.NetworkManagerHUD::StopButtons()
extern void NetworkManagerHUD_StopButtons_mCDF2E1FC08A56AF9904AFD9CCC18172E7EFEFF50 (void);
// 0x00000248 System.Void Mirror.NetworkManagerHUD::.ctor()
extern void NetworkManagerHUD__ctor_mB218F6DAB2DD70F06B61CE59E7A6B40160BE5146 (void);
// 0x00000249 System.Int32 Mirror.NetworkReader::get_Length()
extern void NetworkReader_get_Length_mD4443E7D7B3E49740A8D400B1CA1FC6D13D26D4B (void);
// 0x0000024A System.Void Mirror.NetworkReader::.ctor(System.Byte[])
extern void NetworkReader__ctor_m7472356275E51A91FEE8FEF118A6AF2240F6417D (void);
// 0x0000024B System.Void Mirror.NetworkReader::.ctor(System.ArraySegment`1<System.Byte>)
extern void NetworkReader__ctor_m73E59DA6A9DC39831B7EAC72DD9F9B29BA0918C0 (void);
// 0x0000024C System.Byte Mirror.NetworkReader::ReadByte()
extern void NetworkReader_ReadByte_m5CEA57059C34AA6DC218D6F09207C5188AEA68A8 (void);
// 0x0000024D System.Byte[] Mirror.NetworkReader::ReadBytes(System.Byte[],System.Int32)
extern void NetworkReader_ReadBytes_m8A6348412F267570291A4BD93F4DF1715B35077D (void);
// 0x0000024E System.ArraySegment`1<System.Byte> Mirror.NetworkReader::ReadBytesSegment(System.Int32)
extern void NetworkReader_ReadBytesSegment_mD99D152684B83B9AF600E28FB6642FAB3B4B4CCF (void);
// 0x0000024F System.String Mirror.NetworkReader::ToString()
extern void NetworkReader_ToString_mFEA276025738C2F5F0D5BEE70952B19D278380A0 (void);
// 0x00000250 T Mirror.NetworkReader::Read()
// 0x00000251 System.Byte Mirror.NetworkReaderExtensions::ReadByte(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadByte_m86E3D12D2C1E2C447A4C2F20B808117DAA69571C (void);
// 0x00000252 System.SByte Mirror.NetworkReaderExtensions::ReadSByte(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadSByte_m8D3F2D88094079D5E0495FBBB3815DBAECB71CC1 (void);
// 0x00000253 System.Char Mirror.NetworkReaderExtensions::ReadChar(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadChar_m1CD7BF565346230DFE2B29A62818C0DF4ED89AFD (void);
// 0x00000254 System.Boolean Mirror.NetworkReaderExtensions::ReadBoolean(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBoolean_mA9AED23EBC6083FDFFD749EED46F03BAFC6F0ED8 (void);
// 0x00000255 System.Int16 Mirror.NetworkReaderExtensions::ReadInt16(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadInt16_m125C98DDE87D530D04BA663BCA9CF9FADC69731D (void);
// 0x00000256 System.UInt16 Mirror.NetworkReaderExtensions::ReadUInt16(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUInt16_mA7151420E3D5805C98EE41B169CAA0DB313B33CD (void);
// 0x00000257 System.Int32 Mirror.NetworkReaderExtensions::ReadInt32(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadInt32_mE0BA8E4FD647F4BC6F020B2480AF3EE8872A64A3 (void);
// 0x00000258 System.UInt32 Mirror.NetworkReaderExtensions::ReadUInt32(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUInt32_mF65B5680DA763162DCD1BCB53B225C38ACAE3C08 (void);
// 0x00000259 System.Int64 Mirror.NetworkReaderExtensions::ReadInt64(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadInt64_mAD215AD6D6571611F19D26468F422D93EE3029C2 (void);
// 0x0000025A System.UInt64 Mirror.NetworkReaderExtensions::ReadUInt64(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUInt64_mF8CBCE7593E37A4C819D8D5461746D1360975252 (void);
// 0x0000025B System.Single Mirror.NetworkReaderExtensions::ReadSingle(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadSingle_m2DF644789D260209004F87C7996C41D6022D542F (void);
// 0x0000025C System.Double Mirror.NetworkReaderExtensions::ReadDouble(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadDouble_m5800AF1FD52D7DB9047FE7B58A0BD38E23C94281 (void);
// 0x0000025D System.Decimal Mirror.NetworkReaderExtensions::ReadDecimal(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadDecimal_m0AE7D5057B858DB3A6C838CF3DF716707F3FA1EC (void);
// 0x0000025E System.String Mirror.NetworkReaderExtensions::ReadString(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadString_mA74E4612F529683A4AEEDB55C369CDEA32D222CC (void);
// 0x0000025F System.Byte[] Mirror.NetworkReaderExtensions::ReadBytesAndSize(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBytesAndSize_m2EED0B100EFEDF9E1A4D2E9A429DBF3D4F8EA623 (void);
// 0x00000260 System.ArraySegment`1<System.Byte> Mirror.NetworkReaderExtensions::ReadBytesAndSizeSegment(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadBytesAndSizeSegment_m1750D52A666869DA2EC3C3A25E767834B9DA9E2E (void);
// 0x00000261 UnityEngine.Vector2 Mirror.NetworkReaderExtensions::ReadVector2(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector2_mADCA4D375C619EE39C7ED83BC352C870897FEF6F (void);
// 0x00000262 UnityEngine.Vector3 Mirror.NetworkReaderExtensions::ReadVector3(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector3_mD9D5C83CB3177994353C71C8BC71495A79288416 (void);
// 0x00000263 UnityEngine.Vector4 Mirror.NetworkReaderExtensions::ReadVector4(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector4_m26621D658FD76A6A4D8EE73CE81739620A706997 (void);
// 0x00000264 UnityEngine.Vector2Int Mirror.NetworkReaderExtensions::ReadVector2Int(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector2Int_m0B0397F7BA2F4DE1E1A555E9EFEE676C38ECE40C (void);
// 0x00000265 UnityEngine.Vector3Int Mirror.NetworkReaderExtensions::ReadVector3Int(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadVector3Int_mD9A574478D0EC900DB9C50B000AE39148A429F68 (void);
// 0x00000266 UnityEngine.Color Mirror.NetworkReaderExtensions::ReadColor(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadColor_m21E0B16FA466A4FDAEC8BE1C80E47C9BAE3217A7 (void);
// 0x00000267 UnityEngine.Color32 Mirror.NetworkReaderExtensions::ReadColor32(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadColor32_m7926AFB64529E5FC7C51D390468AAB86722D01F7 (void);
// 0x00000268 UnityEngine.Quaternion Mirror.NetworkReaderExtensions::ReadQuaternion(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadQuaternion_m2946D7DD55E1ED8EDEF82536F0D4246D42A22E19 (void);
// 0x00000269 UnityEngine.Rect Mirror.NetworkReaderExtensions::ReadRect(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadRect_m75BCEF76E5C1CFEBE794A2FE5F52AA55DFBF3F85 (void);
// 0x0000026A UnityEngine.Plane Mirror.NetworkReaderExtensions::ReadPlane(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadPlane_m5D8E59014B3ADD6108143CBB2C25DAA1DDD291C5 (void);
// 0x0000026B UnityEngine.Ray Mirror.NetworkReaderExtensions::ReadRay(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadRay_mD2A5A24032CB890240D7F5D94E8399E2766C05EA (void);
// 0x0000026C UnityEngine.Matrix4x4 Mirror.NetworkReaderExtensions::ReadMatrix4x4(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadMatrix4x4_m8C9D3E72F015488DA355F6E26FDCF6E0C52FC7D8 (void);
// 0x0000026D System.Byte[] Mirror.NetworkReaderExtensions::ReadBytes(Mirror.NetworkReader,System.Int32)
extern void NetworkReaderExtensions_ReadBytes_m256AB25CABB792B276088B8929BBD2BC972719F0 (void);
// 0x0000026E System.Guid Mirror.NetworkReaderExtensions::ReadGuid(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadGuid_m4BEDE3B6CE956D9B64BFF779754151F49B7A82CE (void);
// 0x0000026F UnityEngine.Transform Mirror.NetworkReaderExtensions::ReadTransform(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadTransform_m81B5B4E6C64D5C011B52262BDCDB1F34E0B50034 (void);
// 0x00000270 UnityEngine.GameObject Mirror.NetworkReaderExtensions::ReadGameObject(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadGameObject_m50C52A705CA8F13E6B80746CB3F001CD82D42404 (void);
// 0x00000271 Mirror.NetworkIdentity Mirror.NetworkReaderExtensions::ReadNetworkIdentity(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadNetworkIdentity_m52CEFB599F366ADD367C0C23CFA163EBB0F6C764 (void);
// 0x00000272 Mirror.NetworkBehaviour Mirror.NetworkReaderExtensions::ReadNetworkBehaviour(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadNetworkBehaviour_mAEDC6923777D5F941348F24C73586BB93353E492 (void);
// 0x00000273 T Mirror.NetworkReaderExtensions::ReadNetworkBehaviour(Mirror.NetworkReader)
// 0x00000274 Mirror.NetworkBehaviour/NetworkBehaviourSyncVar Mirror.NetworkReaderExtensions::ReadNetworkBehaviourSyncVar(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadNetworkBehaviourSyncVar_mC735015AC984ED517FB5EC9469C0A70181E6838F (void);
// 0x00000275 System.Collections.Generic.List`1<T> Mirror.NetworkReaderExtensions::ReadList(Mirror.NetworkReader)
// 0x00000276 T[] Mirror.NetworkReaderExtensions::ReadArray(Mirror.NetworkReader)
// 0x00000277 System.Uri Mirror.NetworkReaderExtensions::ReadUri(Mirror.NetworkReader)
extern void NetworkReaderExtensions_ReadUri_m4D5B6761CC4A07C3E7F223B2DDF88EF4CACC3A3B (void);
// 0x00000278 System.Void Mirror.NetworkReaderExtensions::.cctor()
extern void NetworkReaderExtensions__cctor_m40E45B62701A8A5F9F8DAA0544C08D5A4FF20A29 (void);
// 0x00000279 System.Void Mirror.PooledNetworkReader::.ctor(System.Byte[])
extern void PooledNetworkReader__ctor_mEAE1DCDFBAC4CCFA4D40970DE4D20B36931D656E (void);
// 0x0000027A System.Void Mirror.PooledNetworkReader::.ctor(System.ArraySegment`1<System.Byte>)
extern void PooledNetworkReader__ctor_m576FB263D8FEA8501D96F220390DC912FEB24762 (void);
// 0x0000027B System.Void Mirror.PooledNetworkReader::Dispose()
extern void PooledNetworkReader_Dispose_mC35EBC46EDE36DB1EBCF9AFDDCE4375618D32A15 (void);
// 0x0000027C Mirror.PooledNetworkReader Mirror.NetworkReaderPool::GetReader(System.Byte[])
extern void NetworkReaderPool_GetReader_m951A0F2DA2D9D9B6DB00A2F9540BEC414D2DE21C (void);
// 0x0000027D Mirror.PooledNetworkReader Mirror.NetworkReaderPool::GetReader(System.ArraySegment`1<System.Byte>)
extern void NetworkReaderPool_GetReader_mE32ACECE58AE2B54D448549DCA0D920D84DAF79E (void);
// 0x0000027E System.Void Mirror.NetworkReaderPool::Recycle(Mirror.PooledNetworkReader)
extern void NetworkReaderPool_Recycle_mAD1BB938E53E18CF168736FB5256680DBBFD1535 (void);
// 0x0000027F System.Void Mirror.NetworkReaderPool::.cctor()
extern void NetworkReaderPool__cctor_m7818A15817331E4274A35067A7FC010DD07871F6 (void);
// 0x00000280 System.Void Mirror.NetworkReaderPool/<>c::.cctor()
extern void U3CU3Ec__cctor_m312E11FBBE279F10D9D2B17EFB882AA1B0920D12 (void);
// 0x00000281 System.Void Mirror.NetworkReaderPool/<>c::.ctor()
extern void U3CU3Ec__ctor_mDC5278341D19B9BC6F238AF069FB094885AB02D5 (void);
// 0x00000282 Mirror.PooledNetworkReader Mirror.NetworkReaderPool/<>c::<.cctor>b__4_0()
extern void U3CU3Ec_U3C_cctorU3Eb__4_0_m7B2E2E667B272E9C7BD040EDDA22D56ABB94241D (void);
// 0x00000283 Mirror.NetworkConnectionToClient Mirror.NetworkServer::get_localConnection()
extern void NetworkServer_get_localConnection_m5CC8941C493528A76850E9CA0EE95BFAE18C2B7C (void);
// 0x00000284 System.Void Mirror.NetworkServer::set_localConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_set_localConnection_m121D988179415A2FEA40ADA6C77986A764A56CB8 (void);
// 0x00000285 System.Boolean Mirror.NetworkServer::get_localClientActive()
extern void NetworkServer_get_localClientActive_m9C4F4B8848458BB4A2EE6552EDE1E39792C2B3BA (void);
// 0x00000286 System.Boolean Mirror.NetworkServer::get_active()
extern void NetworkServer_get_active_mF055B10F741C963266FE30D5667E781DBF44DEEE (void);
// 0x00000287 System.Void Mirror.NetworkServer::set_active(System.Boolean)
extern void NetworkServer_set_active_m964AD097B146622B9B4439D01DE79A734F658CD6 (void);
// 0x00000288 System.Void Mirror.NetworkServer::Initialize()
extern void NetworkServer_Initialize_m3ACC3E103BE21A4A4911B20175B6D05B11A95525 (void);
// 0x00000289 System.Void Mirror.NetworkServer::AddTransportHandlers()
extern void NetworkServer_AddTransportHandlers_m978B1B0770E285B67A248262C957CD4E1605984A (void);
// 0x0000028A System.Void Mirror.NetworkServer::ActivateHostScene()
extern void NetworkServer_ActivateHostScene_m457CCCEFB3E319567D22F5AFC7209B6A153DE976 (void);
// 0x0000028B System.Void Mirror.NetworkServer::RegisterMessageHandlers()
extern void NetworkServer_RegisterMessageHandlers_mA9D72F9E0CB28A698139D5BEBC3D4764627802B9 (void);
// 0x0000028C System.Void Mirror.NetworkServer::Listen(System.Int32)
extern void NetworkServer_Listen_m0A131ED708A5EE255FC8A2B79C35017D78C9BF18 (void);
// 0x0000028D System.Void Mirror.NetworkServer::CleanupNetworkIdentities()
extern void NetworkServer_CleanupNetworkIdentities_m1A67FCABDDBBF0B8B9D740E88616CC9211481832 (void);
// 0x0000028E System.Void Mirror.NetworkServer::Shutdown()
extern void NetworkServer_Shutdown_mBEA3CA658B95006C84A0A81D5D1D3E47EF564EAF (void);
// 0x0000028F System.Boolean Mirror.NetworkServer::AddConnection(Mirror.NetworkConnectionToClient)
extern void NetworkServer_AddConnection_m765F75E8AF029F8D9D9E9A115663C37AA7C6C9CF (void);
// 0x00000290 System.Boolean Mirror.NetworkServer::RemoveConnection(System.Int32)
extern void NetworkServer_RemoveConnection_m1492C8648F00DB2D4C3F3DC9ABC690BF27C9299B (void);
// 0x00000291 System.Void Mirror.NetworkServer::SetLocalConnection(Mirror.LocalConnectionToClient)
extern void NetworkServer_SetLocalConnection_mEDC58E853ADC35700140B17F47106B5CCF7568AF (void);
// 0x00000292 System.Void Mirror.NetworkServer::RemoveLocalConnection()
extern void NetworkServer_RemoveLocalConnection_m378F55B5F91FCC504A7100491C3832C006EED402 (void);
// 0x00000293 System.Boolean Mirror.NetworkServer::NoExternalConnections()
extern void NetworkServer_NoExternalConnections_mFA58D0B4E2C7D1D227BBCE0815863D71B3AAF785 (void);
// 0x00000294 System.Boolean Mirror.NetworkServer::NoConnections()
extern void NetworkServer_NoConnections_m9601687D6034D96B0646C81411AAB099B74F8CEC (void);
// 0x00000295 System.Void Mirror.NetworkServer::SendToAll(T,System.Int32,System.Boolean)
// 0x00000296 System.Void Mirror.NetworkServer::SendToReady(T,System.Int32)
// 0x00000297 System.Void Mirror.NetworkServer::SendToReady(Mirror.NetworkIdentity,T,System.Boolean,System.Int32)
// 0x00000298 System.Void Mirror.NetworkServer::SendToReady(Mirror.NetworkIdentity,T,System.Int32)
// 0x00000299 System.Void Mirror.NetworkServer::SendToObservers(Mirror.NetworkIdentity,T,System.Int32)
// 0x0000029A System.Void Mirror.NetworkServer::SendToClientOfPlayer(Mirror.NetworkIdentity,T,System.Int32)
// 0x0000029B System.Void Mirror.NetworkServer::OnConnected(System.Int32)
extern void NetworkServer_OnConnected_m351F84980AEE5D8D94D16C85A0708F9431D1E748 (void);
// 0x0000029C System.Void Mirror.NetworkServer::OnConnected(Mirror.NetworkConnectionToClient)
extern void NetworkServer_OnConnected_m92037D3CFABD7D7514108F39242084325D9B8DD6 (void);
// 0x0000029D System.Void Mirror.NetworkServer::OnDataReceived(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void NetworkServer_OnDataReceived_m0A961AB9C89C79EF2F6C32F511295AEDF0DBCC49 (void);
// 0x0000029E System.Void Mirror.NetworkServer::OnDisconnected(System.Int32)
extern void NetworkServer_OnDisconnected_m38180FD30F8E845CB598CE00D002CCA8FB740F05 (void);
// 0x0000029F System.Void Mirror.NetworkServer::OnDisconnected(Mirror.NetworkConnection)
extern void NetworkServer_OnDisconnected_m6E31A50EC85B2F354B68CCCD738A22ECF5A6CD8F (void);
// 0x000002A0 System.Void Mirror.NetworkServer::OnError(System.Int32,System.Exception)
extern void NetworkServer_OnError_m3C684B7F8A69D5F77520F7F867288C34CEB52510 (void);
// 0x000002A1 System.Void Mirror.NetworkServer::RegisterHandler(System.Action`2<Mirror.NetworkConnection,T>,System.Boolean)
// 0x000002A2 System.Void Mirror.NetworkServer::RegisterHandler(System.Action`1<T>,System.Boolean)
// 0x000002A3 System.Void Mirror.NetworkServer::ReplaceHandler(System.Action`2<Mirror.NetworkConnection,T>,System.Boolean)
// 0x000002A4 System.Void Mirror.NetworkServer::ReplaceHandler(System.Action`1<T>,System.Boolean)
// 0x000002A5 System.Void Mirror.NetworkServer::UnregisterHandler()
// 0x000002A6 System.Void Mirror.NetworkServer::ClearHandlers()
extern void NetworkServer_ClearHandlers_m9CA5A2CC1467B94753025BDA3186A20249D1DD52 (void);
// 0x000002A7 System.Boolean Mirror.NetworkServer::GetNetworkIdentity(UnityEngine.GameObject,Mirror.NetworkIdentity&)
extern void NetworkServer_GetNetworkIdentity_mC661B7E843BFE80D1D590A10671B8E9D5C4A98B6 (void);
// 0x000002A8 System.Void Mirror.NetworkServer::DisconnectAll()
extern void NetworkServer_DisconnectAll_mCD86E876F1AEE03A2E835C13C0C977A937CD8A4B (void);
// 0x000002A9 System.Void Mirror.NetworkServer::DisconnectAllExternalConnections()
extern void NetworkServer_DisconnectAllExternalConnections_m1E7DD4EAB047BEB502D8D2B737B3BB53535F89FE (void);
// 0x000002AA System.Void Mirror.NetworkServer::DisconnectAllConnections()
extern void NetworkServer_DisconnectAllConnections_m064C12913B1EE79D9D70755CE09408F16349FD96 (void);
// 0x000002AB System.Boolean Mirror.NetworkServer::AddPlayerForConnection(Mirror.NetworkConnection,UnityEngine.GameObject)
extern void NetworkServer_AddPlayerForConnection_m487EFEA0D07031952F9E47E700F3E2D390AB5640 (void);
// 0x000002AC System.Boolean Mirror.NetworkServer::AddPlayerForConnection(Mirror.NetworkConnection,UnityEngine.GameObject,System.Guid)
extern void NetworkServer_AddPlayerForConnection_m5DE6FDC39EDCE7D8E031D44D9968E680E88C7623 (void);
// 0x000002AD System.Boolean Mirror.NetworkServer::ReplacePlayerForConnection(Mirror.NetworkConnection,UnityEngine.GameObject,System.Boolean)
extern void NetworkServer_ReplacePlayerForConnection_m7BBA94A2B3D39AC57CEE57D9AA6ED3C61F579D06 (void);
// 0x000002AE System.Boolean Mirror.NetworkServer::ReplacePlayerForConnection(Mirror.NetworkConnection,UnityEngine.GameObject,System.Guid,System.Boolean)
extern void NetworkServer_ReplacePlayerForConnection_m48668B43474DC6890E5395F5FBBDEF169349F687 (void);
// 0x000002AF System.Void Mirror.NetworkServer::SetClientReady(Mirror.NetworkConnection)
extern void NetworkServer_SetClientReady_mC3F0DB48250269C36FB173466FC5CB4A508D0791 (void);
// 0x000002B0 System.Void Mirror.NetworkServer::SetClientNotReady(Mirror.NetworkConnection)
extern void NetworkServer_SetClientNotReady_m33DEC070F04798EE47179E8C2C2B3F931C53054B (void);
// 0x000002B1 System.Void Mirror.NetworkServer::SetAllClientsNotReady()
extern void NetworkServer_SetAllClientsNotReady_mCD791365FA6BDA777EB2FB2865B63B0A6BEE4926 (void);
// 0x000002B2 System.Void Mirror.NetworkServer::OnClientReadyMessage(Mirror.NetworkConnection,Mirror.ReadyMessage)
extern void NetworkServer_OnClientReadyMessage_m909779CC240B0AA6C4E7B1A959C5380060264933 (void);
// 0x000002B3 System.Void Mirror.NetworkServer::ShowForConnection(Mirror.NetworkIdentity,Mirror.NetworkConnection)
extern void NetworkServer_ShowForConnection_m65FE936D0D166AA00A3D8EFB5AAA2120F54C2CAE (void);
// 0x000002B4 System.Void Mirror.NetworkServer::HideForConnection(Mirror.NetworkIdentity,Mirror.NetworkConnection)
extern void NetworkServer_HideForConnection_mD1B712AA6BF00CFC2DAAF3EAFE3A9C777AE43ED4 (void);
// 0x000002B5 System.Void Mirror.NetworkServer::RemovePlayerForConnection(Mirror.NetworkConnection,System.Boolean)
extern void NetworkServer_RemovePlayerForConnection_mE52A009050437EC470A9BF46165473ACF841A5D9 (void);
// 0x000002B6 System.Void Mirror.NetworkServer::OnCommandMessage(Mirror.NetworkConnection,Mirror.CommandMessage)
extern void NetworkServer_OnCommandMessage_mA410CAD9636B529259CED14E989D3D01DF2FAD4C (void);
// 0x000002B7 System.ArraySegment`1<System.Byte> Mirror.NetworkServer::CreateSpawnMessagePayload(System.Boolean,Mirror.NetworkIdentity,Mirror.PooledNetworkWriter,Mirror.PooledNetworkWriter)
extern void NetworkServer_CreateSpawnMessagePayload_m607B37EC828537F1A87CE12B8A077C0B861CBF34 (void);
// 0x000002B8 System.Void Mirror.NetworkServer::SendSpawnMessage(Mirror.NetworkIdentity,Mirror.NetworkConnection)
extern void NetworkServer_SendSpawnMessage_mBCD386FE79E43D7C4DC964A4D539A857C5D5D102 (void);
// 0x000002B9 System.Void Mirror.NetworkServer::SpawnObject(UnityEngine.GameObject,Mirror.NetworkConnection)
extern void NetworkServer_SpawnObject_mB3AD303BED0C815C723891DD4583AA2C289CB163 (void);
// 0x000002BA System.Void Mirror.NetworkServer::Spawn(UnityEngine.GameObject,Mirror.NetworkConnection)
extern void NetworkServer_Spawn_mE71DE744EA836EE5C4EA51B6FCDDCDB83C1D7846 (void);
// 0x000002BB System.Void Mirror.NetworkServer::Spawn(UnityEngine.GameObject,UnityEngine.GameObject)
extern void NetworkServer_Spawn_mC82932E22819C7C7682841AB3300F26151AC6D15 (void);
// 0x000002BC System.Void Mirror.NetworkServer::Spawn(UnityEngine.GameObject,System.Guid,Mirror.NetworkConnection)
extern void NetworkServer_Spawn_m14592C4F1111BE2C02BEFDBE75F31AE54150924D (void);
// 0x000002BD System.Boolean Mirror.NetworkServer::ValidateSceneObject(Mirror.NetworkIdentity)
extern void NetworkServer_ValidateSceneObject_m4ED8F63B17AAA36BAA4CB4DA2BA2E70A838EFD71 (void);
// 0x000002BE System.Boolean Mirror.NetworkServer::SpawnObjects()
extern void NetworkServer_SpawnObjects_m9C61D31EB90F43E1219F7C228B38BB01CA7F7A12 (void);
// 0x000002BF System.Void Mirror.NetworkServer::Respawn(Mirror.NetworkIdentity)
extern void NetworkServer_Respawn_mEDAC1931E0963B50A235D2A66D481898B7AE1B07 (void);
// 0x000002C0 System.Void Mirror.NetworkServer::SpawnObserversForConnection(Mirror.NetworkConnection)
extern void NetworkServer_SpawnObserversForConnection_m2DD4734C190A51C5B9FEE12ECFA5CD08E0345CBD (void);
// 0x000002C1 System.Void Mirror.NetworkServer::UnSpawn(UnityEngine.GameObject)
extern void NetworkServer_UnSpawn_m1584C6BCEE95BA77A803C51B77E166E4F94FBCC9 (void);
// 0x000002C2 System.Void Mirror.NetworkServer::DestroyPlayerForConnection(Mirror.NetworkConnection)
extern void NetworkServer_DestroyPlayerForConnection_m543D69BA46FEA3423D0F97CC5E39CBCF7172A577 (void);
// 0x000002C3 System.Void Mirror.NetworkServer::DestroyObject(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkServer_DestroyObject_m810CF5F1A12A15239105388704094176AF6523B7 (void);
// 0x000002C4 System.Void Mirror.NetworkServer::DestroyObject(UnityEngine.GameObject,System.Boolean)
extern void NetworkServer_DestroyObject_mADF3D3E802D75752CDCA05F94F0FAFEBFA5A9F93 (void);
// 0x000002C5 System.Void Mirror.NetworkServer::Destroy(UnityEngine.GameObject)
extern void NetworkServer_Destroy_m113ADC5542A4627A397C75DC3ECC9BC87F404FFB (void);
// 0x000002C6 System.Void Mirror.NetworkServer::AddAllReadyServerConnectionsToObservers(Mirror.NetworkIdentity)
extern void NetworkServer_AddAllReadyServerConnectionsToObservers_mDCCA7E9B82CA04C30C3ABA187C3306DE6939C8A1 (void);
// 0x000002C7 System.Void Mirror.NetworkServer::RebuildObserversDefault(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkServer_RebuildObserversDefault_mE396735085539EB07B92E6A9CBBCB33277C0BAFF (void);
// 0x000002C8 System.Void Mirror.NetworkServer::RebuildObserversCustom(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkServer_RebuildObserversCustom_mBB971C93348EACA1C5977913D7A7E5533043E022 (void);
// 0x000002C9 System.Void Mirror.NetworkServer::RebuildObservers(Mirror.NetworkIdentity,System.Boolean)
extern void NetworkServer_RebuildObservers_m2E49CB20FF4AF0D1EAC08AF0B0271BBEFCDCDABC (void);
// 0x000002CA System.Void Mirror.NetworkServer::NetworkEarlyUpdate()
extern void NetworkServer_NetworkEarlyUpdate_m9990AD91F269B79E8AB54FE45A4EC6251F7C457A (void);
// 0x000002CB System.Void Mirror.NetworkServer::NetworkLateUpdate()
extern void NetworkServer_NetworkLateUpdate_mAC035ABA4BB49EF98A547B2A96F3AA54F2E5D63E (void);
// 0x000002CC System.Void Mirror.NetworkServer::Update()
extern void NetworkServer_Update_mF9F69F127D306FFA0D68AB471CC67B3415D3021C (void);
// 0x000002CD System.Void Mirror.NetworkServer::.cctor()
extern void NetworkServer__cctor_mC0DCCC43EE4E77463F04C3FECCBB84579DAA6C9A (void);
// 0x000002CE System.Void Mirror.NetworkServer/<>c__DisplayClass48_0`1::.ctor()
// 0x000002CF System.Void Mirror.NetworkServer/<>c__DisplayClass48_0`1::<RegisterHandler>b__0(Mirror.NetworkConnection,T)
// 0x000002D0 System.Void Mirror.NetworkServer/<>c__DisplayClass50_0`1::.ctor()
// 0x000002D1 System.Void Mirror.NetworkServer/<>c__DisplayClass50_0`1::<ReplaceHandler>b__0(Mirror.NetworkConnection,T)
// 0x000002D2 System.Void Mirror.NetworkStartPosition::Awake()
extern void NetworkStartPosition_Awake_m957E26CD377C729F2F9442F0C737CC4729A72C47 (void);
// 0x000002D3 System.Void Mirror.NetworkStartPosition::OnDestroy()
extern void NetworkStartPosition_OnDestroy_m00BD40ED4641A481ACE5967C211E6EBF84585FC8 (void);
// 0x000002D4 System.Void Mirror.NetworkStartPosition::.ctor()
extern void NetworkStartPosition__ctor_m0630A7917AC91935D77BF5755391FB6EA6DFBFD0 (void);
// 0x000002D5 System.Void Mirror.NetworkTime::.cctor()
extern void NetworkTime__cctor_m747E3CF7CD3F32E4BFCA9975961B4C76A4737770 (void);
// 0x000002D6 System.Double Mirror.NetworkTime::LocalTime()
extern void NetworkTime_LocalTime_m3AB333530CEE96AE8CB62E7C47FBAA7C382E1CCF (void);
// 0x000002D7 System.Void Mirror.NetworkTime::Reset()
extern void NetworkTime_Reset_mCAB26017F5FA88252E547FA6551A26D6BAEF7298 (void);
// 0x000002D8 System.Void Mirror.NetworkTime::UpdateClient()
extern void NetworkTime_UpdateClient_m852C8649B8BDB874079CAD542EC213D2165D53DD (void);
// 0x000002D9 System.Void Mirror.NetworkTime::OnServerPing(Mirror.NetworkConnection,Mirror.NetworkPingMessage)
extern void NetworkTime_OnServerPing_m68FD3838E10FE31CDFC7F861EF8FE0D9C37D8B54 (void);
// 0x000002DA System.Void Mirror.NetworkTime::OnClientPong(Mirror.NetworkPongMessage)
extern void NetworkTime_OnClientPong_m44B93D45F64A2A215C28274C23018A9CADBDC253 (void);
// 0x000002DB System.Double Mirror.NetworkTime::get_time()
extern void NetworkTime_get_time_m8DE6EBB4100FF44B7A282881FD69798D737C24C6 (void);
// 0x000002DC System.Double Mirror.NetworkTime::get_timeVariance()
extern void NetworkTime_get_timeVariance_m34EB4F607490CA48E1896BE2075B9564AF922BFE (void);
// 0x000002DD System.Double Mirror.NetworkTime::get_timeVar()
extern void NetworkTime_get_timeVar_m9D276BC81BB28039ED914413DDB85A472D41BBF7 (void);
// 0x000002DE System.Double Mirror.NetworkTime::get_timeStandardDeviation()
extern void NetworkTime_get_timeStandardDeviation_m0BA80800A296887072DF1191307EB274579081A7 (void);
// 0x000002DF System.Double Mirror.NetworkTime::get_timeSd()
extern void NetworkTime_get_timeSd_m3701E26030E55483B12EE1BB684ACFA8CC5A3405 (void);
// 0x000002E0 System.Double Mirror.NetworkTime::get_offset()
extern void NetworkTime_get_offset_m962DCC4FAA7B83F01518096E0BD17F34EEA84CBF (void);
// 0x000002E1 System.Double Mirror.NetworkTime::get_rtt()
extern void NetworkTime_get_rtt_m9E4EC224EB95FBB7B8329E9A225D3275D0770C66 (void);
// 0x000002E2 System.Double Mirror.NetworkTime::get_rttVariance()
extern void NetworkTime_get_rttVariance_m7E2A5F27F0741B45B69848E26ED1D03A33F0A037 (void);
// 0x000002E3 System.Double Mirror.NetworkTime::get_rttVar()
extern void NetworkTime_get_rttVar_mB51459101EDA3631D0152687E4EDAEBB28741903 (void);
// 0x000002E4 System.Double Mirror.NetworkTime::get_rttStandardDeviation()
extern void NetworkTime_get_rttStandardDeviation_m557D817D40153F321D01AB0D272E620525F2CA38 (void);
// 0x000002E5 System.Double Mirror.NetworkTime::get_rttSd()
extern void NetworkTime_get_rttSd_m90AD954DF60BA5962609303A376108671F368FC9 (void);
// 0x000002E6 System.Boolean Mirror.NetworkVisibility::OnCheckObserver(Mirror.NetworkConnection)
// 0x000002E7 System.Void Mirror.NetworkVisibility::OnRebuildObservers(System.Collections.Generic.HashSet`1<Mirror.NetworkConnection>,System.Boolean)
// 0x000002E8 System.Void Mirror.NetworkVisibility::OnSetHostVisibility(System.Boolean)
extern void NetworkVisibility_OnSetHostVisibility_m5F33C7B41C04A03A8B9B1601DCE466AB1CF7766F (void);
// 0x000002E9 System.Void Mirror.NetworkVisibility::.ctor()
extern void NetworkVisibility__ctor_m113F49AE3B33B035A6132DC95F52F21506C4BA63 (void);
// 0x000002EA System.Int32 Mirror.NetworkWriter::get_Length()
extern void NetworkWriter_get_Length_m814675F5934A7C6E09BC3EAC5294B0B71EA96C95 (void);
// 0x000002EB System.Int32 Mirror.NetworkWriter::get_Position()
extern void NetworkWriter_get_Position_mCC619999A1AD046AD2377028073AABAEBD1A7BFD (void);
// 0x000002EC System.Void Mirror.NetworkWriter::set_Position(System.Int32)
extern void NetworkWriter_set_Position_m201FBEA9E80A82E2BEC2C2BF48D4C80F1258BE57 (void);
// 0x000002ED System.Void Mirror.NetworkWriter::Reset()
extern void NetworkWriter_Reset_m9F34945A36E170550D63E41D2CF9C26ACB03FF77 (void);
// 0x000002EE System.Void Mirror.NetworkWriter::SetLength(System.Int32)
extern void NetworkWriter_SetLength_m772F0AB2AC0AE5DEDD1EBBE19B86196ED78E240D (void);
// 0x000002EF System.Void Mirror.NetworkWriter::EnsureLength(System.Int32)
extern void NetworkWriter_EnsureLength_mE32CDE81422866487CE9EF603DC16106877E7942 (void);
// 0x000002F0 System.Void Mirror.NetworkWriter::EnsureCapacity(System.Int32)
extern void NetworkWriter_EnsureCapacity_m8686EBB645C1892C928AB87D8FD73148B547BDC9 (void);
// 0x000002F1 System.Byte[] Mirror.NetworkWriter::ToArray()
extern void NetworkWriter_ToArray_mC9A28117C639BA09B09A43343D3D3A3488A187E7 (void);
// 0x000002F2 System.ArraySegment`1<System.Byte> Mirror.NetworkWriter::ToArraySegment()
extern void NetworkWriter_ToArraySegment_m20B3969C06F4320029AD66A820B0523A8D6641EF (void);
// 0x000002F3 System.Void Mirror.NetworkWriter::WriteByte(System.Byte)
extern void NetworkWriter_WriteByte_mC22A6863F21D47005ECAD28F5AB6FC5248B23D0E (void);
// 0x000002F4 System.Void Mirror.NetworkWriter::WriteBytes(System.Byte[],System.Int32,System.Int32)
extern void NetworkWriter_WriteBytes_m1EF0FDBA6E07D885F57CB524710524715BB7E374 (void);
// 0x000002F5 System.Void Mirror.NetworkWriter::Write(T)
// 0x000002F6 System.Void Mirror.NetworkWriter::.ctor()
extern void NetworkWriter__ctor_mD3E7B77EDCCE633CFEE83EBC1DB06355D0460A5F (void);
// 0x000002F7 System.Void Mirror.NetworkWriterExtensions::WriteByte(Mirror.NetworkWriter,System.Byte)
extern void NetworkWriterExtensions_WriteByte_mFFEFC20C5AB176841BBBE10C1E4A866EAF369B96 (void);
// 0x000002F8 System.Void Mirror.NetworkWriterExtensions::WriteSByte(Mirror.NetworkWriter,System.SByte)
extern void NetworkWriterExtensions_WriteSByte_m9AA9E2E5390BD4AC9CEA8A05EAE1234D9C48B8C8 (void);
// 0x000002F9 System.Void Mirror.NetworkWriterExtensions::WriteChar(Mirror.NetworkWriter,System.Char)
extern void NetworkWriterExtensions_WriteChar_m14899B6388B37CA68D9BA49CF0BE0C3EE956E49E (void);
// 0x000002FA System.Void Mirror.NetworkWriterExtensions::WriteBoolean(Mirror.NetworkWriter,System.Boolean)
extern void NetworkWriterExtensions_WriteBoolean_m1286C9865F21B0EB024250572C0ADDC319782162 (void);
// 0x000002FB System.Void Mirror.NetworkWriterExtensions::WriteUInt16(Mirror.NetworkWriter,System.UInt16)
extern void NetworkWriterExtensions_WriteUInt16_m7F8221AB37BDB8AC0765BDA883C6CA916CC812F4 (void);
// 0x000002FC System.Void Mirror.NetworkWriterExtensions::WriteInt16(Mirror.NetworkWriter,System.Int16)
extern void NetworkWriterExtensions_WriteInt16_m5C7AE32A24F49E8EE8F42BE01BF5B5D7065FB47F (void);
// 0x000002FD System.Void Mirror.NetworkWriterExtensions::WriteUInt32(Mirror.NetworkWriter,System.UInt32)
extern void NetworkWriterExtensions_WriteUInt32_mD459163C062BD8349BE6680BFAC598A96619F6F5 (void);
// 0x000002FE System.Void Mirror.NetworkWriterExtensions::WriteInt32(Mirror.NetworkWriter,System.Int32)
extern void NetworkWriterExtensions_WriteInt32_m353DC0D60DCF2126D59A2F25E5C20B1ED74C9B3A (void);
// 0x000002FF System.Void Mirror.NetworkWriterExtensions::WriteUInt64(Mirror.NetworkWriter,System.UInt64)
extern void NetworkWriterExtensions_WriteUInt64_mD91BA22C056821BD989BFF2480CCCA9A65120A86 (void);
// 0x00000300 System.Void Mirror.NetworkWriterExtensions::WriteInt64(Mirror.NetworkWriter,System.Int64)
extern void NetworkWriterExtensions_WriteInt64_m381C3EB4FED2C7C18CD6630ACCAC174FFC5E3C9C (void);
// 0x00000301 System.Void Mirror.NetworkWriterExtensions::WriteSingle(Mirror.NetworkWriter,System.Single)
extern void NetworkWriterExtensions_WriteSingle_m5EDC5A8935BAD2B074C652C5158281D2C253CB08 (void);
// 0x00000302 System.Void Mirror.NetworkWriterExtensions::WriteDouble(Mirror.NetworkWriter,System.Double)
extern void NetworkWriterExtensions_WriteDouble_m40D19C266720079063B22A53B6940E1CF6482BFC (void);
// 0x00000303 System.Void Mirror.NetworkWriterExtensions::WriteDecimal(Mirror.NetworkWriter,System.Decimal)
extern void NetworkWriterExtensions_WriteDecimal_m1F5400E8F7D91C15538FC724A4B430A818479431 (void);
// 0x00000304 System.Void Mirror.NetworkWriterExtensions::WriteString(Mirror.NetworkWriter,System.String)
extern void NetworkWriterExtensions_WriteString_m31C762A5BBACB77129E085AB7D7A9AEEB9ACA95F (void);
// 0x00000305 System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSize(Mirror.NetworkWriter,System.Byte[],System.Int32,System.Int32)
extern void NetworkWriterExtensions_WriteBytesAndSize_m75E79671D06A8D7C3CA09E02844E6316F3A3E56B (void);
// 0x00000306 System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSize(Mirror.NetworkWriter,System.Byte[])
extern void NetworkWriterExtensions_WriteBytesAndSize_mB11A8C12622432C14420D616F763F130DD1A91C9 (void);
// 0x00000307 System.Void Mirror.NetworkWriterExtensions::WriteBytesAndSizeSegment(Mirror.NetworkWriter,System.ArraySegment`1<System.Byte>)
extern void NetworkWriterExtensions_WriteBytesAndSizeSegment_mAD214D7E8A080A5615D400A6C3F2F5218F3AD11B (void);
// 0x00000308 System.Void Mirror.NetworkWriterExtensions::WriteVector2(Mirror.NetworkWriter,UnityEngine.Vector2)
extern void NetworkWriterExtensions_WriteVector2_m52B333B1242AC5E0490DA980D5954157FDF9949F (void);
// 0x00000309 System.Void Mirror.NetworkWriterExtensions::WriteVector3(Mirror.NetworkWriter,UnityEngine.Vector3)
extern void NetworkWriterExtensions_WriteVector3_m2895E336F7B2F52819808DC35F574B10D3317609 (void);
// 0x0000030A System.Void Mirror.NetworkWriterExtensions::WriteVector4(Mirror.NetworkWriter,UnityEngine.Vector4)
extern void NetworkWriterExtensions_WriteVector4_mE75893C683F8C655244CE272B0B050662E751414 (void);
// 0x0000030B System.Void Mirror.NetworkWriterExtensions::WriteVector2Int(Mirror.NetworkWriter,UnityEngine.Vector2Int)
extern void NetworkWriterExtensions_WriteVector2Int_mDA0A622A19099A741DE393233204CD6FB89FF75A (void);
// 0x0000030C System.Void Mirror.NetworkWriterExtensions::WriteVector3Int(Mirror.NetworkWriter,UnityEngine.Vector3Int)
extern void NetworkWriterExtensions_WriteVector3Int_m4911D397D68D855E386699BB99ECF4BD0F5F7060 (void);
// 0x0000030D System.Void Mirror.NetworkWriterExtensions::WriteColor(Mirror.NetworkWriter,UnityEngine.Color)
extern void NetworkWriterExtensions_WriteColor_m7EA61C4CEE7C0F49BCAF7339D1A76F4ECA50626E (void);
// 0x0000030E System.Void Mirror.NetworkWriterExtensions::WriteColor32(Mirror.NetworkWriter,UnityEngine.Color32)
extern void NetworkWriterExtensions_WriteColor32_mC430555DAD65FA8A1ACA7117122960F6412B3079 (void);
// 0x0000030F System.Void Mirror.NetworkWriterExtensions::WriteQuaternion(Mirror.NetworkWriter,UnityEngine.Quaternion)
extern void NetworkWriterExtensions_WriteQuaternion_m96A981CD95861D3F23358155CD1CCBCDF185D909 (void);
// 0x00000310 System.Void Mirror.NetworkWriterExtensions::WriteRect(Mirror.NetworkWriter,UnityEngine.Rect)
extern void NetworkWriterExtensions_WriteRect_m1C462FFD3B7FE7B73B6F733CDECEDCBFFF9F3A05 (void);
// 0x00000311 System.Void Mirror.NetworkWriterExtensions::WritePlane(Mirror.NetworkWriter,UnityEngine.Plane)
extern void NetworkWriterExtensions_WritePlane_m75F27F25C9E50A60C635D376D4C1D993D1376A97 (void);
// 0x00000312 System.Void Mirror.NetworkWriterExtensions::WriteRay(Mirror.NetworkWriter,UnityEngine.Ray)
extern void NetworkWriterExtensions_WriteRay_mF907AD59F7170BBB74CF49611FBBAF069DD6D1F2 (void);
// 0x00000313 System.Void Mirror.NetworkWriterExtensions::WriteMatrix4x4(Mirror.NetworkWriter,UnityEngine.Matrix4x4)
extern void NetworkWriterExtensions_WriteMatrix4x4_m40B7941CFBB2912FA11AFB35F5729E4F2383BEAD (void);
// 0x00000314 System.Void Mirror.NetworkWriterExtensions::WriteGuid(Mirror.NetworkWriter,System.Guid)
extern void NetworkWriterExtensions_WriteGuid_m1700462B08DC1D6B80FF00521C50C5A85E781AFB (void);
// 0x00000315 System.Void Mirror.NetworkWriterExtensions::WriteNetworkIdentity(Mirror.NetworkWriter,Mirror.NetworkIdentity)
extern void NetworkWriterExtensions_WriteNetworkIdentity_m6B7BF775A09551E47F1E98F2603E0C3B8358FC64 (void);
// 0x00000316 System.Void Mirror.NetworkWriterExtensions::WriteNetworkBehaviour(Mirror.NetworkWriter,Mirror.NetworkBehaviour)
extern void NetworkWriterExtensions_WriteNetworkBehaviour_mCA7167729B04CFA80D5F7AB5D9C43FC24B4DF20D (void);
// 0x00000317 System.Void Mirror.NetworkWriterExtensions::WriteTransform(Mirror.NetworkWriter,UnityEngine.Transform)
extern void NetworkWriterExtensions_WriteTransform_m3118EFCE291DE2182B785E099FE30B3D68F36830 (void);
// 0x00000318 System.Void Mirror.NetworkWriterExtensions::WriteGameObject(Mirror.NetworkWriter,UnityEngine.GameObject)
extern void NetworkWriterExtensions_WriteGameObject_m461FDFA27D93D42B1C6C1B5F84EC2314E479E45B (void);
// 0x00000319 System.Void Mirror.NetworkWriterExtensions::WriteUri(Mirror.NetworkWriter,System.Uri)
extern void NetworkWriterExtensions_WriteUri_mECC3E974CD9C900CEBB24E5E4743DF2D41E3533F (void);
// 0x0000031A System.Void Mirror.NetworkWriterExtensions::WriteList(Mirror.NetworkWriter,System.Collections.Generic.List`1<T>)
// 0x0000031B System.Void Mirror.NetworkWriterExtensions::WriteArray(Mirror.NetworkWriter,T[])
// 0x0000031C System.Void Mirror.NetworkWriterExtensions::WriteArraySegment(Mirror.NetworkWriter,System.ArraySegment`1<T>)
// 0x0000031D System.Void Mirror.NetworkWriterExtensions::.cctor()
extern void NetworkWriterExtensions__cctor_mEF540B4CE4511D89773F16F2D0E8092600455D59 (void);
// 0x0000031E System.Void Mirror.PooledNetworkWriter::Dispose()
extern void PooledNetworkWriter_Dispose_m4D1350BAFF1AE5BE1A2C6540EFACB15F02511A18 (void);
// 0x0000031F System.Void Mirror.PooledNetworkWriter::.ctor()
extern void PooledNetworkWriter__ctor_m0575E62AABFA81E2F757C76EA455757C8F27B165 (void);
// 0x00000320 Mirror.PooledNetworkWriter Mirror.NetworkWriterPool::GetWriter()
extern void NetworkWriterPool_GetWriter_m53506C8016911951C82F2F83E45592CE2F9A85AE (void);
// 0x00000321 System.Void Mirror.NetworkWriterPool::Recycle(Mirror.PooledNetworkWriter)
extern void NetworkWriterPool_Recycle_m8E7D8C8ED6F0856380CE750DBEF1D0EC5BAF8FB3 (void);
// 0x00000322 System.Void Mirror.NetworkWriterPool::.cctor()
extern void NetworkWriterPool__cctor_m9274ABE8EE4582DC7D9DF184F7F73F2DF3FC7CF3 (void);
// 0x00000323 System.Void Mirror.NetworkWriterPool/<>c::.cctor()
extern void U3CU3Ec__cctor_m3FD838FF3EDCED87947D48608D148699344D8A93 (void);
// 0x00000324 System.Void Mirror.NetworkWriterPool/<>c::.ctor()
extern void U3CU3Ec__ctor_m54EF1D3A06EA9F18684008AB6E69268AF8AD7A9A (void);
// 0x00000325 Mirror.PooledNetworkWriter Mirror.NetworkWriterPool/<>c::<.cctor>b__3_0()
extern void U3CU3Ec_U3C_cctorU3Eb__3_0_mB0A5B91C0E7EC65AD20C53B230FD500DECA1A061 (void);
// 0x00000326 System.Void Mirror.Pool`1::.ctor(System.Func`1<T>)
// 0x00000327 T Mirror.Pool`1::Take()
// 0x00000328 System.Void Mirror.Pool`1::Return(T)
// 0x00000329 System.Int32 Mirror.Pool`1::get_Count()
// 0x0000032A System.Int32 Mirror.SyncIDictionary`2::get_Count()
// 0x0000032B System.Boolean Mirror.SyncIDictionary`2::get_IsReadOnly()
// 0x0000032C System.Void Mirror.SyncIDictionary`2::set_IsReadOnly(System.Boolean)
// 0x0000032D System.Void Mirror.SyncIDictionary`2::add_Callback(Mirror.SyncIDictionary`2/SyncDictionaryChanged<TKey,TValue>)
// 0x0000032E System.Void Mirror.SyncIDictionary`2::remove_Callback(Mirror.SyncIDictionary`2/SyncDictionaryChanged<TKey,TValue>)
// 0x0000032F System.Void Mirror.SyncIDictionary`2::Reset()
// 0x00000330 System.Boolean Mirror.SyncIDictionary`2::get_IsDirty()
// 0x00000331 System.Collections.Generic.ICollection`1<TKey> Mirror.SyncIDictionary`2::get_Keys()
// 0x00000332 System.Collections.Generic.ICollection`1<TValue> Mirror.SyncIDictionary`2::get_Values()
// 0x00000333 System.Collections.Generic.IEnumerable`1<TKey> Mirror.SyncIDictionary`2::System.Collections.Generic.IReadOnlyDictionary<TKey,TValue>.get_Keys()
// 0x00000334 System.Collections.Generic.IEnumerable`1<TValue> Mirror.SyncIDictionary`2::System.Collections.Generic.IReadOnlyDictionary<TKey,TValue>.get_Values()
// 0x00000335 System.Void Mirror.SyncIDictionary`2::Flush()
// 0x00000336 System.Void Mirror.SyncIDictionary`2::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
// 0x00000337 System.Void Mirror.SyncIDictionary`2::AddOperation(Mirror.SyncIDictionary`2/Operation<TKey,TValue>,TKey,TValue)
// 0x00000338 System.Void Mirror.SyncIDictionary`2::OnSerializeAll(Mirror.NetworkWriter)
// 0x00000339 System.Void Mirror.SyncIDictionary`2::OnSerializeDelta(Mirror.NetworkWriter)
// 0x0000033A System.Void Mirror.SyncIDictionary`2::OnDeserializeAll(Mirror.NetworkReader)
// 0x0000033B System.Void Mirror.SyncIDictionary`2::OnDeserializeDelta(Mirror.NetworkReader)
// 0x0000033C System.Void Mirror.SyncIDictionary`2::Clear()
// 0x0000033D System.Boolean Mirror.SyncIDictionary`2::ContainsKey(TKey)
// 0x0000033E System.Boolean Mirror.SyncIDictionary`2::Remove(TKey)
// 0x0000033F TValue Mirror.SyncIDictionary`2::get_Item(TKey)
// 0x00000340 System.Void Mirror.SyncIDictionary`2::set_Item(TKey,TValue)
// 0x00000341 System.Boolean Mirror.SyncIDictionary`2::TryGetValue(TKey,TValue&)
// 0x00000342 System.Void Mirror.SyncIDictionary`2::Add(TKey,TValue)
// 0x00000343 System.Void Mirror.SyncIDictionary`2::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000344 System.Boolean Mirror.SyncIDictionary`2::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000345 System.Void Mirror.SyncIDictionary`2::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
// 0x00000346 System.Boolean Mirror.SyncIDictionary`2::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
// 0x00000347 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> Mirror.SyncIDictionary`2::GetEnumerator()
// 0x00000348 System.Collections.IEnumerator Mirror.SyncIDictionary`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000349 System.Void Mirror.SyncIDictionary`2/SyncDictionaryChanged::.ctor(System.Object,System.IntPtr)
// 0x0000034A System.Void Mirror.SyncIDictionary`2/SyncDictionaryChanged::Invoke(Mirror.SyncIDictionary`2/Operation<TKey,TValue>,TKey,TValue)
// 0x0000034B System.IAsyncResult Mirror.SyncIDictionary`2/SyncDictionaryChanged::BeginInvoke(Mirror.SyncIDictionary`2/Operation<TKey,TValue>,TKey,TValue,System.AsyncCallback,System.Object)
// 0x0000034C System.Void Mirror.SyncIDictionary`2/SyncDictionaryChanged::EndInvoke(System.IAsyncResult)
// 0x0000034D System.Void Mirror.SyncDictionary`2::.ctor()
// 0x0000034E System.Void Mirror.SyncDictionary`2::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000034F System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> Mirror.SyncDictionary`2::get_Values()
// 0x00000350 System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> Mirror.SyncDictionary`2::get_Keys()
// 0x00000351 System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> Mirror.SyncDictionary`2::GetEnumerator()
// 0x00000352 System.Void Mirror.SyncListString::.ctor()
extern void SyncListString__ctor_mA4D9B6BCC69307496D8FDCEE024CD97D199DF0E7 (void);
// 0x00000353 System.Void Mirror.SyncListFloat::.ctor()
extern void SyncListFloat__ctor_m4B45173BADFFBBDB078AC492AA195341C83AFDE0 (void);
// 0x00000354 System.Void Mirror.SyncListInt::.ctor()
extern void SyncListInt__ctor_m5946A57F58B76D25F4E339924332AAC7DF742ADA (void);
// 0x00000355 System.Void Mirror.SyncListUInt::.ctor()
extern void SyncListUInt__ctor_m00CBE30DC4011DAEDA7F08D946017D0F8792F47A (void);
// 0x00000356 System.Void Mirror.SyncListBool::.ctor()
extern void SyncListBool__ctor_m2916C60036ACE746504B76D4076DE7B039BA5399 (void);
// 0x00000357 System.Int32 Mirror.SyncList`1::get_Count()
// 0x00000358 System.Boolean Mirror.SyncList`1::get_IsReadOnly()
// 0x00000359 System.Void Mirror.SyncList`1::set_IsReadOnly(System.Boolean)
// 0x0000035A System.Void Mirror.SyncList`1::add_Callback(Mirror.SyncList`1/SyncListChanged<T>)
// 0x0000035B System.Void Mirror.SyncList`1::remove_Callback(Mirror.SyncList`1/SyncListChanged<T>)
// 0x0000035C System.Void Mirror.SyncList`1::.ctor()
// 0x0000035D System.Void Mirror.SyncList`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000035E System.Void Mirror.SyncList`1::.ctor(System.Collections.Generic.IList`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000035F System.Boolean Mirror.SyncList`1::get_IsDirty()
// 0x00000360 System.Void Mirror.SyncList`1::Flush()
// 0x00000361 System.Void Mirror.SyncList`1::Reset()
// 0x00000362 System.Void Mirror.SyncList`1::AddOperation(Mirror.SyncList`1/Operation<T>,System.Int32,T,T)
// 0x00000363 System.Void Mirror.SyncList`1::OnSerializeAll(Mirror.NetworkWriter)
// 0x00000364 System.Void Mirror.SyncList`1::OnSerializeDelta(Mirror.NetworkWriter)
// 0x00000365 System.Void Mirror.SyncList`1::OnDeserializeAll(Mirror.NetworkReader)
// 0x00000366 System.Void Mirror.SyncList`1::OnDeserializeDelta(Mirror.NetworkReader)
// 0x00000367 System.Void Mirror.SyncList`1::Add(T)
// 0x00000368 System.Void Mirror.SyncList`1::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000369 System.Void Mirror.SyncList`1::Clear()
// 0x0000036A System.Boolean Mirror.SyncList`1::Contains(T)
// 0x0000036B System.Void Mirror.SyncList`1::CopyTo(T[],System.Int32)
// 0x0000036C System.Int32 Mirror.SyncList`1::IndexOf(T)
// 0x0000036D System.Int32 Mirror.SyncList`1::FindIndex(System.Predicate`1<T>)
// 0x0000036E T Mirror.SyncList`1::Find(System.Predicate`1<T>)
// 0x0000036F System.Collections.Generic.List`1<T> Mirror.SyncList`1::FindAll(System.Predicate`1<T>)
// 0x00000370 System.Void Mirror.SyncList`1::Insert(System.Int32,T)
// 0x00000371 System.Void Mirror.SyncList`1::InsertRange(System.Int32,System.Collections.Generic.IEnumerable`1<T>)
// 0x00000372 System.Boolean Mirror.SyncList`1::Remove(T)
// 0x00000373 System.Void Mirror.SyncList`1::RemoveAt(System.Int32)
// 0x00000374 System.Int32 Mirror.SyncList`1::RemoveAll(System.Predicate`1<T>)
// 0x00000375 T Mirror.SyncList`1::get_Item(System.Int32)
// 0x00000376 System.Void Mirror.SyncList`1::set_Item(System.Int32,T)
// 0x00000377 Mirror.SyncList`1/Enumerator<T> Mirror.SyncList`1::GetEnumerator()
// 0x00000378 System.Collections.Generic.IEnumerator`1<T> Mirror.SyncList`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000379 System.Collections.IEnumerator Mirror.SyncList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000037A System.Void Mirror.SyncList`1/SyncListChanged::.ctor(System.Object,System.IntPtr)
// 0x0000037B System.Void Mirror.SyncList`1/SyncListChanged::Invoke(Mirror.SyncList`1/Operation<T>,System.Int32,T,T)
// 0x0000037C System.IAsyncResult Mirror.SyncList`1/SyncListChanged::BeginInvoke(Mirror.SyncList`1/Operation<T>,System.Int32,T,T,System.AsyncCallback,System.Object)
// 0x0000037D System.Void Mirror.SyncList`1/SyncListChanged::EndInvoke(System.IAsyncResult)
// 0x0000037E T Mirror.SyncList`1/Enumerator::get_Current()
// 0x0000037F System.Void Mirror.SyncList`1/Enumerator::set_Current(T)
// 0x00000380 System.Void Mirror.SyncList`1/Enumerator::.ctor(Mirror.SyncList`1<T>)
// 0x00000381 System.Boolean Mirror.SyncList`1/Enumerator::MoveNext()
// 0x00000382 System.Void Mirror.SyncList`1/Enumerator::Reset()
// 0x00000383 System.Object Mirror.SyncList`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000384 System.Void Mirror.SyncList`1/Enumerator::Dispose()
// 0x00000385 System.Boolean Mirror.SyncObject::get_IsDirty()
// 0x00000386 System.Void Mirror.SyncObject::Flush()
// 0x00000387 System.Void Mirror.SyncObject::OnSerializeAll(Mirror.NetworkWriter)
// 0x00000388 System.Void Mirror.SyncObject::OnSerializeDelta(Mirror.NetworkWriter)
// 0x00000389 System.Void Mirror.SyncObject::OnDeserializeAll(Mirror.NetworkReader)
// 0x0000038A System.Void Mirror.SyncObject::OnDeserializeDelta(Mirror.NetworkReader)
// 0x0000038B System.Void Mirror.SyncObject::Reset()
// 0x0000038C System.Int32 Mirror.SyncSet`1::get_Count()
// 0x0000038D System.Boolean Mirror.SyncSet`1::get_IsReadOnly()
// 0x0000038E System.Void Mirror.SyncSet`1::set_IsReadOnly(System.Boolean)
// 0x0000038F System.Void Mirror.SyncSet`1::add_Callback(Mirror.SyncSet`1/SyncSetChanged<T>)
// 0x00000390 System.Void Mirror.SyncSet`1::remove_Callback(Mirror.SyncSet`1/SyncSetChanged<T>)
// 0x00000391 System.Void Mirror.SyncSet`1::.ctor(System.Collections.Generic.ISet`1<T>)
// 0x00000392 System.Void Mirror.SyncSet`1::Reset()
// 0x00000393 System.Boolean Mirror.SyncSet`1::get_IsDirty()
// 0x00000394 System.Void Mirror.SyncSet`1::Flush()
// 0x00000395 System.Void Mirror.SyncSet`1::AddOperation(Mirror.SyncSet`1/Operation<T>,T)
// 0x00000396 System.Void Mirror.SyncSet`1::AddOperation(Mirror.SyncSet`1/Operation<T>)
// 0x00000397 System.Void Mirror.SyncSet`1::OnSerializeAll(Mirror.NetworkWriter)
// 0x00000398 System.Void Mirror.SyncSet`1::OnSerializeDelta(Mirror.NetworkWriter)
// 0x00000399 System.Void Mirror.SyncSet`1::OnDeserializeAll(Mirror.NetworkReader)
// 0x0000039A System.Void Mirror.SyncSet`1::OnDeserializeDelta(Mirror.NetworkReader)
// 0x0000039B System.Boolean Mirror.SyncSet`1::Add(T)
// 0x0000039C System.Void Mirror.SyncSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000039D System.Void Mirror.SyncSet`1::Clear()
// 0x0000039E System.Boolean Mirror.SyncSet`1::Contains(T)
// 0x0000039F System.Void Mirror.SyncSet`1::CopyTo(T[],System.Int32)
// 0x000003A0 System.Boolean Mirror.SyncSet`1::Remove(T)
// 0x000003A1 System.Collections.Generic.IEnumerator`1<T> Mirror.SyncSet`1::GetEnumerator()
// 0x000003A2 System.Collections.IEnumerator Mirror.SyncSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000003A3 System.Void Mirror.SyncSet`1::ExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A4 System.Void Mirror.SyncSet`1::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A5 System.Void Mirror.SyncSet`1::IntersectWithSet(System.Collections.Generic.ISet`1<T>)
// 0x000003A6 System.Boolean Mirror.SyncSet`1::IsProperSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A7 System.Boolean Mirror.SyncSet`1::IsProperSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A8 System.Boolean Mirror.SyncSet`1::IsSubsetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003A9 System.Boolean Mirror.SyncSet`1::IsSupersetOf(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003AA System.Boolean Mirror.SyncSet`1::Overlaps(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003AB System.Boolean Mirror.SyncSet`1::SetEquals(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003AC System.Void Mirror.SyncSet`1::SymmetricExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003AD System.Void Mirror.SyncSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000003AE System.Void Mirror.SyncSet`1/SyncSetChanged::.ctor(System.Object,System.IntPtr)
// 0x000003AF System.Void Mirror.SyncSet`1/SyncSetChanged::Invoke(Mirror.SyncSet`1/Operation<T>,T)
// 0x000003B0 System.IAsyncResult Mirror.SyncSet`1/SyncSetChanged::BeginInvoke(Mirror.SyncSet`1/Operation<T>,T,System.AsyncCallback,System.Object)
// 0x000003B1 System.Void Mirror.SyncSet`1/SyncSetChanged::EndInvoke(System.IAsyncResult)
// 0x000003B2 System.Void Mirror.SyncHashSet`1::.ctor()
// 0x000003B3 System.Void Mirror.SyncHashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000003B4 System.Collections.Generic.HashSet`1/Enumerator<T> Mirror.SyncHashSet`1::GetEnumerator()
// 0x000003B5 System.Void Mirror.SyncSortedSet`1::.ctor()
// 0x000003B6 System.Void Mirror.SyncSortedSet`1::.ctor(System.Collections.Generic.IComparer`1<T>)
// 0x000003B7 System.Collections.Generic.SortedSet`1/Enumerator<T> Mirror.SyncSortedSet`1::GetEnumerator()
// 0x000003B8 System.Void Mirror.FallbackTransport::Awake()
extern void FallbackTransport_Awake_mB5691DBE3460F01AB2800093B3EF15CB80AB2838 (void);
// 0x000003B9 System.Void Mirror.FallbackTransport::OnEnable()
extern void FallbackTransport_OnEnable_m19FC450BEE25F110AB379997A142F95EED809429 (void);
// 0x000003BA System.Void Mirror.FallbackTransport::OnDisable()
extern void FallbackTransport_OnDisable_mE060E389CBC984B6EF2554968D241D7285A8BFBA (void);
// 0x000003BB Mirror.Transport Mirror.FallbackTransport::GetAvailableTransport()
extern void FallbackTransport_GetAvailableTransport_mCAF4424D46DB412F5697C5C5B9C7F3206D28E27A (void);
// 0x000003BC System.Boolean Mirror.FallbackTransport::Available()
extern void FallbackTransport_Available_m74668CFB8053E5639BC5BE83B2D65C9527ACB51A (void);
// 0x000003BD System.Void Mirror.FallbackTransport::ClientConnect(System.String)
extern void FallbackTransport_ClientConnect_m812512C78B7B54B9AE3C5925CB4D8D5EBD793E60 (void);
// 0x000003BE System.Void Mirror.FallbackTransport::ClientConnect(System.Uri)
extern void FallbackTransport_ClientConnect_m4330C1BDABAB75A6F3FB8B7229B252F57A3F43E1 (void);
// 0x000003BF System.Boolean Mirror.FallbackTransport::ClientConnected()
extern void FallbackTransport_ClientConnected_m75D9F95A99CBA2D67AD1C458407E9C53E1208F21 (void);
// 0x000003C0 System.Void Mirror.FallbackTransport::ClientDisconnect()
extern void FallbackTransport_ClientDisconnect_mFEEB0102A04FF499F7BCDC7BABA1E626E70C07D4 (void);
// 0x000003C1 System.Void Mirror.FallbackTransport::ClientSend(System.Int32,System.ArraySegment`1<System.Byte>)
extern void FallbackTransport_ClientSend_m6170416F05A77017C06C34BD2A17CF100F196EA2 (void);
// 0x000003C2 System.Uri Mirror.FallbackTransport::ServerUri()
extern void FallbackTransport_ServerUri_mE4D4483AC7A20A3B58F68963B9DB142B08781C9D (void);
// 0x000003C3 System.Boolean Mirror.FallbackTransport::ServerActive()
extern void FallbackTransport_ServerActive_mE912487FE1659B230036C278C9A2244FA59A1B5A (void);
// 0x000003C4 System.String Mirror.FallbackTransport::ServerGetClientAddress(System.Int32)
extern void FallbackTransport_ServerGetClientAddress_m7B30A240C8CA734D55070CF717E8011B3CF20EAC (void);
// 0x000003C5 System.Boolean Mirror.FallbackTransport::ServerDisconnect(System.Int32)
extern void FallbackTransport_ServerDisconnect_mFCAFAD5A41E137F030CF62C76FFFC6ADFA65B5CD (void);
// 0x000003C6 System.Void Mirror.FallbackTransport::ServerSend(System.Int32,System.Int32,System.ArraySegment`1<System.Byte>)
extern void FallbackTransport_ServerSend_m64CF877DF200D9ACD0A31BFC41B23BB8473F9A21 (void);
// 0x000003C7 System.Void Mirror.FallbackTransport::ServerStart()
extern void FallbackTransport_ServerStart_mDF3EC98EAC1F16A343CEEB6DCCF508E9D14D6B95 (void);
// 0x000003C8 System.Void Mirror.FallbackTransport::ServerStop()
extern void FallbackTransport_ServerStop_mD99049C603B5CEC7C87C6D96F8C09ACF954A4456 (void);
// 0x000003C9 System.Void Mirror.FallbackTransport::Shutdown()
extern void FallbackTransport_Shutdown_mE9EEC19ADD8B69EBF701F0AF43DECC92FD099F79 (void);
// 0x000003CA System.Int32 Mirror.FallbackTransport::GetMaxPacketSize(System.Int32)
extern void FallbackTransport_GetMaxPacketSize_m846D965A967FAD056657AD12D788B897D0620791 (void);
// 0x000003CB System.String Mirror.FallbackTransport::ToString()
extern void FallbackTransport_ToString_m8B43A1F9E312AC2DD313FE66E22B93B323AD8BC8 (void);
// 0x000003CC System.Void Mirror.FallbackTransport::.ctor()
extern void FallbackTransport__ctor_mBACA7141E782AEB783D06A82D6CD85E2535E1F46 (void);
// 0x000003CD System.Void Mirror.LatencySimulation::Awake()
extern void LatencySimulation_Awake_mEAC826A25C58752C0C69564A5348BD01CA9CB7C1 (void);
// 0x000003CE System.Void Mirror.LatencySimulation::OnEnable()
extern void LatencySimulation_OnEnable_mD242DDC8C43D5466149B69B85EAE0DE562756A9C (void);
// 0x000003CF System.Void Mirror.LatencySimulation::OnDisable()
extern void LatencySimulation_OnDisable_m199817612D997387B3E911992B686D05DB2E07B1 (void);
// 0x000003D0 System.Single Mirror.LatencySimulation::Noise(System.Single)
extern void LatencySimulation_Noise_m1A3BC7F8CD1202484C1A868B4D23FC9791AC5CF5 (void);
// 0x000003D1 System.Single Mirror.LatencySimulation::SimulateLatency(System.Int32)
extern void LatencySimulation_SimulateLatency_mAF38F234D096858BDBD02E0437436E71EF41CB7C (void);
// 0x000003D2 System.Void Mirror.LatencySimulation::SimulateSend(System.Int32,System.Int32,System.ArraySegment`1<System.Byte>,System.Single,System.Collections.Generic.List`1<Mirror.QueuedMessage>,System.Collections.Generic.List`1<Mirror.QueuedMessage>)
extern void LatencySimulation_SimulateSend_mF89BB2801ED96AF1D6F4CD9F3BF79997D20C9EFD (void);
// 0x000003D3 System.Boolean Mirror.LatencySimulation::Available()
extern void LatencySimulation_Available_mB4E68E0825607F4005173F6C7D09373DA21ECD03 (void);
// 0x000003D4 System.Void Mirror.LatencySimulation::ClientConnect(System.String)
extern void LatencySimulation_ClientConnect_m58CFCBC09F36A3BF4B4AFACDE77C3DDBE813A75F (void);
// 0x000003D5 System.Void Mirror.LatencySimulation::ClientConnect(System.Uri)
extern void LatencySimulation_ClientConnect_mA949A0B2766FB09E9D5DFC5739F9BCD2702FFC5F (void);
// 0x000003D6 System.Boolean Mirror.LatencySimulation::ClientConnected()
extern void LatencySimulation_ClientConnected_mDF44EB7042EF3BC3ED0B44748C4B8839D64C6715 (void);
// 0x000003D7 System.Void Mirror.LatencySimulation::ClientDisconnect()
extern void LatencySimulation_ClientDisconnect_m24DA6CF3C4FADC708864D354670ABC2E7B2A5EA9 (void);
// 0x000003D8 System.Void Mirror.LatencySimulation::ClientSend(System.Int32,System.ArraySegment`1<System.Byte>)
extern void LatencySimulation_ClientSend_mE86FA4EDBD91084FE2A88BAC4D48ECFBEBE5B08F (void);
// 0x000003D9 System.Uri Mirror.LatencySimulation::ServerUri()
extern void LatencySimulation_ServerUri_mE307B5DFAA51ACE4E586BC9F9E3F46D72A3CEE9E (void);
// 0x000003DA System.Boolean Mirror.LatencySimulation::ServerActive()
extern void LatencySimulation_ServerActive_mEC3859D604A254EBBE0613A38D095DFC70C9FBB3 (void);
// 0x000003DB System.String Mirror.LatencySimulation::ServerGetClientAddress(System.Int32)
extern void LatencySimulation_ServerGetClientAddress_m49F80F36FAF18A175ABD6AFC040E1916F433AD40 (void);
// 0x000003DC System.Boolean Mirror.LatencySimulation::ServerDisconnect(System.Int32)
extern void LatencySimulation_ServerDisconnect_m209FEF5CBC7543F9A09FDC2C904A1F7DECDE54AB (void);
// 0x000003DD System.Void Mirror.LatencySimulation::ServerSend(System.Int32,System.Int32,System.ArraySegment`1<System.Byte>)
extern void LatencySimulation_ServerSend_m65405229586A20BC49EB56CEB7F4838B203F06A1 (void);
// 0x000003DE System.Void Mirror.LatencySimulation::ServerStart()
extern void LatencySimulation_ServerStart_m1EA1DA984B406C9EF1ED2B70010DB9DF20F51C01 (void);
// 0x000003DF System.Void Mirror.LatencySimulation::ServerStop()
extern void LatencySimulation_ServerStop_m42D319CCA21268E27974470ADBA90C0D478EF316 (void);
// 0x000003E0 System.Void Mirror.LatencySimulation::ClientEarlyUpdate()
extern void LatencySimulation_ClientEarlyUpdate_m53492BCFAB22CBE97F2AD567B5DC1C6E82C851FA (void);
// 0x000003E1 System.Void Mirror.LatencySimulation::ServerEarlyUpdate()
extern void LatencySimulation_ServerEarlyUpdate_mBA19D74A9BB375B315A174B428E691ADD4A30952 (void);
// 0x000003E2 System.Void Mirror.LatencySimulation::ClientLateUpdate()
extern void LatencySimulation_ClientLateUpdate_mF0C4A41CEA86EFB8A86E97633249B22E59122924 (void);
// 0x000003E3 System.Void Mirror.LatencySimulation::ServerLateUpdate()
extern void LatencySimulation_ServerLateUpdate_m374D47AD51A3EE64CA2889A03B508F9A3BFB87E0 (void);
// 0x000003E4 System.Int32 Mirror.LatencySimulation::GetMaxBatchSize(System.Int32)
extern void LatencySimulation_GetMaxBatchSize_m17D26D1A08E47D9DD23BD4784D30175FD6B8B037 (void);
// 0x000003E5 System.Int32 Mirror.LatencySimulation::GetMaxPacketSize(System.Int32)
extern void LatencySimulation_GetMaxPacketSize_m9F0DCF83D8965FB56EE3BAD9CB384BE9EF0CF3B6 (void);
// 0x000003E6 System.Void Mirror.LatencySimulation::Shutdown()
extern void LatencySimulation_Shutdown_mC7DD8B6818B0076AD69EEFDB07C8E4E62A548471 (void);
// 0x000003E7 System.String Mirror.LatencySimulation::ToString()
extern void LatencySimulation_ToString_m2F0B4FAF301FC22A8093E73308E047FC919C5DAB (void);
// 0x000003E8 System.Void Mirror.LatencySimulation::.ctor()
extern void LatencySimulation__ctor_mEBBCAAD0D6369E89B44B1C8F9636D88BD7EABDD4 (void);
// 0x000003E9 System.Boolean Mirror.MiddlewareTransport::Available()
extern void MiddlewareTransport_Available_mF191BD5C8896806E9F28BA6FEC9FE1DDDC2695EB (void);
// 0x000003EA System.Int32 Mirror.MiddlewareTransport::GetMaxPacketSize(System.Int32)
extern void MiddlewareTransport_GetMaxPacketSize_mA0E7EED841343BAF79A3ADB599A1FD4A7EE601B7 (void);
// 0x000003EB System.Void Mirror.MiddlewareTransport::Shutdown()
extern void MiddlewareTransport_Shutdown_m194A579B64BFD146048A8EEE1E9FF244D76F0B3B (void);
// 0x000003EC System.Void Mirror.MiddlewareTransport::ClientConnect(System.String)
extern void MiddlewareTransport_ClientConnect_m5CA5664667B11CFB4938EEB37AC81ED33749C5E1 (void);
// 0x000003ED System.Boolean Mirror.MiddlewareTransport::ClientConnected()
extern void MiddlewareTransport_ClientConnected_mA0F69BF09D2BC66AE0D356E4F9D14AF85CC22932 (void);
// 0x000003EE System.Void Mirror.MiddlewareTransport::ClientDisconnect()
extern void MiddlewareTransport_ClientDisconnect_m0D3A2544636DEF9926928276F00D6265FC970B53 (void);
// 0x000003EF System.Void Mirror.MiddlewareTransport::ClientSend(System.Int32,System.ArraySegment`1<System.Byte>)
extern void MiddlewareTransport_ClientSend_mFEC9A63B83D18B06CC479F545597CA5FAF08DF78 (void);
// 0x000003F0 System.Boolean Mirror.MiddlewareTransport::ServerActive()
extern void MiddlewareTransport_ServerActive_m6D3FEA6629AAC8A1F7DC552699B5E73BAFE0B129 (void);
// 0x000003F1 System.Void Mirror.MiddlewareTransport::ServerStart()
extern void MiddlewareTransport_ServerStart_m0D85BB7EA451FD775587EDF01BBAF13E4DF687A1 (void);
// 0x000003F2 System.Void Mirror.MiddlewareTransport::ServerStop()
extern void MiddlewareTransport_ServerStop_m2211C5FF5EB34510A2D4BA3A8B43426FC12FE3E9 (void);
// 0x000003F3 System.Void Mirror.MiddlewareTransport::ServerSend(System.Int32,System.Int32,System.ArraySegment`1<System.Byte>)
extern void MiddlewareTransport_ServerSend_m531B5F8685EB2D4029E6032F70CDC34B74BF3949 (void);
// 0x000003F4 System.Boolean Mirror.MiddlewareTransport::ServerDisconnect(System.Int32)
extern void MiddlewareTransport_ServerDisconnect_m0E4C34796EA7E75E591F77DDA98FC0E4FADF032C (void);
// 0x000003F5 System.String Mirror.MiddlewareTransport::ServerGetClientAddress(System.Int32)
extern void MiddlewareTransport_ServerGetClientAddress_m489B2ACBA1E4572FDFBCC3C58F9B4C345E08BE5D (void);
// 0x000003F6 System.Uri Mirror.MiddlewareTransport::ServerUri()
extern void MiddlewareTransport_ServerUri_m4811255EF1EE5048D396701A580ED4BDEC892991 (void);
// 0x000003F7 System.Void Mirror.MiddlewareTransport::.ctor()
extern void MiddlewareTransport__ctor_mCB190AEE11176640125AF0F7F62842471D92186B (void);
// 0x000003F8 System.Void Mirror.MultiplexTransport::Awake()
extern void MultiplexTransport_Awake_m26A9019ED0ABDDE43CA027533F27F34A39BC5BB9 (void);
// 0x000003F9 System.Void Mirror.MultiplexTransport::ClientEarlyUpdate()
extern void MultiplexTransport_ClientEarlyUpdate_m25E35E39DD3ED68AFA0F1829474734D1D6B7D50B (void);
// 0x000003FA System.Void Mirror.MultiplexTransport::ServerEarlyUpdate()
extern void MultiplexTransport_ServerEarlyUpdate_m2A91E85584D6897FC0F495DF270220129EB2D2CC (void);
// 0x000003FB System.Void Mirror.MultiplexTransport::ClientLateUpdate()
extern void MultiplexTransport_ClientLateUpdate_m2FF3203709B7766A969E34724CD4ECD393672084 (void);
// 0x000003FC System.Void Mirror.MultiplexTransport::ServerLateUpdate()
extern void MultiplexTransport_ServerLateUpdate_m8B4417CEAC63EBFB5754F5BA315B32329B84F1AD (void);
// 0x000003FD System.Void Mirror.MultiplexTransport::OnEnable()
extern void MultiplexTransport_OnEnable_mB1A69C2F6625B2623669BF8E158A298E3F598E17 (void);
// 0x000003FE System.Void Mirror.MultiplexTransport::OnDisable()
extern void MultiplexTransport_OnDisable_m3D0FB1850D725883E1B012BE2E191B2C4E4E4C24 (void);
// 0x000003FF System.Boolean Mirror.MultiplexTransport::Available()
extern void MultiplexTransport_Available_mD1A9553AAFEA47B7243E432217D1F097BA949DAE (void);
// 0x00000400 System.Void Mirror.MultiplexTransport::ClientConnect(System.String)
extern void MultiplexTransport_ClientConnect_m100247467A74FBD5B85B3053895265A3CD969804 (void);
// 0x00000401 System.Void Mirror.MultiplexTransport::ClientConnect(System.Uri)
extern void MultiplexTransport_ClientConnect_m68EE415F6344B5B068F38B4E9665626B814C3FC7 (void);
// 0x00000402 System.Boolean Mirror.MultiplexTransport::ClientConnected()
extern void MultiplexTransport_ClientConnected_m9A02C200A00E63BEA58542D9FF631389991B0C06 (void);
// 0x00000403 System.Void Mirror.MultiplexTransport::ClientDisconnect()
extern void MultiplexTransport_ClientDisconnect_mC5E015E98AB02572E650F30319414B7344856E2A (void);
// 0x00000404 System.Void Mirror.MultiplexTransport::ClientSend(System.Int32,System.ArraySegment`1<System.Byte>)
extern void MultiplexTransport_ClientSend_m5C1821304190D784483592BC3ACBD631DF4100AE (void);
// 0x00000405 System.Int32 Mirror.MultiplexTransport::FromBaseId(System.Int32,System.Int32)
extern void MultiplexTransport_FromBaseId_m48D22B4B10606A37A35893E55EA3F38CF11F24E4 (void);
// 0x00000406 System.Int32 Mirror.MultiplexTransport::ToBaseId(System.Int32)
extern void MultiplexTransport_ToBaseId_m4483DB4464A8A0664EE667278437C9D6C50E4D26 (void);
// 0x00000407 System.Int32 Mirror.MultiplexTransport::ToTransportId(System.Int32)
extern void MultiplexTransport_ToTransportId_m0750AEEA94DA55CDFFAD5D9C93346686C466C4BD (void);
// 0x00000408 System.Void Mirror.MultiplexTransport::AddServerCallbacks()
extern void MultiplexTransport_AddServerCallbacks_m83C3D43FDF8C41DDC100792514769D8420AA95F3 (void);
// 0x00000409 System.Uri Mirror.MultiplexTransport::ServerUri()
extern void MultiplexTransport_ServerUri_m880F88499ECEAF9A081AEE2B4536FCF3A65AFD87 (void);
// 0x0000040A System.Boolean Mirror.MultiplexTransport::ServerActive()
extern void MultiplexTransport_ServerActive_m01DD6DAD3AC8A8A9D981CB9E345BDA698599453C (void);
// 0x0000040B System.String Mirror.MultiplexTransport::ServerGetClientAddress(System.Int32)
extern void MultiplexTransport_ServerGetClientAddress_mF215E0A6812B259249283C8189D6706AA63DA7C8 (void);
// 0x0000040C System.Boolean Mirror.MultiplexTransport::ServerDisconnect(System.Int32)
extern void MultiplexTransport_ServerDisconnect_mD3BA9993BB66EE3AF048AC74C7CABC6F7C20CF6F (void);
// 0x0000040D System.Void Mirror.MultiplexTransport::ServerSend(System.Int32,System.Int32,System.ArraySegment`1<System.Byte>)
extern void MultiplexTransport_ServerSend_mB3D4F80AE321173C34D69C7FCF81B87C3DAFE043 (void);
// 0x0000040E System.Void Mirror.MultiplexTransport::ServerStart()
extern void MultiplexTransport_ServerStart_m30F65C9378DDB1B84E9C9C25D69F8E2EE1D5A887 (void);
// 0x0000040F System.Void Mirror.MultiplexTransport::ServerStop()
extern void MultiplexTransport_ServerStop_mC1038F61612FC37D90432BEB8E5F2B50672FB447 (void);
// 0x00000410 System.Int32 Mirror.MultiplexTransport::GetMaxPacketSize(System.Int32)
extern void MultiplexTransport_GetMaxPacketSize_m8E1ED512A8C33A0583C506866631DA35575CF644 (void);
// 0x00000411 System.Void Mirror.MultiplexTransport::Shutdown()
extern void MultiplexTransport_Shutdown_mCD3B2370DD52E5F859EC07816FA6BC662A62631A (void);
// 0x00000412 System.String Mirror.MultiplexTransport::ToString()
extern void MultiplexTransport_ToString_m322B81C3495ED803F44376B5E317216B4BA2E441 (void);
// 0x00000413 System.Void Mirror.MultiplexTransport::.ctor()
extern void MultiplexTransport__ctor_mEE455ED05170C5F0676EB3F47E5529D8D3637A4B (void);
// 0x00000414 System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m5F63E485307DE66FF8DF6DA73B14BD0A8C596679 (void);
// 0x00000415 System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__0_m71C260E9F663345051F4ED0DBE5FD51CB051374E (void);
// 0x00000416 System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__1(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__1_mAEB5E470043FEFE9ACA5895E6903C43283DB85D1 (void);
// 0x00000417 System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__2(System.Int32,System.Exception)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__2_m20358BC676721F4AF2C4443B36DF2E03E7DDAFD3 (void);
// 0x00000418 System.Void Mirror.MultiplexTransport/<>c__DisplayClass18_0::<AddServerCallbacks>b__3(System.Int32)
extern void U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__3_m3D94991394A01162B4B35D5D975273EE53A43152 (void);
// 0x00000419 System.Void Mirror.TelepathyTransport::Awake()
extern void TelepathyTransport_Awake_m602F5715334063DC6405856C3AD1FC29C7B7E5A5 (void);
// 0x0000041A System.Boolean Mirror.TelepathyTransport::Available()
extern void TelepathyTransport_Available_mF8EDA3882E796A7C821F5205051E549927AFEB80 (void);
// 0x0000041B System.Boolean Mirror.TelepathyTransport::ClientConnected()
extern void TelepathyTransport_ClientConnected_m8B5228FFC261001AAFD1F4C1DACEAB32FB728BFA (void);
// 0x0000041C System.Void Mirror.TelepathyTransport::ClientConnect(System.String)
extern void TelepathyTransport_ClientConnect_mEC8326A6AE0253B05D929D98B37E774134A8320D (void);
// 0x0000041D System.Void Mirror.TelepathyTransport::ClientConnect(System.Uri)
extern void TelepathyTransport_ClientConnect_m27B5C086CB7ED8E51EDF0D922FACFBEE88CB2BB5 (void);
// 0x0000041E System.Void Mirror.TelepathyTransport::ClientSend(System.Int32,System.ArraySegment`1<System.Byte>)
extern void TelepathyTransport_ClientSend_m95DA556F300DB657FF0FC0A54E0BCCCE883D9F94 (void);
// 0x0000041F System.Void Mirror.TelepathyTransport::ClientDisconnect()
extern void TelepathyTransport_ClientDisconnect_m0A27B27ECCDAF5C60DC6777E95D6BA075FCEA4AC (void);
// 0x00000420 System.Void Mirror.TelepathyTransport::ClientEarlyUpdate()
extern void TelepathyTransport_ClientEarlyUpdate_m03B06C08025B8191282BEE1F6A9EC25C0EE47D34 (void);
// 0x00000421 System.Uri Mirror.TelepathyTransport::ServerUri()
extern void TelepathyTransport_ServerUri_mD3896EBAB74A7FE4BE2970ACB5B8930A1C064D0B (void);
// 0x00000422 System.Boolean Mirror.TelepathyTransport::ServerActive()
extern void TelepathyTransport_ServerActive_m7E31D881004FB4A68CE8E0C7F5C2D3C18ED9909E (void);
// 0x00000423 System.Void Mirror.TelepathyTransport::ServerStart()
extern void TelepathyTransport_ServerStart_m6F3609E2583811D26FB3ED5DF916A40EF848F84D (void);
// 0x00000424 System.Void Mirror.TelepathyTransport::ServerSend(System.Int32,System.Int32,System.ArraySegment`1<System.Byte>)
extern void TelepathyTransport_ServerSend_m998FDA65A8819E3C5470BC34F2D68937E717B838 (void);
// 0x00000425 System.Boolean Mirror.TelepathyTransport::ServerDisconnect(System.Int32)
extern void TelepathyTransport_ServerDisconnect_mADA1668FE8AB923684F437B38CA59255913144E3 (void);
// 0x00000426 System.String Mirror.TelepathyTransport::ServerGetClientAddress(System.Int32)
extern void TelepathyTransport_ServerGetClientAddress_mCAAE5E776410D1E8F8CB92D5AE547207F4F32EEE (void);
// 0x00000427 System.Void Mirror.TelepathyTransport::ServerStop()
extern void TelepathyTransport_ServerStop_m029E82E1FC85B297A37C856ACBB2471C6410E2FB (void);
// 0x00000428 System.Void Mirror.TelepathyTransport::ServerEarlyUpdate()
extern void TelepathyTransport_ServerEarlyUpdate_m970EF7D651ED0FC5FD4898CEDC3878892DC820E0 (void);
// 0x00000429 System.Void Mirror.TelepathyTransport::Shutdown()
extern void TelepathyTransport_Shutdown_mA4F85A710C7C52BB890A74C4942EC7BA64F9FB40 (void);
// 0x0000042A System.Int32 Mirror.TelepathyTransport::GetMaxPacketSize(System.Int32)
extern void TelepathyTransport_GetMaxPacketSize_m81DCD6F50C02F566D328EB5097BEE7081C544A2B (void);
// 0x0000042B System.String Mirror.TelepathyTransport::ToString()
extern void TelepathyTransport_ToString_m528636DD99B5B7A9346BC0B4E628E3C07CAA954C (void);
// 0x0000042C System.Void Mirror.TelepathyTransport::.ctor()
extern void TelepathyTransport__ctor_mA4061E623CDE35A23989AE27570AB68007F43F9D (void);
// 0x0000042D System.Void Mirror.TelepathyTransport::<Awake>b__16_0()
extern void TelepathyTransport_U3CAwakeU3Eb__16_0_m905CFDDDDE4DC4C2DF534C3785C594E98316A038 (void);
// 0x0000042E System.Void Mirror.TelepathyTransport::<Awake>b__16_1(System.ArraySegment`1<System.Byte>)
extern void TelepathyTransport_U3CAwakeU3Eb__16_1_mADDB68E8C56E511F7B71AF2C9CF3A450C7F5DBF7 (void);
// 0x0000042F System.Void Mirror.TelepathyTransport::<Awake>b__16_2()
extern void TelepathyTransport_U3CAwakeU3Eb__16_2_mB5CD19395BFC6D0020C35F8D6C4CE87624B7CDB6 (void);
// 0x00000430 System.Void Mirror.TelepathyTransport::<Awake>b__16_3(System.Int32)
extern void TelepathyTransport_U3CAwakeU3Eb__16_3_m449CD1AB3CC3F97916947FCD69B0412704FF1A1E (void);
// 0x00000431 System.Void Mirror.TelepathyTransport::<Awake>b__16_4(System.Int32,System.ArraySegment`1<System.Byte>)
extern void TelepathyTransport_U3CAwakeU3Eb__16_4_m2999458B1882358091529A2D113CDD0681BAF6B9 (void);
// 0x00000432 System.Void Mirror.TelepathyTransport::<Awake>b__16_5(System.Int32)
extern void TelepathyTransport_U3CAwakeU3Eb__16_5_m5C9F8614ED86F5630676C4E29B4423EA0DD5D9F6 (void);
// 0x00000433 System.Boolean Mirror.TelepathyTransport::<Awake>b__16_6()
extern void TelepathyTransport_U3CAwakeU3Eb__16_6_m874F866E355A4DCE73B942608DAF1DBCFEF1B9A7 (void);
// 0x00000434 System.Boolean Mirror.Transport::Available()
// 0x00000435 System.Boolean Mirror.Transport::ClientConnected()
// 0x00000436 System.Void Mirror.Transport::ClientConnect(System.String)
// 0x00000437 System.Void Mirror.Transport::ClientConnect(System.Uri)
extern void Transport_ClientConnect_mBA9A630061B0F73E6B0383A762693985C3AEEA6D (void);
// 0x00000438 System.Void Mirror.Transport::ClientSend(System.Int32,System.ArraySegment`1<System.Byte>)
// 0x00000439 System.Void Mirror.Transport::ClientDisconnect()
// 0x0000043A System.Uri Mirror.Transport::ServerUri()
// 0x0000043B System.Boolean Mirror.Transport::ServerActive()
// 0x0000043C System.Void Mirror.Transport::ServerStart()
// 0x0000043D System.Void Mirror.Transport::ServerSend(System.Int32,System.Int32,System.ArraySegment`1<System.Byte>)
// 0x0000043E System.Boolean Mirror.Transport::ServerDisconnect(System.Int32)
// 0x0000043F System.String Mirror.Transport::ServerGetClientAddress(System.Int32)
// 0x00000440 System.Void Mirror.Transport::ServerStop()
// 0x00000441 System.Int32 Mirror.Transport::GetMaxPacketSize(System.Int32)
// 0x00000442 System.Int32 Mirror.Transport::GetMaxBatchSize(System.Int32)
extern void Transport_GetMaxBatchSize_mCDEC1092208777286584F4982E9713DA5E7DA5E4 (void);
// 0x00000443 System.Void Mirror.Transport::Update()
extern void Transport_Update_mE9D7C9DCA9E54CA74C6D1B47C38834ABE4C7D796 (void);
// 0x00000444 System.Void Mirror.Transport::LateUpdate()
extern void Transport_LateUpdate_m458595D25E89D7222C3424D8A95AE08BF0D646FD (void);
// 0x00000445 System.Void Mirror.Transport::ClientEarlyUpdate()
extern void Transport_ClientEarlyUpdate_m0722DB6D5B6FC924657EAEC9A7E0DAE15497AEBE (void);
// 0x00000446 System.Void Mirror.Transport::ServerEarlyUpdate()
extern void Transport_ServerEarlyUpdate_mFD20DA364D3891A0C0F053D96FECF8E79E717D80 (void);
// 0x00000447 System.Void Mirror.Transport::ClientLateUpdate()
extern void Transport_ClientLateUpdate_mD6FF8477B2174BD8B8DB9EA6F2CB02BDD473A135 (void);
// 0x00000448 System.Void Mirror.Transport::ServerLateUpdate()
extern void Transport_ServerLateUpdate_m0FC3D74ECF4029F06B92999BB34025E58F34CC8C (void);
// 0x00000449 System.Void Mirror.Transport::Shutdown()
// 0x0000044A System.Void Mirror.Transport::OnApplicationQuit()
extern void Transport_OnApplicationQuit_mC51DBEB57BAB46E2B780F49420F5E205D79675BB (void);
// 0x0000044B System.Void Mirror.Transport::.ctor()
extern void Transport__ctor_m18499670B379DE249BA0A4D8D978335F0C1E0376 (void);
// 0x0000044C System.Void Mirror.Transport/<>c::.cctor()
extern void U3CU3Ec__cctor_m304817F15C080D3EEC191046CF1E9890B95CC53C (void);
// 0x0000044D System.Void Mirror.Transport/<>c::.ctor()
extern void U3CU3Ec__ctor_m5310ACAAE5FAD5B20FD9F10931FAB299C78950EC (void);
// 0x0000044E System.Void Mirror.Transport/<>c::<.ctor>b__32_0()
extern void U3CU3Ec_U3C_ctorU3Eb__32_0_mF7FDB3F2F0DFF66E559442E14A7D4464D0E14D79 (void);
// 0x0000044F System.Void Mirror.Transport/<>c::<.ctor>b__32_1(System.ArraySegment`1<System.Byte>,System.Int32)
extern void U3CU3Ec_U3C_ctorU3Eb__32_1_m54E5403E2D3851EA7C6A6D343300323AC7110927 (void);
// 0x00000450 System.Void Mirror.Transport/<>c::<.ctor>b__32_2(System.Exception)
extern void U3CU3Ec_U3C_ctorU3Eb__32_2_mEE5BBB94A91E911340820E2237772AFDD1872CD7 (void);
// 0x00000451 System.Void Mirror.Transport/<>c::<.ctor>b__32_3()
extern void U3CU3Ec_U3C_ctorU3Eb__32_3_mC6026CC78089FE13D779CC367A1D87FFCE82D604 (void);
// 0x00000452 System.Void Mirror.Transport/<>c::<.ctor>b__32_4(System.Int32)
extern void U3CU3Ec_U3C_ctorU3Eb__32_4_m195B635652A49A8FC937C05492F1610FAAC6F0BB (void);
// 0x00000453 System.Void Mirror.Transport/<>c::<.ctor>b__32_5(System.Int32,System.ArraySegment`1<System.Byte>,System.Int32)
extern void U3CU3Ec_U3C_ctorU3Eb__32_5_m76101E9DC27C90650C0BA52687E86B115CA542A2 (void);
// 0x00000454 System.Void Mirror.Transport/<>c::<.ctor>b__32_6(System.Int32,System.Exception)
extern void U3CU3Ec_U3C_ctorU3Eb__32_6_m324D78CD5F370CACCC239A8ACAB0E17767C01C1C (void);
// 0x00000455 System.Void Mirror.Transport/<>c::<.ctor>b__32_7(System.Int32)
extern void U3CU3Ec_U3C_ctorU3Eb__32_7_m3976B6943CBB65C428F9FE631793BB65EA539C8D (void);
// 0x00000456 System.Void Mirror.NetworkMessageDelegate::.ctor(System.Object,System.IntPtr)
extern void NetworkMessageDelegate__ctor_m8A66FF4EABB5ED3BEDBA1E5F5BDEF8582502FFEF (void);
// 0x00000457 System.Void Mirror.NetworkMessageDelegate::Invoke(Mirror.NetworkConnection,Mirror.NetworkReader,System.Int32)
extern void NetworkMessageDelegate_Invoke_mAEE9C9A0218BA47AA1BAED072D5042DB7C28E7F1 (void);
// 0x00000458 System.IAsyncResult Mirror.NetworkMessageDelegate::BeginInvoke(Mirror.NetworkConnection,Mirror.NetworkReader,System.Int32,System.AsyncCallback,System.Object)
extern void NetworkMessageDelegate_BeginInvoke_m72BB2CC861D8E6F765FF70B2EA5DDB947385613F (void);
// 0x00000459 System.Void Mirror.NetworkMessageDelegate::EndInvoke(System.IAsyncResult)
extern void NetworkMessageDelegate_EndInvoke_mE3EFA49B7DE1173FC104A1CCEEFCC0DE10114520 (void);
// 0x0000045A System.Void Mirror.SpawnDelegate::.ctor(System.Object,System.IntPtr)
extern void SpawnDelegate__ctor_m85296496F410FDA1A8F167B0E65E39B176491618 (void);
// 0x0000045B UnityEngine.GameObject Mirror.SpawnDelegate::Invoke(UnityEngine.Vector3,System.Guid)
extern void SpawnDelegate_Invoke_m647484B13C3CB741C8D7D910D355FEC904FBFBA3 (void);
// 0x0000045C System.IAsyncResult Mirror.SpawnDelegate::BeginInvoke(UnityEngine.Vector3,System.Guid,System.AsyncCallback,System.Object)
extern void SpawnDelegate_BeginInvoke_m6BDC7169A26F222C731A4CC93B5D2C1A9A5DCF7B (void);
// 0x0000045D UnityEngine.GameObject Mirror.SpawnDelegate::EndInvoke(System.IAsyncResult)
extern void SpawnDelegate_EndInvoke_m3DC601F0E76C04C3159289012B2B4AC40BDA0AB5 (void);
// 0x0000045E System.Void Mirror.SpawnHandlerDelegate::.ctor(System.Object,System.IntPtr)
extern void SpawnHandlerDelegate__ctor_m5D9B376DBB3C85A6B66ED69AB8B7E7010A3130C9 (void);
// 0x0000045F UnityEngine.GameObject Mirror.SpawnHandlerDelegate::Invoke(Mirror.SpawnMessage)
extern void SpawnHandlerDelegate_Invoke_m8DCF7DA5325363F4860AF5A135A7D9318A8C21D0 (void);
// 0x00000460 System.IAsyncResult Mirror.SpawnHandlerDelegate::BeginInvoke(Mirror.SpawnMessage,System.AsyncCallback,System.Object)
extern void SpawnHandlerDelegate_BeginInvoke_mA8C0FE11CC475B4F433E7CE81A0D91EDC68631C4 (void);
// 0x00000461 UnityEngine.GameObject Mirror.SpawnHandlerDelegate::EndInvoke(System.IAsyncResult)
extern void SpawnHandlerDelegate_EndInvoke_m20D6E4B8796890F64D0FDF48450469B8FF716965 (void);
// 0x00000462 System.Void Mirror.UnSpawnDelegate::.ctor(System.Object,System.IntPtr)
extern void UnSpawnDelegate__ctor_mB9B060274806A21287BA156858B167BDCC028AE1 (void);
// 0x00000463 System.Void Mirror.UnSpawnDelegate::Invoke(UnityEngine.GameObject)
extern void UnSpawnDelegate_Invoke_m253B17666AEEE399A94FF14D3B4FA5AFD7078D57 (void);
// 0x00000464 System.IAsyncResult Mirror.UnSpawnDelegate::BeginInvoke(UnityEngine.GameObject,System.AsyncCallback,System.Object)
extern void UnSpawnDelegate_BeginInvoke_m281EC91CBC5162D34ACF38393B3881A800CE3058 (void);
// 0x00000465 System.Void Mirror.UnSpawnDelegate::EndInvoke(System.IAsyncResult)
extern void UnSpawnDelegate_EndInvoke_mECA5B4C0F9415708A4E4D354020E3F4F48927B57 (void);
// 0x00000466 System.UInt32 Mirror.Utils::GetTrueRandomUInt()
extern void Utils_GetTrueRandomUInt_m42E9457ACD0D68D5B81A8B127F0F5B3D68C8A60E (void);
// 0x00000467 System.Boolean Mirror.Utils::IsPrefab(UnityEngine.GameObject)
extern void Utils_IsPrefab_mF9AE489EFFD6D206956CB7C33F27A6515578F502 (void);
// 0x00000468 System.Boolean Mirror.Utils::IsSceneObjectWithPrefabParent(UnityEngine.GameObject,UnityEngine.GameObject&)
extern void Utils_IsSceneObjectWithPrefabParent_mF68AE61BD662AC25BD9EA4DDA45CF00D5D5666BE (void);
// 0x00000469 System.Void Mirror.RemoteCalls.CmdDelegate::.ctor(System.Object,System.IntPtr)
extern void CmdDelegate__ctor_m25AAD7CB2265BEF6CC263E5F620322D0B874CAD8 (void);
// 0x0000046A System.Void Mirror.RemoteCalls.CmdDelegate::Invoke(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient)
extern void CmdDelegate_Invoke_m8DA7D950737AA88947D43EBAED1C35604D49DC2B (void);
// 0x0000046B System.IAsyncResult Mirror.RemoteCalls.CmdDelegate::BeginInvoke(Mirror.NetworkBehaviour,Mirror.NetworkReader,Mirror.NetworkConnectionToClient,System.AsyncCallback,System.Object)
extern void CmdDelegate_BeginInvoke_mAF28854F9A731D1171A91241F3529A45D12A66B6 (void);
// 0x0000046C System.Void Mirror.RemoteCalls.CmdDelegate::EndInvoke(System.IAsyncResult)
extern void CmdDelegate_EndInvoke_m307F0E541435EA4AE4C8EF22AECBA53412908553 (void);
// 0x0000046D System.Boolean Mirror.RemoteCalls.Invoker::AreEqual(System.Type,Mirror.MirrorInvokeType,Mirror.RemoteCalls.CmdDelegate)
extern void Invoker_AreEqual_mECE8ABA850E084829C577D749E3E69F164829552 (void);
// 0x0000046E System.Void Mirror.RemoteCalls.Invoker::.ctor()
extern void Invoker__ctor_mAB4C1E969716FCF8DB4257BCF45F152446555DF4 (void);
// 0x0000046F System.Int32 Mirror.RemoteCalls.RemoteCallHelper::GetMethodHash(System.Type,System.String)
extern void RemoteCallHelper_GetMethodHash_mA618B5FB67E05E07A97E30F731F381A7EF5E6AB4 (void);
// 0x00000470 System.Int32 Mirror.RemoteCalls.RemoteCallHelper::RegisterDelegate(System.Type,System.String,Mirror.MirrorInvokeType,Mirror.RemoteCalls.CmdDelegate,System.Boolean)
extern void RemoteCallHelper_RegisterDelegate_mC8AD98E6FE1096C3FE88DAC05A2B09E3CDB5F977 (void);
// 0x00000471 System.Boolean Mirror.RemoteCalls.RemoteCallHelper::CheckIfDeligateExists(System.Type,Mirror.MirrorInvokeType,Mirror.RemoteCalls.CmdDelegate,System.Int32)
extern void RemoteCallHelper_CheckIfDeligateExists_mB7136608721A4539332B7C090A64CA13BEFCDBAA (void);
// 0x00000472 System.Void Mirror.RemoteCalls.RemoteCallHelper::RegisterCommandDelegate(System.Type,System.String,Mirror.RemoteCalls.CmdDelegate,System.Boolean)
extern void RemoteCallHelper_RegisterCommandDelegate_m196440B96E1D7A909D38037A770734FC5139842F (void);
// 0x00000473 System.Void Mirror.RemoteCalls.RemoteCallHelper::RegisterRpcDelegate(System.Type,System.String,Mirror.RemoteCalls.CmdDelegate)
extern void RemoteCallHelper_RegisterRpcDelegate_m3381432C36F433DF9E6D924514BEBAD8B37B7AC9 (void);
// 0x00000474 System.Void Mirror.RemoteCalls.RemoteCallHelper::RemoveDelegate(System.Int32)
extern void RemoteCallHelper_RemoveDelegate_m5E404B31C4616919F1755E414DF3C115693EC696 (void);
// 0x00000475 System.Boolean Mirror.RemoteCalls.RemoteCallHelper::GetInvokerForHash(System.Int32,Mirror.MirrorInvokeType,Mirror.RemoteCalls.Invoker&)
extern void RemoteCallHelper_GetInvokerForHash_mEF65749F25DF0C8F8770F042978A52296834866E (void);
// 0x00000476 System.Boolean Mirror.RemoteCalls.RemoteCallHelper::InvokeHandlerDelegate(System.Int32,Mirror.MirrorInvokeType,Mirror.NetworkReader,Mirror.NetworkBehaviour,Mirror.NetworkConnectionToClient)
extern void RemoteCallHelper_InvokeHandlerDelegate_mD913C9F9303C0186A3BF47E09DDCEFBACB7459B9 (void);
// 0x00000477 Mirror.RemoteCalls.CommandInfo Mirror.RemoteCalls.RemoteCallHelper::GetCommandInfo(System.Int32,Mirror.NetworkBehaviour)
extern void RemoteCallHelper_GetCommandInfo_m26146856DB6082F9D0AE81E0276C9455CE2969A3 (void);
// 0x00000478 Mirror.RemoteCalls.CmdDelegate Mirror.RemoteCalls.RemoteCallHelper::GetDelegate(System.Int32)
extern void RemoteCallHelper_GetDelegate_mB573EBBF9D129EE42980B107FBE0E0FADD2F50E5 (void);
// 0x00000479 System.Void Mirror.RemoteCalls.RemoteCallHelper::.cctor()
extern void RemoteCallHelper__cctor_mE8A90B144EE99C80B996010B2C7D7D4D9F0E502B (void);
static Il2CppMethodPointer s_methodPointers[1145] = 
{
	EmbeddedAttribute__ctor_mBAD8A67721F7598BE1151A9142ACE8229337284E,
	IsReadOnlyAttribute__ctor_m2EA1DDCC3375C83D2922C167CD0BA462DA85B421,
	HelpAttribute__ctor_m63346A32916D628231B0C7F7A414557F38012EE0,
	LRMDirectConnectModule_Awake_mEC4458D15832F875FAFE029FC064855739FB9535,
	LRMDirectConnectModule_StartServer_m546F44C56D6F7EF5224AE478A0D2FDDE004B7D0D,
	LRMDirectConnectModule_StopServer_mDAFAB2CC6E40F30CD73284F4F1F7028E63A01E22,
	LRMDirectConnectModule_JoinServer_m01BB689C9D3F72AB79B90BF262E1F233FC773E61,
	LRMDirectConnectModule_SetTransportPort_mE74ECF7015E531FB4177DFCC9642ACDEE861CEC7,
	LRMDirectConnectModule_GetTransportPort_m8F30CFB6FC3D84D45267C9220E0D96EEB1835AF8,
	LRMDirectConnectModule_SupportsNATPunch_m8789E7511768E43970EB97781351147A822A5A15,
	LRMDirectConnectModule_KickClient_mCA064B64D2D4A5D11398F3A9EB470785C9FAE206,
	LRMDirectConnectModule_ClientDisconnect_mCCC633387389458A30BFB3E7241FC434210D3F40,
	LRMDirectConnectModule_ServerSend_mEBD4B72ED1D613F094FB86DA28D3D48D4C5AA40B,
	LRMDirectConnectModule_ClientSend_mBADEF5ACE416C5F3AAE7B377DF89C32AE894CB7C,
	LRMDirectConnectModule_OnServerConnected_m4CEE888E150C215DF56CA7426FD82229781A1FA3,
	LRMDirectConnectModule_OnServerDataReceived_m55FF68D55DFA42E8BD75937FAB854953ABF7B56D,
	LRMDirectConnectModule_OnServerDisconnected_mDE779FE03224232B24EBAD2D3611BE25A2399EDD,
	LRMDirectConnectModule_OnServerError_mC6C29AED15ED6A386C58AF9851AE76391BDAE26F,
	LRMDirectConnectModule_OnClientConnected_mA3201A9FAB69D57F1346DAE49A82CF794A78F5FB,
	LRMDirectConnectModule_OnClientDisconnected_mA4560F887041E5F2A4EE87DD8378F1EAA302F0AB,
	LRMDirectConnectModule_OnClientDataReceived_m5C24E9CADE03E9941A3805A0942B1205A5F520E8,
	LRMDirectConnectModule_OnClientError_mC32910382ADC51E2A3C6330460F2BA4C11869582,
	LRMDirectConnectModule__ctor_mF00514B8F505588DB473BAC7124AEDEE67850518,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LRMTools_WriteByte_m46932D1564BBD00029DA6A3F0DC7B6DC07061307,
	LRMTools_ReadByte_m4CF4175B9617DA677BAE555C76807B992E9B5B7E,
	LRMTools_WriteBool_m9681206EA15FF3F7D26B4ED2E1461E786F0C4C7F,
	LRMTools_ReadBool_m23C9E333CC44B6120BE7FDB7CA3898313F913021,
	LRMTools_WriteString_m670EAF5DE20387F8917BD9B73302BDAE51D356A4,
	LRMTools_ReadString_mF27A9D875373AA7FB836D41568ACE9B4F778272A,
	LRMTools_WriteBytes_mD65C540FF51B3D7D27BBA01B66927BF0F77F7AE3,
	LRMTools_ReadBytes_m980980D75AA20094E855A3F2489C00A5C6D635DF,
	LRMTools_WriteChar_mD44F266656465556B9D8A545527B85A452E1D99D,
	LRMTools_ReadChar_m519D7FA943126A3E42E17F62AC045D60C3F909FB,
	LRMTools_WriteInt_m61658435FBD3024FD1919E948AFEA66DF95FB553,
	LRMTools_ReadInt_m640B1BD8E7E1A324D6E4FD99363BFE8689FFB458,
	CompressorExtensions_Decompress_mABE9D3BDB437B0B11F92BD054FA57F58BD26FFEB,
	LightReflectiveMirrorTransport_set_relayServerList_m864AAF361A722A5FAA238BE1C6DA01F38224AD00,
	LightReflectiveMirrorTransport_get_relayServerList_mC55A665EF2A23B7AEA6104117D9E002510ED1C12,
	LightReflectiveMirrorTransport_ClientConnected_m417C77CBD1763FF00BB0C53118AF78B5F997F3CB,
	LightReflectiveMirrorTransport_OnConnectedToRelay_mC85C728706F33FB359A8C1A0BC77F287C569FA78,
	LightReflectiveMirrorTransport_IsAuthenticated_mCE1748BE7D14614B57960E53E3D7E2FDCD5D9DC9,
	LightReflectiveMirrorTransport_ServerActive_mA8602CB7D1E57D1A6E83CF6E52A0A4AAF0AFAEAE,
	LightReflectiveMirrorTransport_Available_m195D962667B9760CC36E2676140969810A358C06,
	LightReflectiveMirrorTransport_ClientConnect_m32A35C87C480F1828648AA4EC981A2E914B7670E,
	LightReflectiveMirrorTransport_GetMaxPacketSize_mDC3F1A4273C1C4C69649F3E54E3F15B7DAD09EEF,
	LightReflectiveMirrorTransport_ServerGetClientAddress_m2EF195FD89F310B3ADE080499874E5D8880CFAE2,
	LightReflectiveMirrorTransport_ClientEarlyUpdate_m24057440EEC5DE42DF48E1F1CAF5AB67746C75C1,
	LightReflectiveMirrorTransport_ClientLateUpdate_mBFFD9F2AF42685FBAB7632D5D6084DB807A20866,
	LightReflectiveMirrorTransport_ServerEarlyUpdate_m28AC3BC848D37E82FD795994E3211C77F164E049,
	LightReflectiveMirrorTransport_RecvData_m60237396B44A7433664C4786C8644D800BC987C9,
	LightReflectiveMirrorTransport_ServerProcessProxyData_m7815E3A19FF1C7078B3D2299F449BB37BD5C0490,
	LightReflectiveMirrorTransport_ClientProcessProxyData_mC11BD7F61EC9994D18A46AA6C51BC557102EA929,
	LightReflectiveMirrorTransport_ServerLateUpdate_m13E5B4F9E1E8DFBAB7D43A8B7F581E431AA4366B,
	LightReflectiveMirrorTransport_Awake_m135B27F6A5B6C429D8AF5EF1E50289FB2606D941,
	LightReflectiveMirrorTransport_SetupCallbacks_m3B5035292970239A80E9FF178AD937D5AC36D07F,
	LightReflectiveMirrorTransport_Disconnected_m5557C59387B692554FD445860800284B32C01C7F,
	LightReflectiveMirrorTransport_ConnectToRelay_m218751FC55C78E57A8320BAA718F7B1666F81A9A,
	LightReflectiveMirrorTransport_SendHeartbeat_m787EB035A673E35BE7B1A87DECDB901B10588EE6,
	LightReflectiveMirrorTransport_RequestServerList_m97128167F0BA44C83429C4DCC4197703F1D58A46,
	LightReflectiveMirrorTransport_NATPunch_m262DFE42E3FA2E16A28CC594EEED355092BBEB25,
	LightReflectiveMirrorTransport_DataReceived_m76C79362AE4F92138A231FFE3E3CFAE9B8AF7914,
	LightReflectiveMirrorTransport_GetServerList_m23FA6123771FB2F1573D3E7191DC7488AF648DF7,
	LightReflectiveMirrorTransport_UpdateRoomInfo_mD3DE38A013C8171A229930BF117559687FBEBC6A,
	LightReflectiveMirrorTransport_SendAuthKey_mBE4CDC5040585E06614E028FC8DE5107280F3DC6,
	LightReflectiveMirrorTransport_ClientConnect_m0B76174186B97A4370724519FDD1975B3359779A,
	LightReflectiveMirrorTransport_ClientDisconnect_mE564E5030FBA427C4A3CF5639476E525A23C29AD,
	LightReflectiveMirrorTransport_ClientSend_mFF858CCD0DBECAB115086367C625B11475EB673B,
	LightReflectiveMirrorTransport_ServerDisconnect_m4A6C1C0A484C6E6020870B738655E30BFD5314AE,
	LightReflectiveMirrorTransport_ServerSend_m95119F0BECC59C1A7A642E6B78C86CDFD66DE954,
	LightReflectiveMirrorTransport_ServerStart_m22919D8DA904D7598FEA55516CBD6579950EEC3A,
	LightReflectiveMirrorTransport_ServerStop_mBE2F5610041D3BD91E23B11E53250036ECEFC4DF,
	LightReflectiveMirrorTransport_ServerUri_m4B53BE39251E16F005D57D087F3E179C8B78A349,
	LightReflectiveMirrorTransport_Shutdown_m62B44DA6A3419EBD6A2B436DEB6A9160FEB750F3,
	LightReflectiveMirrorTransport_GetLocalIp_mBD8393477D0C387F14DF44483A8A1C85A78BFCE7,
	LightReflectiveMirrorTransport_DirectAddClient_mA8765DD930FB2E2AF31ADC909E564AA203972478,
	LightReflectiveMirrorTransport_DirectRemoveClient_m02BD751B1D2AD6B2BE2ABBE41CF27B45E49EB9F9,
	LightReflectiveMirrorTransport_DirectReceiveData_m895AFBD7DEE630965609016AF7F2E17E2D1BF44F,
	LightReflectiveMirrorTransport_DirectClientConnected_mBB3350A5BD5566BC34EA33263202D965DDF6109C,
	LightReflectiveMirrorTransport_DirectDisconnected_m18F01A13BF74EC522C56B9642E3041529F5C1563,
	LightReflectiveMirrorTransport__ctor_m69FB2137F359F74F3AC3694FAAAF46C188E09892,
	U3CNATPunchU3Ed__60__ctor_m8AFFC096C0D5CB45DB1ADD9963088414BED799A3,
	U3CNATPunchU3Ed__60_System_IDisposable_Dispose_m24B6085FE737785C5AA79962EE4C7430ADCFAE59,
	U3CNATPunchU3Ed__60_MoveNext_m938D530EE227ACC4CF6752F2C6B3529D05784548,
	U3CNATPunchU3Ed__60_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF552C1CA316A3847A928D558B9FA7C9132F77719,
	U3CNATPunchU3Ed__60_System_Collections_IEnumerator_Reset_mF9BB6188618B8206B2210F35BE183A7B269DC967,
	U3CNATPunchU3Ed__60_System_Collections_IEnumerator_get_Current_m48C8FA01805E9BDE54DD0BE71475F963E37BBD66,
	U3CGetServerListU3Ed__62__ctor_mF2829A884C695D524F5DEFD794A050B9D5287B51,
	U3CGetServerListU3Ed__62_System_IDisposable_Dispose_m1E30448DCC8FF3CDC1A67E590C43DA3C875F54C9,
	U3CGetServerListU3Ed__62_MoveNext_mEC4597B38482B5A0BE251FE286B903BA09516FB9,
	U3CGetServerListU3Ed__62_U3CU3Em__Finally1_m5AC54002B1028E121C7CCB1716CECA48FD620E3C,
	U3CGetServerListU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m07BAE2737A26A9B0EF500D9B43C6B2599166386C,
	U3CGetServerListU3Ed__62_System_Collections_IEnumerator_Reset_m41CD9566134AA4481D77F62703EBD7D48CBFAF2F,
	U3CGetServerListU3Ed__62_System_Collections_IEnumerator_get_Current_mDC593EA4675E9C4334BCC34C0BBD7812DA961CBD,
	SocketProxy__ctor_m5AFC2469A9389CB8736A4490A8C54D42BCA39563,
	SocketProxy__ctor_m9A2BE733F6825F86A883D572ECE79CBE6BC49144,
	SocketProxy_RelayData_mFD76CE0CF373054899441994B01C15BFD4342761,
	SocketProxy_ClientRelayData_m2ACCB759BE9FF926CAAF282017BBFA62E32FDDCF,
	SocketProxy_Dispose_mF8A1212804F0FF230BC2D9B1FF02295189C759A5,
	SocketProxy_RecvData_mA02E281CDE6D06A99BF178F0D474832C7946EA22,
	KcpTransport_Awake_m62B40AF0A488B3ECE15089856A16BE4623A51BEE,
	KcpTransport_Available_mC28A19601D78A80E81F13231A059929D2E118C54,
	KcpTransport_ClientConnected_m4370351A8E8B89BABF8AC5BF2423F05BEC959358,
	KcpTransport_ClientConnect_mDB9736490D95A3F288721B88A33DC79FCEFB98DD,
	KcpTransport_ClientSend_mB01F20BA3B12A84B379AD1C9E2630E8C902E1F03,
	KcpTransport_ClientDisconnect_mADDC6439D972EC6CA801E57F55FE211B755093B1,
	KcpTransport_ClientEarlyUpdate_m4AE48F297CFB0B6ED62E6C89FF7AD5B5BE04B1CF,
	KcpTransport_ClientLateUpdate_mFCA6124C57C7836AE664951F986CCB11F3206D75,
	KcpTransport_OnEnable_m41EBE345D83D2C022290C3ED0027822F97CA7AC2,
	KcpTransport_OnDisable_m8E7252C83ABAC0CBDC39BB4D95C7A9891AE7A05E,
	KcpTransport_ServerUri_mB32E80AE3973B31A562FA9BE8954A9EC833B3E2D,
	KcpTransport_ServerActive_mFFF858F3F362B80BFBB8DA38870901126AD33D0F,
	KcpTransport_ServerStart_mB19F14CED645C0AF8BEE683B2911105972CB4B17,
	KcpTransport_ServerSend_mD2BF36CDE8B85CF99D1C4E149FCFC53E18288968,
	KcpTransport_ServerDisconnect_m3A57930E65019F03CD4E60CA12D37F32F9465A74,
	KcpTransport_ServerGetClientAddress_m0DC394B917B355C3FD4798EE0C0022B438188BCA,
	KcpTransport_ServerStop_m3EE5CD2DE9953E2EFE10720382DC9D5DC4F13E3C,
	KcpTransport_ServerEarlyUpdate_m79B9CC4F151CD8FCE544D2A8F549B54A049BCEEF,
	KcpTransport_ServerLateUpdate_m06B80D50F2CC1A33AC95FAE891E4E95F5B2DF97B,
	KcpTransport_Shutdown_m6CF63A2AC8B565B3411F462352420A5427A0A3BC,
	KcpTransport_GetMaxPacketSize_mEB9B1E5DE977F2A6225576122ECE6C505B8DC257,
	KcpTransport_GetMaxBatchSize_m22EDA55F53E193054768C72C67DBF68FB91F1E0A,
	KcpTransport_GetAverageMaxSendRate_mA9F73B527E675130B8EB848A0F5A760F401108E1,
	KcpTransport_GetAverageMaxReceiveRate_mFFB2312D0735C076527DAB81D7A3D033FD1BA2C6,
	KcpTransport_GetTotalSendQueue_m53E4091F9E3B93F287C7672C3210C33A1826B57A,
	KcpTransport_GetTotalReceiveQueue_m0ECBEBD26783AEB47B7CD98CDDDD0AEC4F3F5E75,
	KcpTransport_GetTotalSendBuffer_m62E270E288AEE80D94074FAE809462D4E4EA3EDF,
	KcpTransport_GetTotalReceiveBuffer_m21FE207058439DCAE1BDE0AF7BEC22C098C2025C,
	KcpTransport_PrettyBytes_m60CBBB4165C1D52DD392C9D6E4F7359F28345CA4,
	KcpTransport_OnGUI_m70129E74516C2A543EA6EE0C11023677CC8BD2EA,
	KcpTransport_OnLogStatistics_m6251A4F09FBD2348C24444E46A89E385F32D6693,
	KcpTransport_ToString_mAE9B5329A0035FC14BD3ECB6BFAA3E3B9C0911E5,
	KcpTransport__ctor_m0CF4CC09A52CDE5F1CB9A576C0650B0FA7A21075,
	KcpTransport_U3CAwakeU3Eb__13_1_mBA80BFEA53D389F1C6CD1A9DA97012FD408E2A8E,
	KcpTransport_U3CAwakeU3Eb__13_2_m0F9E548314A3B634B62487A2562C810B229D4C40,
	KcpTransport_U3CAwakeU3Eb__13_3_mB5E4C4292B31BED5A89C5A9C2854D9EBEF3454B0,
	KcpTransport_U3CAwakeU3Eb__13_4_m59DD0FFE7DA7A5830C6EF59F4394C18BC0BD1754,
	KcpTransport_U3CAwakeU3Eb__13_5_mE5452CB5EC509ACA8CE7AAC4CBD5BC849CEC8B03,
	KcpTransport_U3CAwakeU3Eb__13_6_mA75EC14D01A4EAD0B4FA2FAD0DD5DF09D76E7359,
	U3CU3Ec__cctor_m74DDBDACB2E47803B0ECB09111C9BCD8E1FBC2FF,
	U3CU3Ec__ctor_m198B95BF0E13C9FF9E4CF6A86E7F21801E5234E0,
	U3CU3Ec_U3CAwakeU3Eb__13_0_m734C59CAF6A3A5F86C7C74B6D0B502FE1660C9E9,
	U3CU3Ec_U3CGetAverageMaxSendRateU3Eb__35_0_m4FA0E31E7D8E09D87780AE77B6ADBF3B4C67F39C,
	U3CU3Ec_U3CGetAverageMaxReceiveRateU3Eb__36_0_m35CC40E5FEEBD969C13D40C1D310FC9ECAB1DBA3,
	U3CU3Ec_U3CGetTotalSendQueueU3Eb__37_0_mA750649976DD14B26E587CF216B33FF05773C12C,
	U3CU3Ec_U3CGetTotalReceiveQueueU3Eb__38_0_m2CC37246A2B07544CE74082F7DFD5B4B68663AB9,
	U3CU3Ec_U3CGetTotalSendBufferU3Eb__39_0_m86D09D49E1E98A625F5CB78DF23CED311719A71A,
	U3CU3Ec_U3CGetTotalReceiveBufferU3Eb__40_0_m7028B49A8E209110599965F22C9E24A4148922DB,
	SyncVarAttribute__ctor_m4E99C2B45DDA311F1D9B684B967978541348C875,
	CommandAttribute__ctor_m583940E69A6305050FEF54E57CBAF339A8E77FFC,
	ClientRpcAttribute__ctor_mCE8A483D94F06D1FA29EA2342BE860018B29BCA7,
	TargetRpcAttribute__ctor_m5954E4349796FC0A7AA8BE4857791C38E867CA8C,
	ServerAttribute__ctor_m413F135143ED76C9B8C3F41B2DB6597E4382E0A6,
	ServerCallbackAttribute__ctor_m7258C253CBA5BF5C92BAAB6345240F9418C0B61D,
	ClientAttribute__ctor_mD3E93370918D30F063F1CBCA1CF1BC46B90D9065,
	ClientCallbackAttribute__ctor_m1CCA6B90CA8E84DD7A6C6AC9C862434AA9560703,
	SceneAttribute__ctor_m7F8095E94F9D74BDB458EE8B5CF10781C8EEEB62,
	ShowInInspectorAttribute__ctor_mFA3EF79FD7E9F573D31F156DE62A6CBDBE254688,
	ClientScene_get_localPlayer_m7354A403C57BAD08DDD216140CAD2DADF66E14CB,
	ClientScene_set_localPlayer_m9B10A22E524AB15A6AFA5718F1091D376A4F6601,
	ClientScene_get_ready_mB54FAEC0572DA5D58EBB567A0EE6859645447D88,
	ClientScene_set_ready_mABEC71FE5F73FC877590968BE73E334AAE3273E5,
	ClientScene_get_readyConnection_m859DA753D266F4E23C94506EA7DC4B1912F9D7B2,
	ClientScene_set_readyConnection_m3A7D2DB324742C36362604CE01227FF74F6F1A52,
	ClientScene_get_prefabs_mD45B7363513B74457FB9C4F7655871D6B4925581,
	ClientScene_AddPlayer_m717002A8CC2C09CBEC0A1D77CCF3D4FC212D330B,
	ClientScene_Ready_m6F20EC8711B95E25AC2CD86A7AAFFACD94B0B0C8,
	ClientScene_PrepareToSpawnSceneObjects_m76376BBBD0D412406A7DE3F5F53D35DC3321E6C2,
	ClientScene_GetPrefab_m5B13A0AEBB8C211F607E2A30EE6A16EF12A2C2A8,
	ClientScene_RegisterPrefab_m599C53F7DF8D5A96902E837FD34DD50F4ECBCFA0,
	ClientScene_RegisterPrefab_m375B868F146218C25F23599A22850C3720F27A44,
	ClientScene_RegisterPrefab_m32E3953CA679E0B9A810F9A6705C1C7D05C57088,
	ClientScene_RegisterPrefab_mD255CBA550EA3CC7C8526D306ED510A27EF05E48,
	ClientScene_RegisterPrefab_m9EA45290A99BFDA6EA6266E29D82C97ED8B063CC,
	ClientScene_RegisterPrefab_m7697016B21EF3BA13BD038714805EA7603387B7A,
	ClientScene_UnregisterPrefab_mBC420F46C60944C88672D583503567804D99E495,
	ClientScene_RegisterSpawnHandler_m2A4533EA9EFA2F9DE52E7CB876EB7AF0179E4D01,
	ClientScene_RegisterSpawnHandler_m8100B832CE32D2E6B95081A47B385F07364DEEBE,
	ClientScene_UnregisterSpawnHandler_mE01566E3C9DCA81951D4ABBF4F3721AF206164EE,
	ClientScene_ClearSpawners_mC043809CA24CA4132F2B7D1AC836EAD9F0DD0CCA,
	ClientScene_DestroyAllClientObjects_m0E9698A38AEA490C1C862BF296F9646502E2A6C0,
	Compression_LargestAbsoluteComponentIndex_mB176E3E544821911CE8E10C6E766E73063A18F8F,
	Compression_ScaleFloatToUShort_mAFB60A1D68E5F3B953184D66C1DE4A57E9D21A3F,
	Compression_ScaleUShortToFloat_mAB1609B5E7B4C38EAA298BBF3249C06A272B5865,
	Compression_QuaternionElement_m60537C4CF1CA1B44907E0F6D49F7A88E6CF4E604,
	Compression_CompressQuaternion_mC1DD43E3FA152477293A562CA2713DAC89B89CFE,
	Compression_QuaternionNormalizeSafe_mD1517F3E0392C60CCEF02EAC2857A64312F83F09,
	Compression_DecompressQuaternion_m235BB50DA4C0504C27545528ED733C1E0CD246EC,
	ExponentialMovingAverage_get_Value_mB278B5333872C5EAEAF519E1F95BF4B437F1A00E,
	ExponentialMovingAverage_set_Value_m7839F5E48C1B64C159F98A9D14213DD5C390EEA7,
	ExponentialMovingAverage_get_Var_m9ED5A56A0D2B778547F20E4D762562F0F927D8BD,
	ExponentialMovingAverage_set_Var_m58941EF0646BC5D3EF2CD0EDCAC7C6D539AF0D54,
	ExponentialMovingAverage__ctor_m3EB10AAA23643AF85E68D21E0EEDE15219287268,
	ExponentialMovingAverage_Add_mC19B600AC4A4ABB827290B157B0A830103D54F11,
	Extensions_GetStableHashCode_m0592E21267B4D12BAF6A3124018FFDB85AA1EDB9,
	Extensions_GetMethodName_mBD201B7767E472A8E4F2116F18AD20D437783E0F,
	InterestManagement_Awake_m0020EDD8ABA4470A17089BC2A660147FA087F3D9,
	NULL,
	NULL,
	InterestManagement_RebuildAll_m143CB3DE5E99AF95FA1285E5E2D91E5FE2DEA1AD,
	InterestManagement__ctor_mC2CB6AFC383E02ACBABDD728A970658A1C3CCFF4,
	LocalConnectionToClient__ctor_m428D1E3E51E34E982DF40E0DCE961195AA003337,
	LocalConnectionToClient_get_address_m2D7FB00302E2915434C1E5DB9797D4A5A4E33059,
	LocalConnectionToClient_Send_m2D2251FEA3914828444ED06315BFBE2643AF019E,
	LocalConnectionToClient_IsAlive_m7A9507EB1C35012CFF5DB54FC6B30C58232A73D6,
	LocalConnectionToClient_DisconnectInternal_m703C5C7C8D1B2C4C30BF993266497288D47016C2,
	LocalConnectionToClient_Disconnect_m62485E44133767987CFE31DC010CE7281AE84A92,
	LocalConnectionToServer_get_address_m0C6B7946CCE095F5AD805DBD421620B031AA6953,
	LocalConnectionToServer_QueueConnectedEvent_m5248D0F3CB749C8DAB1C8D2F8067D0C4291A03BB,
	LocalConnectionToServer_QueueDisconnectedEvent_m5199F8C95A1A01F7367E0C1B32B2B8E08D673168,
	LocalConnectionToServer_Send_m7101806705A74ECC702860F1DA86A8E61A3FCC7E,
	LocalConnectionToServer_Update_mD992865B7AB76B7164B41CE40EB7ABBD978805D8,
	LocalConnectionToServer_DisconnectInternal_m9F0657A0BEBDCCA743A4F3BBCF6AA4E624A9B89B,
	LocalConnectionToServer_Disconnect_m702169BB2063D9E348421A2EB47E83FF04D31E4D,
	LocalConnectionToServer_IsAlive_m249A5454FEE2D49C444B5D2F181EBB9E773E906D,
	LocalConnectionToServer__ctor_m958A380BF9B110F4AD9C701DE3E61583FE6D58AA,
	NULL,
	NULL,
	MessagePacking_Unpack_mAC1CB53798EC435BEF84F601D015968F78DA809E,
	MessagePacking_UnpackMessage_m6EC1ECC6F23DBC743A41869942E255B6BB3BCB1B,
	NULL,
	NULL,
	NULL,
	MessageBase__ctor_mDC9103481D5807FBF080F7D92B051ED5F642D916,
	NetworkPingMessage__ctor_mF340573CD5E74C9BECF60F7789C8170559A95C5D,
	UnityEventNetworkConnection__ctor_m2E143132E593982075D6CFB3ACE9FC22A2E66F55,
	NetworkAuthenticator_OnStartServer_m846E62361157BE3A9A6280B0314DA5D22BDF11B8,
	NetworkAuthenticator_OnStopServer_m41CE4A338AE0CA96A3BAB3034A5A56A35A4BF72A,
	NULL,
	NetworkAuthenticator_ServerAccept_m07EB37E92BACEE4DD3CCB27ACA443F79276BF322,
	NetworkAuthenticator_ServerReject_m4A3B6983A2212970095DBED053E743E849071034,
	NetworkAuthenticator_OnStartClient_m5C32E06D0A5DD87660C6B25EDD8FBDB30D9BC644,
	NetworkAuthenticator_OnStopClient_m1679E3E9ECFA23353E52BABF80DB494ADE6B5B62,
	NULL,
	NetworkAuthenticator_ClientAccept_mE0A35177C44F4DFD7C20B1A48180E75EEA90467C,
	NetworkAuthenticator_ClientReject_m8FA28F908B17DB620AF67913BD591610BF79B580,
	NetworkAuthenticator_OnValidate_mFD01EC300AB0E1FFEA3E127B9591D5D0FFBCDAA4,
	NetworkAuthenticator__ctor_m1EFE9D58997FA1E95FE79D6196AEBDE5CEC414E5,
	NetworkBehaviour_get_isServer_m6CF3499812C1F2679BB924165AA79C59E6D2EBCF,
	NetworkBehaviour_get_isClient_m87FF41CC03AD448B05627F3711F8E27C63D5C615,
	NetworkBehaviour_get_isLocalPlayer_mFA35EE97B42DEEE92E4FD5562C8C6A1717607DE0,
	NetworkBehaviour_get_isServerOnly_m6247E79FF74BBEC947D4D605095A8A24E5E16FD8,
	NetworkBehaviour_get_isClientOnly_m1082A236ADF41204D352DAEE63F652244950C3B1,
	NetworkBehaviour_get_hasAuthority_m8C249EBFD9F083DE67F38A74E3245727C6035167,
	NetworkBehaviour_get_netId_mFD41F9D183B23443AA528BC0244E0835CCC94826,
	NetworkBehaviour_get_connectionToServer_m269C003EE53BFA1EE6A2DBA16A975723C8829421,
	NetworkBehaviour_get_connectionToClient_m59547ED6321FBCB2B5BFF0B58F22202E981C2BD2,
	NetworkBehaviour_get_syncVarDirtyBits_m1690C7716CF78A4CC6360088FB682385EE76C9E3,
	NetworkBehaviour_set_syncVarDirtyBits_m1FA90B23C70060A48A39E1284A9516B24F996824,
	NetworkBehaviour_getSyncVarHookGuard_m04C7E518D055E786F2EDD648D995543282812D09,
	NetworkBehaviour_setSyncVarHookGuard_m44C276207929797CD147789B1E2D5EFF0E8D0B98,
	NetworkBehaviour_get_netIdentity_m67CAF485E29AFA1CB0540A7ADBE68FCE2326151D,
	NetworkBehaviour_get_ComponentIndex_mACCD123A66C72A3D062535CF936618C2158E9D76,
	NetworkBehaviour_InitSyncObject_m627E07D5152DFE01AC855565E09043BA70692CDF,
	NetworkBehaviour_SendCommandInternal_m81D09DA3B313767B581AFB3F5BC4535807CC5700,
	NetworkBehaviour_SendRPCInternal_m3E76275A3E52E54BF11483D924A21D58285586DF,
	NetworkBehaviour_SendTargetRPCInternal_m0FEC7EA740CF00DABE333A27172BAB1217430756,
	NetworkBehaviour_SyncVarGameObjectEqual_m538EA781DEC2D9D0DF307035D039850460DC5690,
	NetworkBehaviour_SetSyncVarGameObject_m58A39D6DE471F63603EDDA893781EACE528246AF,
	NetworkBehaviour_GetSyncVarGameObject_mBD2A0B361900E77AF101885A491B0921FDA4AE3F,
	NetworkBehaviour_SyncVarNetworkIdentityEqual_mBF505C08B55B2AA9120AE69F365A0C1D7EC092E2,
	NetworkBehaviour_SetSyncVarNetworkIdentity_m662595FF753AE6B0E87DBC7E72517250A4244339,
	NetworkBehaviour_GetSyncVarNetworkIdentity_m7C2CF1934178FA42C8D93CD008F4E174FD944927,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkBehaviour_SetDirtyBit_mDE4C6DE9ECC6E882CBDC4BF66BB65CC3B92A49E5,
	NetworkBehaviour_ClearAllDirtyBits_mD37CF3E0D4864DA51521989A6D3F4D012BC5E3BE,
	NetworkBehaviour_AnySyncObjectDirty_m2A8BAF5C173B492706D9847E02CA23FD799C3F00,
	NetworkBehaviour_IsDirty_m50CC8C762BF12BBF3D796B0266BBF1E0D90BA1EA,
	NetworkBehaviour_OnSerialize_mBE2870081075933577F25C4760A3758FC5A73391,
	NetworkBehaviour_OnDeserialize_mDA83841054FF1970D8C87C246CB56F426195768A,
	NetworkBehaviour_SerializeSyncVars_m4AF8BE097726BA018E3762C14E33BDBF3A5C4819,
	NetworkBehaviour_DeserializeSyncVars_mC87ECFD6F9C2D5C1306ABDF2CF6498864C84677A,
	NetworkBehaviour_DirtyObjectBits_m072FC9684EFE21FB3C5C8743E22D098BD091D8BC,
	NetworkBehaviour_SerializeObjectsAll_mE77BC9DA6B1BA7417C5FF4AA5CB0F31F1882ABF9,
	NetworkBehaviour_SerializeObjectsDelta_mF7FA7AF00832E10CB3A3A9E9E18A9DFEF315C8FF,
	NetworkBehaviour_DeSerializeObjectsAll_m6597B6F683F3DCD0FF34B30AE24A95B5CD004C4F,
	NetworkBehaviour_DeSerializeObjectsDelta_m33229FDA09E4E1EB32F3432C26DFF174563D57CA,
	NetworkBehaviour_ResetSyncObjects_mCC577FB1959F7BE96220152489E55AD993C98D14,
	NetworkBehaviour_OnStartServer_m7E498D9B74008DCD512C4A01C847795893A5ED2D,
	NetworkBehaviour_OnStopServer_m3553611E0C2705734BD005AF6396A6A7C4994EF1,
	NetworkBehaviour_OnStartClient_mBE0F27A3CDF5C4F76C777885B2D21AB60A9647E8,
	NetworkBehaviour_OnStopClient_m6D76DD7B8DC4A4E29D1F58DD69CA6C84E0CC9B81,
	NetworkBehaviour_OnStartLocalPlayer_m884B22DB458ECACC0E00809576CC2DD2F41DE2B4,
	NetworkBehaviour_OnStartAuthority_mC224FDE4601A24F19B2B4A085010764783BB2CED,
	NetworkBehaviour_OnStopAuthority_m3040A6FF682924E89E26C06E7B629D024D87D376,
	NetworkBehaviour__ctor_mB98FF8F52DCEBEB3BC7679DE03FA50785207EE78,
	NetworkBehaviourSyncVar__ctor_mFB219E84A8139BFFCC899543F4057E03D963B0FF,
	NetworkBehaviourSyncVar_Equals_m2EE0BD631368A1EA28D3E4422A9F14C43B9B0A27,
	NetworkBehaviourSyncVar_Equals_m2EE82223A7405A371A233BCED2C78292E69AA85F,
	NetworkBehaviourSyncVar_ToString_m4DE9A94D17F9F28FCDB963AC8665144C37076363,
	NetworkClient_get_connection_m5439CD4BADA80C781B783F013464517F581F9557,
	NetworkClient_set_connection_mED66882874FA7C5891570DFDC873B4EC5B678AFC,
	NetworkClient_get_readyConnection_mE2865B9681090E2743E3B49F44683D0825FC0B05,
	NetworkClient_get_localPlayer_mFA6B51032C92C1B18EAA3B7FCD0369A089A0020C,
	NetworkClient_set_localPlayer_mCBB4FBB1B611801ABEB2B2BBD6A0FD03365D534F,
	NetworkClient_get_serverIp_m3952EAE51FB2B2067755FDFFE7481BB95A4E146B,
	NetworkClient_get_active_m80C7ACA728DE7F6F6B5DC6C0E80B5AD0D287EA37,
	NetworkClient_get_isConnecting_mEFCFA8E2F8F117CED0643652F62F0683B2350C72,
	NetworkClient_get_isConnected_m58D71078B898D6DA731CDC3D4EDA737E7B04A4E6,
	NetworkClient_get_isLocalClient_mE3722DD1EFD1B66370682ACD271ADF980AC15288,
	NetworkClient_AddTransportHandlers_mF0A64666B0628413F9F37D72B169C8A7C9DBC920,
	NetworkClient_RegisterSystemHandlers_m5BF1C0E926656B42C70C67A146B65FFF8378AB55,
	NetworkClient_Connect_m1012B6CB8305CF83DE993CB961DCBAC2E5366489,
	NetworkClient_Connect_mD685E75AAE9CF8F6765C91640C84437777546925,
	NetworkClient_ConnectHost_m2359D0E1763DEE430B6920B2C7B4D67A270927C9,
	NetworkClient_ConnectLocalServer_mCCDFBA08C7B898756DF7812127468B9F68966C7E,
	NetworkClient_Disconnect_mD13276168C810E45CA0ED75ABDC3DBF7CBC2F439,
	NetworkClient_DisconnectLocalServer_m104FA4038FF5069FB3317822722597560503E9ED,
	NetworkClient_OnConnected_mD2B7E0A0007140112D7B406DFAF3E10FAAF5C3ED,
	NetworkClient_OnDataReceived_m6DC574679B247729F8E6E0740D488AA68FF6E1BB,
	NetworkClient_OnDisconnected_m68881B16A8A8A85C1618EAB923DE40E2FA67FEA1,
	NetworkClient_OnError_m3CBE84BB09E2B3B7D4741ECA4F9F2DDAAF5184A1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkClient_GetPrefab_mB24435E12D19697A887BFADE8F8B3D586742999E,
	NetworkClient_RegisterPrefabIdentity_m70F93AE7E4C64AA218627822BB68FFA632E40238,
	NetworkClient_RegisterPrefab_mDDF91101ACDA56F5CF4F69D165E33040C587BA80,
	NetworkClient_RegisterPrefab_mD3E596F19CF1A608C524A4A95CEF56BE6EBC5FF4,
	NetworkClient_RegisterPrefab_mADEC9110EDEDFC7D2769EDFBC5AABE4BA782D24A,
	NetworkClient_RegisterPrefab_m83A8C1F431CC6E48040A642CB71B3B8E2ED91E2B,
	NetworkClient_RegisterPrefab_mFBD356840D4DD5EF716B650E3C09D5A6CA0A23BB,
	NetworkClient_RegisterPrefab_mA11E07E698EDFC41138E2F9FD3A9CCD94ADAEF05,
	NetworkClient_UnregisterPrefab_m624DDE9ADA9681E06079404F2B9A80F9D16023E6,
	NetworkClient_RegisterSpawnHandler_m5246FEC89A9B3C563F54F2775BA6CFFC3BA00D9D,
	NetworkClient_RegisterSpawnHandler_m1391B1F148D5E508A90FCC69B39B4A3913A98AB9,
	NetworkClient_UnregisterSpawnHandler_mFCBC83634E281D816898E45D25C41337B3B69AAE,
	NetworkClient_ClearSpawners_m92F7ADAD3F0327103A44F3D132D9F89E60F917A7,
	NetworkClient_InvokeUnSpawnHandler_m303A55418A1FBA4403512BF5B0D7F037850999DB,
	NetworkClient_Ready_m0290F8ED5D23A2C8DC4DCEAE68295AAC9644B926,
	NetworkClient_Ready_m70AF81088C3D82C47973BD23EF814AD46FE0D58A,
	NetworkClient_InternalAddPlayer_m53CB34CF7951291A275A589AAB770D9668363B70,
	NetworkClient_AddPlayer_m0C6015BEBE42CDDEB5AE5C5F787108737505B107,
	NetworkClient_AddPlayer_mCC3962505C3611B0851E8C102CD374638249D948,
	NetworkClient_ApplySpawnPayload_mEB9CA6042E62F380DA7B3F49BAB4538448FF84FF,
	NetworkClient_FindOrSpawnObject_m73DBFF69AAEC9489D2B8AD54381DB5B39E35A189,
	NetworkClient_GetExistingObject_m8B4FB7CA960913040F4AD6B2082D6106A2E875E2,
	NetworkClient_SpawnPrefab_m31B1515BF10E16933F18CA5930B06BDD0EEF6595,
	NetworkClient_SpawnSceneObject_mECFD944994566F90482C2D8ECE23931DE98D7571,
	NetworkClient_GetAndRemoveSceneObject_mF4E2C3C8F607D9848D5D449AAD77D90A6551CD42,
	NetworkClient_ConsiderForSpawning_m775C21D2195F17002BF0F7F60F0C052AAA6934B2,
	NetworkClient_PrepareToSpawnSceneObjects_m1F51AE31B33D48FD9DDBF1CEC08AA546AEB50D30,
	NetworkClient_OnObjectSpawnStarted_m2F1A04282BE3688D0750D225100B003AE5FE7658,
	NetworkClient_OnObjectSpawnFinished_m08BC661EAE49DB5C7955421AB8D43CBA1CD78B90,
	NetworkClient_ClearNullFromSpawned_m294A3CF498EC00F34F3E5D27E07E42DB6CF120A6,
	NetworkClient_OnHostClientObjectDestroy_m821445496B743A10A13697A519B1B7BE76D7154B,
	NetworkClient_OnHostClientObjectHide_m9C1D529B105CAD107D472163C75E52130C5CB8BB,
	NetworkClient_OnHostClientSpawn_m0B461537BFDC210A94C92142EF85F18E772FCAA5,
	NetworkClient_OnUpdateVarsMessage_m86917F97D01CE0695F635CB993EFB4393F9EF904,
	NetworkClient_OnRPCMessage_mDB1DC6D93DA81BCFC6604686FBF20951BD087242,
	NetworkClient_OnObjectHide_mECE8726EE3F62C50CB3541A558AB0A478995D0E7,
	NetworkClient_OnObjectDestroy_mCD8DDAF1FD872969E841C2C7E9946E8FA5299766,
	NetworkClient_OnSpawn_m757BA45D182CA43B9FFBF7E03120358BC0836C98,
	NetworkClient_CheckForLocalPlayer_m9AA764A2840876F38D3CC123AE637365A9A025C8,
	NetworkClient_DestroyObject_m58E70E859CF6E27F0018B060C2990757BFC5DEFC,
	NetworkClient_NetworkEarlyUpdate_m4BB991D8FD06E909BDE076B87895113E9A510F94,
	NetworkClient_NetworkLateUpdate_mD996BE78A2574AA44050056BAC9E3F7AC4FFDBDB,
	NetworkClient_Update_mE5F6424944504CF457A06BF18B8D1877B259D4F1,
	NetworkClient_DestroyAllClientObjects_m895C02874F863A82A1FDB4015F223CF4DDBAD324,
	NetworkClient_Shutdown_m362C444833B9302098E6CC73EFA59309FC7B3D4D,
	NetworkClient__cctor_m051C6F073FEAF3D99E7E41C2F5A72AACBA005313,
	U3CU3Ec__cctor_mBED38AB857BA061D80B1B06261A24C9C56744C10,
	U3CU3Ec__ctor_m66C0ED4876EBA658BD0CF465BFA73ECA9EBDA408,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__31_0_mBB73054C087C5BE037928DE02DE4A95F2C362FE9,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__31_1_m6B0938DA6FB2F882894A1CF7DEC1A7F7A8647566,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__31_2_m1CF74B2A83C5D72B56F58D2526DE536296734F2A,
	U3CU3Ec_U3CRegisterSystemHandlersU3Eb__31_3_m144B2391E0D61068B1CC719EBABF266A2058A559,
	U3CU3Ec_U3COnObjectSpawnFinishedU3Eb__76_0_m9650D466911A0B71616F89A878B302721616A9FC,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass52_0__ctor_m083F79DCE5353AD7B8FB2DD76622252E571D1A8C,
	U3CU3Ec__DisplayClass52_0_U3CRegisterPrefabU3Eb__0_mDEEE7295F00571B18554B708EF45479606DBA09B,
	U3CU3Ec__DisplayClass53_0__ctor_mA4170F1DD5BDC1B10B5C797F4B7C29C8E7F3869A,
	U3CU3Ec__DisplayClass53_0_U3CRegisterPrefabU3Eb__0_m0D570AC51B427E497769324C83236D801BE0EB78,
	U3CU3Ec__DisplayClass57_0__ctor_mB83A6AF7A70B4DC66FEC2C7C3E814543357902F1,
	U3CU3Ec__DisplayClass57_0_U3CRegisterSpawnHandlerU3Eb__0_m1626DA596A45631087BF6945FEAAF5BEFC34D6BB,
	NULL,
	NetworkConnection_get_identity_mF8F7D1AA28117C2F53450E6697D2966DB1B16F45,
	NetworkConnection_set_identity_m6BD8F3D5B3542256F7BB47F17C18AC51B6EB5CC9,
	NetworkConnection__ctor_mA4169F8A1B0EF786615F138B3395E621AC733E3D,
	NetworkConnection__ctor_m617332B65E39A90B5D3AC66B867A5338BDFEB85E,
	NULL,
	NetworkConnection_SetHandlers_mEE69D50F1A14AF77AEF54F961F151BC96D857852,
	NULL,
	NetworkConnection_ValidatePacketSize_m623E054D7EACF8628D11D6CC96CBF18D5575F42E,
	NULL,
	NetworkConnection_ToString_m15D8D27A4FE3967B3BE72F9B5BFC08D4EF2D0CDB,
	NetworkConnection_AddToObserving_mD2B65BA312D85B6E7608E057DCCAD78A8AAFBE04,
	NetworkConnection_RemoveFromObserving_mA35C23978CB2F27BD59BFA0E4DEA171B7CED03C0,
	NetworkConnection_RemoveObservers_m2D462ECA3222CCEEBBA825CEB64E55628960F8A8,
	NetworkConnection_UnpackAndInvoke_m0B4A668A43B74300757AECD2BB2C9906BB46BE3D,
	NetworkConnection_TransportReceive_m473F63A4BCD1C0C9E7AE6C68CD070AB2BB25F049,
	NetworkConnection_IsAlive_mB23FF1F3CB3DCF3ECE7533A1179FC1977B9524FC,
	NetworkConnection_AddOwnedObject_m93256C83BF06EFAC884A0C715C96AECF7779B241,
	NetworkConnection_RemoveOwnedObject_m6D5767CAD88E52A83159CC6D54935C5D599A6A09,
	NetworkConnection_DestroyOwnedObjects_mF3297F8824C855C68E66E7A252278B65D01EA939,
	NetworkConnectionToClient_get_address_mB7234483EEF04B6B7E5F3837918B606ACA8F77BD,
	NetworkConnectionToClient__ctor_m1EBB90F7A66264C36B5033A3EEF522D1EE415835,
	NetworkConnectionToClient_GetBatchForChannelId_m2AA219CC7E0BF7D29177C276956506294BE5CBBD,
	NetworkConnectionToClient_SendBatch_m1332DE6A017E771C7D59363D97CDAC82CD986F02,
	NetworkConnectionToClient_Send_m7CB08498C245714AE1683C91049AD4968BF365C9,
	NetworkConnectionToClient_Update_m7044EA736760C6ED0115CA1F391DD368AFAD1A87,
	NetworkConnectionToClient_Disconnect_mF451E9202AE6F4664F777BBE2A6CCA497EB9E7AC,
	Batch__ctor_mC38954B624162C3C15FE977087CEAB6D395B3FAF,
	NetworkConnectionToServer_get_address_m93A6B095B543ECB8D3E8D7C62FD04ED19B9DF3F2,
	NetworkConnectionToServer_Send_mA01F41D34EA1FB8EE4F5F2498ED7428B2143AD15,
	NetworkConnectionToServer_Disconnect_mAF11CAB599FAAA6907A6A53DF0FF35E34903EDC1,
	NetworkConnectionToServer__ctor_m59A1F6CDD13D875CDC80886DF418F29331FD12F4,
	NetworkDiagnostics_add_OutMessageEvent_m509EFD06CA54100CA6D78D553E686D705AA7E14B,
	NetworkDiagnostics_remove_OutMessageEvent_m1FE3832B053A95A4B9DE0775797F009BEA175AB2,
	NULL,
	NetworkDiagnostics_add_InMessageEvent_mC6E41C981DFCF20D9FAA9C2CB603D15F543E2729,
	NetworkDiagnostics_remove_InMessageEvent_m548727ACDD2579D73D281D4422E1DFC561FE96FB,
	NULL,
	MessageInfo__ctor_mAC67988BF4FBFD84F5201A8DBFEA962167787671,
	NetworkIdentity_get_isClient_mA664D866C9B791BA0D554F5F1373FCB0FA1A571C,
	NetworkIdentity_set_isClient_m6B0DD90AF225C53612890CD949C344DC3AF17CB6,
	NetworkIdentity_get_isServer_m4BE8C60D704E40109BC7A11007F77178A557B6F2,
	NetworkIdentity_set_isServer_mE11EFCB6F277961CF8BF2FF363F3D4ACA936C2B3,
	NetworkIdentity_get_isLocalPlayer_m9E8156906B197598DD5E56CCB463BFD80EF0BC76,
	NetworkIdentity_set_isLocalPlayer_mC5BD33CD0B88ED65D5B55FA49FF86F7635291193,
	NetworkIdentity_get_isServerOnly_m7FE05E43DDC8562E9C848BE93EA80827F8B2431C,
	NetworkIdentity_get_isClientOnly_m315291F7D383386522570D0E3455CBA9A8DD2871,
	NetworkIdentity_get_hasAuthority_mC0FA6F347408FB65CC629E407CA2DBE9EB4B520B,
	NetworkIdentity_set_hasAuthority_mB13BDE976C8B19F28C6F8219BBC28D426CB92EE8,
	NetworkIdentity_get_netId_m3FF02B719B8AE0B6A3483063A373AFFB2489C0FA,
	NetworkIdentity_set_netId_m7DCA5820ABD4B361D3B6B08A93429BB932A610A9,
	NetworkIdentity_get_connectionToServer_m5B5462ECC017128A99205EDEF095B3FFDBF4EF39,
	NetworkIdentity_set_connectionToServer_m4E36D6FCC72BB5F99830549C9DE19DD350535ADF,
	NetworkIdentity_get_connectionToClient_mF85737F2CC90FC7E77FE8385F35F2FF2E692D82A,
	NetworkIdentity_set_connectionToClient_mD0DBB925AA6F5BEFD9D79C0BB1EE4E0362971129,
	NetworkIdentity_get_NetworkBehaviours_m7D27CEBD20ABC0925B9C7187E04E43CAC99AE8B0,
	NetworkIdentity_get_visibility_mABBA00BB1370A3CB3C5FC79277EB507381E0DDC0,
	NetworkIdentity_get_assetId_m3285D38FE7DE4D1F3640E5E50B2D2C88D44BA2F6,
	NetworkIdentity_set_assetId_m695FA033D0740CBF83F1EEDE8B29FDF5AA8D83AF,
	NetworkIdentity_GetSceneIdentity_mD960C920AAEA6C0BF5BD16A3DEDED4A80F7DFF05,
	NetworkIdentity_SetClientOwner_m840CB3D53A219C47CF00777D59BB12FA82292AB2,
	NetworkIdentity_GetNextNetworkId_m4B5B89CE5A7B120E0F1AC8092103D2A7C959D3B3,
	NetworkIdentity_ResetNextNetworkId_m53391AEF97823B5133DB4F523A957998AF349014,
	NetworkIdentity_add_clientAuthorityCallback_m633CD9516DED0EB53EB4E1E87BB9EAA15AD40308,
	NetworkIdentity_remove_clientAuthorityCallback_mB13DCC6DA39776FA5286B9A836FB6EE302740AE0,
	NetworkIdentity_RemoveObserverInternal_m92C4D050393BE63F326B85815D52FD75060D6944,
	NetworkIdentity_get_SpawnedFromInstantiate_mF9CCC6D13CD07F8D25FD2D4762D3A2E82F12E2CC,
	NetworkIdentity_set_SpawnedFromInstantiate_mF6F78EE5B73A8AD10495BE8EF356812CE273D469,
	NetworkIdentity_Awake_m5278295D326C59B7A020FD899C7BC97922D05FE6,
	NetworkIdentity_OnValidate_m226702DAC2E56D685492FD4AC1C6F06A6C1B22E4,
	NetworkIdentity_OnDestroy_mBE43AC5BCEC90EA29BEF95B400892C62439E4BF0,
	NetworkIdentity_OnStartServer_m0F2FCCC99FA0C984742DED2406C184A9B883226A,
	NetworkIdentity_OnStopServer_m256B4D399B052376C17A63ED5866B9B1AD22F2EF,
	NetworkIdentity_OnStartClient_m32E83A6A595E3F99CCC214FC71AB952D4B409541,
	NetworkIdentity_OnStopClient_m0BAA5DBA51A92275C387D90D17A54D8033733D82,
	NetworkIdentity_OnStartLocalPlayer_m8BB0D60CFE045D1D76E78C71BB12287275F1195E,
	NetworkIdentity_NotifyAuthority_mABE2E3765FF6730E79FFA54064DA772FF2F185F3,
	NetworkIdentity_OnStartAuthority_mF7593974D2A81557BE023303A33B724590D8E194,
	NetworkIdentity_OnStopAuthority_m6124985322A401EFFAA46CB41C4B5141E6DBA592,
	NetworkIdentity_RebuildObservers_m4D470C5582F2C09787280AE96B410328E387085D,
	NetworkIdentity_OnSetHostVisibility_mF32DAB5040C377758170B27CAD39BB7E2C0501EC,
	NetworkIdentity_OnSerializeSafely_m5612BD0E29030C4166F0BF245E7F4253C0EC1A46,
	NetworkIdentity_OnSerializeAllSafely_m241671203CD1B3A5123AE9124A8756905D1D6357,
	NetworkIdentity_OnDeserializeSafely_m930E63C2BA46D9E0678F3A7F9AA98E347EDD6BBA,
	NetworkIdentity_OnDeserializeAllSafely_m09CD02B14342498B755DCEA0B498E4085CE45699,
	NetworkIdentity_HandleRemoteCall_m7A6C4A9C31DCAA6261EF0F6193BC8CF6EB3AF792,
	NetworkIdentity_GetCommandInfo_mC6246232E15D3E658D7240777C91406D12D0B7D0,
	NetworkIdentity_ClearObservers_m989F20077372E5E0E1248D5DB8865BF1BED11216,
	NetworkIdentity_AddObserver_m5CFA5EF3EE411EA32D356BE89299078072CA5B65,
	NetworkIdentity_AssignClientAuthority_m4949EA554E5308A38FECB4D27901E812EBDB985E,
	NetworkIdentity_RemoveClientAuthority_mB0A56953931D826CA1ACF66DF53F98B961674E8C,
	NetworkIdentity_Reset_m883C3F9B8DE395014DB12FE2787BA4975BA3E50E,
	NetworkIdentity_ClearAllComponentsDirtyBits_m98279299BDD30A3A319544445922E128065FCFEB,
	NetworkIdentity_ClearDirtyComponentsDirtyBits_m02EBEB1FF48E0892B9E33863D1ABE6200B916FD9,
	NetworkIdentity_ResetSyncObjects_m01A06B2CED1EFF81D34B22058741D54AADDCB2BC,
	NetworkIdentity__ctor_m4105C071BA18EBD036577EB3F22D2D346A512A28,
	NetworkIdentity__cctor_m0715F33040D20006B1E16209699F5A4BA7F5FE5E,
	ClientAuthorityCallback__ctor_m5CB257AFC43DEA6B8CF7BDC3B8F05DB42B6BD7C7,
	ClientAuthorityCallback_Invoke_m7D934B03E2A5EECD750AEA84F0581DFCAB5FCAB5,
	ClientAuthorityCallback_BeginInvoke_m7C864923D390D7907FAFFA752AA01B181E3DAD4E,
	ClientAuthorityCallback_EndInvoke_m9D51E32557679438EA41FEE710455EBF916C4C97,
	NetworkLoop_FindPlayerLoopEntryIndex_m8401214D75A51D4212D332E8E5CB5D9337B27FEE,
	NetworkLoop_AddToPlayerLoop_mA203DBCE1BA2E03091F1A59F981936532FFC3101,
	NetworkLoop_RuntimeInitializeOnLoad_m9DCD6DC6C58A6A3E1D750A2BE7B910D9C4C013D8,
	NetworkLoop_NetworkEarlyUpdate_m45238F04DFD59A6D6937BCE61DEA4EB5C512141F,
	NetworkLoop_NetworkLateUpdate_m5A9039F8703B7D81FC5CD97674E42F9EFE90007E,
	U3CU3Ec__DisplayClass1_0__ctor_m15643106A40EEFAFAC816B59F96EBCB72FA70518,
	U3CU3Ec__DisplayClass1_0_U3CFindPlayerLoopEntryIndexU3Eb__0_m3154F15A533CFE0182B0AE7D57B513F9407D351D,
	NetworkManager_get_singleton_m3687E70BBF51C41E6F20B606EF0E0E40D13E7641,
	NetworkManager_set_singleton_m3E3F67766BFA7EFD8A3360377DEABEAAFCC271F9,
	NetworkManager_get_numPlayers_mA4CC3D7D3A2C4967F21E4EC6554CADB2EB239816,
	NetworkManager_get_mode_m7DDDC4AFB7EC130F64E6BA4E916235B46C338337,
	NetworkManager_set_mode_mFD99BDFC3978E7B505C6062BBB31BFDE7A3751FF,
	NetworkManager_OnValidate_m1E5FDC67423166450D021672C314C8346B0CECA0,
	NetworkManager_Awake_mED76ED7E0D4767AD1F7ADB7E845C2679E84E6B8A,
	NetworkManager_Start_mD2069EBE9124584CAF85D88EEC28A841F386A4F0,
	NetworkManager_LateUpdate_m5A729F6E28B3C3D49254D02A1A6BACFC9255A8F2,
	NetworkManager_IsServerOnlineSceneChangeNeeded_m635D78E9B5D3883BC0DB0526DC8BE88F4C9FB9C4,
	NetworkManager_IsSceneActive_mA50E21AD716E48D445A23383FFD249D21149F3E2,
	NetworkManager_SetupServer_m9D6BCC8D75D3F91E34218B4DCFA6EE3E813CD2F0,
	NetworkManager_StartServer_m7F3D716905D518790D749225F100D0841051B017,
	NetworkManager_StartClient_m76D46DACCCD2C70BB78014AD376FD99D91EC36C3,
	NetworkManager_StartClient_mC4B472F3B09F109813AD8B7AC133D0475BBCECAE,
	NetworkManager_StartHost_m075A9DF45CC23B3CE251028E7258583AB32B5710,
	NetworkManager_FinishStartHost_mA7C04417B8D8E5FCF593CC8BDAD1B7A1316B0B6E,
	NetworkManager_StartHostClient_m63E4184E021964794FB3F6BD67C3C15036203F4D,
	NetworkManager_StopHost_m9BE1DF0275204D698472DCB8B1DBF62FC83994CF,
	NetworkManager_StopServer_m796F0F4CA3CA42289EBD5F1F6CA6975C9993D988,
	NetworkManager_StopClient_m749293B1F5FA88A3CBD079B8A5A45B6400F3FE47,
	NetworkManager_OnApplicationQuit_m720A6D0FE0EC77E009A55942C2FB006CE9253DA0,
	NetworkManager_ConfigureServerFrameRate_m0940490CF1C0798B5B0CE00DAE010C0C0939A28B,
	NetworkManager_InitializeSingleton_m2C2E16FAF3FC4D0F4246B344C83E2F864DB21F71,
	NetworkManager_RegisterServerMessages_m62B2385AE088F6F8C8708221989D9E8601FB025C,
	NetworkManager_RegisterClientMessages_m59E1AB1EC3667065785D052268B8DC5115B18091,
	NetworkManager_Shutdown_m8E5824821C3188358A8683640CB208728597B9F7,
	NetworkManager_OnDestroy_m4BA90829754CAA8DBC58D177E18D6AE8E4F0842D,
	NetworkManager_get_networkSceneName_m80DB5F4473BF7D3CCD9F5A5F3E0F2DDCEA7D76BD,
	NetworkManager_set_networkSceneName_mBEC371D12F3669C066AC523EB7FD6A9885BB3685,
	NetworkManager_ServerChangeScene_mE7B47AD1D7BA092859F28E4500046C8B6BF52D8B,
	NetworkManager_ClientChangeScene_m6F6D640C2E6D989B53DA115B766555057D279B23,
	NetworkManager_OnSceneLoaded_mB69266EA76D21E1DA2CC2364C9EA4D4D1D2971F2,
	NetworkManager_UpdateScene_m0DC87E5D4A8881A2363E79F42C71C14C0B0E31E0,
	NetworkManager_FinishLoadScene_m2EF8C54A639E7BE29E9FC587B3CE651C96602AF4,
	NetworkManager_FinishLoadSceneHost_m055894F1A6AA4D805395C8302D7861744A9FC931,
	NetworkManager_FinishLoadSceneServerOnly_mBF8ABAF755EE1A2887DBF1B9E0D8946BEF6B6EB9,
	NetworkManager_FinishLoadSceneClientOnly_mDC883960B695D08D84046B1584F4D1766D7FEEF2,
	NetworkManager_RegisterStartPosition_mDE79A6E850F2627C193B28E33307C73C120B7C8E,
	NetworkManager_UnRegisterStartPosition_mC51E52EFA2E8B98D0077FA9D654C7548F12CED28,
	NetworkManager_GetStartPosition_m25078697FA64905AF7618A26E75ED10750B1A754,
	NetworkManager_OnServerConnectInternal_m22C84AA41EF747A326DB68A65839E2C7DD75EE2E,
	NetworkManager_OnServerAuthenticated_m18A8F6EB0B0104AFEC3F6856BD7FC3A0DD8364B0,
	NetworkManager_OnServerDisconnectInternal_m79DA7FE2A9522BA50C0BF4C3CCC083DAC0B9C56A,
	NetworkManager_OnServerReadyMessageInternal_m839A57A605435CEB8C94D114B5DE4A116A0B19F2,
	NetworkManager_OnServerAddPlayerInternal_m9928BD170680E3DBD9ACBC2D33C4025468A7BBE4,
	NetworkManager_OnClientConnectInternal_m3598A44D5ED77226B51464DE553F99537ED7085A,
	NetworkManager_OnClientAuthenticated_m605807F8A208467EDB8DA114F6400BA496EEE53E,
	NetworkManager_OnClientDisconnectInternal_mBABA2400A07727928C4DAFD31AD0F6C518CC516D,
	NetworkManager_OnClientNotReadyMessageInternal_m59761D864BB5B6E5AB9E8773F5B3084B7BF3C097,
	NetworkManager_OnClientSceneInternal_m577FA355F23D1BBA446A69CD950CF45FCAD34019,
	NetworkManager_OnServerConnect_m35D78B9144A937ADDD1FEA17E15A8DE6DF606AB6,
	NetworkManager_OnServerDisconnect_m2A4B300BE2D36D5FFFA0C7AF59296E527CB10EE2,
	NetworkManager_OnServerReady_m23B3C0B320257CA426A134ACC6086945E856EF8B,
	NetworkManager_OnServerAddPlayer_m2E0E936F41E55AFB82D0AB9908A3918A60F237DC,
	NetworkManager_OnServerError_mB7621EB1EBDE940902E7658C9382F56B23158493,
	NetworkManager_OnServerChangeScene_m78A746FB47504CEFACC6AF32A27B7C22C21ECC0A,
	NetworkManager_OnServerSceneChanged_m3A6AD198811FBCA588A5B4429A003180AEE8A08B,
	NetworkManager_OnClientConnect_m23A364AC2D7C3AEE64B43C25453A7BC2378CCC1A,
	NetworkManager_OnClientDisconnect_m02FA88A6123126271FF5EB97903A4149A0D5D69B,
	NetworkManager_OnClientError_mCBD5732D0CB7A41DD608F8169F0B3A0F6BCE0EDC,
	NetworkManager_OnClientNotReady_m1378C1AD4DCB54A286F0A35EFC73287C8A2A2C7F,
	NetworkManager_OnClientChangeScene_mA38EC559CA0D4CFA58ACD65B6E0568683748F411,
	NetworkManager_OnClientSceneChanged_mE627B70A20ABD429D2D8F8C3D0E32F70323442AE,
	NetworkManager_OnStartHost_m377F4A7C84B04AB4F90A786995899EA7EE38B3CE,
	NetworkManager_OnStartServer_m05E1F97124AFA01D00BC671476F834089724C97A,
	NetworkManager_OnStartClient_m6C90B4D7405831A55142A14BB05BD89154116C24,
	NetworkManager_OnStopServer_mBE7B67DF4F17E2302A423B6BAAF6C3A137DCA77D,
	NetworkManager_OnStopClient_m84A76651AF99B829787A74F417B32D44688681BD,
	NetworkManager_OnStopHost_mF7DF15C4F927F247E625E3F9B97962190AF08F0F,
	NetworkManager__ctor_m831DF4BB5F616C10CC2B272AD9DA2C660B3A1924,
	NetworkManager__cctor_mB48BA1AD4B577F594D044976E4F190A877F0198F,
	U3CU3Ec__cctor_m37D13BDF4CBED4CBCBB1249A2281187B67BA788E,
	U3CU3Ec__ctor_m450CE0D75EBA856B0B9F8B7F888C07156E680F20,
	U3CU3Ec_U3Cget_numPlayersU3Eb__26_0_m2B608786386286EAFF249E77B10E54B6CE68914D,
	U3CU3Ec_U3CRegisterClientMessagesU3Eb__55_0_m58EE1182E5BABAAE81670C7F35A2DDD6B47F2319,
	U3CU3Ec_U3CRegisterStartPositionU3Eb__72_0_mF7915E6A5ADA857C925F8240D62090FB1C0F3924,
	U3CU3Ec_U3CGetStartPositionU3Eb__74_0_m05AFD0C503A61EC61B25F88CE277A173C0AD071A,
	NetworkManagerHUD_Awake_m4EA9A71E137E6FB2CF2F69CD1355B8FFAEB2C91A,
	NetworkManagerHUD_OnGUI_mF1BD1A2A022586475B91CC9491377150601855E4,
	NetworkManagerHUD_StartButtons_m77E114195F788922578CAEAD16F04C8529070859,
	NetworkManagerHUD_StatusLabels_m5BC7A80BFA34A4CDEB4F8DFF207766A36CFD6B56,
	NetworkManagerHUD_StopButtons_mCDF2E1FC08A56AF9904AFD9CCC18172E7EFEFF50,
	NetworkManagerHUD__ctor_mB218F6DAB2DD70F06B61CE59E7A6B40160BE5146,
	NetworkReader_get_Length_mD4443E7D7B3E49740A8D400B1CA1FC6D13D26D4B,
	NetworkReader__ctor_m7472356275E51A91FEE8FEF118A6AF2240F6417D,
	NetworkReader__ctor_m73E59DA6A9DC39831B7EAC72DD9F9B29BA0918C0,
	NetworkReader_ReadByte_m5CEA57059C34AA6DC218D6F09207C5188AEA68A8,
	NetworkReader_ReadBytes_m8A6348412F267570291A4BD93F4DF1715B35077D,
	NetworkReader_ReadBytesSegment_mD99D152684B83B9AF600E28FB6642FAB3B4B4CCF,
	NetworkReader_ToString_mFEA276025738C2F5F0D5BEE70952B19D278380A0,
	NULL,
	NetworkReaderExtensions_ReadByte_m86E3D12D2C1E2C447A4C2F20B808117DAA69571C,
	NetworkReaderExtensions_ReadSByte_m8D3F2D88094079D5E0495FBBB3815DBAECB71CC1,
	NetworkReaderExtensions_ReadChar_m1CD7BF565346230DFE2B29A62818C0DF4ED89AFD,
	NetworkReaderExtensions_ReadBoolean_mA9AED23EBC6083FDFFD749EED46F03BAFC6F0ED8,
	NetworkReaderExtensions_ReadInt16_m125C98DDE87D530D04BA663BCA9CF9FADC69731D,
	NetworkReaderExtensions_ReadUInt16_mA7151420E3D5805C98EE41B169CAA0DB313B33CD,
	NetworkReaderExtensions_ReadInt32_mE0BA8E4FD647F4BC6F020B2480AF3EE8872A64A3,
	NetworkReaderExtensions_ReadUInt32_mF65B5680DA763162DCD1BCB53B225C38ACAE3C08,
	NetworkReaderExtensions_ReadInt64_mAD215AD6D6571611F19D26468F422D93EE3029C2,
	NetworkReaderExtensions_ReadUInt64_mF8CBCE7593E37A4C819D8D5461746D1360975252,
	NetworkReaderExtensions_ReadSingle_m2DF644789D260209004F87C7996C41D6022D542F,
	NetworkReaderExtensions_ReadDouble_m5800AF1FD52D7DB9047FE7B58A0BD38E23C94281,
	NetworkReaderExtensions_ReadDecimal_m0AE7D5057B858DB3A6C838CF3DF716707F3FA1EC,
	NetworkReaderExtensions_ReadString_mA74E4612F529683A4AEEDB55C369CDEA32D222CC,
	NetworkReaderExtensions_ReadBytesAndSize_m2EED0B100EFEDF9E1A4D2E9A429DBF3D4F8EA623,
	NetworkReaderExtensions_ReadBytesAndSizeSegment_m1750D52A666869DA2EC3C3A25E767834B9DA9E2E,
	NetworkReaderExtensions_ReadVector2_mADCA4D375C619EE39C7ED83BC352C870897FEF6F,
	NetworkReaderExtensions_ReadVector3_mD9D5C83CB3177994353C71C8BC71495A79288416,
	NetworkReaderExtensions_ReadVector4_m26621D658FD76A6A4D8EE73CE81739620A706997,
	NetworkReaderExtensions_ReadVector2Int_m0B0397F7BA2F4DE1E1A555E9EFEE676C38ECE40C,
	NetworkReaderExtensions_ReadVector3Int_mD9A574478D0EC900DB9C50B000AE39148A429F68,
	NetworkReaderExtensions_ReadColor_m21E0B16FA466A4FDAEC8BE1C80E47C9BAE3217A7,
	NetworkReaderExtensions_ReadColor32_m7926AFB64529E5FC7C51D390468AAB86722D01F7,
	NetworkReaderExtensions_ReadQuaternion_m2946D7DD55E1ED8EDEF82536F0D4246D42A22E19,
	NetworkReaderExtensions_ReadRect_m75BCEF76E5C1CFEBE794A2FE5F52AA55DFBF3F85,
	NetworkReaderExtensions_ReadPlane_m5D8E59014B3ADD6108143CBB2C25DAA1DDD291C5,
	NetworkReaderExtensions_ReadRay_mD2A5A24032CB890240D7F5D94E8399E2766C05EA,
	NetworkReaderExtensions_ReadMatrix4x4_m8C9D3E72F015488DA355F6E26FDCF6E0C52FC7D8,
	NetworkReaderExtensions_ReadBytes_m256AB25CABB792B276088B8929BBD2BC972719F0,
	NetworkReaderExtensions_ReadGuid_m4BEDE3B6CE956D9B64BFF779754151F49B7A82CE,
	NetworkReaderExtensions_ReadTransform_m81B5B4E6C64D5C011B52262BDCDB1F34E0B50034,
	NetworkReaderExtensions_ReadGameObject_m50C52A705CA8F13E6B80746CB3F001CD82D42404,
	NetworkReaderExtensions_ReadNetworkIdentity_m52CEFB599F366ADD367C0C23CFA163EBB0F6C764,
	NetworkReaderExtensions_ReadNetworkBehaviour_mAEDC6923777D5F941348F24C73586BB93353E492,
	NULL,
	NetworkReaderExtensions_ReadNetworkBehaviourSyncVar_mC735015AC984ED517FB5EC9469C0A70181E6838F,
	NULL,
	NULL,
	NetworkReaderExtensions_ReadUri_m4D5B6761CC4A07C3E7F223B2DDF88EF4CACC3A3B,
	NetworkReaderExtensions__cctor_m40E45B62701A8A5F9F8DAA0544C08D5A4FF20A29,
	PooledNetworkReader__ctor_mEAE1DCDFBAC4CCFA4D40970DE4D20B36931D656E,
	PooledNetworkReader__ctor_m576FB263D8FEA8501D96F220390DC912FEB24762,
	PooledNetworkReader_Dispose_mC35EBC46EDE36DB1EBCF9AFDDCE4375618D32A15,
	NetworkReaderPool_GetReader_m951A0F2DA2D9D9B6DB00A2F9540BEC414D2DE21C,
	NetworkReaderPool_GetReader_mE32ACECE58AE2B54D448549DCA0D920D84DAF79E,
	NetworkReaderPool_Recycle_mAD1BB938E53E18CF168736FB5256680DBBFD1535,
	NetworkReaderPool__cctor_m7818A15817331E4274A35067A7FC010DD07871F6,
	U3CU3Ec__cctor_m312E11FBBE279F10D9D2B17EFB882AA1B0920D12,
	U3CU3Ec__ctor_mDC5278341D19B9BC6F238AF069FB094885AB02D5,
	U3CU3Ec_U3C_cctorU3Eb__4_0_m7B2E2E667B272E9C7BD040EDDA22D56ABB94241D,
	NetworkServer_get_localConnection_m5CC8941C493528A76850E9CA0EE95BFAE18C2B7C,
	NetworkServer_set_localConnection_m121D988179415A2FEA40ADA6C77986A764A56CB8,
	NetworkServer_get_localClientActive_m9C4F4B8848458BB4A2EE6552EDE1E39792C2B3BA,
	NetworkServer_get_active_mF055B10F741C963266FE30D5667E781DBF44DEEE,
	NetworkServer_set_active_m964AD097B146622B9B4439D01DE79A734F658CD6,
	NetworkServer_Initialize_m3ACC3E103BE21A4A4911B20175B6D05B11A95525,
	NetworkServer_AddTransportHandlers_m978B1B0770E285B67A248262C957CD4E1605984A,
	NetworkServer_ActivateHostScene_m457CCCEFB3E319567D22F5AFC7209B6A153DE976,
	NetworkServer_RegisterMessageHandlers_mA9D72F9E0CB28A698139D5BEBC3D4764627802B9,
	NetworkServer_Listen_m0A131ED708A5EE255FC8A2B79C35017D78C9BF18,
	NetworkServer_CleanupNetworkIdentities_m1A67FCABDDBBF0B8B9D740E88616CC9211481832,
	NetworkServer_Shutdown_mBEA3CA658B95006C84A0A81D5D1D3E47EF564EAF,
	NetworkServer_AddConnection_m765F75E8AF029F8D9D9E9A115663C37AA7C6C9CF,
	NetworkServer_RemoveConnection_m1492C8648F00DB2D4C3F3DC9ABC690BF27C9299B,
	NetworkServer_SetLocalConnection_mEDC58E853ADC35700140B17F47106B5CCF7568AF,
	NetworkServer_RemoveLocalConnection_m378F55B5F91FCC504A7100491C3832C006EED402,
	NetworkServer_NoExternalConnections_mFA58D0B4E2C7D1D227BBCE0815863D71B3AAF785,
	NetworkServer_NoConnections_m9601687D6034D96B0646C81411AAB099B74F8CEC,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkServer_OnConnected_m351F84980AEE5D8D94D16C85A0708F9431D1E748,
	NetworkServer_OnConnected_m92037D3CFABD7D7514108F39242084325D9B8DD6,
	NetworkServer_OnDataReceived_m0A961AB9C89C79EF2F6C32F511295AEDF0DBCC49,
	NetworkServer_OnDisconnected_m38180FD30F8E845CB598CE00D002CCA8FB740F05,
	NetworkServer_OnDisconnected_m6E31A50EC85B2F354B68CCCD738A22ECF5A6CD8F,
	NetworkServer_OnError_m3C684B7F8A69D5F77520F7F867288C34CEB52510,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkServer_ClearHandlers_m9CA5A2CC1467B94753025BDA3186A20249D1DD52,
	NetworkServer_GetNetworkIdentity_mC661B7E843BFE80D1D590A10671B8E9D5C4A98B6,
	NetworkServer_DisconnectAll_mCD86E876F1AEE03A2E835C13C0C977A937CD8A4B,
	NetworkServer_DisconnectAllExternalConnections_m1E7DD4EAB047BEB502D8D2B737B3BB53535F89FE,
	NetworkServer_DisconnectAllConnections_m064C12913B1EE79D9D70755CE09408F16349FD96,
	NetworkServer_AddPlayerForConnection_m487EFEA0D07031952F9E47E700F3E2D390AB5640,
	NetworkServer_AddPlayerForConnection_m5DE6FDC39EDCE7D8E031D44D9968E680E88C7623,
	NetworkServer_ReplacePlayerForConnection_m7BBA94A2B3D39AC57CEE57D9AA6ED3C61F579D06,
	NetworkServer_ReplacePlayerForConnection_m48668B43474DC6890E5395F5FBBDEF169349F687,
	NetworkServer_SetClientReady_mC3F0DB48250269C36FB173466FC5CB4A508D0791,
	NetworkServer_SetClientNotReady_m33DEC070F04798EE47179E8C2C2B3F931C53054B,
	NetworkServer_SetAllClientsNotReady_mCD791365FA6BDA777EB2FB2865B63B0A6BEE4926,
	NetworkServer_OnClientReadyMessage_m909779CC240B0AA6C4E7B1A959C5380060264933,
	NetworkServer_ShowForConnection_m65FE936D0D166AA00A3D8EFB5AAA2120F54C2CAE,
	NetworkServer_HideForConnection_mD1B712AA6BF00CFC2DAAF3EAFE3A9C777AE43ED4,
	NetworkServer_RemovePlayerForConnection_mE52A009050437EC470A9BF46165473ACF841A5D9,
	NetworkServer_OnCommandMessage_mA410CAD9636B529259CED14E989D3D01DF2FAD4C,
	NetworkServer_CreateSpawnMessagePayload_m607B37EC828537F1A87CE12B8A077C0B861CBF34,
	NetworkServer_SendSpawnMessage_mBCD386FE79E43D7C4DC964A4D539A857C5D5D102,
	NetworkServer_SpawnObject_mB3AD303BED0C815C723891DD4583AA2C289CB163,
	NetworkServer_Spawn_mE71DE744EA836EE5C4EA51B6FCDDCDB83C1D7846,
	NetworkServer_Spawn_mC82932E22819C7C7682841AB3300F26151AC6D15,
	NetworkServer_Spawn_m14592C4F1111BE2C02BEFDBE75F31AE54150924D,
	NetworkServer_ValidateSceneObject_m4ED8F63B17AAA36BAA4CB4DA2BA2E70A838EFD71,
	NetworkServer_SpawnObjects_m9C61D31EB90F43E1219F7C228B38BB01CA7F7A12,
	NetworkServer_Respawn_mEDAC1931E0963B50A235D2A66D481898B7AE1B07,
	NetworkServer_SpawnObserversForConnection_m2DD4734C190A51C5B9FEE12ECFA5CD08E0345CBD,
	NetworkServer_UnSpawn_m1584C6BCEE95BA77A803C51B77E166E4F94FBCC9,
	NetworkServer_DestroyPlayerForConnection_m543D69BA46FEA3423D0F97CC5E39CBCF7172A577,
	NetworkServer_DestroyObject_m810CF5F1A12A15239105388704094176AF6523B7,
	NetworkServer_DestroyObject_mADF3D3E802D75752CDCA05F94F0FAFEBFA5A9F93,
	NetworkServer_Destroy_m113ADC5542A4627A397C75DC3ECC9BC87F404FFB,
	NetworkServer_AddAllReadyServerConnectionsToObservers_mDCCA7E9B82CA04C30C3ABA187C3306DE6939C8A1,
	NetworkServer_RebuildObserversDefault_mE396735085539EB07B92E6A9CBBCB33277C0BAFF,
	NetworkServer_RebuildObserversCustom_mBB971C93348EACA1C5977913D7A7E5533043E022,
	NetworkServer_RebuildObservers_m2E49CB20FF4AF0D1EAC08AF0B0271BBEFCDCDABC,
	NetworkServer_NetworkEarlyUpdate_m9990AD91F269B79E8AB54FE45A4EC6251F7C457A,
	NetworkServer_NetworkLateUpdate_mAC035ABA4BB49EF98A547B2A96F3AA54F2E5D63E,
	NetworkServer_Update_mF9F69F127D306FFA0D68AB471CC67B3415D3021C,
	NetworkServer__cctor_mC0DCCC43EE4E77463F04C3FECCBB84579DAA6C9A,
	NULL,
	NULL,
	NULL,
	NULL,
	NetworkStartPosition_Awake_m957E26CD377C729F2F9442F0C737CC4729A72C47,
	NetworkStartPosition_OnDestroy_m00BD40ED4641A481ACE5967C211E6EBF84585FC8,
	NetworkStartPosition__ctor_m0630A7917AC91935D77BF5755391FB6EA6DFBFD0,
	NetworkTime__cctor_m747E3CF7CD3F32E4BFCA9975961B4C76A4737770,
	NetworkTime_LocalTime_m3AB333530CEE96AE8CB62E7C47FBAA7C382E1CCF,
	NetworkTime_Reset_mCAB26017F5FA88252E547FA6551A26D6BAEF7298,
	NetworkTime_UpdateClient_m852C8649B8BDB874079CAD542EC213D2165D53DD,
	NetworkTime_OnServerPing_m68FD3838E10FE31CDFC7F861EF8FE0D9C37D8B54,
	NetworkTime_OnClientPong_m44B93D45F64A2A215C28274C23018A9CADBDC253,
	NetworkTime_get_time_m8DE6EBB4100FF44B7A282881FD69798D737C24C6,
	NetworkTime_get_timeVariance_m34EB4F607490CA48E1896BE2075B9564AF922BFE,
	NetworkTime_get_timeVar_m9D276BC81BB28039ED914413DDB85A472D41BBF7,
	NetworkTime_get_timeStandardDeviation_m0BA80800A296887072DF1191307EB274579081A7,
	NetworkTime_get_timeSd_m3701E26030E55483B12EE1BB684ACFA8CC5A3405,
	NetworkTime_get_offset_m962DCC4FAA7B83F01518096E0BD17F34EEA84CBF,
	NetworkTime_get_rtt_m9E4EC224EB95FBB7B8329E9A225D3275D0770C66,
	NetworkTime_get_rttVariance_m7E2A5F27F0741B45B69848E26ED1D03A33F0A037,
	NetworkTime_get_rttVar_mB51459101EDA3631D0152687E4EDAEBB28741903,
	NetworkTime_get_rttStandardDeviation_m557D817D40153F321D01AB0D272E620525F2CA38,
	NetworkTime_get_rttSd_m90AD954DF60BA5962609303A376108671F368FC9,
	NULL,
	NULL,
	NetworkVisibility_OnSetHostVisibility_m5F33C7B41C04A03A8B9B1601DCE466AB1CF7766F,
	NetworkVisibility__ctor_m113F49AE3B33B035A6132DC95F52F21506C4BA63,
	NetworkWriter_get_Length_m814675F5934A7C6E09BC3EAC5294B0B71EA96C95,
	NetworkWriter_get_Position_mCC619999A1AD046AD2377028073AABAEBD1A7BFD,
	NetworkWriter_set_Position_m201FBEA9E80A82E2BEC2C2BF48D4C80F1258BE57,
	NetworkWriter_Reset_m9F34945A36E170550D63E41D2CF9C26ACB03FF77,
	NetworkWriter_SetLength_m772F0AB2AC0AE5DEDD1EBBE19B86196ED78E240D,
	NetworkWriter_EnsureLength_mE32CDE81422866487CE9EF603DC16106877E7942,
	NetworkWriter_EnsureCapacity_m8686EBB645C1892C928AB87D8FD73148B547BDC9,
	NetworkWriter_ToArray_mC9A28117C639BA09B09A43343D3D3A3488A187E7,
	NetworkWriter_ToArraySegment_m20B3969C06F4320029AD66A820B0523A8D6641EF,
	NetworkWriter_WriteByte_mC22A6863F21D47005ECAD28F5AB6FC5248B23D0E,
	NetworkWriter_WriteBytes_m1EF0FDBA6E07D885F57CB524710524715BB7E374,
	NULL,
	NetworkWriter__ctor_mD3E7B77EDCCE633CFEE83EBC1DB06355D0460A5F,
	NetworkWriterExtensions_WriteByte_mFFEFC20C5AB176841BBBE10C1E4A866EAF369B96,
	NetworkWriterExtensions_WriteSByte_m9AA9E2E5390BD4AC9CEA8A05EAE1234D9C48B8C8,
	NetworkWriterExtensions_WriteChar_m14899B6388B37CA68D9BA49CF0BE0C3EE956E49E,
	NetworkWriterExtensions_WriteBoolean_m1286C9865F21B0EB024250572C0ADDC319782162,
	NetworkWriterExtensions_WriteUInt16_m7F8221AB37BDB8AC0765BDA883C6CA916CC812F4,
	NetworkWriterExtensions_WriteInt16_m5C7AE32A24F49E8EE8F42BE01BF5B5D7065FB47F,
	NetworkWriterExtensions_WriteUInt32_mD459163C062BD8349BE6680BFAC598A96619F6F5,
	NetworkWriterExtensions_WriteInt32_m353DC0D60DCF2126D59A2F25E5C20B1ED74C9B3A,
	NetworkWriterExtensions_WriteUInt64_mD91BA22C056821BD989BFF2480CCCA9A65120A86,
	NetworkWriterExtensions_WriteInt64_m381C3EB4FED2C7C18CD6630ACCAC174FFC5E3C9C,
	NetworkWriterExtensions_WriteSingle_m5EDC5A8935BAD2B074C652C5158281D2C253CB08,
	NetworkWriterExtensions_WriteDouble_m40D19C266720079063B22A53B6940E1CF6482BFC,
	NetworkWriterExtensions_WriteDecimal_m1F5400E8F7D91C15538FC724A4B430A818479431,
	NetworkWriterExtensions_WriteString_m31C762A5BBACB77129E085AB7D7A9AEEB9ACA95F,
	NetworkWriterExtensions_WriteBytesAndSize_m75E79671D06A8D7C3CA09E02844E6316F3A3E56B,
	NetworkWriterExtensions_WriteBytesAndSize_mB11A8C12622432C14420D616F763F130DD1A91C9,
	NetworkWriterExtensions_WriteBytesAndSizeSegment_mAD214D7E8A080A5615D400A6C3F2F5218F3AD11B,
	NetworkWriterExtensions_WriteVector2_m52B333B1242AC5E0490DA980D5954157FDF9949F,
	NetworkWriterExtensions_WriteVector3_m2895E336F7B2F52819808DC35F574B10D3317609,
	NetworkWriterExtensions_WriteVector4_mE75893C683F8C655244CE272B0B050662E751414,
	NetworkWriterExtensions_WriteVector2Int_mDA0A622A19099A741DE393233204CD6FB89FF75A,
	NetworkWriterExtensions_WriteVector3Int_m4911D397D68D855E386699BB99ECF4BD0F5F7060,
	NetworkWriterExtensions_WriteColor_m7EA61C4CEE7C0F49BCAF7339D1A76F4ECA50626E,
	NetworkWriterExtensions_WriteColor32_mC430555DAD65FA8A1ACA7117122960F6412B3079,
	NetworkWriterExtensions_WriteQuaternion_m96A981CD95861D3F23358155CD1CCBCDF185D909,
	NetworkWriterExtensions_WriteRect_m1C462FFD3B7FE7B73B6F733CDECEDCBFFF9F3A05,
	NetworkWriterExtensions_WritePlane_m75F27F25C9E50A60C635D376D4C1D993D1376A97,
	NetworkWriterExtensions_WriteRay_mF907AD59F7170BBB74CF49611FBBAF069DD6D1F2,
	NetworkWriterExtensions_WriteMatrix4x4_m40B7941CFBB2912FA11AFB35F5729E4F2383BEAD,
	NetworkWriterExtensions_WriteGuid_m1700462B08DC1D6B80FF00521C50C5A85E781AFB,
	NetworkWriterExtensions_WriteNetworkIdentity_m6B7BF775A09551E47F1E98F2603E0C3B8358FC64,
	NetworkWriterExtensions_WriteNetworkBehaviour_mCA7167729B04CFA80D5F7AB5D9C43FC24B4DF20D,
	NetworkWriterExtensions_WriteTransform_m3118EFCE291DE2182B785E099FE30B3D68F36830,
	NetworkWriterExtensions_WriteGameObject_m461FDFA27D93D42B1C6C1B5F84EC2314E479E45B,
	NetworkWriterExtensions_WriteUri_mECC3E974CD9C900CEBB24E5E4743DF2D41E3533F,
	NULL,
	NULL,
	NULL,
	NetworkWriterExtensions__cctor_mEF540B4CE4511D89773F16F2D0E8092600455D59,
	PooledNetworkWriter_Dispose_m4D1350BAFF1AE5BE1A2C6540EFACB15F02511A18,
	PooledNetworkWriter__ctor_m0575E62AABFA81E2F757C76EA455757C8F27B165,
	NetworkWriterPool_GetWriter_m53506C8016911951C82F2F83E45592CE2F9A85AE,
	NetworkWriterPool_Recycle_m8E7D8C8ED6F0856380CE750DBEF1D0EC5BAF8FB3,
	NetworkWriterPool__cctor_m9274ABE8EE4582DC7D9DF184F7F73F2DF3FC7CF3,
	U3CU3Ec__cctor_m3FD838FF3EDCED87947D48608D148699344D8A93,
	U3CU3Ec__ctor_m54EF1D3A06EA9F18684008AB6E69268AF8AD7A9A,
	U3CU3Ec_U3C_cctorU3Eb__3_0_mB0A5B91C0E7EC65AD20C53B230FD500DECA1A061,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SyncListString__ctor_mA4D9B6BCC69307496D8FDCEE024CD97D199DF0E7,
	SyncListFloat__ctor_m4B45173BADFFBBDB078AC492AA195341C83AFDE0,
	SyncListInt__ctor_m5946A57F58B76D25F4E339924332AAC7DF742ADA,
	SyncListUInt__ctor_m00CBE30DC4011DAEDA7F08D946017D0F8792F47A,
	SyncListBool__ctor_m2916C60036ACE746504B76D4076DE7B039BA5399,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FallbackTransport_Awake_mB5691DBE3460F01AB2800093B3EF15CB80AB2838,
	FallbackTransport_OnEnable_m19FC450BEE25F110AB379997A142F95EED809429,
	FallbackTransport_OnDisable_mE060E389CBC984B6EF2554968D241D7285A8BFBA,
	FallbackTransport_GetAvailableTransport_mCAF4424D46DB412F5697C5C5B9C7F3206D28E27A,
	FallbackTransport_Available_m74668CFB8053E5639BC5BE83B2D65C9527ACB51A,
	FallbackTransport_ClientConnect_m812512C78B7B54B9AE3C5925CB4D8D5EBD793E60,
	FallbackTransport_ClientConnect_m4330C1BDABAB75A6F3FB8B7229B252F57A3F43E1,
	FallbackTransport_ClientConnected_m75D9F95A99CBA2D67AD1C458407E9C53E1208F21,
	FallbackTransport_ClientDisconnect_mFEEB0102A04FF499F7BCDC7BABA1E626E70C07D4,
	FallbackTransport_ClientSend_m6170416F05A77017C06C34BD2A17CF100F196EA2,
	FallbackTransport_ServerUri_mE4D4483AC7A20A3B58F68963B9DB142B08781C9D,
	FallbackTransport_ServerActive_mE912487FE1659B230036C278C9A2244FA59A1B5A,
	FallbackTransport_ServerGetClientAddress_m7B30A240C8CA734D55070CF717E8011B3CF20EAC,
	FallbackTransport_ServerDisconnect_mFCAFAD5A41E137F030CF62C76FFFC6ADFA65B5CD,
	FallbackTransport_ServerSend_m64CF877DF200D9ACD0A31BFC41B23BB8473F9A21,
	FallbackTransport_ServerStart_mDF3EC98EAC1F16A343CEEB6DCCF508E9D14D6B95,
	FallbackTransport_ServerStop_mD99049C603B5CEC7C87C6D96F8C09ACF954A4456,
	FallbackTransport_Shutdown_mE9EEC19ADD8B69EBF701F0AF43DECC92FD099F79,
	FallbackTransport_GetMaxPacketSize_m846D965A967FAD056657AD12D788B897D0620791,
	FallbackTransport_ToString_m8B43A1F9E312AC2DD313FE66E22B93B323AD8BC8,
	FallbackTransport__ctor_mBACA7141E782AEB783D06A82D6CD85E2535E1F46,
	LatencySimulation_Awake_mEAC826A25C58752C0C69564A5348BD01CA9CB7C1,
	LatencySimulation_OnEnable_mD242DDC8C43D5466149B69B85EAE0DE562756A9C,
	LatencySimulation_OnDisable_m199817612D997387B3E911992B686D05DB2E07B1,
	LatencySimulation_Noise_m1A3BC7F8CD1202484C1A868B4D23FC9791AC5CF5,
	LatencySimulation_SimulateLatency_mAF38F234D096858BDBD02E0437436E71EF41CB7C,
	LatencySimulation_SimulateSend_mF89BB2801ED96AF1D6F4CD9F3BF79997D20C9EFD,
	LatencySimulation_Available_mB4E68E0825607F4005173F6C7D09373DA21ECD03,
	LatencySimulation_ClientConnect_m58CFCBC09F36A3BF4B4AFACDE77C3DDBE813A75F,
	LatencySimulation_ClientConnect_mA949A0B2766FB09E9D5DFC5739F9BCD2702FFC5F,
	LatencySimulation_ClientConnected_mDF44EB7042EF3BC3ED0B44748C4B8839D64C6715,
	LatencySimulation_ClientDisconnect_m24DA6CF3C4FADC708864D354670ABC2E7B2A5EA9,
	LatencySimulation_ClientSend_mE86FA4EDBD91084FE2A88BAC4D48ECFBEBE5B08F,
	LatencySimulation_ServerUri_mE307B5DFAA51ACE4E586BC9F9E3F46D72A3CEE9E,
	LatencySimulation_ServerActive_mEC3859D604A254EBBE0613A38D095DFC70C9FBB3,
	LatencySimulation_ServerGetClientAddress_m49F80F36FAF18A175ABD6AFC040E1916F433AD40,
	LatencySimulation_ServerDisconnect_m209FEF5CBC7543F9A09FDC2C904A1F7DECDE54AB,
	LatencySimulation_ServerSend_m65405229586A20BC49EB56CEB7F4838B203F06A1,
	LatencySimulation_ServerStart_m1EA1DA984B406C9EF1ED2B70010DB9DF20F51C01,
	LatencySimulation_ServerStop_m42D319CCA21268E27974470ADBA90C0D478EF316,
	LatencySimulation_ClientEarlyUpdate_m53492BCFAB22CBE97F2AD567B5DC1C6E82C851FA,
	LatencySimulation_ServerEarlyUpdate_mBA19D74A9BB375B315A174B428E691ADD4A30952,
	LatencySimulation_ClientLateUpdate_mF0C4A41CEA86EFB8A86E97633249B22E59122924,
	LatencySimulation_ServerLateUpdate_m374D47AD51A3EE64CA2889A03B508F9A3BFB87E0,
	LatencySimulation_GetMaxBatchSize_m17D26D1A08E47D9DD23BD4784D30175FD6B8B037,
	LatencySimulation_GetMaxPacketSize_m9F0DCF83D8965FB56EE3BAD9CB384BE9EF0CF3B6,
	LatencySimulation_Shutdown_mC7DD8B6818B0076AD69EEFDB07C8E4E62A548471,
	LatencySimulation_ToString_m2F0B4FAF301FC22A8093E73308E047FC919C5DAB,
	LatencySimulation__ctor_mEBBCAAD0D6369E89B44B1C8F9636D88BD7EABDD4,
	MiddlewareTransport_Available_mF191BD5C8896806E9F28BA6FEC9FE1DDDC2695EB,
	MiddlewareTransport_GetMaxPacketSize_mA0E7EED841343BAF79A3ADB599A1FD4A7EE601B7,
	MiddlewareTransport_Shutdown_m194A579B64BFD146048A8EEE1E9FF244D76F0B3B,
	MiddlewareTransport_ClientConnect_m5CA5664667B11CFB4938EEB37AC81ED33749C5E1,
	MiddlewareTransport_ClientConnected_mA0F69BF09D2BC66AE0D356E4F9D14AF85CC22932,
	MiddlewareTransport_ClientDisconnect_m0D3A2544636DEF9926928276F00D6265FC970B53,
	MiddlewareTransport_ClientSend_mFEC9A63B83D18B06CC479F545597CA5FAF08DF78,
	MiddlewareTransport_ServerActive_m6D3FEA6629AAC8A1F7DC552699B5E73BAFE0B129,
	MiddlewareTransport_ServerStart_m0D85BB7EA451FD775587EDF01BBAF13E4DF687A1,
	MiddlewareTransport_ServerStop_m2211C5FF5EB34510A2D4BA3A8B43426FC12FE3E9,
	MiddlewareTransport_ServerSend_m531B5F8685EB2D4029E6032F70CDC34B74BF3949,
	MiddlewareTransport_ServerDisconnect_m0E4C34796EA7E75E591F77DDA98FC0E4FADF032C,
	MiddlewareTransport_ServerGetClientAddress_m489B2ACBA1E4572FDFBCC3C58F9B4C345E08BE5D,
	MiddlewareTransport_ServerUri_m4811255EF1EE5048D396701A580ED4BDEC892991,
	MiddlewareTransport__ctor_mCB190AEE11176640125AF0F7F62842471D92186B,
	MultiplexTransport_Awake_m26A9019ED0ABDDE43CA027533F27F34A39BC5BB9,
	MultiplexTransport_ClientEarlyUpdate_m25E35E39DD3ED68AFA0F1829474734D1D6B7D50B,
	MultiplexTransport_ServerEarlyUpdate_m2A91E85584D6897FC0F495DF270220129EB2D2CC,
	MultiplexTransport_ClientLateUpdate_m2FF3203709B7766A969E34724CD4ECD393672084,
	MultiplexTransport_ServerLateUpdate_m8B4417CEAC63EBFB5754F5BA315B32329B84F1AD,
	MultiplexTransport_OnEnable_mB1A69C2F6625B2623669BF8E158A298E3F598E17,
	MultiplexTransport_OnDisable_m3D0FB1850D725883E1B012BE2E191B2C4E4E4C24,
	MultiplexTransport_Available_mD1A9553AAFEA47B7243E432217D1F097BA949DAE,
	MultiplexTransport_ClientConnect_m100247467A74FBD5B85B3053895265A3CD969804,
	MultiplexTransport_ClientConnect_m68EE415F6344B5B068F38B4E9665626B814C3FC7,
	MultiplexTransport_ClientConnected_m9A02C200A00E63BEA58542D9FF631389991B0C06,
	MultiplexTransport_ClientDisconnect_mC5E015E98AB02572E650F30319414B7344856E2A,
	MultiplexTransport_ClientSend_m5C1821304190D784483592BC3ACBD631DF4100AE,
	MultiplexTransport_FromBaseId_m48D22B4B10606A37A35893E55EA3F38CF11F24E4,
	MultiplexTransport_ToBaseId_m4483DB4464A8A0664EE667278437C9D6C50E4D26,
	MultiplexTransport_ToTransportId_m0750AEEA94DA55CDFFAD5D9C93346686C466C4BD,
	MultiplexTransport_AddServerCallbacks_m83C3D43FDF8C41DDC100792514769D8420AA95F3,
	MultiplexTransport_ServerUri_m880F88499ECEAF9A081AEE2B4536FCF3A65AFD87,
	MultiplexTransport_ServerActive_m01DD6DAD3AC8A8A9D981CB9E345BDA698599453C,
	MultiplexTransport_ServerGetClientAddress_mF215E0A6812B259249283C8189D6706AA63DA7C8,
	MultiplexTransport_ServerDisconnect_mD3BA9993BB66EE3AF048AC74C7CABC6F7C20CF6F,
	MultiplexTransport_ServerSend_mB3D4F80AE321173C34D69C7FCF81B87C3DAFE043,
	MultiplexTransport_ServerStart_m30F65C9378DDB1B84E9C9C25D69F8E2EE1D5A887,
	MultiplexTransport_ServerStop_mC1038F61612FC37D90432BEB8E5F2B50672FB447,
	MultiplexTransport_GetMaxPacketSize_m8E1ED512A8C33A0583C506866631DA35575CF644,
	MultiplexTransport_Shutdown_mCD3B2370DD52E5F859EC07816FA6BC662A62631A,
	MultiplexTransport_ToString_m322B81C3495ED803F44376B5E317216B4BA2E441,
	MultiplexTransport__ctor_mEE455ED05170C5F0676EB3F47E5529D8D3637A4B,
	U3CU3Ec__DisplayClass18_0__ctor_m5F63E485307DE66FF8DF6DA73B14BD0A8C596679,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__0_m71C260E9F663345051F4ED0DBE5FD51CB051374E,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__1_mAEB5E470043FEFE9ACA5895E6903C43283DB85D1,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__2_m20358BC676721F4AF2C4443B36DF2E03E7DDAFD3,
	U3CU3Ec__DisplayClass18_0_U3CAddServerCallbacksU3Eb__3_m3D94991394A01162B4B35D5D975273EE53A43152,
	TelepathyTransport_Awake_m602F5715334063DC6405856C3AD1FC29C7B7E5A5,
	TelepathyTransport_Available_mF8EDA3882E796A7C821F5205051E549927AFEB80,
	TelepathyTransport_ClientConnected_m8B5228FFC261001AAFD1F4C1DACEAB32FB728BFA,
	TelepathyTransport_ClientConnect_mEC8326A6AE0253B05D929D98B37E774134A8320D,
	TelepathyTransport_ClientConnect_m27B5C086CB7ED8E51EDF0D922FACFBEE88CB2BB5,
	TelepathyTransport_ClientSend_m95DA556F300DB657FF0FC0A54E0BCCCE883D9F94,
	TelepathyTransport_ClientDisconnect_m0A27B27ECCDAF5C60DC6777E95D6BA075FCEA4AC,
	TelepathyTransport_ClientEarlyUpdate_m03B06C08025B8191282BEE1F6A9EC25C0EE47D34,
	TelepathyTransport_ServerUri_mD3896EBAB74A7FE4BE2970ACB5B8930A1C064D0B,
	TelepathyTransport_ServerActive_m7E31D881004FB4A68CE8E0C7F5C2D3C18ED9909E,
	TelepathyTransport_ServerStart_m6F3609E2583811D26FB3ED5DF916A40EF848F84D,
	TelepathyTransport_ServerSend_m998FDA65A8819E3C5470BC34F2D68937E717B838,
	TelepathyTransport_ServerDisconnect_mADA1668FE8AB923684F437B38CA59255913144E3,
	TelepathyTransport_ServerGetClientAddress_mCAAE5E776410D1E8F8CB92D5AE547207F4F32EEE,
	TelepathyTransport_ServerStop_m029E82E1FC85B297A37C856ACBB2471C6410E2FB,
	TelepathyTransport_ServerEarlyUpdate_m970EF7D651ED0FC5FD4898CEDC3878892DC820E0,
	TelepathyTransport_Shutdown_mA4F85A710C7C52BB890A74C4942EC7BA64F9FB40,
	TelepathyTransport_GetMaxPacketSize_m81DCD6F50C02F566D328EB5097BEE7081C544A2B,
	TelepathyTransport_ToString_m528636DD99B5B7A9346BC0B4E628E3C07CAA954C,
	TelepathyTransport__ctor_mA4061E623CDE35A23989AE27570AB68007F43F9D,
	TelepathyTransport_U3CAwakeU3Eb__16_0_m905CFDDDDE4DC4C2DF534C3785C594E98316A038,
	TelepathyTransport_U3CAwakeU3Eb__16_1_mADDB68E8C56E511F7B71AF2C9CF3A450C7F5DBF7,
	TelepathyTransport_U3CAwakeU3Eb__16_2_mB5CD19395BFC6D0020C35F8D6C4CE87624B7CDB6,
	TelepathyTransport_U3CAwakeU3Eb__16_3_m449CD1AB3CC3F97916947FCD69B0412704FF1A1E,
	TelepathyTransport_U3CAwakeU3Eb__16_4_m2999458B1882358091529A2D113CDD0681BAF6B9,
	TelepathyTransport_U3CAwakeU3Eb__16_5_m5C9F8614ED86F5630676C4E29B4423EA0DD5D9F6,
	TelepathyTransport_U3CAwakeU3Eb__16_6_m874F866E355A4DCE73B942608DAF1DBCFEF1B9A7,
	NULL,
	NULL,
	NULL,
	Transport_ClientConnect_mBA9A630061B0F73E6B0383A762693985C3AEEA6D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Transport_GetMaxBatchSize_mCDEC1092208777286584F4982E9713DA5E7DA5E4,
	Transport_Update_mE9D7C9DCA9E54CA74C6D1B47C38834ABE4C7D796,
	Transport_LateUpdate_m458595D25E89D7222C3424D8A95AE08BF0D646FD,
	Transport_ClientEarlyUpdate_m0722DB6D5B6FC924657EAEC9A7E0DAE15497AEBE,
	Transport_ServerEarlyUpdate_mFD20DA364D3891A0C0F053D96FECF8E79E717D80,
	Transport_ClientLateUpdate_mD6FF8477B2174BD8B8DB9EA6F2CB02BDD473A135,
	Transport_ServerLateUpdate_m0FC3D74ECF4029F06B92999BB34025E58F34CC8C,
	NULL,
	Transport_OnApplicationQuit_mC51DBEB57BAB46E2B780F49420F5E205D79675BB,
	Transport__ctor_m18499670B379DE249BA0A4D8D978335F0C1E0376,
	U3CU3Ec__cctor_m304817F15C080D3EEC191046CF1E9890B95CC53C,
	U3CU3Ec__ctor_m5310ACAAE5FAD5B20FD9F10931FAB299C78950EC,
	U3CU3Ec_U3C_ctorU3Eb__32_0_mF7FDB3F2F0DFF66E559442E14A7D4464D0E14D79,
	U3CU3Ec_U3C_ctorU3Eb__32_1_m54E5403E2D3851EA7C6A6D343300323AC7110927,
	U3CU3Ec_U3C_ctorU3Eb__32_2_mEE5BBB94A91E911340820E2237772AFDD1872CD7,
	U3CU3Ec_U3C_ctorU3Eb__32_3_mC6026CC78089FE13D779CC367A1D87FFCE82D604,
	U3CU3Ec_U3C_ctorU3Eb__32_4_m195B635652A49A8FC937C05492F1610FAAC6F0BB,
	U3CU3Ec_U3C_ctorU3Eb__32_5_m76101E9DC27C90650C0BA52687E86B115CA542A2,
	U3CU3Ec_U3C_ctorU3Eb__32_6_m324D78CD5F370CACCC239A8ACAB0E17767C01C1C,
	U3CU3Ec_U3C_ctorU3Eb__32_7_m3976B6943CBB65C428F9FE631793BB65EA539C8D,
	NetworkMessageDelegate__ctor_m8A66FF4EABB5ED3BEDBA1E5F5BDEF8582502FFEF,
	NetworkMessageDelegate_Invoke_mAEE9C9A0218BA47AA1BAED072D5042DB7C28E7F1,
	NetworkMessageDelegate_BeginInvoke_m72BB2CC861D8E6F765FF70B2EA5DDB947385613F,
	NetworkMessageDelegate_EndInvoke_mE3EFA49B7DE1173FC104A1CCEEFCC0DE10114520,
	SpawnDelegate__ctor_m85296496F410FDA1A8F167B0E65E39B176491618,
	SpawnDelegate_Invoke_m647484B13C3CB741C8D7D910D355FEC904FBFBA3,
	SpawnDelegate_BeginInvoke_m6BDC7169A26F222C731A4CC93B5D2C1A9A5DCF7B,
	SpawnDelegate_EndInvoke_m3DC601F0E76C04C3159289012B2B4AC40BDA0AB5,
	SpawnHandlerDelegate__ctor_m5D9B376DBB3C85A6B66ED69AB8B7E7010A3130C9,
	SpawnHandlerDelegate_Invoke_m8DCF7DA5325363F4860AF5A135A7D9318A8C21D0,
	SpawnHandlerDelegate_BeginInvoke_mA8C0FE11CC475B4F433E7CE81A0D91EDC68631C4,
	SpawnHandlerDelegate_EndInvoke_m20D6E4B8796890F64D0FDF48450469B8FF716965,
	UnSpawnDelegate__ctor_mB9B060274806A21287BA156858B167BDCC028AE1,
	UnSpawnDelegate_Invoke_m253B17666AEEE399A94FF14D3B4FA5AFD7078D57,
	UnSpawnDelegate_BeginInvoke_m281EC91CBC5162D34ACF38393B3881A800CE3058,
	UnSpawnDelegate_EndInvoke_mECA5B4C0F9415708A4E4D354020E3F4F48927B57,
	Utils_GetTrueRandomUInt_m42E9457ACD0D68D5B81A8B127F0F5B3D68C8A60E,
	Utils_IsPrefab_mF9AE489EFFD6D206956CB7C33F27A6515578F502,
	Utils_IsSceneObjectWithPrefabParent_mF68AE61BD662AC25BD9EA4DDA45CF00D5D5666BE,
	CmdDelegate__ctor_m25AAD7CB2265BEF6CC263E5F620322D0B874CAD8,
	CmdDelegate_Invoke_m8DA7D950737AA88947D43EBAED1C35604D49DC2B,
	CmdDelegate_BeginInvoke_mAF28854F9A731D1171A91241F3529A45D12A66B6,
	CmdDelegate_EndInvoke_m307F0E541435EA4AE4C8EF22AECBA53412908553,
	Invoker_AreEqual_mECE8ABA850E084829C577D749E3E69F164829552,
	Invoker__ctor_mAB4C1E969716FCF8DB4257BCF45F152446555DF4,
	RemoteCallHelper_GetMethodHash_mA618B5FB67E05E07A97E30F731F381A7EF5E6AB4,
	RemoteCallHelper_RegisterDelegate_mC8AD98E6FE1096C3FE88DAC05A2B09E3CDB5F977,
	RemoteCallHelper_CheckIfDeligateExists_mB7136608721A4539332B7C090A64CA13BEFCDBAA,
	RemoteCallHelper_RegisterCommandDelegate_m196440B96E1D7A909D38037A770734FC5139842F,
	RemoteCallHelper_RegisterRpcDelegate_m3381432C36F433DF9E6D924514BEBAD8B37B7AC9,
	RemoteCallHelper_RemoveDelegate_m5E404B31C4616919F1755E414DF3C115693EC696,
	RemoteCallHelper_GetInvokerForHash_mEF65749F25DF0C8F8770F042978A52296834866E,
	RemoteCallHelper_InvokeHandlerDelegate_mD913C9F9303C0186A3BF47E09DDCEFBACB7459B9,
	RemoteCallHelper_GetCommandInfo_m26146856DB6082F9D0AE81E0276C9455CE2969A3,
	RemoteCallHelper_GetDelegate_mB573EBBF9D129EE42980B107FBE0E0FADD2F50E5,
	RemoteCallHelper__cctor_mE8A90B144EE99C80B996010B2C7D7D4D9F0E502B,
};
extern void NetworkPingMessage__ctor_mF340573CD5E74C9BECF60F7789C8170559A95C5D_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar__ctor_mFB219E84A8139BFFCC899543F4057E03D963B0FF_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar_Equals_m2EE0BD631368A1EA28D3E4422A9F14C43B9B0A27_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar_Equals_m2EE82223A7405A371A233BCED2C78292E69AA85F_AdjustorThunk (void);
extern void NetworkBehaviourSyncVar_ToString_m4DE9A94D17F9F28FCDB963AC8665144C37076363_AdjustorThunk (void);
extern void MessageInfo__ctor_mAC67988BF4FBFD84F5201A8DBFEA962167787671_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[6] = 
{
	{ 0x060000E8, NetworkPingMessage__ctor_mF340573CD5E74C9BECF60F7789C8170559A95C5D_AdjustorThunk },
	{ 0x0600012A, NetworkBehaviourSyncVar__ctor_mFB219E84A8139BFFCC899543F4057E03D963B0FF_AdjustorThunk },
	{ 0x0600012B, NetworkBehaviourSyncVar_Equals_m2EE0BD631368A1EA28D3E4422A9F14C43B9B0A27_AdjustorThunk },
	{ 0x0600012C, NetworkBehaviourSyncVar_Equals_m2EE82223A7405A371A233BCED2C78292E69AA85F_AdjustorThunk },
	{ 0x0600012D, NetworkBehaviourSyncVar_ToString_m4DE9A94D17F9F28FCDB963AC8665144C37076363_AdjustorThunk },
	{ 0x060001AF, MessageInfo__ctor_mAC67988BF4FBFD84F5201A8DBFEA962167787671_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1145] = 
{
	2754,
	2754,
	1339,
	2754,
	2261,
	2754,
	1339,
	2261,
	2679,
	2729,
	1994,
	2754,
	814,
	1128,
	2261,
	814,
	2261,
	1253,
	2754,
	2754,
	1128,
	2279,
	2754,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3510,
	3713,
	3510,
	3713,
	3509,
	3648,
	3509,
	3648,
	3507,
	3591,
	3508,
	3605,
	4033,
	2279,
	2699,
	2729,
	2754,
	2729,
	2729,
	2729,
	2279,
	1682,
	1802,
	2754,
	2754,
	2754,
	2279,
	1348,
	1348,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	1806,
	1128,
	2699,
	558,
	2754,
	2279,
	2754,
	1153,
	1994,
	819,
	2754,
	2754,
	2699,
	2754,
	4181,
	2261,
	2261,
	801,
	2754,
	2754,
	2754,
	2261,
	2754,
	2729,
	2699,
	2754,
	2699,
	2261,
	2754,
	2729,
	2754,
	2699,
	2754,
	2699,
	1253,
	2261,
	1339,
	1339,
	2754,
	2279,
	2754,
	2729,
	2729,
	2279,
	1153,
	2754,
	2754,
	2754,
	2754,
	2754,
	2699,
	2729,
	2754,
	819,
	1994,
	1802,
	2754,
	2754,
	2754,
	2754,
	1682,
	1682,
	2679,
	2679,
	2679,
	2679,
	2679,
	2679,
	4031,
	2754,
	2754,
	2699,
	2754,
	2754,
	2133,
	2754,
	2261,
	1153,
	2261,
	4198,
	2754,
	2279,
	1696,
	1696,
	1696,
	1696,
	1696,
	1696,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	4181,
	4131,
	4190,
	4140,
	4181,
	4131,
	4181,
	4075,
	4075,
	4198,
	3700,
	3811,
	4131,
	3274,
	3524,
	3274,
	3524,
	4131,
	3488,
	3488,
	4127,
	4198,
	4198,
	3360,
	2929,
	3043,
	3745,
	3965,
	4058,
	4056,
	2673,
	2254,
	2673,
	2254,
	2261,
	2254,
	3964,
	4033,
	2754,
	1064,
	861,
	2754,
	2754,
	2754,
	2699,
	1128,
	2039,
	2754,
	2754,
	2699,
	2754,
	2754,
	1128,
	2754,
	2754,
	2754,
	2039,
	2754,
	-1,
	-1,
	3713,
	3713,
	-1,
	-1,
	-1,
	2754,
	2254,
	2754,
	2754,
	2754,
	2279,
	2279,
	2279,
	2754,
	2754,
	2279,
	2279,
	2279,
	2754,
	2754,
	2729,
	2729,
	2729,
	2729,
	2729,
	2729,
	2679,
	2699,
	2699,
	2680,
	2262,
	1995,
	1313,
	2699,
	2679,
	2279,
	269,
	269,
	270,
	1062,
	543,
	983,
	1062,
	543,
	983,
	-1,
	-1,
	-1,
	-1,
	-1,
	2262,
	2754,
	2729,
	2729,
	1065,
	1361,
	1065,
	1361,
	2680,
	2011,
	2011,
	2279,
	2279,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	1239,
	2068,
	1049,
	2699,
	4181,
	4131,
	4181,
	4181,
	4131,
	4181,
	4190,
	4190,
	4190,
	4190,
	4198,
	4140,
	4131,
	4131,
	4198,
	4198,
	4198,
	4198,
	4198,
	3775,
	4198,
	4131,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3700,
	4131,
	3811,
	4131,
	3274,
	3524,
	3274,
	3524,
	4131,
	3488,
	3488,
	4127,
	4198,
	3702,
	4190,
	4075,
	4131,
	4190,
	4075,
	3842,
	3731,
	4030,
	4041,
	4041,
	4031,
	4075,
	4198,
	4135,
	4134,
	4198,
	4132,
	4133,
	4144,
	4146,
	4139,
	4133,
	4132,
	4144,
	4131,
	4128,
	4198,
	4198,
	4198,
	4198,
	4198,
	4198,
	4198,
	2754,
	2277,
	2283,
	2282,
	2326,
	1696,
	-1,
	-1,
	-1,
	-1,
	2754,
	1813,
	2754,
	1813,
	2754,
	1813,
	2699,
	2699,
	2279,
	2754,
	2261,
	2754,
	2279,
	-1,
	3680,
	1128,
	2699,
	2279,
	1361,
	2754,
	1062,
	1128,
	2039,
	2279,
	2279,
	2754,
	2699,
	834,
	1802,
	1253,
	1128,
	2754,
	2754,
	2754,
	2699,
	1128,
	2754,
	2754,
	4131,
	4131,
	-1,
	4131,
	4131,
	-1,
	548,
	2729,
	2307,
	2729,
	2307,
	2729,
	2307,
	2729,
	2729,
	2729,
	2307,
	2679,
	2261,
	2699,
	2279,
	2699,
	2279,
	2699,
	2699,
	2676,
	2258,
	4031,
	2279,
	4176,
	4198,
	4131,
	4131,
	2279,
	2729,
	2307,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2307,
	2307,
	780,
	285,
	861,
	1361,
	246,
	906,
	2754,
	2279,
	2011,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	4198,
	1341,
	861,
	224,
	2279,
	3354,
	3032,
	4198,
	4198,
	4198,
	2754,
	2022,
	4181,
	4131,
	2679,
	2679,
	2261,
	2754,
	2754,
	2754,
	2754,
	2729,
	4075,
	2754,
	2754,
	2754,
	2279,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2729,
	2754,
	2754,
	4198,
	2754,
	4181,
	4131,
	2279,
	869,
	1392,
	2754,
	2754,
	2754,
	2754,
	2754,
	4131,
	4131,
	2699,
	2279,
	2279,
	2279,
	1357,
	1328,
	2754,
	2279,
	2754,
	2278,
	2309,
	2279,
	2279,
	2279,
	2279,
	1339,
	2279,
	2279,
	2279,
	2279,
	1339,
	2279,
	869,
	2279,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	4198,
	4198,
	2754,
	1896,
	2011,
	1696,
	2011,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2679,
	2279,
	2133,
	2729,
	995,
	1426,
	2699,
	-1,
	4075,
	4075,
	3951,
	4075,
	3951,
	3951,
	3964,
	3964,
	3976,
	3976,
	4097,
	3940,
	3932,
	4033,
	4033,
	3883,
	4110,
	4116,
	4121,
	4114,
	4119,
	3911,
	3914,
	4057,
	4064,
	4054,
	4060,
	3989,
	3652,
	3945,
	4033,
	4033,
	4033,
	4033,
	-1,
	4154,
	-1,
	-1,
	4033,
	4198,
	2279,
	2133,
	2754,
	4033,
	3995,
	4131,
	4198,
	4198,
	2754,
	2699,
	4181,
	4131,
	4190,
	4190,
	4140,
	4198,
	4198,
	4198,
	4198,
	4128,
	4198,
	4198,
	4075,
	4072,
	4131,
	4198,
	4190,
	4190,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4128,
	4131,
	3490,
	4128,
	4131,
	3790,
	-1,
	-1,
	-1,
	-1,
	-1,
	4198,
	3713,
	4198,
	4198,
	4198,
	3717,
	3442,
	3445,
	3227,
	4131,
	4131,
	4198,
	3831,
	3822,
	3822,
	3835,
	3807,
	3074,
	3822,
	3822,
	3822,
	3822,
	3511,
	4075,
	4190,
	4131,
	4131,
	4131,
	4131,
	3835,
	3835,
	4131,
	4131,
	3835,
	3835,
	3835,
	4198,
	4198,
	4198,
	4198,
	-1,
	-1,
	-1,
	-1,
	2754,
	2754,
	2754,
	4198,
	4173,
	4198,
	4198,
	3819,
	4130,
	4173,
	4173,
	4173,
	4173,
	4173,
	4173,
	4173,
	4173,
	4173,
	4173,
	4173,
	2011,
	1361,
	2307,
	2754,
	2679,
	2679,
	2261,
	2754,
	2261,
	2261,
	2261,
	2699,
	2412,
	2307,
	850,
	-1,
	2754,
	3835,
	3835,
	3812,
	3835,
	3812,
	3812,
	3813,
	3813,
	3814,
	3814,
	3841,
	3810,
	3809,
	3822,
	3284,
	3822,
	3800,
	3844,
	3846,
	3848,
	3845,
	3847,
	3805,
	3806,
	3829,
	3832,
	3827,
	3830,
	3818,
	3811,
	3822,
	3822,
	3822,
	3822,
	3822,
	-1,
	-1,
	-1,
	4198,
	2754,
	2754,
	4181,
	4131,
	4198,
	4198,
	2754,
	2699,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2754,
	2754,
	2754,
	2754,
	2754,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2729,
	2754,
	2279,
	2279,
	2279,
	2279,
	2754,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2754,
	2754,
	2754,
	2699,
	2729,
	2279,
	2279,
	2729,
	2754,
	1153,
	2699,
	2729,
	1802,
	1994,
	819,
	2754,
	2754,
	2754,
	1682,
	2699,
	2754,
	2754,
	2754,
	2754,
	2099,
	2097,
	148,
	2729,
	2279,
	2279,
	2729,
	2754,
	1153,
	2699,
	2729,
	1802,
	1994,
	819,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	1682,
	1682,
	2754,
	2699,
	2754,
	2729,
	1682,
	2754,
	2279,
	2729,
	2754,
	1153,
	2729,
	2754,
	2754,
	819,
	1994,
	1802,
	2699,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2729,
	2279,
	2279,
	2729,
	2754,
	1153,
	929,
	1682,
	1682,
	2754,
	2699,
	2729,
	1802,
	1994,
	819,
	2754,
	2754,
	1682,
	2754,
	2699,
	2754,
	2754,
	2261,
	814,
	1253,
	2261,
	2754,
	2729,
	2729,
	2279,
	2279,
	1153,
	2754,
	2754,
	2699,
	2729,
	2754,
	819,
	1994,
	1802,
	2754,
	2754,
	2754,
	1682,
	2699,
	2754,
	2754,
	2133,
	2754,
	2261,
	1153,
	2261,
	2729,
	2729,
	2729,
	2279,
	2279,
	1153,
	2754,
	2699,
	2729,
	2754,
	819,
	1994,
	1802,
	2754,
	1682,
	1682,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	2754,
	4198,
	2754,
	2754,
	1128,
	2279,
	2754,
	2261,
	814,
	1253,
	2261,
	1341,
	859,
	221,
	2279,
	1341,
	1007,
	484,
	1806,
	1341,
	1813,
	724,
	1806,
	1341,
	2279,
	693,
	2279,
	4176,
	4075,
	3713,
	1341,
	860,
	223,
	2279,
	775,
	2754,
	3608,
	2984,
	3223,
	3287,
	3524,
	4128,
	3429,
	3023,
	3567,
	4030,
	4198,
};
static const Il2CppTokenRangePair s_rgctxIndices[49] = 
{
	{ 0x02000007, { 0, 6 } },
	{ 0x02000024, { 12, 5 } },
	{ 0x0200003E, { 44, 1 } },
	{ 0x0200003F, { 45, 1 } },
	{ 0x0200005D, { 87, 1 } },
	{ 0x0200005E, { 88, 1 } },
	{ 0x02000069, { 100, 6 } },
	{ 0x0200006A, { 106, 29 } },
	{ 0x0200006E, { 135, 8 } },
	{ 0x02000074, { 143, 39 } },
	{ 0x02000078, { 182, 5 } },
	{ 0x0200007A, { 187, 32 } },
	{ 0x0200007E, { 219, 8 } },
	{ 0x0200007F, { 227, 8 } },
	{ 0x060000E0, { 6, 1 } },
	{ 0x060000E1, { 7, 2 } },
	{ 0x060000E4, { 9, 3 } },
	{ 0x0600010F, { 17, 1 } },
	{ 0x06000110, { 18, 1 } },
	{ 0x06000111, { 19, 1 } },
	{ 0x06000112, { 20, 3 } },
	{ 0x06000144, { 23, 1 } },
	{ 0x06000145, { 24, 3 } },
	{ 0x06000146, { 27, 8 } },
	{ 0x06000147, { 35, 2 } },
	{ 0x06000148, { 37, 6 } },
	{ 0x06000149, { 43, 1 } },
	{ 0x06000190, { 46, 2 } },
	{ 0x060001AB, { 48, 1 } },
	{ 0x060001AE, { 49, 1 } },
	{ 0x06000250, { 50, 3 } },
	{ 0x06000273, { 53, 1 } },
	{ 0x06000275, { 54, 4 } },
	{ 0x06000276, { 58, 2 } },
	{ 0x06000295, { 60, 2 } },
	{ 0x06000296, { 62, 1 } },
	{ 0x06000297, { 63, 2 } },
	{ 0x06000298, { 65, 1 } },
	{ 0x06000299, { 66, 2 } },
	{ 0x0600029A, { 68, 1 } },
	{ 0x060002A1, { 69, 3 } },
	{ 0x060002A2, { 72, 6 } },
	{ 0x060002A3, { 78, 2 } },
	{ 0x060002A4, { 80, 6 } },
	{ 0x060002A5, { 86, 1 } },
	{ 0x060002F5, { 89, 3 } },
	{ 0x0600031A, { 92, 3 } },
	{ 0x0600031B, { 95, 1 } },
	{ 0x0600031C, { 96, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[235] = 
{
	{ (Il2CppRGCTXDataType)2, 2041 },
	{ (Il2CppRGCTXDataType)2, 2055 },
	{ (Il2CppRGCTXDataType)2, 1230 },
	{ (Il2CppRGCTXDataType)3, 2909 },
	{ (Il2CppRGCTXDataType)2, 1252 },
	{ (Il2CppRGCTXDataType)3, 2954 },
	{ (Il2CppRGCTXDataType)1, 164 },
	{ (Il2CppRGCTXDataType)3, 19655 },
	{ (Il2CppRGCTXDataType)3, 19896 },
	{ (Il2CppRGCTXDataType)2, 806 },
	{ (Il2CppRGCTXDataType)3, 197 },
	{ (Il2CppRGCTXDataType)3, 198 },
	{ (Il2CppRGCTXDataType)1, 516 },
	{ (Il2CppRGCTXDataType)3, 19842 },
	{ (Il2CppRGCTXDataType)3, 19794 },
	{ (Il2CppRGCTXDataType)2, 691 },
	{ (Il2CppRGCTXDataType)3, 559 },
	{ (Il2CppRGCTXDataType)2, 169 },
	{ (Il2CppRGCTXDataType)2, 171 },
	{ (Il2CppRGCTXDataType)2, 172 },
	{ (Il2CppRGCTXDataType)3, 6739 },
	{ (Il2CppRGCTXDataType)2, 1480 },
	{ (Il2CppRGCTXDataType)3, 6738 },
	{ (Il2CppRGCTXDataType)3, 19774 },
	{ (Il2CppRGCTXDataType)3, 19658 },
	{ (Il2CppRGCTXDataType)1, 175 },
	{ (Il2CppRGCTXDataType)3, 19706 },
	{ (Il2CppRGCTXDataType)2, 793 },
	{ (Il2CppRGCTXDataType)3, 131 },
	{ (Il2CppRGCTXDataType)3, 19657 },
	{ (Il2CppRGCTXDataType)1, 174 },
	{ (Il2CppRGCTXDataType)3, 132 },
	{ (Il2CppRGCTXDataType)2, 942 },
	{ (Il2CppRGCTXDataType)3, 578 },
	{ (Il2CppRGCTXDataType)3, 19705 },
	{ (Il2CppRGCTXDataType)3, 19659 },
	{ (Il2CppRGCTXDataType)3, 19707 },
	{ (Il2CppRGCTXDataType)2, 797 },
	{ (Il2CppRGCTXDataType)3, 169 },
	{ (Il2CppRGCTXDataType)3, 170 },
	{ (Il2CppRGCTXDataType)2, 944 },
	{ (Il2CppRGCTXDataType)3, 579 },
	{ (Il2CppRGCTXDataType)3, 19771 },
	{ (Il2CppRGCTXDataType)3, 19656 },
	{ (Il2CppRGCTXDataType)3, 456 },
	{ (Il2CppRGCTXDataType)3, 457 },
	{ (Il2CppRGCTXDataType)3, 19682 },
	{ (Il2CppRGCTXDataType)3, 19814 },
	{ (Il2CppRGCTXDataType)2, 181 },
	{ (Il2CppRGCTXDataType)2, 180 },
	{ (Il2CppRGCTXDataType)2, 3182 },
	{ (Il2CppRGCTXDataType)1, 182 },
	{ (Il2CppRGCTXDataType)3, 7540 },
	{ (Il2CppRGCTXDataType)2, 184 },
	{ (Il2CppRGCTXDataType)2, 2724 },
	{ (Il2CppRGCTXDataType)3, 10391 },
	{ (Il2CppRGCTXDataType)3, 19837 },
	{ (Il2CppRGCTXDataType)3, 10392 },
	{ (Il2CppRGCTXDataType)2, 4022 },
	{ (Il2CppRGCTXDataType)3, 19838 },
	{ (Il2CppRGCTXDataType)3, 19683 },
	{ (Il2CppRGCTXDataType)3, 19815 },
	{ (Il2CppRGCTXDataType)3, 19890 },
	{ (Il2CppRGCTXDataType)3, 19685 },
	{ (Il2CppRGCTXDataType)3, 19817 },
	{ (Il2CppRGCTXDataType)3, 19893 },
	{ (Il2CppRGCTXDataType)3, 19684 },
	{ (Il2CppRGCTXDataType)3, 19816 },
	{ (Il2CppRGCTXDataType)3, 19775 },
	{ (Il2CppRGCTXDataType)3, 19660 },
	{ (Il2CppRGCTXDataType)1, 187 },
	{ (Il2CppRGCTXDataType)3, 19708 },
	{ (Il2CppRGCTXDataType)2, 798 },
	{ (Il2CppRGCTXDataType)3, 171 },
	{ (Il2CppRGCTXDataType)3, 172 },
	{ (Il2CppRGCTXDataType)2, 946 },
	{ (Il2CppRGCTXDataType)3, 580 },
	{ (Il2CppRGCTXDataType)3, 19880 },
	{ (Il2CppRGCTXDataType)3, 19661 },
	{ (Il2CppRGCTXDataType)3, 19709 },
	{ (Il2CppRGCTXDataType)2, 800 },
	{ (Il2CppRGCTXDataType)3, 179 },
	{ (Il2CppRGCTXDataType)3, 180 },
	{ (Il2CppRGCTXDataType)2, 948 },
	{ (Il2CppRGCTXDataType)3, 581 },
	{ (Il2CppRGCTXDataType)3, 19888 },
	{ (Il2CppRGCTXDataType)3, 19662 },
	{ (Il2CppRGCTXDataType)3, 458 },
	{ (Il2CppRGCTXDataType)3, 459 },
	{ (Il2CppRGCTXDataType)2, 3553 },
	{ (Il2CppRGCTXDataType)1, 197 },
	{ (Il2CppRGCTXDataType)3, 589 },
	{ (Il2CppRGCTXDataType)3, 10393 },
	{ (Il2CppRGCTXDataType)3, 10394 },
	{ (Il2CppRGCTXDataType)3, 19899 },
	{ (Il2CppRGCTXDataType)3, 19897 },
	{ (Il2CppRGCTXDataType)3, 913 },
	{ (Il2CppRGCTXDataType)3, 912 },
	{ (Il2CppRGCTXDataType)3, 914 },
	{ (Il2CppRGCTXDataType)3, 19898 },
	{ (Il2CppRGCTXDataType)2, 3291 },
	{ (Il2CppRGCTXDataType)3, 15056 },
	{ (Il2CppRGCTXDataType)3, 15059 },
	{ (Il2CppRGCTXDataType)3, 7383 },
	{ (Il2CppRGCTXDataType)3, 15057 },
	{ (Il2CppRGCTXDataType)3, 15058 },
	{ (Il2CppRGCTXDataType)2, 1958 },
	{ (Il2CppRGCTXDataType)2, 3310 },
	{ (Il2CppRGCTXDataType)3, 19548 },
	{ (Il2CppRGCTXDataType)3, 15188 },
	{ (Il2CppRGCTXDataType)3, 10748 },
	{ (Il2CppRGCTXDataType)3, 10749 },
	{ (Il2CppRGCTXDataType)2, 2053 },
	{ (Il2CppRGCTXDataType)2, 2752 },
	{ (Il2CppRGCTXDataType)3, 10746 },
	{ (Il2CppRGCTXDataType)3, 15187 },
	{ (Il2CppRGCTXDataType)3, 10747 },
	{ (Il2CppRGCTXDataType)3, 15129 },
	{ (Il2CppRGCTXDataType)2, 2191 },
	{ (Il2CppRGCTXDataType)2, 2313 },
	{ (Il2CppRGCTXDataType)3, 9995 },
	{ (Il2CppRGCTXDataType)3, 19901 },
	{ (Il2CppRGCTXDataType)3, 9996 },
	{ (Il2CppRGCTXDataType)3, 19905 },
	{ (Il2CppRGCTXDataType)3, 10750 },
	{ (Il2CppRGCTXDataType)3, 19839 },
	{ (Il2CppRGCTXDataType)3, 19844 },
	{ (Il2CppRGCTXDataType)3, 15183 },
	{ (Il2CppRGCTXDataType)3, 15184 },
	{ (Il2CppRGCTXDataType)3, 15182 },
	{ (Il2CppRGCTXDataType)3, 15185 },
	{ (Il2CppRGCTXDataType)3, 6796 },
	{ (Il2CppRGCTXDataType)2, 1508 },
	{ (Il2CppRGCTXDataType)3, 6795 },
	{ (Il2CppRGCTXDataType)3, 15186 },
	{ (Il2CppRGCTXDataType)2, 1237 },
	{ (Il2CppRGCTXDataType)3, 2939 },
	{ (Il2CppRGCTXDataType)3, 15155 },
	{ (Il2CppRGCTXDataType)2, 3315 },
	{ (Il2CppRGCTXDataType)3, 2940 },
	{ (Il2CppRGCTXDataType)3, 2943 },
	{ (Il2CppRGCTXDataType)3, 2942 },
	{ (Il2CppRGCTXDataType)3, 2941 },
	{ (Il2CppRGCTXDataType)2, 1937 },
	{ (Il2CppRGCTXDataType)2, 3316 },
	{ (Il2CppRGCTXDataType)3, 19549 },
	{ (Il2CppRGCTXDataType)3, 6781 },
	{ (Il2CppRGCTXDataType)2, 1500 },
	{ (Il2CppRGCTXDataType)3, 15276 },
	{ (Il2CppRGCTXDataType)2, 2748 },
	{ (Il2CppRGCTXDataType)3, 10448 },
	{ (Il2CppRGCTXDataType)2, 2737 },
	{ (Il2CppRGCTXDataType)3, 10433 },
	{ (Il2CppRGCTXDataType)3, 10451 },
	{ (Il2CppRGCTXDataType)3, 10450 },
	{ (Il2CppRGCTXDataType)3, 15285 },
	{ (Il2CppRGCTXDataType)3, 15284 },
	{ (Il2CppRGCTXDataType)3, 10449 },
	{ (Il2CppRGCTXDataType)3, 15255 },
	{ (Il2CppRGCTXDataType)2, 2486 },
	{ (Il2CppRGCTXDataType)3, 19902 },
	{ (Il2CppRGCTXDataType)3, 10452 },
	{ (Il2CppRGCTXDataType)3, 19840 },
	{ (Il2CppRGCTXDataType)3, 15278 },
	{ (Il2CppRGCTXDataType)2, 2144 },
	{ (Il2CppRGCTXDataType)2, 2265 },
	{ (Il2CppRGCTXDataType)3, 15277 },
	{ (Il2CppRGCTXDataType)3, 15280 },
	{ (Il2CppRGCTXDataType)2, 2377 },
	{ (Il2CppRGCTXDataType)3, 14423 },
	{ (Il2CppRGCTXDataType)3, 15279 },
	{ (Il2CppRGCTXDataType)3, 10434 },
	{ (Il2CppRGCTXDataType)3, 15281 },
	{ (Il2CppRGCTXDataType)3, 15283 },
	{ (Il2CppRGCTXDataType)3, 10435 },
	{ (Il2CppRGCTXDataType)3, 5560 },
	{ (Il2CppRGCTXDataType)3, 15282 },
	{ (Il2CppRGCTXDataType)3, 5559 },
	{ (Il2CppRGCTXDataType)2, 1456 },
	{ (Il2CppRGCTXDataType)3, 10436 },
	{ (Il2CppRGCTXDataType)2, 1457 },
	{ (Il2CppRGCTXDataType)3, 5561 },
	{ (Il2CppRGCTXDataType)3, 5582 },
	{ (Il2CppRGCTXDataType)3, 15286 },
	{ (Il2CppRGCTXDataType)3, 15287 },
	{ (Il2CppRGCTXDataType)3, 5581 },
	{ (Il2CppRGCTXDataType)2, 551 },
	{ (Il2CppRGCTXDataType)2, 1938 },
	{ (Il2CppRGCTXDataType)2, 3325 },
	{ (Il2CppRGCTXDataType)3, 19550 },
	{ (Il2CppRGCTXDataType)2, 2750 },
	{ (Il2CppRGCTXDataType)3, 10453 },
	{ (Il2CppRGCTXDataType)3, 15526 },
	{ (Il2CppRGCTXDataType)3, 10455 },
	{ (Il2CppRGCTXDataType)3, 10456 },
	{ (Il2CppRGCTXDataType)3, 15525 },
	{ (Il2CppRGCTXDataType)3, 10454 },
	{ (Il2CppRGCTXDataType)3, 15485 },
	{ (Il2CppRGCTXDataType)3, 15520 },
	{ (Il2CppRGCTXDataType)2, 2145 },
	{ (Il2CppRGCTXDataType)2, 2266 },
	{ (Il2CppRGCTXDataType)3, 19903 },
	{ (Il2CppRGCTXDataType)3, 10457 },
	{ (Il2CppRGCTXDataType)3, 19841 },
	{ (Il2CppRGCTXDataType)2, 2586 },
	{ (Il2CppRGCTXDataType)3, 15519 },
	{ (Il2CppRGCTXDataType)3, 15522 },
	{ (Il2CppRGCTXDataType)3, 15521 },
	{ (Il2CppRGCTXDataType)3, 15524 },
	{ (Il2CppRGCTXDataType)3, 15523 },
	{ (Il2CppRGCTXDataType)2, 1887 },
	{ (Il2CppRGCTXDataType)3, 8279 },
	{ (Il2CppRGCTXDataType)2, 2738 },
	{ (Il2CppRGCTXDataType)3, 10437 },
	{ (Il2CppRGCTXDataType)3, 10438 },
	{ (Il2CppRGCTXDataType)3, 5563 },
	{ (Il2CppRGCTXDataType)3, 5562 },
	{ (Il2CppRGCTXDataType)2, 1458 },
	{ (Il2CppRGCTXDataType)3, 15518 },
	{ (Il2CppRGCTXDataType)3, 6780 },
	{ (Il2CppRGCTXDataType)2, 1499 },
	{ (Il2CppRGCTXDataType)3, 15151 },
	{ (Il2CppRGCTXDataType)2, 1886 },
	{ (Il2CppRGCTXDataType)3, 8277 },
	{ (Il2CppRGCTXDataType)3, 15490 },
	{ (Il2CppRGCTXDataType)2, 3327 },
	{ (Il2CppRGCTXDataType)3, 8278 },
	{ (Il2CppRGCTXDataType)3, 2099 },
	{ (Il2CppRGCTXDataType)2, 1128 },
	{ (Il2CppRGCTXDataType)3, 15589 },
	{ (Il2CppRGCTXDataType)2, 3270 },
	{ (Il2CppRGCTXDataType)3, 14929 },
	{ (Il2CppRGCTXDataType)3, 15527 },
	{ (Il2CppRGCTXDataType)2, 3328 },
	{ (Il2CppRGCTXDataType)3, 14930 },
};
extern const CustomAttributesCacheGenerator g_Mirror_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Mirror_CodeGenModule;
const Il2CppCodeGenModule g_Mirror_CodeGenModule = 
{
	"Mirror.dll",
	1145,
	s_methodPointers,
	6,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	49,
	s_rgctxIndices,
	235,
	s_rgctxValues,
	NULL,
	g_Mirror_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
