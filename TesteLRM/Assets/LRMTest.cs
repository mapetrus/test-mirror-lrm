﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using LightReflectiveMirror;
using Mirror;
using UnityEngine;
using UnityEngine.UI;
public class LRMTest : MonoBehaviour
{

   public Transform scrollParent;
   public GameObject serverEntry;
   
   private LightReflectiveMirrorTransport _LMR;

   private void Start()
   {
      if (_LMR== null)
      {
         _LMR = (LightReflectiveMirrorTransport) Transport.activeTransport;
         _LMR.serverListUpdated.AddListener(ServerListUpdate);
      }
   }

   private void OnDisable()
   {
      if (_LMR!=null)
      {
         _LMR.serverListUpdated.RemoveListener(ServerListUpdate);
      }
   }

   public void RefreshServerList()
   {
      _LMR.RequestServerList();
   }
   
   void ServerListUpdate()
   {
      foreach (Transform t in scrollParent)
      {
         Destroy(t.gameObject);
      }

      for (int i = 0; i < _LMR.relayServerList.Count; i++)
      {
         var newEntry = Instantiate(serverEntry, scrollParent);
         newEntry.transform.GetChild(0).GetComponent<Text>().text = _LMR.relayServerList[i].serverName;

         int serverId = _LMR.relayServerList[i].serverId;
            
         newEntry.GetComponent<Button>().onClick.AddListener(()=>{ConnectToserver(serverId);});
      }
   }

   void ConnectToserver(int serverId)
   {
      NetworkManager.singleton.networkAddress = serverId.ToString();
      NetworkManager.singleton.StartClient();
   }
}
